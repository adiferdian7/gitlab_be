<?php

use Carbon\Carbon;
use Xendit\Xendit;
use App\Utils\Util;
use App\Models\User;

use App\Models\Level;
use App\Utils\Wilayah;
use App\Mail\MySendMail;
use App\Models\Pengaturan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BankController;

use App\Http\Controllers\CronController;
use App\Http\Controllers\MbtiController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\KelasController;

use App\Http\Controllers\LevelController;
use App\Http\Controllers\MapelController;
use App\Http\Controllers\PaketController;
use App\Http\Controllers\KursusController;
use App\Http\Controllers\ProdukController;


use App\Http\Controllers\RumpunController;
use App\Http\Controllers\TryoutController;
use App\Http\Controllers\DimensiController;
use App\Http\Controllers\JenjangController;
use App\Http\Controllers\KlasterController;

use App\Http\Controllers\StudentController;

use App\Http\Controllers\TeacherController;
use App\Http\Controllers\BundlingController;

use App\Http\Controllers\MaterialController;
use App\Http\Controllers\OrangTuaController;
use App\Http\Controllers\PenarikanController;
use App\Http\Controllers\PengaduanController;
use App\Http\Controllers\TransaksiController;


use App\Http\Controllers\ExportImportController;

use App\Http\Controllers\KategoriToController;
use App\Http\Controllers\PendapatanController;
use App\Http\Controllers\PengaturanController;
use App\Http\Controllers\PenjurusanController;
use App\Http\Controllers\SoalTryoutController;

use App\Http\Controllers\SuperAdminController;

use App\Http\Controllers\TryoutUserController;
use Illuminate\Validation\ValidationException;

use App\Http\Controllers\KepribadianController;

use App\Http\Controllers\KursusSiswaController;
use App\Http\Controllers\MbtiDimensiController;

use App\Http\Controllers\MbtiJawabanController;

use App\Http\Controllers\GoogleHandleController;

use App\Http\Controllers\KursusJadwalController;
use App\Http\Controllers\KursusMateriController;
use App\Http\Controllers\KursusUlasanController;
use App\Http\Controllers\NotificationController;

use App\Http\Controllers\ProgramStudiController;

use App\Http\Controllers\MbtiPertanyaanController;
use App\Http\Controllers\SoalPertanyaanController;
use App\Http\Controllers\UserPermissionController;
use App\Http\Controllers\BundlingProductController;
use App\Http\Controllers\PerguruanTinggiController;
use App\Http\Controllers\MbtiJawabanDetailController;
use App\Http\Controllers\TryoutUserJawabanController;
use App\Http\Controllers\contentController;
use App\Http\Controllers\ProgramStudiBindMapelController;
use App\Http\Controllers\ProgramStudiPerguruanTinggiController as TranStudi;

// use Validator;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function (Request $request) {
    return response()->json(['success' => false, 'message' => 'Unauthenticated.'], 401);
})->name('notlogin');

// Registrasi
Route::post('/users/superAdmin', [SuperAdminController::class, 'store']);
Route::post('/users/admin', [AdminController::class, 'store']);
Route::post('/users/siswa', [StudentController::class, 'store']);
Route::post('/users/teacher', [TeacherController::class, 'store']);

// login and registrasi menggunakan google
Route::post('/users/google-login/{type?}', [GoogleHandleController::class, "googleLogin"]);
Route::post('/users/google-signup/{type?}', [GoogleHandleController::class, "googleSignup"]);

// Login
Route::post('/users/login', [AuthController::class, 'login']);

// Pengaturan
Route::get('/public/pengaturan', function (Request $request) {
    $pengaturanModel = new Pengaturan();
    $data = $pengaturanModel->getData('data', []);

    return responseData($data);
});

// Cron Job
Route::post('/cron/expired-trx-check', [CronController::class, 'expiredTransactionCheck']);

Route::middleware(['auth:sanctum', 'userpermission'])->group(function () {

    Route::get('/users/logout', [AuthController::class, 'logout']);

    Route::get('/users', [AuthController::class, 'getUser']);

    /* User Permission */
    Route::get('/permission/list', [UserPermissionController::class, 'list']);
    Route::get('/permission/mine', [UserPermissionController::class, 'mine']);
    Route::post('/permission/assign', [UserPermissionController::class, 'assign']);

    /* SuperAdmin Role */
    Route::post('/admin/create', [AdminController::class, 'create']);
    Route::get('/admin/dashboard', [AdminController::class, 'dashboard']);
    Route::get('/verifikasi/patner/{id}', [TeacherController::class, 'verifikasi']);

    Route::get('/users/superAdmin', [SuperAdminController::class, 'index']);
    Route::post('/users/superAdmin/create', [SuperAdminController::class, 'store']);
    Route::put('/users/superAdmin/update/{id}', [SuperAdminController::class, 'update']);
    Route::get('/users/superAdmin/find/{id}', [SuperAdminController::class, 'show']);
    Route::delete('/users/superAdmin/delete/{id}', [SuperAdminController::class, 'destroy']);
    Route::post('/users/superAdmin/undo', [SuperAdminController::class, 'undo']);

    /* Administrator Role */
    Route::get('/users/admin', [AdminController::class, 'index']);
    Route::post('/users/admin/create', [AdminController::class, 'store']);
    Route::put('/users/admin/update/{id}', [AdminController::class, 'update']);
    Route::get('/users/admin/find/{id}', [AdminController::class, 'show']);
    Route::delete('/users/admin/delete/{id}', [AdminController::class, 'destroy']);
    Route::post('/users/admin/undo', [AdminController::class, 'undo']);

    /* Student Role */
    Route::get('/users/siswa', [StudentController::class, 'index']);
    Route::get('/users/siswa/cek', [StudentController::class, 'cek']);
    Route::get('/users/siswa/dashboard', [StudentController::class, 'dashboard']);
    Route::post('/users/siswa/create', [StudentController::class, 'store']);
    Route::put('/users/siswa/update/{id}', [StudentController::class, 'update']);
    Route::get('/users/siswa/find/{id}', [StudentController::class, 'show']);
    Route::delete('/users/siswa/delete/{id}', [StudentController::class, 'destroy']);
    Route::post('/users/siswa/undo', [StudentController::class, 'undo']);

    /* Orang Tua Role*/
    Route::get('/users/parent', [OrangTuaController::class, 'index']);
    Route::get('/users/parent/cek', [OrangTuaController::class, 'cek']);
    Route::get('/users/parent/dashboard', [OrangTuaController::class, 'dashboard']);
    Route::post('/users/parent/create', [OrangTuaController::class, 'store']);
    Route::put('/users/parent/update/{id}', [OrangTuaController::class, 'update']);
    Route::get('/users/parent/find/{id}', [OrangTuaController::class, 'show']);
    Route::delete('/users/parent/delete/{id}', [OrangTuaController::class, 'destroy']);
    Route::post('/users/parent/undo', [OrangTuaController::class, 'undo']);

    /* Teacher / Patner */
    Route::get('/users/teacher', [TeacherController::class, 'index']);
    Route::get('/users/teacher/cek', [TeacherController::class, 'cek']);
    Route::post('/users/teacher/create', [TeacherController::class, 'store']);
    Route::put('/users/teacher/update/{id}', [TeacherController::class, 'update']);
    Route::get('/users/teacher/find/{id}', [TeacherController::class, 'show']);
    Route::delete('/users/teacher/delete/{id}', [TeacherController::class, 'destroy']);
    Route::post('/users/teacher/undo', [TeacherController::class, 'undo']);

    /* Jenjang */
    Route::get('/jenjang', [JenjangController::class, 'index']);
    Route::post('/jenjang/create', [JenjangController::class, 'store']);
    Route::put('/jenjang/update/{id}', [JenjangController::class, 'update']);
    Route::get('/jenjang/find/{id}', [JenjangController::class, 'show']);
    Route::delete('/jenjang/delete/{id}', [JenjangController::class, 'destroy']);

    /* Kelas */
    Route::get('/kelas', [KelasController::class, 'index']);
    Route::post('/kelas/create', [KelasController::class, 'store']);
    Route::put('/kelas/update/{id}', [KelasController::class, 'update']);
    Route::get('/kelas/find/{id}', [KelasController::class, 'show']);
    Route::delete('/kelas/delete/{id}', [KelasController::class, 'destroy']);

    /* Penjurusan */
    Route::get('/penjurusan', [PenjurusanController::class, 'index']);
    Route::post('/penjurusan/create', [PenjurusanController::class, 'store']);
    Route::put('/penjurusan/update/{id}', [PenjurusanController::class, 'update']);
    Route::get('/penjurusan/find/{id}', [PenjurusanController::class, 'show']);
    Route::delete('/penjurusan/delete/{id}', [PenjurusanController::class, 'destroy']);

    /* Kategori TO */
    Route::get('/kategori-to', [KategoriToController::class, 'index']);
    Route::get('/kategori-to/list', [KategoriToController::class, 'indexWithoutPagination']);
    Route::get('/kategori-to/find/{id}', [KategoriToController::class, 'show']);

    /* Rumpun */
    Route::get('/rumpun', [RumpunController::class, 'index']);
    Route::post('/rumpun/create', [RumpunController::class, 'store']);
    Route::put('/rumpun/update/{id}', [RumpunController::class, 'update']);
    Route::get('/rumpun/find/{id}', [RumpunController::class, 'show']);
    Route::delete('/rumpun/delete/{id}', [RumpunController::class, 'destroy']);
    Route::get('/klaster', [RumpunController::class, 'indexKlaster']);
    Route::get('/klaster/find/{id}', [RumpunController::class, 'showKlaster']);

    /* Rumpun Klaster */
    Route::post('/klaster/create', [KlasterController::class, 'store']);
    Route::post('/klaster/createBulk', [KlasterController::class, 'storeBulk']);
    Route::put('/klaster/update/{id}', [KlasterController::class, 'update']);
    Route::delete('/klaster/delete/{id}', [KlasterController::class, 'destroy']);

    /* Mapel */
    Route::get('/mapel', [MapelController::class, 'index']);
    Route::post('/mapel/create', [MapelController::class, 'store']);
    Route::put('/mapel/update/{id}', [MapelController::class, 'update']);
    Route::get('/mapel/find/{id}', [MapelController::class, 'show']);
    Route::delete('/mapel/delete/{id}', [MapelController::class, 'destroy']);

    /* Perguruan Tinggi */
    Route::get('/perguruanTinggi', [PerguruanTinggiController::class, 'index']);
    Route::post('/perguruanTinggi/create', [PerguruanTinggiController::class, 'store']);
    Route::put('/perguruanTinggi/update/{id}', [PerguruanTinggiController::class, 'update']);
    Route::get('/perguruanTinggi/find/{id}', [PerguruanTinggiController::class, 'show']);
    Route::delete('/perguruanTinggi/delete/{id}', [PerguruanTinggiController::class, 'destroy']);

    /* Program Studi*/
    Route::get('/programStudi', [ProgramStudiController::class, 'index']);
    Route::post('/programStudi/create', [ProgramStudiController::class, 'store']);
    Route::put('/programStudi/update/{id}', [ProgramStudiController::class, 'update']);
    Route::get('/programStudi/find/{id}', [ProgramStudiController::class, 'show']);
    Route::delete('/programStudi/delete/{id}', [ProgramStudiController::class, 'destroy']);

    Route::post('/programStudi/create/multiple', [ProgramStudiController::class, 'multiple']);
    Route::put('/programStudi/update/multiple/{id}', [ProgramStudiController::class, 'updateMultiple']);


    /* ProgramStudiPerguruanTinggiController */
    Route::get('/tranStudiPerguruan', [TranStudi::class, 'index']);
    Route::post('/tranStudiPerguruan/create', [TranStudi::class, 'store']);
    Route::post('/tranStudiPerguruan/import', [TranStudi::class, 'import']);
    Route::put('/tranStudiPerguruan/update/{id}', [TranStudi::class, 'update']);
    Route::get('/tranStudiPerguruan/find/{id}', [TranStudi::class, 'show']);
    Route::delete('/tranStudiPerguruan/delete/{id}', [TranStudi::class, 'destroy']);

    /* ProgramStudiBindMapelController */
    Route::delete('/program-studi-bind-mapel/delete/{id}', [ProgramStudiBindMapelController::class, 'destroy']);

    //  Tryout
    Route::get('/tryout', [TryoutController::class, 'index']);
    Route::post('/tryout/create', [TryoutController::class, 'store']);
    Route::put('/tryout/update/{id}', [TryoutController::class, 'update']);
    Route::put('/tryout/update/{id}/panduan', [TryoutController::class, 'updatePanduan']);
    Route::get('/tryout/find/{id}', [TryoutController::class, 'show']);
    Route::get('/tryout/find/{id}/detail', [TryoutController::class, 'show_detail']);
    Route::get('/tryout/find/{id}/soal', [TryoutController::class, 'list_soal']);
    Route::get('/tryout/nomor-soal/{id}', [TryoutController::class, 'nomor_soal']);
    Route::get('/tryout/list-soal/{id}', [TryoutController::class, 'list_soal']);
    Route::delete('/tryout/delete/{id}', [TryoutController::class, 'destroy']);

    // Soal Tryout
    Route::get('/tryout/soal', [SoalTryoutController::class, 'index']);
    Route::post('/tryout/soal/create', [SoalTryoutController::class, 'store']);
    Route::post('/tryout/soal/create/multiple', [SoalTryoutController::class, 'multiInsert']);
    Route::put('/tryout/soal/update/{id}', [SoalTryoutController::class, 'update']);
    Route::get('/tryout/soal/find/{id}', [SoalTryoutController::class, 'show']);
    Route::delete('/tryout/soal/delete/{id}', [SoalTryoutController::class, 'destroy']);


    // Soal Pertanyaan Tryout
    Route::get('/soal/pertanyaan', [SoalPertanyaanController::class, 'index']);
    Route::post('/soal/pertanyaan/create', [SoalPertanyaanController::class, 'store']);
    Route::put('/soal/pertanyaan/update/{id}', [SoalPertanyaanController::class, 'update']);
    Route::get('/soal/pertanyaan/find/{id}', [SoalPertanyaanController::class, 'show']);
    Route::delete('/soal/pertanyaan/delete/{id}', [SoalPertanyaanController::class, 'destroy']);

    // Produk Event
    Route::get('/produk', [ProdukController::class, 'index']);
    Route::get('/produk/option-data', [ProdukController::class, 'listOptionData']);
    Route::get('/produk-uktt', [ProdukController::class, 'indexUKTT']);
    Route::post('/produk/create', [ProdukController::class, 'store']);
    Route::post('/produk/export-csv', [ProdukController::class, 'generatecsv']);
    Route::put('/produk/update/{id}', [ProdukController::class, 'update']);
    Route::get('/produk/find/{id}', [ProdukController::class, 'show']);
    Route::get('/produk/find/{id}/detail', [ProdukController::class, 'show_detail']);
    Route::get('/produk/find/{id}/list-tryout', [ProdukController::class, 'get_produk_tryout']);
    Route::delete('/produk/delete/{id}', [ProdukController::class, 'destroy']);

    // Produk Paket
    Route::get('/produk/paket', [PaketController::class, 'index']);
    Route::post('/produk/paket/create', [PaketController::class, 'store']);
    Route::put('/produk/paket/update/{id}', [PaketController::class, 'update']);
    Route::get('/produk/paket/find/{id}', [PaketController::class, 'show']);
    Route::delete('/produk/paket/delete/{id}', [PaketController::class, 'destroy']);

    // Bundling
    Route::get('/bundling', [BundlingController::class, 'index']);
    Route::get('/bundling/find/{id}', [BundlingController::class, 'detail']);
    Route::get('/bundling/find/{id}/detail', [BundlingController::class, 'detailWithTrans']);
    Route::post('/bundling/create', [BundlingController::class, 'insert']);
    Route::put('/bundling/update/{id}', [BundlingController::class, 'update']);
    Route::delete('/bundling/delete/{id}', [BundlingController::class, 'delete']);

    // Bundling Product
    Route::delete('/bundling-product/delete/{id}', [BundlingProductController::class, 'delete']);

    // Material
    Route::get('/material', [MaterialController::class, 'index']);
    Route::get('/material/find/{id}', [MaterialController::class, 'detail']);
    Route::get('/material/find/{id}/detail', [MaterialController::class, 'detailWithTrans']);
    Route::post('/material/create', [MaterialController::class, 'insert']);
    Route::put('/material/update/{id}', [MaterialController::class, 'update']);
    Route::delete('/material/delete/{id}', [MaterialController::class, 'delete']);

    // Modul MBTI

    Route::get('/dimensi', [DimensiController::class, 'index']);
    Route::post('/dimensi/create', [DimensiController::class, 'store']);
    Route::put('/dimensi/update/{id}', [DimensiController::class, 'update']);
    Route::get('/dimensi/find/{id}', [DimensiController::class, 'show']);
    Route::delete('/dimensi/delete/{id}', [DimensiController::class, 'destroy']);

    Route::get('/kepribadian', [KepribadianController::class, 'index']);
    Route::post('/kepribadian/create', [KepribadianController::class, 'store']);
    Route::put('/kepribadian/update/{id}', [KepribadianController::class, 'update']);
    Route::get('/kepribadian/find/{id}', [KepribadianController::class, 'show']);
    Route::delete('/kepribadian/delete/{id}', [KepribadianController::class, 'destroy']);

    Route::get('/mbti', [MbtiController::class, 'index']);
    Route::post('/mbti/create', [MbtiController::class, 'store']);
    Route::put('/mbti/update/{id}', [MbtiController::class, 'update']);
    Route::put('/mbti/update/{id}/status', [MbtiController::class, 'update_status']);
    Route::get('/mbti/active', [MbtiController::class, 'show_active']);

    Route::get('/mbti/user', [MbtiController::class, 'mbti_user']);
    Route::get('/mbti/nomor-soal/{id}', [MbtiController::class, 'nomor_soal']);
    Route::get('/mbti/find/{id}', [MbtiController::class, 'show']);

    Route::delete('/mbti/delete/{id}', [MbtiController::class, 'destroy']);

    Route::get('/mbti-pertanyaan', [MbtiPertanyaanController::class, 'index']);
    Route::post('/mbti-pertanyaan/create', [MbtiPertanyaanController::class, 'store']);
    Route::put('/mbti-pertanyaan/update/{id}', [MbtiPertanyaanController::class, 'update']);
    Route::get('/mbti-pertanyaan/find/{id}', [MbtiPertanyaanController::class, 'show']);
    Route::delete('/mbti-pertanyaan/delete/{id}', [MbtiPertanyaanController::class, 'destroy']);

    Route::get('/mbti-dimensi', [MbtiDimensiController::class, 'index']);
    Route::post('/mbti-dimensi/create', [MbtiDimensiController::class, 'store']);
    Route::put('/mbti-dimensi/update/{id}', [MbtiDimensiController::class, 'update']);
    Route::get('/mbti-dimensi/find/{id}', [MbtiDimensiController::class, 'show']);
    Route::delete('/mbti-dimensi/delete/{id}', [MbtiDimensiController::class, 'destroy']);

    // MBTI Jawaban
    Route::post('/mbti-jawaban/create', [MbtiJawabanController::class, 'store']);
    Route::delete('/mbti-jawaban/delete/{id}', [MbtiJawabanController::class, 'destroy']);

    // MBTI Jawaban Detail
    Route::post('/mbti-jawaban-detail/create/multiple', [MbtiJawabanDetailController::class, 'insert_multiple']);


    /* Transaksi */
    Route::get('/transaksi', [TransaksiController::class, 'index']); //
    Route::get('/transaksi/mine', [TransaksiController::class, 'mine']);
    Route::post('/transaksi/create', [TransaksiController::class, 'store']);
    Route::put('/transaksi/update/{id}', [TransaksiController::class, 'update']); //
    Route::get('/transaksi/find/{id}', [TransaksiController::class, 'show']); //
    Route::delete('/transaksi/delete/{id}', [TransaksiController::class, 'destroy']);
    Route::put('/transaksi/upload/{id}', [TransaksiController::class, 'update_upload']);
    Route::put('/transaksi/update-status/{id}', [TransaksiController::class, 'update_status']);


    /* Bank */
    Route::get('/bank', [BankController::class, 'index']);
    Route::post('/bank/create', [BankController::class, 'store']);
    Route::put('/bank/update/multiple', [BankController::class, 'updateMultiple']);
    Route::put('/bank/update/{id}', [BankController::class, 'update']);
    Route::get('/bank/find/{id}', [BankController::class, 'show']);
    Route::delete('/bank/delete/{id}', [BankController::class, 'destroy']);

    /* MODULE UJIAN */

    // TRYOUT USER
    Route::get('/tryout_user', [TryoutUserController::class, 'index']);
    Route::post('/tryout_user/create', [TryoutUserController::class, 'store']);
    Route::put('/tryout_user/update/{id}', [TryoutUserController::class, 'update']);
    Route::get('/tryout_user/find/{id}', [TryoutUserController::class, 'show']);
    Route::get('/tryout_user/per-tryout-produk', [TryoutUserController::class, 'show_ujian_per_tryout_and_produk']);

    Route::get('/tryout_user/archived', [TryoutUserController::class, 'showArchived']);
    Route::put('/tryout_user/archived/{id}', [TryoutUserController::class, 'doArchive']);

    Route::get('/tryout_user/uktt-hasil', [TryoutUserController::class, 'uktt_detail_and_jawaban']);
    Route::get('/tryout_user/hasil-pengerjaan', [TryoutUserController::class, 'tryout_hasil_pengerjaan']);
    Route::get('/tryout_user/riwayat-pengerjaan', [TryoutUserController::class, 'tryout_riwayat_pengerjaan']);
    Route::get('/tryout_user/rerata-persentase-hasil', [TryoutUserController::class, 'tryout_persentase_skor']);
    Route::get('/tryout_user/rerata-persentase-hasil-aspd', [TryoutUserController::class, 'tryout_persentase_skor_aspd']);

    Route::get('/tryout_user/grafik-hasil-satuan-pengerjaan', [TryoutUserController::class, 'tryout_hasil_per_pengerjaan']);
    Route::get('/tryout_user/grafik-hasil-nilai-permapel', [TryoutUserController::class, 'tryout_hasil_nilai_per_mapel']);
    Route::get('/tryout_user/grafik-hasil-nilai-perumpun', [TryoutUserController::class, 'tryout_hasil_nilai_perumpun']);

    Route::post('/tryout_user/generate-certificate', [TryoutUserController::class, 'tryout_generate_certificate']);
    Route::post('/tryout_user/to-reset', [TryoutUserController::class, 'to_reset']);

    // ASPD
    Route::get('/tryout_user/hasil-pengerjaan-aspd', [TryoutUserController::class, 'tryout_hasil_pengerjaan_aspd']);

    Route::delete('/tryout_user/delete/{id}', [TryoutUserController::class, 'destroy']);
    Route::delete('/tryout_user/uktt-reset/{id}', [TryoutUserController::class, 'uktt_reset']);

    // TRYOUT USER JAWABAN
    Route::get('/tryout_user_jawaban', [TryoutUserJawabanController::class, 'index']);
    Route::post('/tryout_user_jawaban/create', [TryoutUserJawabanController::class, 'store']);
    Route::post('/tryout_user_jawaban/create/multiple', [TryoutUserJawabanController::class, 'insert_multiple']);
    Route::put('/tryout_user_jawaban/update/{id}', [TryoutUserJawabanController::class, 'update']);
    Route::get('/tryout_user_jawaban/find/{id}', [TryoutUserJawabanController::class, 'show']);
    Route::delete('/tryout_user_jawaban/delete/{id}', [TryoutUserJawabanController::class, 'destroy']);

    /* Kursus */
    Route::get('/kursus', [KursusController::class, 'index']); //
    Route::get('/kursus/mine', [KursusController::class, 'ownByStudent']);
    Route::post('/kursus/create', [KursusController::class, 'store']);
    Route::put('/kursus/update/{id}', [KursusController::class, 'update']);
    Route::put('/kursus/update/{id}/status', [KursusController::class, 'toggleActive']); //
    Route::put('/kursus/update/{id}/verification', [KursusController::class, 'toggleVerification']); //
    Route::get('/kursus/find/{id}', [KursusController::class, 'detail']); //
    Route::delete('/kursus/delete/{id}', [KursusController::class, 'delete']);
    Route::get('/kursus/price-option', [KursusController::class, 'coursePriceOption']);
    Route::get('/kursus/kelas-lain/{id}', [KursusController::class, 'getKelasLain']);

    /* Kursus-Jadwal */
    Route::post('/kursus-jadwal/create', [KursusJadwalController::class, 'store']);
    Route::put('/kursus-jadwal/update/{id}', [KursusJadwalController::class, 'update']);
    Route::delete('/kursus-jadwal/delete/{id}', [KursusJadwalController::class, 'delete']);

    /* Kursus-Materi */
    Route::get('/kursus-materi', [KursusMateriController::class, 'index']);
    Route::get('/kursus-materi/pagination', [KursusMateriController::class, 'indexPagination']);
    Route::get('/kursus-materi/find/{id}', [KursusMateriController::class, 'detail']);
    Route::post('/kursus-materi/create', [KursusMateriController::class, 'store']);
    Route::put('/kursus-materi/update/{id}', [KursusMateriController::class, 'update']);
    Route::delete('/kursus-materi/delete/{id}', [KursusMateriController::class, 'delete']);

    /* Kursus-Siswa */
    Route::get('/kursus-siswa', [KursusSiswaController::class, 'index']);
    Route::get('/kursus-siswa/find/{id}', [KursusSiswaController::class, 'detail']);
    Route::get('/kursus-siswa/find/{id}/student', [KursusSiswaController::class, 'studentCheck']);
    Route::post('/kursus-siswa/create', [KursusSiswaController::class, 'store']);
    Route::post('/kursus-siswa/join-after-reject', [KursusSiswaController::class, 'joinAfterRejected']);
    Route::put('/kursus-siswa/update/{id}', [KursusSiswaController::class, 'update']);
    Route::put('/kursus-siswa/update/{id}/status', [KursusSiswaController::class, 'updateStatus']);
    Route::delete('/kursus-siswa/delete/{id}', [KursusSiswaController::class, 'delete']);

    /* Kursus-Ulasan */
    Route::get('/kursus-ulasan', [KursusUlasanController::class, 'index']);
    Route::get('/kursus-ulasan/find/{id}', [KursusUlasanController::class, 'detail']);
    Route::post('/kursus-ulasan/create', [KursusUlasanController::class, 'store']);
    Route::put('/kursus-ulasan/update/{id}', [KursusUlasanController::class, 'update']);
    Route::put('/kursus-ulasan/reply/{id}', [KursusUlasanController::class, 'tentorReplyUlasan']);
    Route::delete('/kursus-ulasan/delete/{id}', [KursusUlasanController::class, 'delete']);

    /* Pendapatan */
    Route::get('/pendapatan/tentor/total-data', [PendapatanController::class, 'getPendapatanByTentor']);
    Route::get('/pendapatan/tentor/grafik-bulanan', [PendapatanController::class, 'getPendapatanByTentorGrafikBulanan']);
    Route::get('/pendapatan/tentor/perkursus', [PendapatanController::class, 'pendapatanPerKursusTentor']);
    Route::get('/pendapatan/tentor/perkursus/{id}', [PendapatanController::class, 'pendapatanPerKursusTentorDetail']);
    Route::get('/pendapatan/tentor/perkursus/{id}/list', [PendapatanController::class, 'pendapatanPerKursusTentorDetailList']);

    /* Penarikan */
    // --- admin
    Route::get('/penarikan/riwayat', [PenarikanController::class, 'historyPenarikan']);
    Route::put('/penarikan/proses-dana/{id}', [PenarikanController::class, 'prosesPenarikan']);
    Route::put('/penarikan/kirim-dana/{id}', [PenarikanController::class, 'kirimDanaPenarikan']);
    // --- tentor
    Route::post('/penarikan/tentor', [PenarikanController::class, 'createPenarikan']);
    Route::get('/penarikan/tentor', [PenarikanController::class, 'historyPenarikan']);

    /* Pengaduan */
    Route::get('/pengaduan', [PengaduanController::class, 'index']); //
    Route::get('/pengaduan/{id}', [PengaduanController::class, 'detail']); //
    Route::post('/pengaduan', [PengaduanController::class, 'create']);
    Route::put('/pengaduan/update/{id}', [PengaduanController::class, 'update']);
    Route::delete('/pengaduan/delete/{id}', [PengaduanController::class, 'delete']);
    Route::put('/pengaduan/update-status/{id}', [PengaduanController::class, 'updateStatus']); //
    Route::put('/pengaduan/update-prioritas/{id}', [PengaduanController::class, 'updatePrioritas']); //
    Route::put('/pengaduan/assign-admin/{id}', [PengaduanController::class, 'assignAdmin']); //

    // Notifikasi
    Route::get('/notification', [NotificationController::class, 'index']);
    Route::post('/notification/create', [NotificationController::class, 'create']);
    Route::put('/notification/open/{id}', [NotificationController::class, 'triggerOpen']);

    // content CMS
    Route::get('/cms/halaman-utama/get', [contentController::class, 'getContentHalamanUtama']);
    Route::post('/cms/halaman-utama', [contentController::class, 'ContentHalamanUtama']);
    Route::post('/cms/halaman-utama/delete/{id}', [contentController::class, 'deleteContentHalamanUtama']);

    // Pengaturan
    // Route::get('/pengaturan', function (Request $request) {
    //     $pengaturanModel = new Pengaturan();
    //     $data = $pengaturanModel->getData('data', []);

    //     return responseData($data);
    // });

    Route::get('/pengaturan', [PengaturanController::class, 'index']);
    Route::get('/pengaturan/{key}', [PengaturanController::class, 'detail']);
    Route::put('/pengaturan/multiple', [PengaturanController::class, 'updateMultiple']);

    // Level
    // Route::get('/level', function (Request $request) {
    //     $levelModel = new Level();
    //     $data = $levelModel->getData('data', []);

    //     return responseData($data);
    // });
    Route::get('/level', [LevelController::class, 'index']);
    Route::put('/level/multiple', [LevelController::class, 'updateMultiple']);

    // Testing Xendit

    Route::post('/xendit/invoice', function (Request $request) {
        $params = [
            'external_id' => 'demo_147580196270',
            'payer_email' => 'sample_email@xendit.co',
            'description' => 'Trip to Bali',
            'amount' => 32000
        ];

        $util = new Util();
        $data = $util->invoice($params);

        return response()->json($data);
    });
    Route::get('/xendit/balance', function (Request $request) {
        $params = [];

        $util = new Util();
        $data = $util->getBalance($params);

        $data['balance_label'] = "Rp " . number_format($data['balance'], 0, ",", ".");

        return response()->json($data);
    });
});

// Reset Password

Route::post('/resetPassword', [AuthController::class, 'resetPassword']);
Route::post('/activePassword', [AuthController::class, 'activePassword']);



// PUBLIC PRODI
Route::get('/program/studi', [ProgramStudiController::class, 'index']);
Route::get('/program/studi/find/{id}', [ProgramStudiController::class, 'detail_slug']);


// PUBLIC API MAPEL PENJURUSAN
Route::get('/mapel/list', [MapelController::class, 'index']);
Route::get('/penjurusan/list', [PenjurusanController::class, 'index']);
Route::get('/perguruan/list', [PerguruanTinggiController::class, 'index']);

// Verifikasi Email

Route::get('/verifikasi/{id}', function (Request $request, $id) {

    $user = User::where('token', $id)->first();

    if ($user == NULL || ($user && $user->verifikasi == 2)) {
        return response()->json([
            'success' => false,
            'message' => 'Token invalid!'
        ], 400);
    }

    try {

        $user->verifikasi = 2;
        $user->save();

        $date = Carbon::now();
        $token =  $user->createToken($date)->plainTextToken;

        $response = [
            'user'  => $user,
            'token' => $token,
        ];

        return response()->json(['success' => true, 'message' => 'Token valid! User berhasil diverifikasi.', 'data' => $response]);
    } catch (\Exception $e) {

        return response()->json([
            'success' => false,
            'messages' => $e->getMessage()
        ], 400);
    }
});

// testing email send

Route::post('send/email', function (Request $request) {

    $mailData = [
        'token' => 'jgejrgjeogreoin',
    ];

    try {

        Mail::to($request->email)->send(new MySendMail($mailData));

        return response()->json(['messages' => true]);
    } catch (\Exception $e) {
        return response()->json(['messages' => false, 'data' => $e->getMessage()]);
    }
});

// testing upload foto

Route::post('upload/image', function (Request $request) {
    $validator = Validator::make($request->all(), [
        'image' => 'required|image:jpeg,png,jpg,gif,svg|max:2048'
    ]);

    if ($validator->fails()) {
        return response()->json([
            'success' => false,
            'messages' => $validator->messages()
        ], 422);
    }

    $uploadFolder = 'users';
    $image = $request->file('image');
    $image_uploaded_path = $image->store($uploadFolder, 'public');
    $uploadedImageResponse = array(
        "image_name" => basename($image_uploaded_path),
        "image_url" => Storage::disk('public')->url($image_uploaded_path),
        "mime" => $image->getClientMimeType()
    );
    return response()->json([
        'success' => true,
        'messages' => 'File Uploaded Successfully', 'data' => $uploadedImageResponse
    ], 200);
});

Route::post('upload/file', function (Request $request) {
    $validator = Validator::make($request->all(), [
        'file' => 'required|file|mimes:pdf,doc,docx,xls,xlsx,ppt,zip|max:2048'
    ]);

    if ($validator->fails()) {
        return response()->json([
            'success' => false,
            'messages' => $validator->messages()
        ], 422);
    }

    $uploadFolder = 'files';
    $file = $request->file('file');
    $file_uploaded_path = $file->store($uploadFolder, 'public');
    $uploadedImageResponse = array(
        "file_name" => basename($file_uploaded_path),
        "file_url" => Storage::disk('public')->url($file_uploaded_path),
        "mime" => $file->getClientMimeType()
    );
    return response()->json([
        'success' => true,
        'messages' => 'File Uploaded Successfully', 'data' => $uploadedImageResponse
    ], 200);
});




/* Api Wilayah */

Route::get('/provinsi', function (Request $request) {
    $data = Wilayah::GetProvinsi();

    return response()->json(['data' => $data]);
});

Route::get('/kota_kabupaten/{id}', function (Request $request, $id) {
    $data = Wilayah::GetKota($id);

    return response()->json(['data' => $data]);
});

Route::get('/kecamatan/{id}', function (Request $request, $id) {
    $data = Wilayah::GetKecamatan($id);

    return response()->json(['data' => $data]);
});

/* Api Import */
Route::post('import/soal_pertanyaan', [ExportImportController::class, 'import_soal_pertanyaan']);
