<?php

// use Illuminate\Support\Facades\Artisan;

use App\Http\Controllers\ExportImportController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$version = '1.2';

Route::get('/', function () use ($version) {
    return response()->json([
        'app' => 'Ujiaja.com',
        'version' => $version
    ], 200);
});

Route::middleware(['auth:sanctum', 'userpermission'])->group(function () {
    Route::get('export', [ExportImportController::class, 'export_soal_pertanyaan'])->name('export_soal_pertanyaan');
});

// Route::get('/storage', function () {
// 	Artisan::call('storage:link');
// });
