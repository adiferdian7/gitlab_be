<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\Admin;
use App\Models\Bank;
use App\Models\SuperAdmin;
use App\Models\OrangTua;
use App\Models\Jenjang;
use App\Models\KategoriTo;
use App\Models\Kelas;
use App\Models\Mapel;
use App\Models\Penjurusan;
use App\Models\PerguruanTinggi;
use App\Models\ProgramStudi;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class DummyData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();

        try {

            $superAdmin = User::create([
                'username'   => 'superadmin',
                'password'   => Hash::make('password'),
                'role_user'  => 'superAdmin',
                'email'      => 'superAdmin@gmail.com',
                'token'      => Str::random(40),
                'verifikasi' => 2,
            ]);

            SuperAdmin::create([
                'id_super_admin'    => $superAdmin->id,
                'username'          => 'superadmin',
                'nama_lengkap'      => 'Super Admin',
                'email'             => 'superAdmin@gmail.com',
                'nomor_telephone'   => '08988590348',
            ]);


            $Admin = User::create([
                'username'   => 'admin',
                'password'   => Hash::make('password'),
                'role_user'  => 'admin',
                'email'      => 'administrator@gmail.com',
                'token'      => Str::random(40),
                'verifikasi' => 2,
            ]);

            Admin::create([
                'id_admin'            => $Admin->id,
                'username'          => 'admin',
                'nama_lengkap'      => 'Administrator',
                'email'             => 'Administrator@gmail.com',
                'nomor_telephone'   => '08988590348',
            ]);


            $teacher = User::create([
                'username'   => 'teacher',
                'password'   => Hash::make('password'),
                'role_user'  => 'teacher',
                'token'      => Str::random(40),
                'email'      => 'teacher@gmail.com',
                'verifikasi' => 2,
            ]);

            Teacher::create([
                'id_teacher'          => $teacher->id,
                'nama_lengkap'        => 'Si Sampel Guru',
                'email'               => 'teacher@gmail.com',
                'nomor_telephone'     => '0000098989899',
                'info'                => 'sosial media',
                'verifikasi'          => 1,
                'id_level' => 1,
                'nama_level' => 'BASE',
            ]);


            $student = User::create([
                'username'   => 'student_sample',
                'password'   => Hash::make('password'),
                'role_user'  => 'siswa',
                'token'      => Str::random(40),
                'email'      => 'student@gmail.com',
                'verifikasi' => 2,
            ]);

            $insertStudent = Student::create([
                'id_siswa'          => $student->id,
                'nama_lengkap'      => 'Si Sampel Siswa',
                'email'             => 'student@gmail.com',
                'nomor_telephone'   => '0809346098998',
                'info'              => 'sosial media',
            ]);

            $orangtua = User::create([
                'username'   => 'orangtua_siswa',
                'password'   => Hash::make('password'),
                'role_user'  => 'parent',
                'token'      => Str::random(40),
                'email'      => 'orangtua@gmail.com',
                'verifikasi' => 2,
            ]);

            OrangTua::create([
                'id_user'           => $orangtua->id,
                'id_orang_tua'      => $student->id,
                'nama_lengkap'      => 'OrangTuaSiswa',
                'email'             => 'orangtua@gmail.com',
                'nomor_telephone'   => '069586985659',
            ]);


            Mapel::insert([
                [
                    'kode' => 'PU',
                    'nama_mapel'     => 'Kemampuan Penalaran Umum',
                    'created_at'    => Carbon::now()->toDateTimeString(),
                    'updated_at'    => Carbon::now()->toDateTimeString(),
                ],
                [
                    'kode' => 'PK',
                    'nama_mapel'     => 'Pengetahuan Kuantitatif',
                    'created_at'    => Carbon::now()->toDateTimeString(),
                    'updated_at'    => Carbon::now()->toDateTimeString(),
                ],
                [
                    'kode' => 'PPU',
                    'nama_mapel'     => 'Pengetahuan dan Pemahaman Umum',
                    'created_at'    => Carbon::now()->toDateTimeString(),
                    'updated_at'    => Carbon::now()->toDateTimeString(),
                ],
                [
                    'kode' => 'MBM',
                    'nama_mapel'     => 'Kemampuan Memahami Bacaan dan Menulis',
                    'created_at'    => Carbon::now()->toDateTimeString(),
                    'updated_at'    => Carbon::now()->toDateTimeString(),
                ],
                [
                    'kode' => 'GEO',
                    'nama_mapel'     => 'Geografi',
                    'created_at'    => Carbon::now()->toDateTimeString(),
                    'updated_at'    => Carbon::now()->toDateTimeString(),
                ],
                [
                    'kode' => 'SJR',
                    'nama_mapel'     => 'Sejarah',
                    'created_at'    => Carbon::now()->toDateTimeString(),
                    'updated_at'    => Carbon::now()->toDateTimeString(),
                ],
                [
                    'kode' => 'SOS',
                    'nama_mapel'     => 'Sosiologi',
                    'created_at'    => Carbon::now()->toDateTimeString(),
                    'updated_at'    => Carbon::now()->toDateTimeString(),
                ],
                [
                    'kode' => 'EKO',
                    'nama_mapel'     => 'Ekonomi',
                    'created_at'    => Carbon::now()->toDateTimeString(),
                    'updated_at'    => Carbon::now()->toDateTimeString(),
                ],
                [
                    'kode' => 'BINGG',
                    'nama_mapel'     => 'Bahasa Inggris',
                    'created_at'    => Carbon::now()->toDateTimeString(),
                    'updated_at'    => Carbon::now()->toDateTimeString(),
                ],
                [
                    'kode' => 'BIND',
                    'nama_mapel'     => 'Bahasa Indonesia',
                    'created_at'    => Carbon::now()->toDateTimeString(),
                    'updated_at'    => Carbon::now()->toDateTimeString(),
                ],
                [
                    'kode' => 'MTK',
                    'nama_mapel'     => 'Matematika',
                    'created_at'    => Carbon::now()->toDateTimeString(),
                    'updated_at'    => Carbon::now()->toDateTimeString(),
                ],
                [
                    'kode' => 'FSK',
                    'nama_mapel'     => 'Fisika',
                    'created_at'    => Carbon::now()->toDateTimeString(),
                    'updated_at'    => Carbon::now()->toDateTimeString(),
                ],
                [
                    'kode' => 'KIM',
                    'nama_mapel'     => 'Kimia',
                    'created_at'    => Carbon::now()->toDateTimeString(),
                    'updated_at'    => Carbon::now()->toDateTimeString(),
                ],
                [
                    'kode' => 'BIO',
                    'nama_mapel'     => 'Biologi',
                    'created_at'    => Carbon::now()->toDateTimeString(),
                    'updated_at'    => Carbon::now()->toDateTimeString(),
                ]
            ]);

            $kategoriTOInsertData = [];

            // UTBK
            array_push($kategoriTOInsertData, [
                'kategori' => 'UTBK',
                'kelompok' => 'SAINTEK',
                'id_jenjang' => null,
                'jenjang' => null,
                'id_kelas' => null,
                'kelas' => null,
                'id_penjurusan' => null,
                'penjurusan' => null,
            ]);
            array_push($kategoriTOInsertData, [
                'kategori' => 'UTBK',
                'kelompok' => 'SOSHUM',
                'id_jenjang' => null,
                'jenjang' => null,
                'id_kelas' => null,
                'kelas' => null,
                'id_penjurusan' => null,
                'penjurusan' => null,
            ]);

            Jenjang::insert([
                [
                    'nama_jenjang' => 'SD',
                    'created_at'    => Carbon::now()->toDateTimeString(),
                    'updated_at'    => Carbon::now()->toDateTimeString(),
                ],
                [
                    'nama_jenjang' => 'SMP',
                    'created_at'    => Carbon::now()->toDateTimeString(),
                    'updated_at'    => Carbon::now()->toDateTimeString(),
                ],
                [
                    'nama_jenjang' => 'SMA',
                    'created_at'    => Carbon::now()->toDateTimeString(),
                    'updated_at'    => Carbon::now()->toDateTimeString(),
                ]
            ]);

            // ASPD
            array_push($kategoriTOInsertData, [
                'kategori' => 'ASPD',
                'kelompok' => null,
                'id_jenjang' => 1, //SD
                'jenjang' => 'SD',
                'id_kelas' => null,
                'kelas' => null,
                'id_penjurusan' => null,
                'penjurusan' => null,
            ]);

            array_push($kategoriTOInsertData, [
                'kategori' => 'ASPD',
                'kelompok' => null,
                'id_jenjang' => 2, //SMP
                'jenjang' => 'SMP',
                'id_kelas' => null,
                'kelas' => null,
                'id_penjurusan' => null,
                'penjurusan' => null,
            ]);

            array_push($kategoriTOInsertData, [
                'kategori' => 'ASPD',
                'kelompok' => null,
                'id_jenjang' => 3, //SMA
                'jenjang' => 'SMA',
                'id_kelas' => null,
                'kelas' => null,
                'id_penjurusan' => null,
                'penjurusan' => null,
            ]);

            // PAS
            array_push($kategoriTOInsertData, [
                'kategori' => 'PAS',
                'kelompok' => null,
                'id_jenjang' => 1, //SD
                'jenjang' => 'SD',
                'id_kelas' => null,
                'kelas' => null,
                'id_penjurusan' => null,
                'penjurusan' => null,
            ]);

            array_push($kategoriTOInsertData, [
                'kategori' => 'PAS',
                'kelompok' => null,
                'id_jenjang' => 2, //SMP
                'jenjang' => 'SMP',
                'id_kelas' => null,
                'kelas' => null,
                'id_penjurusan' => null,
                'penjurusan' => null,
            ]);

            // PAS SMA
            array_push($kategoriTOInsertData, [
                'kategori' => 'PAS',
                'kelompok' => null,
                'id_jenjang' => 3, //SMA
                'jenjang' => 'SMA',
                'id_penjurusan' => null,
                'penjurusan' => 'IPA',
                'id_kelas' => null,
                'kelas' => null,
            ]);
            array_push($kategoriTOInsertData, [
                'kategori' => 'PAS',
                'kelompok' => null,
                'id_jenjang' => 3, //SMA
                'jenjang' => 'SMA',
                'id_penjurusan' => null,
                'penjurusan' => 'IPS',
                'id_kelas' => null,
                'kelas' => null,
            ]);

            for ($i = 1; $i <= 6; $i++) {
                $kelasCreated = Kelas::create(['nama_kelas' => 'Kelas ' . $i, 'id_jenjang' => 1]);

                if ($i > 3) {
                    // PAT SD kelas 4 - 6
                    array_push($kategoriTOInsertData, [
                        'kategori' => 'PAT',
                        'kelompok' => null,
                        'id_jenjang' => 1, //SD
                        'jenjang' => 'SD',
                        'id_kelas' => $kelasCreated->id,
                        'kelas' => $kelasCreated->nama_kelas,
                        'id_penjurusan' => null,
                        'penjurusan' => null,
                    ]);
                }
            }

            for ($i = 7; $i <= 9; $i++) {
                $kelasCreated = Kelas::create(['nama_kelas' => 'Kelas ' . $i, 'id_jenjang' => 2]);
                // PAT SMP
                array_push($kategoriTOInsertData, [
                    'kategori' => 'PAT',
                    'kelompok' => null,
                    'id_jenjang' => 2, //SMP
                    'jenjang' => 'SMP',
                    'id_kelas' => $kelasCreated->id,
                    'kelas' => $kelasCreated->nama_kelas,
                    'id_penjurusan' => null,
                    'penjurusan' => null,
                ]);
            }

            for ($i = 10; $i <= 12; $i++) {
                Kelas::create(['nama_kelas' => 'Kelas ' . $i, 'id_jenjang' => 3]);
            }

            for ($i = 1; $i <= 10; $i++) {
                $penjurusanInserted = Penjurusan::create([
                    'nama_penjurusan' => '-',
                    'id_kelas' => $i,
                ]);

                if ($i >= 10) {
                    $penjurusanDetail = Penjurusan::with(['kelas'])->find($penjurusanInserted->id);
                    array_push($kategoriTOInsertData, [
                        'kategori' => 'PAT',
                        'kelompok' => null,
                        'id_jenjang' => 3, //SMA
                        'jenjang' => 'SMA',
                        'id_kelas' => $penjurusanDetail->kelas ? $penjurusanDetail->kelas->id : null,
                        'kelas' => $penjurusanDetail->kelas ? $penjurusanDetail->kelas->nama_kelas : null,
                        'id_penjurusan' => $penjurusanDetail->id,
                        'penjurusan' => $penjurusanDetail->nama_penjurusan,
                    ]);
                }
            }

            Penjurusan::insert([
                [
                    'nama_penjurusan'     => 'IPA',
                    'id_kelas'             => 11,
                    'created_at'        => Carbon::now()->toDateTimeString(),
                    'updated_at'        => Carbon::now()->toDateTimeString(),
                ],
                [
                    'nama_penjurusan'     => 'IPS',
                    'id_kelas'             => 11,
                    'created_at'        => Carbon::now()->toDateTimeString(),
                    'updated_at'        => Carbon::now()->toDateTimeString(),
                ],

                [
                    'nama_penjurusan'     => 'IPA',
                    'id_kelas'             => 12,
                    'created_at'        => Carbon::now()->toDateTimeString(),
                    'updated_at'        => Carbon::now()->toDateTimeString(),
                ],
                [
                    'nama_penjurusan'     => 'IPS',
                    'id_kelas'             => 12,
                    'created_at'        => Carbon::now()->toDateTimeString(),
                    'updated_at'        => Carbon::now()->toDateTimeString(),
                ]
            ]);

            // PAT SMA
            array_push($kategoriTOInsertData, [
                'kategori' => 'PAT',
                'kelompok' => null,
                'id_jenjang' => 3, //SMA
                'jenjang' => 'SMA',
                'id_kelas' => 11,
                'kelas' => 'Kelas 11',
                'id_penjurusan' => 11,
                'penjurusan' => 'IPA',
            ]);
            array_push($kategoriTOInsertData, [
                'kategori' => 'PAT',
                'kelompok' => null,
                'id_jenjang' => 3, //SMA
                'jenjang' => 'SMA',
                'id_kelas' => 11,
                'kelas' => 'Kelas 11',
                'id_penjurusan' => 12,
                'penjurusan' => 'IPS',
            ]);
            array_push($kategoriTOInsertData, [
                'kategori' => 'PAT',
                'kelompok' => null,
                'id_jenjang' => 3, //SMA
                'jenjang' => 'SMA',
                'id_kelas' => 12,
                'kelas' => 'Kelas 12',
                'id_penjurusan' => 13,
                'penjurusan' => 'IPA',
            ]);
            array_push($kategoriTOInsertData, [
                'kategori' => 'PAT',
                'kelompok' => null,
                'id_jenjang' => 3, //SMA
                'jenjang' => 'SMA',
                'id_kelas' => 12,
                'kelas' => 'Kelas 12',
                'id_penjurusan' => 14,
                'penjurusan' => 'IPS',
            ]);

            // KategoriTo::insert($kategoriTOInsertData);

            PerguruanTinggi::insert([
                [
                    'nama_perguruan'    => 'Universitas Gadjah Mada',
                    'akreditasi'        => 'A',
                    'created_at'        => Carbon::now()->toDateTimeString(),
                    'updated_at'        => Carbon::now()->toDateTimeString(),
                ],
                [
                    'nama_perguruan'    => 'Universitas Indonesia',
                    'akreditasi'        => 'A',
                    'created_at'        => Carbon::now()->toDateTimeString(),
                    'updated_at'        => Carbon::now()->toDateTimeString(),
                ],
                [
                    'nama_perguruan'     => 'Universitas Teknologi Bandung',
                    'akreditasi'        => 'A',
                    'created_at'        => Carbon::now()->toDateTimeString(),
                    'updated_at'        => Carbon::now()->toDateTimeString(),
                ],
            ]);

            ProgramStudi::insert([
                [
                    'nama_studi'    => 'Teknik Informatika',
                    'id_penjurusan'    => null,
                    'id_mapel'        => null,
                    'icon_prodi'        => null,
                    'alasan'        => null,
                    'prospek'        => null,
                    'slug'        => null,
                    'created_at'        => Carbon::now()->toDateTimeString(),
                    'updated_at'        => Carbon::now()->toDateTimeString(),
                ],
                [
                    'nama_studi'    => 'Teknik Pemesinan',
                    'id_penjurusan'    => null,
                    'id_mapel'        => null,
                    'icon_prodi'        => null,
                    'alasan'        => null,
                    'prospek'        => null,
                    'slug'        => null,
                    'created_at'        => Carbon::now()->toDateTimeString(),
                    'updated_at'        => Carbon::now()->toDateTimeString(),
                ],
                [
                    'nama_studi'    => 'Ilmu Komunikasi',
                    'id_penjurusan'    => null,
                    'id_mapel'        => null,
                    'icon_prodi'        => null,
                    'alasan'        => null,
                    'prospek'        => null,
                    'slug'        => null,
                    'created_at'        => Carbon::now()->toDateTimeString(),
                    'updated_at'        => Carbon::now()->toDateTimeString(),
                ],
            ]);

            Bank::insert([
                [
                    'nama_bank'           => 'BCA',
                    'nama_pemilik'      => 'Rahardian Era',
                    'nomor_rekening'      => '12345678900',
                    'cabang'             => NULL,
                    'created_at'        => Carbon::now()->toDateTimeString(),
                ],
                [
                    'nama_bank'           => 'BRI',
                    'nama_pemilik'      => 'Rahardian Era',
                    'nomor_rekening'      => '12345678900',
                    'cabang'             => NULL,
                    'created_at'        => Carbon::now()->toDateTimeString(),
                ],
                [
                    'nama_bank'           => 'BNI',
                    'nama_pemilik'      => 'Rahardian Era',
                    'nomor_rekening'      => '12345678900',
                    'cabang'             => NULL,
                    'created_at'        => Carbon::now()->toDateTimeString(),
                ],
                [
                    'nama_bank'           => 'Mandiri',
                    'nama_pemilik'      => 'Rahardian Era',
                    'nomor_rekening'      => '12345678900',
                    'cabang'             => NULL,
                    'created_at'        => Carbon::now()->toDateTimeString(),
                ]
            ]);

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }
}
