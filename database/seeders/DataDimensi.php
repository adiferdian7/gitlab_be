<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Dimensi;
use Carbon\Carbon;

class DataDimensi extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data=[
        	[
        		'nama'      	=> 'Dimensi Pemusatan Perhatian',
                'psikologi_1' 	=> 'Introvert',
                'psikologi_2'	=> 'Ekstrovert',
                'deskripsi' 	=> '',
                'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(), 
        	],
        	[
        		'nama'      	=> 'Dimensi Memahami Informasi Dari Luar',
                'psikologi_1' 	=> 'Sensing',
                'psikologi_2'	=> 'Intuition',
                'deskripsi' 	=> '',
                'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(), 
        	],
        	[
        		'nama'      	=> 'Dimensi Menarik Kesimpulan & Keputusan',
                'psikologi_1' 	=> 'Thinking',
                'psikologi_2'	=> 'Feeling',
                'deskripsi' 	=> '',
                'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(), 
        	],
        	[
        		'nama'      	=> 'Dimensi Pola Hidup',
                'psikologi_1' 	=> 'Judging',
                'psikologi_2'	=> 'Perceiving',
                'deskripsi' 	=> '',
                'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(), 
        	],
        ];

        Dimensi::insert($data);
    }
}
