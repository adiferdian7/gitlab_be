<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            DataLevel::class,
        	DummyData::class,
            DataDimensi::class,
            DataKepribadian::class,
            DataPengaturan::class,
            DataUktt::class,
        ]);
    }
}
