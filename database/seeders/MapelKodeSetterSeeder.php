<?php

namespace Database\Seeders;

use App\Models\Mapel;
use Illuminate\Database\Seeder;

class MapelKodeSetterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mapelData = Mapel::whereNull('kode')->get();
        foreach ($mapelData as $key => $value) {
            $find = Mapel::find($value->id);
            $find->update(['kode' => 'M0' . $value->id]);
        }
        echo 'success' . PHP_EOL;
    }
}
