<?php

namespace Database\Seeders;

use App\Models\Produk;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DataUktt extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $data = [
      [
        'nama_produk'  => 'Tes Kompetensi Dasar UjiAja - Jenjang SD',
        'kategori_produk' => 'UKTT',
        'status_produk' => 'Aktif',
        'id_uktt' => NULL,
        'uktt_level' => 'Dasar',
        'uktt_nilai_minimal' => 0,
        'uktt_id_level' => 2, // SUPER,

        // NOT FOR UKTT
        'jenis_produk' => NULL,
        'harga_produk' => 0,
        'pakai_sertifikat' => NULL,
        'tipe_paket' => NULL,
        'tanggal_mulai' => NULL,
        'tanggal_berakhir' => NULL,
        'pakai_perankingan' => NULL,
        'perhitungan' => '{"correct":2,"wrong":-1,"unfilled":0}',
        'deskripsi_produk' => NULL,
        'bonus' => NULL,
        'maksimal_peserta' => NULL,
        'created_at'    => Carbon::now()->toDateTimeString(),
        'updated_at'    => Carbon::now()->toDateTimeString(),
      ],
      [
        'nama_produk'  => 'Tes Kompetensi Lanjutan 1.0 UjiAja - Jenjang SD',
        'kategori_produk' => 'UKTT',
        'status_produk' => 'Aktif',
        'id_uktt' => NULL,
        'uktt_level' => 'Lanjutan 1.0',
        'uktt_nilai_minimal' => 0,
        'uktt_id_level' => 3, // ELITE

        // NOT FOR UKTT
        'jenis_produk' => NULL,
        'harga_produk' => 0,
        'pakai_sertifikat' => NULL,
        'tipe_paket' => NULL,
        'tanggal_mulai' => NULL,
        'tanggal_berakhir' => NULL,
        'pakai_perankingan' => NULL,
        'perhitungan' => '{"correct":2,"wrong":-1,"unfilled":0}',
        'deskripsi_produk' => NULL,
        'bonus' => NULL,
        'maksimal_peserta' => NULL,
        'created_at'    => Carbon::now()->toDateTimeString(),
        'updated_at'    => Carbon::now()->toDateTimeString(),
      ],
      [
        'nama_produk'  => 'Tes Kompetensi Lanjutan 2.0 UjiAja - Jenjang SD',
        'kategori_produk' => 'UKTT',
        'status_produk' => 'Aktif',
        'id_uktt' => NULL,
        'uktt_level' => 'Lanjutan 2.0',
        'uktt_nilai_minimal' => 0,
        'uktt_id_level' => 4, // MASTER

        // NOT FOR UKTT
        'jenis_produk' => NULL,
        'harga_produk' => 0,
        'pakai_sertifikat' => NULL,
        'tipe_paket' => NULL,
        'tanggal_mulai' => NULL,
        'tanggal_berakhir' => NULL,
        'pakai_perankingan' => NULL,
        'perhitungan' => '{"correct":2,"wrong":-1,"unfilled":0}',
        'deskripsi_produk' => NULL,
        'bonus' => NULL,
        'maksimal_peserta' => NULL,
        'created_at'    => Carbon::now()->toDateTimeString(),
        'updated_at'    => Carbon::now()->toDateTimeString(),
      ],
      [
        'nama_produk'  => 'Tes Kompetensi Lanjutan 3.0 UjiAja - Jenjang SD',
        'kategori_produk' => 'UKTT',
        'status_produk' => 'Aktif',
        'id_uktt' => NULL,
        'uktt_level' => 'Lanjutan 3.0',
        'uktt_nilai_minimal' => 0,
        'uktt_id_level' => 5, // MASTER

        // NOT FOR UKTT
        'jenis_produk' => NULL,
        'harga_produk' => 0,
        'pakai_sertifikat' => NULL,
        'tipe_paket' => NULL,
        'tanggal_mulai' => NULL,
        'tanggal_berakhir' => NULL,
        'pakai_perankingan' => NULL,
        'perhitungan' => '{"correct":2,"wrong":-1,"unfilled":0}',
        'deskripsi_produk' => NULL,
        'bonus' => NULL,
        'maksimal_peserta' => NULL,
        'created_at'    => Carbon::now()->toDateTimeString(),
        'updated_at'    => Carbon::now()->toDateTimeString(),
      ],
    ];

    Produk::insert($data);
  }
}
