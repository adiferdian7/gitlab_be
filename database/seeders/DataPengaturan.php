<?php

namespace Database\Seeders;

use App\Models\Pengaturan;
use Illuminate\Database\Seeder;

class DataPengaturan extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataPengaturan = [
            // Informasi
            [
                'key' => 'logo',
                'label' => 'Logo',
                'isi' => 'logo.png'
            ],
            [
                'key' => 'alamat_kantor',
                'label' => 'Alamat Kantor Pusat',
                'isi' => 'Jl. Sunaryo No.14, Kotabaru, Kec. Gondokusuman, Kota Yogyakarta, Daerah Istimewa Yogyakarta 55224'
            ],
            // Sosmed
            [
                'key' => 'telp',
                'label' => 'Telepon',
                'isi' => '021345678'
            ],
            [
                'key' => 'whatsapp',
                'label' => 'Telepon',
                'isi' => '6285773716731'
            ],
            [
                'key' => 'instagram',
                'label' => 'Instagram',
                'isi' => 'https://instagram.com/username_anda'
            ],
            [
                'key' => 'facebook',
                'label' => 'Facebook',
                'isi' => 'https://facebook.com/username_anda'
            ],
            [
                'key' => 'youtube',
                'label' => 'Youtube Channel',
                'isi' => 'https://youtube.com/c/username_anda'
            ],
            [
                'key' => 'email',
                'label' => 'Email',
                'isi' => 'tanya@ujiaja.co.id'
            ],
            // Fee
            [
                'key' => 'fee_banktrf',
                'label' => 'Fee Bank Transfer',
                'isi' => 1000,
            ],
            [
                'key' => 'fee_xendit',
                'label' => 'Fee Pembayaran Instan',
                'isi' => 5000,
            ],
            [
                'key' => 'fee_admin',
                'label' => 'Fee Admin',
                'isi' => 5, // percent
            ],
            [
                'key' => 'min_penarikan',
                'label' => 'Minimal Nominal Penarikan',
                'isi' => 25000,
            ],
            [
                'key' => 'biaya_transfer',
                'label' => 'Biaya Transfer',
                'isi' => 6500,
            ],
            [
                'key' => 'pembayaran_uktt',
                'label' => 'Pembayaran UKTT',
                'isi' => 'Gratis', // Gratis or Berbayar
            ],
            // SEO 
            [
                'key' => 'seo_web_name',
                'label' => 'SEO - Nama Website',
                'isi' => 'UjiAja - Tagline Here'
            ],
        ];

        foreach ($dataPengaturan as $pengaturan) {
            if(Pengaturan::where('key', $pengaturan['key'])->count() == 0) {
                Pengaturan::create($pengaturan);
            }
        }
    }
}
