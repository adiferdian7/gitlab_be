<?php

namespace Database\Seeders;

use App\Models\Level;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DataLevel extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        	[
        		'nama_level'	=> 'BASE',
                'deskripsi_level' => '',
                'honor_level' => 50000,
                'sesi_honor_level' => 90,
                'id_uktt' => null,
                'minimal_rate_mengajar' => 0,
                'minimal_total_mengajar' => 0,
                'maksimal_waktu_mengajar' => 0,
                    'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(),
            ],
            [
                    'nama_level'	=> 'SUPER',
                'deskripsi_level' => '',
                'honor_level' => 75000,
                'sesi_honor_level' => 90,
                'id_uktt' => 1,
                'minimal_rate_mengajar' => 3,
                'minimal_total_mengajar' => 5,
                'maksimal_waktu_mengajar' => 90,
                    'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(),
            ],
            [
                    'nama_level'	=> 'ELITE',
                'deskripsi_level' => '',
                'honor_level' => 125000,
                'sesi_honor_level' => 90,
                'id_uktt' => 2,
                'minimal_rate_mengajar' => 4,
                'minimal_total_mengajar' => 15,
                'maksimal_waktu_mengajar' => 150,
                'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(),
            ],
            [
                    'nama_level'	=> 'MASTER',
                'deskripsi_level' => '',
                'honor_level' => 200000,
                'sesi_honor_level' => 90,
                'id_uktt' => 3,
                'minimal_rate_mengajar' => 4,
                'minimal_total_mengajar' => 50,
                'maksimal_waktu_mengajar' => 180,
                    'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(),
            ],
            [
                    'nama_level'	=> 'LEGEND',
                'deskripsi_level' => '',
                'honor_level' => 350000,
                'sesi_honor_level' => 90,
                'id_uktt' => 4,
                'minimal_rate_mengajar' => 5,
                'minimal_total_mengajar' => 75,
                'maksimal_waktu_mengajar' => 360,
                    'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(),
        	]
        ];

        Level::insert($data);
    }
}
