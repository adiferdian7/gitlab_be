<?php

namespace Database\Seeders;

use App\Models\KategoriTo;
use App\Models\Kelas;
use App\Models\Penjurusan;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KategoriToSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kategoriTOInsertData = [];

        DB::beginTransaction();

        DB::statement('TRUNCATE to_kategori');

        try {

            // UTBK
            array_push($kategoriTOInsertData, [
                'kategori' => 'UTBK',
                'kelompok' => 'SAINTEK',
                'id_jenjang' => null,
                'jenjang' => null,
                'id_kelas' => null,
                'kelas' => null,
                'id_penjurusan' => null,
                'penjurusan' => null,
            ]);
            array_push($kategoriTOInsertData, [
                'kategori' => 'UTBK',
                'kelompok' => 'SOSHUM',
                'id_jenjang' => null,
                'jenjang' => null,
                'id_kelas' => null,
                'kelas' => null,
                'id_penjurusan' => null,
                'penjurusan' => null,
            ]);

            // ASPD
            array_push($kategoriTOInsertData, [
                'kategori' => 'ASPD',
                'kelompok' => null,
                'id_jenjang' => 1, //SD
                'jenjang' => 'SD',
                'id_kelas' => null,
                'kelas' => null,
                'id_penjurusan' => null,
                'penjurusan' => null,
            ]);

            array_push($kategoriTOInsertData, [
                'kategori' => 'ASPD',
                'kelompok' => null,
                'id_jenjang' => 2, //SMP
                'jenjang' => 'SMP',
                'id_kelas' => null,
                'kelas' => null,
                'id_penjurusan' => null,
                'penjurusan' => null,
            ]);

            array_push($kategoriTOInsertData, [
                'kategori' => 'ASPD',
                'kelompok' => null,
                'id_jenjang' => 3, //SMA
                'jenjang' => 'SMA',
                'id_kelas' => null,
                'kelas' => null,
                'id_penjurusan' => null,
                'penjurusan' => null,
            ]);

            // // PAS
            // array_push($kategoriTOInsertData, [
            //     'kategori' => 'PAS',
            //     'kelompok' => null,
            //     'id_jenjang' => 1, //SD
            //     'jenjang' => 'SD',
            //     'id_kelas' => null,
            //     'kelas' => null,
            //     'id_penjurusan' => null,
            //     'penjurusan' => null,
            // ]);

            // array_push($kategoriTOInsertData, [
            //     'kategori' => 'PAS',
            //     'kelompok' => null,
            //     'id_jenjang' => 2, //SMP
            //     'jenjang' => 'SMP',
            //     'id_kelas' => null,
            //     'kelas' => null,
            //     'id_penjurusan' => null,
            //     'penjurusan' => null,
            // ]);

            // // PAS SMA
            // array_push($kategoriTOInsertData, [
            //     'kategori' => 'PAS',
            //     'kelompok' => null,
            //     'id_jenjang' => 3, //SMA
            //     'jenjang' => 'SMA',
            //     'id_penjurusan' => null,
            //     'penjurusan' => 'IPA',
            //     'id_kelas' => null,
            //     'kelas' => null,
            // ]);
            // array_push($kategoriTOInsertData, [
            //     'kategori' => 'PAS',
            //     'kelompok' => null,
            //     'id_jenjang' => 3, //SMA
            //     'jenjang' => 'SMA',
            //     'id_penjurusan' => null,
            //     'penjurusan' => 'IPS',
            //     'id_kelas' => null,
            //     'kelas' => null,
            // ]);

            for ($i = 1; $i <= 6; $i++) {
                $kelasDetail = Kelas::where(['nama_kelas' => 'Kelas ' . $i, 'id_jenjang' => 1])->first();

                if ($i > 3 && !empty($kelasDetail)) {
                    // PAT SD kelas 4 - 6
                    array_push($kategoriTOInsertData, [
                        'kategori' => 'PAT',
                        'kelompok' => null,
                        'id_jenjang' => 1, //SD
                        'jenjang' => 'SD',
                        'id_kelas' => $kelasDetail->id,
                        'kelas' => $kelasDetail->nama_kelas,
                        'id_penjurusan' => null,
                        'penjurusan' => null,
                    ]);
                    // PAS SD kelas 4 - 6
                    array_push($kategoriTOInsertData, [
                        'kategori' => 'PAS',
                        'kelompok' => null,
                        'id_jenjang' => 1, //SD
                        'jenjang' => 'SD',
                        'id_kelas' => $kelasDetail->id,
                        'kelas' => $kelasDetail->nama_kelas,
                        'id_penjurusan' => null,
                        'penjurusan' => null,
                    ]);
                }
            }

            for ($i = 7; $i <= 9; $i++) {
                $kelasDetail = Kelas::where(['nama_kelas' => 'Kelas ' . $i, 'id_jenjang' => 2])->first();
                if (!empty($kelasDetail)) {
                    // PAT SMP
                    array_push($kategoriTOInsertData, [
                        'kategori' => 'PAT',
                        'kelompok' => null,
                        'id_jenjang' => 2, //SMP
                        'jenjang' => 'SMP',
                        'id_kelas' => $kelasDetail->id,
                        'kelas' => $kelasDetail->nama_kelas,
                        'id_penjurusan' => null,
                        'penjurusan' => null,
                    ]);

                    // PAS SMP
                    array_push($kategoriTOInsertData, [
                        'kategori' => 'PAS',
                        'kelompok' => null,
                        'id_jenjang' => 2, //SMP
                        'jenjang' => 'SMP',
                        'id_kelas' => $kelasDetail->id,
                        'kelas' => $kelasDetail->nama_kelas,
                        'id_penjurusan' => null,
                        'penjurusan' => null,
                    ]);
                }
            }

            for ($i = 10; $i <= 12; $i++) {
                $penjurusanDetail = Penjurusan::with(['kelas'])->where('id_kelas', $i)->get();
                foreach ($penjurusanDetail as $penjurusanDetailRow) {
                    // PAT SMA
                    array_push($kategoriTOInsertData, [
                        'kategori' => 'PAT',
                        'kelompok' => null,
                        'id_jenjang' => 3, //SMA
                        'jenjang' => 'SMA',
                        'id_kelas' => $penjurusanDetailRow->kelas ? $penjurusanDetailRow->kelas->id : null,
                        'kelas' => $penjurusanDetailRow->kelas ? $penjurusanDetailRow->kelas->nama_kelas : null,
                        'id_penjurusan' => $penjurusanDetailRow->nama_penjurusan && $penjurusanDetailRow->nama_penjurusan != '-' ? $penjurusanDetailRow->id : null,
                        'penjurusan' => $penjurusanDetailRow->nama_penjurusan && $penjurusanDetailRow->nama_penjurusan != '-' ? $penjurusanDetailRow->nama_penjurusan : null,
                    ]);

                    // PAS SMA
                    array_push($kategoriTOInsertData, [
                        'kategori' => 'PAS',
                        'kelompok' => null,
                        'id_jenjang' => 3, //SMA
                        'jenjang' => 'SMA',
                        'id_kelas' => $penjurusanDetailRow->kelas ? $penjurusanDetailRow->kelas->id : null,
                        'kelas' => $penjurusanDetailRow->kelas ? $penjurusanDetailRow->kelas->nama_kelas : null,
                        'id_penjurusan' => $penjurusanDetailRow->nama_penjurusan && $penjurusanDetailRow->nama_penjurusan != '-' ? $penjurusanDetailRow->id : null,
                        'penjurusan' => $penjurusanDetailRow->nama_penjurusan && $penjurusanDetailRow->nama_penjurusan != '-' ? $penjurusanDetailRow->nama_penjurusan : null,
                    ]);
                }
            }

            // PAT SMA
            // array_push($kategoriTOInsertData, [
            //     'kategori' => 'PAT',
            //     'kelompok' => null,
            //     'id_jenjang' => 3, //SMA
            //     'jenjang' => 'SMA',
            //     'id_kelas' => 11,
            //     'kelas' => 'Kelas 11',
            //     'id_penjurusan' => 11,
            //     'penjurusan' => 'IPA',
            // ]);
            // array_push($kategoriTOInsertData, [
            //     'kategori' => 'PAT',
            //     'kelompok' => null,
            //     'id_jenjang' => 3, //SMA
            //     'jenjang' => 'SMA',
            //     'id_kelas' => 11,
            //     'kelas' => 'Kelas 11',
            //     'id_penjurusan' => 12,
            //     'penjurusan' => 'IPS',
            // ]);
            // array_push($kategoriTOInsertData, [
            //     'kategori' => 'PAT',
            //     'kelompok' => null,
            //     'id_jenjang' => 3, //SMA
            //     'jenjang' => 'SMA',
            //     'id_kelas' => 12,
            //     'kelas' => 'Kelas 12',
            //     'id_penjurusan' => 13,
            //     'penjurusan' => 'IPA',
            // ]);
            // array_push($kategoriTOInsertData, [
            //     'kategori' => 'PAT',
            //     'kelompok' => null,
            //     'id_jenjang' => 3, //SMA
            //     'jenjang' => 'SMA',
            //     'id_kelas' => 12,
            //     'kelas' => 'Kelas 12',
            //     'id_penjurusan' => 14,
            //     'penjurusan' => 'IPS',
            // ]);

            KategoriTo::insert($kategoriTOInsertData);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }
}
