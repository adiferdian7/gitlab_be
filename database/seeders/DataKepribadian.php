<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Kepribadian;
use Carbon\Carbon;

class DataKepribadian extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        	[	
        		'nama_singkat'	=> 'ENFJ',
        		'nama_panjang'	=> '-',
        		'jenis'			=> 'Meyakinkan',
        		'deskripsi'		=> '-',
        		'saran'			=> '-',
        		'profesi'		=> 'Konsultan, Psikolog, Konselor, Pengajar, Marketing, HRD, Event Coordinator, Entertainer, Penulis, Motivator',
        		'partner'		=> 'INFP atau ISFP',
        		'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(), 
        	],
        	[	
        		'nama_singkat'	=> 'ENFP',
        		'nama_panjang'	=> '-',
        		'jenis'			=> 'Optimis',
        		'deskripsi'		=> '-',
        		'saran'			=> '-',
        		'profesi'		=> 'Konselor, Psikolog, Entertainer, Pengajar, Motivator, Presenter, Reporter, MC, Seniman, Hospitality',
        		'partner'		=> 'INTJ atau INFJ',
        		'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(), 
        	],
        	[	
        		'nama_singkat'	=> 'ENTJ',
        		'nama_panjang'	=> '-',
        		'jenis'			=> 'Pemimpin Alami',
        		'deskripsi'		=> '-',
        		'saran'			=> '-',
        		'profesi'		=> 'Konsultan, Pengajar, HRD, Event Coordinator, Entertainer, Dokter, Polisi, Pilot, Hakim, Bidang Manajemen',
        		'partner'		=> 'INFP atau ISFP',
        		'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(), 
        	],
        	[	
        		'nama_singkat'	=> 'ENTP',
        		'nama_panjang'	=> '-',
        		'jenis'			=> 'Inovatif - Kreatif',
        		'deskripsi'		=> '-',
        		'saran'			=> '-',
        		'profesi'		=> 'Pengacara, Psikolog, Konsultan, Ilmuwan, Aktor,Marketing, Programmer, Fotografer',
        		'partner'		=> 'INFJ atau INTJ',
        		'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(), 
        	],
        	[	
        		'nama_singkat'	=> 'ESFJ',
        		'nama_panjang'	=> '-',
        		'jenis'			=> 'Harmonis',
        		'deskripsi'		=> '-',
        		'saran'			=> '-',
        		'profesi'		=> 'Perencana Keuangan, Perawat, Guru, Bidang anak-anak, Konselor, Administratif, Hospitality',
        		'partner'		=> 'ISFP atau INFP',
        		'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(), 
        	],
        	[	
        		'nama_singkat'	=> 'ESFP',
        		'nama_panjang'	=> '-',
        		'jenis'			=> 'Murah Hati',
        		'deskripsi'		=> '-',
        		'saran'			=> '-',
        		'profesi'		=> 'Entertainer, Seniman, Marketing, Konselor, Designer, Tour Guide, Bidang Anak-anak, Bidang Hospitality',
        		'partner'		=> 'ISTJ atau ISFJ',
        		'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(), 
        	],
        	[	
        		'nama_singkat'	=> 'ESTJ',
        		'nama_panjang'	=> '-',
        		'jenis'			=> 'Konservatif - Disiplin',
        		'deskripsi'		=> '-',
        		'saran'			=> '-',
        		'profesi'		=> 'Militer, Manajer, Polisi, Hakim, Pengacara, Guru, Sales, Auditor, Akuntan, Sistem Analis',
        		'partner'		=> 'ISTP atau INTP',
        		'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(), 
        	],
        	[	
        		'nama_singkat'	=> 'ESTP',
        		'nama_panjang'	=> '-',
        		'jenis'			=> 'Spontan',
        		'deskripsi'		=> '-',
        		'saran'			=> '-',
        		'profesi'		=> 'Marketing, Sales, Polisi, Entrepreneur, Pialang Saham, Technical Support',
        		'partner'		=> 'ISFJ atau ISTJ',
        		'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(), 
        	],
        	[	
        		'nama_singkat'	=> 'INFJ',
        		'nama_panjang'	=> '-',
        		'jenis'			=> 'Reflektif',
        		'deskripsi'		=> '-',
        		'saran'			=> '-',
        		'profesi'		=> 'Pengajar, Psikolog, Dokter, Konselor, Pekerja Sosial, Fotografer, Seniman, Designer, Child Care',
        		'partner'		=> 'ESFP atau ESTP',
        		'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(), 
        	],
        	[	
        		'nama_singkat'	=> 'INFP',
        		'nama_panjang'	=> '-',
        		'jenis'			=> 'Idealis',
        		'deskripsi'		=> '-',
        		'saran'			=> '-',
        		'profesi'		=> 'Penulis, Sastrawan, Dokter, Konselor, Psikolog, Pengajar, Seniman, Rohaniawan, Bidang Hospitality',
        		'partner'		=> 'ENFJ atau ESFJ',
        		'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(), 
        	],
        	[	
        		'nama_singkat'	=> 'INTJ',
        		'nama_panjang'	=> '-',
        		'jenis'			=> 'Independen',
        		'deskripsi'		=> '-',
        		'saran'			=> '-',
        		'profesi'		=> 'Peneliti, Ilmuwan, Insinyur, Teknisi, Pengajar, Profesor, Dokter, Research & Development, Business Analyst, System Analyst, Pengacara, Hakim, Programmers, Posisi Strategis dalam organisasi',
        		'partner'		=> 'ENFP atau ENTP',
        		'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(), 
        	],
        	[	
        		'nama_singkat'	=> 'INTP',
        		'nama_panjang'	=> '-',
        		'jenis'			=> 'Konseptual',
        		'deskripsi'		=> '-',
        		'saran'			=> '-',
        		'profesi'		=> 'Ilmuwan, Fotografer, Programmer, Ahli Komputer, Sistem Analis, Penulis Buku Teknis, Ahli Forensik, Jaksa, Pengacara, Teknisi',
        		'partner'		=> 'ENTJ atau ESTJ',
        		'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(), 
        	],
        	[	
        		'nama_singkat'	=> 'ISFJ',
        		'nama_panjang'	=> '-',
        		'jenis'			=> 'Setia',
        		'deskripsi'		=> '-',
        		'saran'			=> '-',
        		'profesi'		=> 'Arsitek, Interior Designer, Perawat, Administratif, Designer, Child Care, Konselor, Back Office Manager, Penjaga Toko/Perpustakaan, Dunia Perhotelan',
        		'partner'		=> 'ESFP atau ESTP',
        		'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(), 
        	],
        	[	
        		'nama_singkat'	=> 'ISFP',
        		'nama_panjang'	=> '-',
        		'jenis'			=> 'Artistik',
        		'deskripsi'		=> '-',
        		'saran'			=> '-',
        		'profesi'		=> 'Seniman, Designer, Pekerja Sosial, Konselor, Psikolog, Guru, Aktor, Bidang Hospitality',
        		'partner'		=> 'ESFJ atau ENFJ',
        		'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(), 
        	],
        	[	
        		'nama_singkat'	=> 'ISTJ',
        		'nama_panjang'	=> '-',
        		'jenis'			=> 'Bertanggung jawab',
        		'deskripsi'		=> '-',
        		'saran'			=> '-',
        		'profesi'		=> 'Manajemen, Polisi, Intelijen, Hakim, Pengacara, Dokter, Akuntan (Staf Keuangan), Programmer atau yang berhubungan dengan IT, System Analys, Pemimpin Militer',
        		'partner'		=> 'ESFP atau ESTP',
        		'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(), 
        	],
        	[	
        		'nama_singkat'	=> 'ISTP',
        		'nama_panjang'	=> '-',
        		'jenis'			=> 'Pragmatis',
        		'deskripsi'		=> '-',
        		'saran'			=> '-',
        		'profesi'		=> 'Polisi, Ahli Forensik, Programmer, Ahli Komputer, Sistem Analis, Teknisi, Insinyur, Mekanik, Pilot, Atlet, Pengusaha',
        		'partner'		=> 'ESTJ atau ENTJ',
        		'created_at'    => Carbon::now()->toDateTimeString(),
                'updated_at'    => Carbon::now()->toDateTimeString(), 
        	],
        ];

        Kepribadian::insert($data);
    }
}
