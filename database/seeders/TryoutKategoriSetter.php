<?php

namespace Database\Seeders;

use App\Models\Penjurusan;
use App\Models\Tryout;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TryoutKategoriSetter extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $all_tryouts = Tryout::all();

        foreach ($all_tryouts as $tKey => $tryoutData) {

            $tryoutDetail = Tryout::find($tryoutData->id);
            if ($tryoutDetail) {

                $id_penjurusan = $tryoutDetail->id_penjurusan;
                $id_kelas = $tryoutDetail->id_kelas;
                $id_jenjang = $tryoutDetail->id_jenjang;
                $kategoriTo =  DB::table('to_kategori');
                $kategoriToDetail = null;
                $id_to_kategori = null;
                if ($tryoutDetail->kategori == 'UTBK') {
                    $kategoriToDetail = $kategoriTo->where('kategori', $tryoutDetail->kategori)
                        ->where('kelompok', $tryoutDetail->kelompok_soal)->first();
                } else {
                    $penjurusanDetail = Penjurusan::with(['kelas.jenjang'])->where('id', $id_penjurusan)->first();
                    if ($penjurusanDetail && empty($id_jenjang)) {
                        $id_jenjang = $penjurusanDetail->kelas && $penjurusanDetail->kelas->jenjang ? $penjurusanDetail->kelas->jenjang->id : null;
                    }

                    if ($tryoutDetail->kategori == 'ASPD') {
                        $kategoriToDetail = $kategoriTo->where('kategori', $tryoutDetail->kategori)
                            ->where('id_jenjang', $id_jenjang)->first();
                    } else if ($tryoutDetail->kategori == 'PAS' || $tryoutDetail->kategori == 'PAT') {
                        if ($penjurusanDetail && $penjurusanDetail->nama_penjurusan != '-' && $penjurusanDetail->nama_penjurusan != null) {
                            $kategoriToDetail = $kategoriTo->where('kategori', $tryoutDetail->kategori)
                                ->where('id_jenjang', $id_jenjang)
                                ->where('penjurusan', $penjurusanDetail->nama_penjurusan)
                                ->first();
                        } else {
                            $kategoriToDetail = $kategoriTo->where('kategori', $tryoutDetail->kategori)
                                ->where('id_jenjang', $id_jenjang)
                                ->first();
                        }
                    }

                    if ($penjurusanDetail && $kategoriToDetail && empty($id_kelas)) {
                        $id_kelas = $penjurusanDetail->kelas ? $penjurusanDetail->kelas->id : null;
                    }
                }
                if ($kategoriToDetail) {
                    $id_to_kategori = $kategoriToDetail->id;
                }

                $tryoutDetail->id_penjurusan         = $id_penjurusan;
                $tryoutDetail->id_jenjang    = $id_jenjang;
                $tryoutDetail->id_to_kategori    = $id_to_kategori;
                $tryoutDetail->save();
            }
        }
    }
}
