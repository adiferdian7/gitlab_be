<?php

namespace Database\Seeders;

use App\Models\Bank;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DataBank extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Bank::insert([
      [
        'nama_bank'           => 'BCA',
        'nama_pemilik'      => 'Rahardian Era',
        'nomor_rekening'      => '12345678900',
        'cabang'             => NULL,
        'created_at'        => Carbon::now()->toDateTimeString(),
      ],
      [
        'nama_bank'           => 'BRI',
        'nama_pemilik'      => 'Rahardian Era',
        'nomor_rekening'      => '12345678900',
        'cabang'             => NULL,
        'created_at'        => Carbon::now()->toDateTimeString(),
      ],
      [
        'nama_bank'           => 'BNI',
        'nama_pemilik'      => 'Rahardian Era',
        'nomor_rekening'      => '12345678900',
        'cabang'             => NULL,
        'created_at'        => Carbon::now()->toDateTimeString(),
      ],
      [
        'nama_bank'           => 'Mandiri',
        'nama_pemilik'      => 'Rahardian Era',
        'nomor_rekening'      => '12345678900',
        'cabang'             => NULL,
        'created_at'        => Carbon::now()->toDateTimeString(),
      ]
    ]);
  }
}
