<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKlastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // create klaster table
        Schema::create('klaster', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_rumpun')->constrained('rumpun', 'id');
            $table->foreignId('id_mapel')->constrained('mapels', 'id');
            $table->string('nilai');
            $table->timestamps();
            $table->softDeletes();
        });

        // modify prodi table
        Schema::table('program_studi', function (Blueprint $table) {
            $table->integer('id_rumpun')->unsigned()->nullable();
        });

        // modify mapel table
        Schema::table('mapels', function (Blueprint $table) {
            $table->string('kode')->nullable();
            // $table->string('kelompok')->nullable(); 
        });

         // modify tryouts table
         Schema::table('tryouts', function (Blueprint $table) {
            // $table->integer('id_kelas')->unsigned()->nullable();
            $table->integer('id_to_kategori')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //klaster
        Schema::dropIfExists('klaster');
        //program_studi
        Schema::table('program_studi', function (Blueprint $table) {
            $table->dropColumn('id_rumpun');
        });
        // mapels
        Schema::table('mapels', function (Blueprint $table) {
            $table->dropColumn('kode');
            // $table->dropColumn('kelompok');
        });
        // tryouts
        Schema::table('tryouts', function (Blueprint $table) {
            // $table->dropColumn('id_kelas');
            $table->dropColumn('id_to_kategori');
        });
    }
}
