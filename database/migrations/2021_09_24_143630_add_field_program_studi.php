<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldProgramStudi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('program_studi', function (Blueprint $table) {
            $table->longText('icon_prodi')->nullable();
            $table->string('passing_grade', 50)->nullable();
            $table->longText('alasan')->nullable();
            $table->text('prospek')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('program_studi', function (Blueprint $table) {
            
            $table->dropColumn('icon_prodi');
            $table->dropColumn('passing_grade');
            $table->dropColumn('alasan');
            $table->dropColumn('prospek');

        });
    }
}
