<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyProgramStudiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('program_studi', function (Blueprint $table) {
            $table->text('deskripsi')->nullable()->change();
            $table->integer('id_penjurusan')->unsigned()->nullable()->change();
            $table->integer('id_mapel')->unsigned()->nullable()->change();
            $table->string('kelompok')->nullable();
            $table->string('rumpun')->nullable();
            $table->string('portofolio')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('program_studi', function (Blueprint $table) {
            $table->text('deskripsi')->nullable(false)->change();
            $table->integer('id_penjurusan')->unsigned()->nullable(false)->change();
            $table->integer('id_mapel')->unsigned()->nullable(false)->change();
            $table->dropColumn('kelompok');
            $table->dropColumn('rumpun');
            $table->dropColumn('portofolio');
        });
    }
}
