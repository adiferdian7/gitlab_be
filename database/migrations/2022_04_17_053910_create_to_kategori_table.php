<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateToKategoriTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    // create tryOut kategori table
    Schema::create('to_kategori', function (Blueprint $table) {
      $table->id();
      $table->string('kategori');
      $table->string('kelompok')->nullable();
      $table->integer('id_penjurusan')->unsigned()->nullable();
      $table->string('penjurusan')->nullable();
      $table->integer('id_jenjang')->unsigned()->nullable();
      $table->string('jenjang')->nullable();
      $table->integer('id_kelas')->unsigned()->nullable();
      $table->string('kelas')->nullable();
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    //to_kategori
    Schema::dropIfExists('to_kategori');
  }
}
