<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInvoiceToTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaksis', function (Blueprint $table) {
            $table->string('kode',50)->after('id_produk');
        });
        Schema::table('tabel_xendits', function (Blueprint $table) {
            $table->longText('response');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaksis', function (Blueprint $table) {
            $table->dropColumn('kode');
        });
        Schema::table('tabel_xendits', function (Blueprint $table) {
            $table->dropColumn('response');
        });
    }
}
