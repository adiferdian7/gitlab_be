<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusToMbtiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_mbtis', function (Blueprint $table) {
            $table->enum('status', ['Aktif','Tidak Aktif'])->after('panduan')->default('Tidak Aktif');
            $table->enum('sertifikat', ['Pakai','Tidak Pakai'])->after('status')->default('Pakai');
            $table->integer('harga')->after('sertifikat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_mbtis', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('sertifikat');
            $table->dropColumn('harga');
        });
    }
}
