<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyStudentTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('students', function (Blueprint $table) {
      $table->string('info', 100)->nullable()->change();
      $table->text('foto')->nullable()->change();
      $table->integer('id_provinsi')->nullable()->change();
    });
    
    Schema::table('teachers', function (Blueprint $table) {
      $table->integer('id_provinsi')->nullable()->change();
      $table->string('alamat_lengkap', 255)->nullable()->change();
      $table->string('tempat_lahir', 50)->nullable()->change();
      $table->dropColumn('agama');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('students', function (Blueprint $table) {
      $table->string('info', 10)->nullable()->change();
      $table->string('foto')->nullable()->change();
      $table->integer('id_provinsi')->nullable()->change();
    });

    Schema::table('teachers', function (Blueprint $table) {
      $table->integer('id_provinsi')->nullable()->change();
      $table->string('alamat_lengkap', 50)->nullable()->change();
      $table->string('tempat_lahir', 10)->nullable()->change();
      $table->string('agama', 10)->nullable();
    });
  }
}
