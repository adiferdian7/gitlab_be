<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengaduansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengaduans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_user')->nullable()->constrained('users', 'id')->onDelete('cascade');
            $table->foreignId('id_admin')->nullable()->constrained('users', 'id')->onDelete('SET NULL');
            $table->string('kode')->unique();
            $table->string('subjek');
            $table->string('permasalahan');
            $table->enum('prioritas', ['Rendah', 'Sedang', 'Tinggi'])->default('Rendah');
            $table->enum('status', ['Buka', 'Ditutup'])->default('Buka');
            $table->dateTime('tgl_buka')->nullable();
            $table->dateTime('tgl_ditutup')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengaduans');
    }
}
