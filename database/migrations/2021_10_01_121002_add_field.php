<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('soal_pertanyaans', 'jawaban_pertanyaan')){
            Schema::table('soal_pertanyaans', function (Blueprint $table) {

                $table->dropColumn('jawaban_pertanyaan');
            });
        }
            Schema::table('soal_pertanyaans', function (Blueprint $table) {

                $table->json('jawaban_pertanyaan')->nullable();

            });


        if(Schema::hasColumn('produks', 'perhitungan')){
            Schema::table('produks', function (Blueprint $table) {
             $table->dropColumn('perhitungan');
            });
        }

        Schema::table('produks', function (Blueprint $table) {
            $table->json('perhitungan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
    }
}
