<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDocTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_docs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users', 'id')->onDelete('cascade');
            $table->string('doc_label')->nullable();
            $table->string('doc_type')->nullable();
            $table->string('doc_file')->nullable();
            $table->enum('doc_status', ['Pending', 'Approved', 'Rejected'])->nullable();
            $table->foreignId('admin_id')->nullable()->constrained('admins', 'id_admin')->onDelete('cascade');
            $table->timestamps();
        });

        // teachers
        Schema::table('teachers', function (Blueprint $table) {
            $table->string('pendidikan_terakhir')->change();
            $table->string('jurusan_pendidikan')->nullable();
            $table->string('status_pendidikan')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_docs');

        Schema::table('teachers', function (Blueprint $table) {
            $table->string('pendidikan_terakhir', 10)->change();
            $table->dropColumn('jurusan_pendidikan');
            $table->dropColumn('status_pendidikan');
        });
    }
}
