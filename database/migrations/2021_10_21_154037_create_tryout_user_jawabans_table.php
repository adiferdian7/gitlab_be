<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTryoutUserJawabansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tryout_user_jawabans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_tryout_user')->constrained('tryout_users')->onDelete('cascade');
            $table->foreignId('id_soal_tryout')->constrained('soal_tryouts');
            $table->foreignId('id_soal_pertanyaan')->constrained('soal_pertanyaans');
            $table->string('jawaban_user', 100)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tryout_user_jawabans');
    }
}
