<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TabelMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mapels', function (Blueprint $table) {
            $table->id();
            $table->string('nama_mapel');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('perguruan_tinggi', function (Blueprint $table) {
            $table->id();
            $table->string('nama_perguruan');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('program_studi', function (Blueprint $table) {
            $table->id();
            $table->string('nama_studi', 50);
            $table->string('akreditasi', 20);
            $table->text('deskripsi');
            $table->integer('id_penjurusan')->unsigned();
            $table->integer('id_mapel')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('program_studi_and_perguruan_tinggi', function (Blueprint $table) {
            $table->id();
            $table->integer('id_program_studi')->unsigned();
            $table->integer('id_perguruan_tinggi')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mapels');
        Schema::dropIfExists('perguruan_tinggi');
        Schema::dropIfExists('program_studi');
        Schema::dropIfExists('program_studi_and_perguruan_tinggi');
    }
}
