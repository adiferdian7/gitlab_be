<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TeacherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers', function (Blueprint $table) {
            $table->unsignedBigInteger('id_teacher');
            $table->foreign('id_teacher')->references('id')->on('users');
            $table->string('foto', 200)->nullable();
            $table->string('nama_lengkap', 50);
            $table->string('email', 50)->unique();
            $table->string('nomor_telephone', 14)->nullable();
            $table->string('alamat_lengkap', 50)->nullable();
            $table->string('tempat_lahir', 20)->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->string('agama', 10)->nullable();
            $table->string('pendidikan_terakhir', 10)->nullable();
            $table->boolean('verifikasi');
            $table->string('info', 20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
    }
}
