<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldTableRevision extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->string('foto')->nullable();
            $table->string('tempat_lahir')->nullable();
            $table->date('tgl_lahir')->nullable();
            $table->string('jenis_kelamin')->nullable();
            $table->string('alamat')->nullable();
            $table->tinyInteger('id_provinsi')->nullable();
            $table->unsignedInteger('id_kota')->nullable();
            $table->unsignedInteger('id_kecamatan')->nullable();
            $table->unsignedInteger('id_penjurusan')->nullable();
        });

        Schema::table('teachers', function (Blueprint $table) {
            $table->string('jenis_kelamin')->nullable();
            $table->tinyInteger('id_provinsi')->nullable();
            $table->unsignedInteger('id_kota')->nullable();
            $table->unsignedInteger('id_kecamatan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */ 
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->dropColumn('foto');
            $table->dropColumn('tempat_lahir');
            $table->dropColumn('tgl_lahir');
            $table->dropColumn('jenis_kelamin');
            $table->dropColumn('alamat');
            $table->dropColumn('id_provinsi');
            $table->dropColumn('id_kota');
            $table->dropColumn('id_kecamatan');
            $table->dropColumn('id_penjurusan');
        });

        Schema::table('teachers', function (Blueprint $table) {
            $table->dropColumn('jenis_kelamin')->nullable();
            $table->dropColumn('id_provinsi')->nullable();
            $table->dropColumn('id_kota')->nullable();
            $table->dropColumn('id_kecamatan')->nullable();
        });
    }
}
