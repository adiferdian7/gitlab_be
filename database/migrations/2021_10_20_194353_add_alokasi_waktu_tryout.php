<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAlokasiWaktuTryout extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('soal_tryouts', function (Blueprint $table) {
            $table->integer('jeda_waktu_antar_mapel')->unsigned()->nullable();
            $table->integer('alokasi_waktu_per_mapel')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('soal_tryouts', function (Blueprint $table) {
            $table->dropColumn('alokasi_waktu_per_mapel');
            $table->dropColumn('jeda_waktu_antar_mapel');
        });
    }
}
