<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMbtiJawabanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mbti_jawabans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_mbti')->constrained('tb_mbtis', 'id')->onDelete('cascade');
            $table->foreignId('id_user')->constrained('users', 'id')->onDelete('cascade');
            $table->foreignId('id_kepribadian')->nullable()->constrained('kepribadians', 'id')->onDelete('SET NULL');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('mbti_jawaban_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_mbti_jawaban')->constrained('mbti_jawabans', 'id')->onDelete('cascade');
            $table->foreignId('id_mbti_pertanyaan')->constrained('mbti_pertanyaans', 'id')->onDelete('cascade');
            $table->string('jawaban_user', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mbti_jawaban_details');
        Schema::dropIfExists('mbti_jawabans');
    }
}
