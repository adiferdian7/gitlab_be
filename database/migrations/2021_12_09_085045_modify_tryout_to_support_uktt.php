<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ModifyTryoutToSupportUktt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tryouts', function (Blueprint $table) {
            $table->integer('id_jenjang')->nullable();
        });
        Schema::table('produks', function (Blueprint $table) {
            $table->integer('id_uktt')->nullable(); // requirement bisa ambil ujian harus lulus ujian lain dulu atau tidak
            $table->string('uktt_level', 255)->nullable();
            $table->integer('uktt_nilai_minimal')->nullable();
            $table->foreignId('uktt_id_level')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropColumns('tryouts', [
             'id_jenjang',
        ]);
        Schema::dropColumns('produks', [
            'id_uktt', 
            'uktt_level', 
            'uktt_nilai_minimal',
            'uktt_id_level',
        ]);
    }
}
