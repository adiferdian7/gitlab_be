<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKelasRelatedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // levels
        Schema::create('levels', function (Blueprint $table) {
            $table->id();
            $table->string('nama_level');
            $table->text('deskripsi_level')->nullable();
            $table->integer('honor_level');
            $table->integer('sesi_honor_level');
            $table->integer('id_uktt')->unsigned()->nullable();
            $table->integer('minimal_rate_mengajar')->default(0);
            $table->integer('minimal_total_mengajar')->default(0);
            $table->integer('maksimal_waktu_mengajar')->default(0);
            $table->timestamps();
        });

        // teachers
        Schema::table('teachers', function (Blueprint $table) {
            $table->foreignId('id_level')->nullable()->constrained('levels')->onDelete('SET NULL');
            $table->string('nama_level')->nullable();
            $table->float('rate_mengajar')->default(0);
        });

        // kelas kursus
        Schema::create('kursus', function (Blueprint $table) {
            $table->id();
            $table->string('nama_kursus');
            $table->text('deskripsi_kursus');
            $table->string('file_kursus', 255)->nullable();
            $table->string('video_kursus');
            $table->string('sampul_kursus')->nullable();
            $table->foreignId('id_tentor')->constrained('teachers', 'id_teacher')->onDelete('cascade');
            $table->foreignId('id_mapel')->nullable()->constrained('mapels')->onDelete('SET NULL');
            $table->foreignId('id_jenjang')->nullable()->constrained('jenjangs')->onDelete('SET NULL');
            $table->foreignId('id_kelas')->nullable()->constrained('kelas')->onDelete('SET NULL');
            $table->foreignId('id_penjurusan')->nullable()->constrained('penjurusans')->onDelete('SET NULL');
            $table->integer('harga_kursus');
            $table->integer('batas_peserta')->nullable();
            $table->tinyInteger('menerima_peserta')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });

        // jadwal dari kelas kursus
        Schema::create('kursus_jadwal', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_kursus')->constrained('kursus')->onDelete('cascade');
            $table->string('hari_jadwal');
            $table->time('jam_mulai_jadwal')->nullable();
            $table->time('jam_akhir_jadwal')->nullable();
            $table->tinyInteger('is_tutup')->default(0);
            $table->timestamps();
        });

        // siswa dari kelas kursus
        Schema::create('kursus_siswa', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_kursus')->nullable()->constrained('kursus')->onDelete('cascade');
            $table->foreignId('id_siswa')->constrained('students', 'id_siswa')->onDelete('cascade');
            $table->enum('status_dikelas', ['Pending', 'Bergabung', 'Keluar'])->default('Pending');
            $table->dateTime('tanggal_gabung')->nullable();
            $table->dateTime('tanggal_keluar')->nullable();
            $table->timestamps();
        });

        // materi dari kelas kursus
        Schema::create('kursus_materi', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_kursus')->constrained('kursus')->onDelete('cascade');
            $table->string('judul_materi');
            $table->text('deskripsi_materi');
            $table->string('link_file_materi', 255);
            $table->string('link_video_materi', 255);
            $table->tinyInteger('is_gratis')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // teachers
        Schema::table('teachers', function (Blueprint $table) {
           $table->dropForeign(['id_level']);
           $table->dropColumn('id_level');
           $table->dropColumn('nama_level');
           $table->dropColumn('rate_mengajar');
        });

        // levels
        Schema::dropIfExists('levels');

        // kelas related
        Schema::dropIfExists('kursus_jadwal');
        Schema::dropIfExists('kursus_siswa');
        Schema::dropIfExists('kursus_materi');
        Schema::dropIfExists('kursus');
    }
}
