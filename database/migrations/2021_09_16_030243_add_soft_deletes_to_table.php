<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSoftDeletesToTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('students', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('teachers', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('orang_tua', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('super_admins', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('admins', function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('students', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('teachers', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('orang_tua', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('super_admins', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('admins', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
