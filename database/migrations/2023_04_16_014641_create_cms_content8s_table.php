<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCmsContent8sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_content8s', function (Blueprint $table) {
            $table->id();
            $table->string('judul')->nullable();
            $table->integer('id_content')->unsigned();
            $table->text('text');
            $table->string('nama')->nullable();
            $table->string('foto')->nullable();
            $table->string('jurusan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_content8s');
    }
}
