<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AllTableMasterKelas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jenjangs', function (Blueprint $table) {
            $table->id();
            $table->string('nama_jenjang');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('kelas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_jenjang')->references('id')->on('jenjangs');
            $table->string('nama_kelas');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('penjurusans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_kelas')->references('id')->on('kelas');
            $table->string('nama_penjurusan');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jenjangs');
        Schema::dropIfExists('kelas');
        Schema::dropIfExists('penjurusans');
    }
}
