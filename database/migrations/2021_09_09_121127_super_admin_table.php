<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SuperAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('super_admins', function (Blueprint $table) {
            $table->unsignedBigInteger('id_super_admin');
            $table->foreign('id_super_admin')->references('id')->on('users');
            $table->string('username', 15);
            $table->string('nama_lengkap', 50);
            $table->string('email', 50)->unique();
            $table->string('nomor_telephone', 14);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('super_admins');
    }
}
