<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableModulMbti extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('dimensis', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->string('psikologi_1', 30);
            $table->string('psikologi_2',30);
            $table->longText('deskripsi')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('kepribadians', function (Blueprint $table) {
            $table->id();
            $table->string('nama_singkat', 40);
            $table->text('nama_panjang');
            $table->text('jenis');
            $table->longText('deskripsi');
            $table->longText('saran');
            $table->timestamps();
            $table->softDeletes();
        });

         Schema::create('tb_mbtis', function (Blueprint $table) {
            $table->id();
            $table->string('judul');
            $table->longText('deskripsi');
            $table->longText('panduan');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('mbti_dimensis', function (Blueprint $table) {
            $table->id();
            $table->integer('id_mbti')->unsigned()->nullable();
            $table->integer('id_dimensi')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('mbti_pertanyaans', function (Blueprint $table) {
            $table->id();
            $table->integer('id_mbti_dimensi')->unsigned()->nullable();
            $table->longText('pertanyaan');
            $table->json('opsi')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dimensis');
        Schema::dropIfExists('kepribadians');
        Schema::dropIfExists('tb_mbtis');
        Schema::dropIfExists('mbti_dimensis');
        Schema::dropIfExists('mbti_pertanyaans');

    }
}
