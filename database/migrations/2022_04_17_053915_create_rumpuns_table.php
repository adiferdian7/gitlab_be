<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRumpunsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // create rumpun table
        Schema::create('rumpun', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            // $table->string('kelompok');
            $table->integer('id_to_kategori')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //rumpun
        Schema::dropIfExists('rumpun');
    }
}
