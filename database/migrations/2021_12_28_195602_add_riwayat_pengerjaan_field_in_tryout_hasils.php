<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRiwayatPengerjaanFieldInTryoutHasils extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tryout_hasils', function (Blueprint $table) {
            $table->longText('riwayat_pengerjaan')->nullable()->after('riwayat_perhitungan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tryout_hasils', function (Blueprint $table) {
            $table->dropColumn('riwayat_pengerjaan');
        });
    }
}
