<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldSoalPertanyaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('produks', function (Blueprint $table) {
            $table->longText('perhitungan')->nullable();
        });

        Schema::table('soal_pertanyaans', function (Blueprint $table) {
            $table->longText('jawaban_pertanyaan')->nullable();
        });
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('produks', function (Blueprint $table) {
            $table->dropColumn('perhitungan');
        });

        Schema::table('soal_pertanyaans', function (Blueprint $table) {
            $table->dropColumn('jawaban_pertanyaan');
        });
    }
}
