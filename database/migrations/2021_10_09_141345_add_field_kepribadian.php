<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldKepribadian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kepribadians', function (Blueprint $table) {

            $table->longText('profesi')->nullable();
            $table->longText('partner')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kepribadians', function (Blueprint $table) {

            $table->dropColumn('profesi');
            $table->dropColumn('partner');

        });
    }
}
