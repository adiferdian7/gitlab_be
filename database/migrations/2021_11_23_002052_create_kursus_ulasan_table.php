<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateKursusUlasanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // modify kelas kursus
        Schema::table('kursus', function (Blueprint $table) {
            $table->boolean('status_verifikasi')->default(false)->after('id');
        });

        // modify kursus_siswa
        Schema::table('kursus_siswa', function (Blueprint $table) {
            $table->string('referensi')->nullable()->after('id');
            // DB::getDoctrineSchemaManager()->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
            DB::statement("ALTER TABLE kursus_siswa MODIFY status_dikelas ENUM('Pending', 'Bergabung', 'Ditolak', 'Ditolak & Sudah Bergabung Lagi', 'Menunggu Konfirmasi Selesai', 'Sesi Selesai') NOT NULL DEFAULT 'Pending'");
            $table->string('alasan_penolakan')->nullable()->after('status_dikelas');
            $table->softDeletes();
        });

        // create kursus_ulasan
        Schema::create('kursus_ulasan', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_kursus')->constrained('kursus')->onDelete('cascade');
            $table->foreignId('id_siswa')->constrained('students', 'id_siswa')->onDelete('cascade');
            $table->enum('nilai', ['1', '2', '3', '4', '5'])->default('3');
            $table->text('ulasan')->nullable();
            $table->enum('privasi', ['Publik', 'Anonim'])->default('Publik');
            $table->text('balasan')->nullable();
            $table->dateTime('tanggal_balasan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // rollback
        Schema::table('kursus', function (Blueprint $table) {
            $table->dropColumn('status_verifikasi');
        });

        Schema::table('kursus_siswa', function (Blueprint $table) {
            $table->dropColumn('referensi');
            $table->dropSoftDeletes();
            // $table->enum('status_dikelas', ['Pending', 'Bergabung', 'Keluar'])->default('Pending')->change();
            DB::statement("ALTER TABLE kursus_siswa MODIFY status_dikelas ENUM('Pending', 'Bergabung', 'Keluar') NOT NULL DEFAULT 'Pending'");
        });


        Schema::dropIfExists('kursus_ulasan');
    }
}
