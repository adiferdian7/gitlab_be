<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->bigIncrements('notification_id');
            $table->bigInteger('notification_recipient_id')->nullable(); // id_user
            $table->tinyInteger('notification_recipient_type'); // 0: Superadmin, 1: Admin, 2: so on
            $table->tinyInteger('notification_type'); // 0: Transaction, 1: so on
            $table->string('notification_title', 255);
            $table->text('notification_body')->nullable();
            $table->longText('notification_data')->nullable(); // history data
            $table->boolean('notification_open')->default(false); // status
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
