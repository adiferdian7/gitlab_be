<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramStudiBindMapelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_studi_bind_mapel', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_program_studi')->constrained('program_studi', 'id')->onDelete('cascade');
            $table->foreignId('id_mapel')->constrained('mapels', 'id')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_studi_bind_mapel');
    }
}
