<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreatePenarikansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penarikans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_tentor')->constrained('teachers', 'id_teacher');
            $table->double('nominal_penarikan');
            $table->enum('status_penarikan', ['Pending', 'Proses', 'Selesai'])->default('Pending');
            $table->string('bukti_transfer', 255)->nullable();
            $table->string('nama_rekening', 255);
            $table->string('bank_rekening', 255);
            $table->string('nomor_rekening', 255);
            $table->timestamps();
        });

        DB::statement("ALTER TABLE teachers MODIFY COLUMN foto VARCHAR(255) AFTER nomor_telephone");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penarikans');

        DB::statement("ALTER TABLE teachers MODIFY COLUMN foto VARCHAR(255) AFTER id_teacher");
    }
}
