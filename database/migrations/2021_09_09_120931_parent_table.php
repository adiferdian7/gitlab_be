<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ParentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orang_tua', function (Blueprint $table) {
            $table->unsignedBigInteger('id_orang_tua');
            $table->unsignedBigInteger('id_user');
            $table->foreign('id_orang_tua')->references('id_siswa')->on('students');
            $table->foreign('id_user')->references('id')->on('users');
            $table->string('nama_lengkap', 50);
            $table->string('email', 50)->unique();
            $table->string('nomor_telephone', 14);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('orang_tua');
    }
}
