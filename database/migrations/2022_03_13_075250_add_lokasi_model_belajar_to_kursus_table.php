<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLokasiModelBelajarToKursusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kursus', function (Blueprint $table) {
            $table->string('model_belajar')->default('Online');
            $table->string('alamat')->nullable();
            $table->string('id_provinsi')->nullable();
            $table->string('nama_provinsi')->nullable();
            $table->string('id_kota')->nullable();
            $table->string('nama_kota')->nullable();
            $table->string('id_kecamatan')->nullable();
            $table->string('nama_kecamatan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kursus', function (Blueprint $table) {
            $table->dropColumn([
                'model_belajar',
                'alamat',
                'id_provinsi',
                'nama_provinsi',
                'id_kota',
                'nama_kota',
                'id_kecamatan',
                'nama_kecamatan',
            ]);
        });
    }
}
