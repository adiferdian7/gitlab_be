<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdditionalAddressFieldToTeachersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('teachers', function (Blueprint $table) {
      // address
      $table->string('nama_provinsi', 255)->nullable()->after('id_provinsi');
      $table->string('nama_kota', 255)->nullable()->after('id_kota');
      $table->string('nama_kecamatan', 255)->nullable()->after('id_kecamatan');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('teachers', function (Blueprint $table) {
      $table->dropColumn(['nama_provinsi', 'nama_kota', 'nama_kecamatan', 'nama_penjurusan']);
    });
  }
}
