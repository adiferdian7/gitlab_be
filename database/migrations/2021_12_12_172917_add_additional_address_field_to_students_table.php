<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdditionalAddressFieldToStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {

            // address
            $table->string('nama_provinsi', 255)->nullable()->after('id_provinsi');
            $table->string('nama_kota', 255)->nullable()->after('id_kota');
            $table->string('nama_kecamatan', 255)->nullable()->after('id_kecamatan');
            $table->string('nama_penjurusan', 255)->nullable()->after('id_penjurusan');
            
            // prodi
            $table->foreignId('id_prodi_bind_perguruan')->nullable()->constrained('program_studi_and_perguruan_tinggi')->onDelete('RESTRICT');
            $table->foreignId('id_prodi_bind_perguruan_2')->nullable()->constrained('program_studi_and_perguruan_tinggi')->onDelete('RESTRICT');

            // profile
            $table->string('nama_sekolah', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->dropColumn(['nama_provinsi', 'nama_kota', 'nama_kecamatan', 'nama_penjurusan', 'nama_sekolah']);

            $table->dropConstrainedForeignId('id_prodi_bind_perguruan');
            $table->dropConstrainedForeignId('id_prodi_bind_perguruan_2');
        });
    }
}
