<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldAkreditasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('program_studi_and_perguruan_tinggi', function (Blueprint $table) {
            $table->string('akreditasi_program_studi')->nullable();
        });

        Schema::table('program_studi', function (Blueprint $table) {
            $table->dropColumn('akreditasi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('perguruan_tinggi', function (Blueprint $table) {
            $table->dropColumn('akreditasi_program_studi');
        });

         Schema::table('program_studi', function (Blueprint $table) {
            $table->string('akreditasi', 20);
        });
    }
}
