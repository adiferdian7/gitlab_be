<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHargaToTableTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaksis', function (Blueprint $table) {
            $table->integer('harga_produk')->nullable();
            $table->integer('kode_unik')->nullable();
            $table->integer('biaya_adm')->nullable();
            $table->integer('total_harga')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaksis', function (Blueprint $table) {
            $table->dropColumn('harga_produk');
            $table->dropColumn('kode_unik');
            $table->dropColumn('biaya_adm');
            $table->dropColumn('total_harga');
        });
    }
}
