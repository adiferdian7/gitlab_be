<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyRumpunTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rumpun', function (Blueprint $table) {
            $table->dropColumn('id_to_kategori');
            $table->string('kelompok')->default('SAINTEK');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rumpun', function (Blueprint $table) {
            $table->integer('id_to_kategori')->unsigned()->nullable();
            $table->dropColumn('kelompok');
        });
    }
}
