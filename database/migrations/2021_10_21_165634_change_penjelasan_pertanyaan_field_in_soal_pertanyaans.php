<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangePenjelasanPertanyaanFieldInSoalPertanyaans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('soal_pertanyaans', function (Blueprint $table) {
            $table->longText('penjelasan_pertanyaan')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('soal_pertanyaans', function (Blueprint $table) {
            $table->string('penjelasan_pertanyaan')->nullable()->change();
        });
    }
}
