<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldProgramStudiAndPerguruanTinggi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('program_studi_and_perguruan_tinggi', function (Blueprint $table) {
            $table->string('passing_grade_prodi', 50)->nullable();
        });

        Schema::table('program_studi', function (Blueprint $table) {
            $table->dropColumn('passing_grade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('program_studi', function (Blueprint $table) {
            $table->string('passing_grade', 50)->nullable();
        });

        Schema::table('program_studi_and_perguruan_tinggi', function (Blueprint $table) {
            $table->dropColumn('passing_grade_prodi');

        });
    }
}
