<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RevisionTableTryout extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('soal_tryouts', function (Blueprint $table) {
            $table->integer('alokasi_waktu_per_mapel')->unsigned()->nullable();
            $table->integer('jeda_waktu_antar_mapel')->unsigned()->nullable();
        });

        Schema::table('produks', function (Blueprint $table) {
            $table->dropColumn('alokasi_waktu_perkelompok');
            $table->dropColumn('jeda_waktu_antarkelompok');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('soal_tryouts', function (Blueprint $table) {
            $table->dropColumn('alokasi_waktu_per_mapel');
            $table->dropColumn('jeda_waktu_antar_mapel');
        });
    }
}
