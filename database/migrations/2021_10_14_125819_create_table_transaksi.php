<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksis', function (Blueprint $table) {
            $table->id();
            $table->integer('id_user')->unsigned();
            $table->integer('id_produk')->unsigned();
            $table->integer('id_bank')->unsigned()->nullable();
            $table->enum('status', ['Menunggu Pembayaran', 'Menunggu Verifikasi', 'Sudah Diverifikasi', 'Ditolak', 'Kadaluarsa', 'Dibatalkan'])->index();
            $table->dateTime('tanggal_transaksi');
            $table->dateTime('tanggal_konfirmasi')->nullable();
            $table->text('alasan_penolakan')->nullable();
            $table->text('alasan_pembatalan')->nullable();
            $table->enum('tipe', ['Bank Transfer', 'Pihak Ketiga']);
            $table->longText('bukti_pembayaran')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksis');
    }
}
