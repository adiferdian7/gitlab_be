<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableTryout extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tryouts', function (Blueprint $table) {
            $table->id();
            $table->string('judul');
            $table->text('deskripsi');
            $table->string('kategori', 30);
            $table->string('jenis_soal', 30)->nullable();
            $table->string('kelompok_soal', 30)->nullable();
            $table->string('template_soal')->nullable();
            $table->integer('id_penjurusan')->unsigned()->nullable();
            $table->text('panduan_pengerjaan')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('soal_tryouts',  function (Blueprint $table) {
            $table->id();
            $table->integer('id_tryout')->unsigned();
            $table->string('jenis_soal', 30)->nullable();
            $table->string('kelompok_soal', 30)->nullable();
            $table->integer('id_mapel')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('soal_pertanyaans',  function (Blueprint $table) {
            $table->id();
            $table->integer('id_soal_tryout')->unsigned();
            $table->json('bab_mapel')->nullable();
            $table->string('penjelasan_pertanyaan')->nullable();
            $table->string('template_pertanyaan');
            $table->longText('soal')->nullable();
            $table->longText('gambar')->nullable();
            $table->json('opsi_pertanyaan')->nullable();
            $table->longText('pembahasan_pertanyaan')->nullable();
            $table->integer('parent_soal_pertanyaan')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('produks',  function (Blueprint $table) {
            $table->id();
            $table->string('nama_produk', 50);
            $table->string('kategori_produk', 30);
            $table->string('jenis_produk', 30)->nullable();
            $table->double('harga_produk')->nullable();
            $table->enum('pakai_sertifikat', ['Ya', 'Tidak'])->nullable();
            $table->enum('status_produk', ['Aktif', 'Tidak'])->nullable();
            $table->integer('alokasi_waktu_perkelompok')->unsigned()->nullable();
            $table->integer('jeda_waktu_antarkelompok')->unsigned()->nullable();
            $table->string('tipe_paket', 30)->nullable();
            $table->dateTime('tanggal_mulai')->nullable();
            $table->dateTime('tanggal_berakhir')->nullable();
            $table->enum('pakai_perankingan', ['Ya', 'Tidak'])->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('produk_pakets',  function (Blueprint $table) {
            $table->id();
            $table->integer('id_produk')->unsigned()->nullable();
            $table->integer('id_tryout')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tryouts');
        Schema::dropIfExists('soal_tryouts');
        Schema::dropIfExists('soal_pertanyaans');
        Schema::dropIfExists('produks');
        Schema::dropIfExists('produk_pakets');

    }
}
