@component('mail::message')
# Halo, {{$mailData['username']}} !

Email anda telah di daftarkan sebagai Administrator kami. <br>

Gunakan username <strong>{{ $mailData['username'] }}</strong> dan password <strong>{{ $mailData['password'] }}</strong>, saat Login.

Sebelum itu, silakan verifikasi email Anda terlebih dahulu.

@component('mail::button', ['url' => 'http://localhost:8000/api/verifikasi/'. $mailData['token']])
Verifikasi
@endcomponent

Thanks, UjiAja.com<br>
@endcomponent