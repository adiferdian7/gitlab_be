@component('mail::message')
# Terima Kasih Telah Mendaftar

Silakan Verifikasi email anda

@component('mail::button', ['url' => 'http://localhost:8000/api/verifikasi/'. $mailData['token']])
Verifikasi
@endcomponent

Thanks, UjiAja.com<br>
@endcomponent