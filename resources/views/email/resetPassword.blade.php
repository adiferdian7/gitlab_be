@component('mail::message')
# Reset Password

Silakan klik button berikut

@component('mail::button', ['url' => $mailData['url']])
Verifikasi
@endcomponent

Thanks, UjiAja.com<br>
@endcomponent