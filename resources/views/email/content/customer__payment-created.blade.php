@extends('email.template')

@section('content')
    <table id="u_content_text_1" style="font-family:'Raleway',sans-serif;" role="presentation" cellpadding="0"
        cellspacing="0" width="100%" border="0">
        <tbody>
            <tr>
                <td class="v-container-padding-padding"
                    style="overflow-wrap:break-word;word-break:break-word;padding:10px 20px;font-family:'Raleway',sans-serif;"
                    align="left">

                    <div class="v-text-align"
                        style="color: #132f40; line-height: 140%; text-align: left; word-wrap: break-word;">
                        <p style="line-height: 140%; font-size: 14px;"><span style="font-family: Rubik, sans-serif;"><span
                                    style="font-size: 16px; line-height: 22.4px;">Halo,
                                    <strong>{{ $toName }}</strong>.
                                    {!! $content['title'] !!}</span></span></p>
                    </div>

                </td>
            </tr>
        </tbody>
    </table>

    <table style="font-family:'Raleway',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%"
        border="0">
        <tbody>
            <tr>
                <td class="v-container-padding-padding"
                    style="overflow-wrap:break-word;word-break:break-word;padding:10px 20px;font-family:'Raleway',sans-serif;"
                    align="left">

                    <div class="v-text-align"
                        style="color: #333333; line-height: 180%; text-align: left; word-wrap: break-word;">
                        <p style="font-size: 14px; line-height: 180%;"><span
                                style="font-family: Raleway, sans-serif; font-size: 14px; line-height: 25.2px;">{!! $content['body'] !!}</span>
                        </p>
                    </div>

                </td>
            </tr>
        </tbody>
    </table>

    {{-- <table style="font-family:'Raleway',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%"
        border="0">
        <tbody>
            <tr>
                <td>
                    <h2 class="title">Yuk, selesaikan pembayaranmu!</h2>
                    <p>Terima kasih sudah mempercayakan Uji Aja untuk memenuhi kebutuhan belajarmu!</p>
                </td>
            </tr>
        </tbody>
    </table> --}}

    <table style="font-family:'Raleway',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%"
        border="0">
        <tbody>
            <tr>
                <td class="v-container-padding-padding"
                    style="overflow-wrap:break-word;word-break:break-word;padding:10px 20px 15px;font-family:'Raleway',sans-serif; width: 50%"
                    align="left">

                    <div class="v-text-align"
                        style="color: #333333; line-height: 180%; text-align: left; word-wrap: break-word;">
                        @if ($data['produk'] != null || $data['mbti'] != null)
                            <div class="p-0" style="width: 100%">
                                <p class="mb-1" style="color: #9490A4;">Produk</p>
                                @if ($data['produk'] != null)
                                    <p class="">{{ $data['produk']['nama_produk'] }}</p>
                                @elseif ($data['mbti'] != null)
                                    <p class="">{{ $data['mbti']['judul'] }}</p>
                                @endif
                                <p class="mb-1" style="color: #9490A4;">Batas Waktu Pembayaran</p>
                                @if ($data['batas_pembayaran'] != null)
                                    <p>{{ date('l, d F Y HH:mm', strtotime($data['batas_pembayaran'])) }} WIB</p>
                                @else
                                    <p>{{ date('l, d F Y HH:mm', strtotime($data['expiry_date'])) }} WIB</p>
                                @endif
                                <p class="mb-1" style="color: #9490A4;">No.Invoice</p>
                                <p class="">{{ $data['kode'] }}</p>
                            </div>
                        @elseif ($data['kursus'] != null)
                            <div class="p-0" style="width: 100%">
                                <p class="mb-1" style="color: #9490A4;">Produk</p>
                                <p class="">{{ $data['kursus']['nama_kursus'] }}</p>
                                <p class="mb-1" style="color: #9490A4;">Batas Waktu Pembayaran</p>
                                @if ($data['batas_pembayaran'] != null)
                                    <p>{{ date('l, d F Y HH:mm', strtotime($data['batas_pembayaran'])) }} WIB</p>
                                @else
                                    <p>{{ date('l, d F Y HH:mm', strtotime($data['expiry_date'])) }} WIB</p>
                                @endif
                                <p class="mb-1" style="color: #9490A4;">No. Invoice</p>
                                <p class="">{{ $data['kode'] }}</p>
                            </div>
                        @endif
                    </div>
                </td>
                <td class="v-container-padding-padding"
                    style="overflow-wrap:break-word;word-break:break-word;padding:10px 20px 15px;font-family:'Raleway',sans-serif; width: 50%"
                    align="left">
                    <div class="v-text-align"
                        style="color: #333333; line-height: 180%; text-align: left; word-wrap: break-word;">
                        @if ($data['produk'] != null || $data['mbti'] != null || $data['kursus'] != null)
                            <div style="width: 100%">
                                <p class="mb-1" style="color: #9490A4;">Total Pembayaran</p>
                                @if ($data['jenis_transaksi'] == 'Bonus MBTI')
                                    <p class="">
                                        Rp {{ number_format($data['harga_produk'], 0, ',', '.') }}
                                    </p>
                                @else
                                    <p class="">
                                        Rp {{ number_format($data['total_harga'], 0, ',', '.') }}
                                    </p>
                                @endif
                                <p class="mb-1" style="color: #9490A4;">Metode Pembayaran</p>
                                @if ($data['tipe'] == 'Bank Transfer')
                                    <p class="">
                                        Bank Transfer - {{ $data['bank']['nama_bank'] }}
                                    </p>
                                @endif
                                @if ($data['tipe'] == 'Pihak Ketiga')
                                    <p class="">
                                        Pembayaran Instan
                                    </p>
                                @endif
                                <p class="mb-1" style="color: #9490A4;">Status Pembayaran</p>
                                <p class="">{{ $data['status'] }}</p>
                            </div>
                        @endif
                    </div>
                </td>
            </tr>
        </tbody>
    </table>

    @if ($content['paymentMethod'] == 'Bank Transfer')
        <table style="font-family:'Raleway',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%"
            border="0">
            <tbody>
                <tr>
                    <td class="v-container-padding-padding"
                        style="overflow-wrap:break-word;word-break:break-word;padding:10px 20px 15px;font-family:'Raleway',sans-serif;"
                        align="left">

                        <div class="v-text-align"
                            style="color: #333333; line-height: 180%; text-align: left; word-wrap: break-word;">
                            @if ($data['jenis_transaksi'] == 'Bonus MBTI')
                            <h5>Harap Transfer sejumlah <b>Rp {{ number_format($data['harga_produk'], 0, ',', '.') }}</b> ke nomor rekening dibawah :</h5>
                            @else
                            <h5>Harap Transfer sejumlah <b>Rp {{ number_format($data['total_harga'], 0, ',', '.') }}</b> ke nomor rekening dibawah :</h5>
                            @endif
                            <p style="font-size: 14px; line-height: 180%;"><span
                                    style="font-family: Raleway, sans-serif; font-size: 16px; line-height: 25.2px;">
                                    {{ $data['bank']['nama_bank'] }} - {{ $data['bank']['nomor_rekening'] }} - a.n {{ $data['bank']['nama_pemilik'] }}
                                </span>
                            </p>
                        </div>

                    </td>
                </tr>
            </tbody>
        </table>
    @endif

    <table style="font-family:'Raleway',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%"
        border="0">
        <tbody>
            <tr>
                <td class="v-container-padding-padding"
                    style="overflow-wrap:break-word;word-break:break-word;padding:10px 20px 15px;font-family:'Raleway',sans-serif;"
                    align="left">

                    <div class="v-text-align"
                        style="color: #333333; line-height: 180%; text-align: left; word-wrap: break-word;">
                        <p style="font-size: 14px; line-height: 180%;"><span
                                style="font-family: Raleway, sans-serif; font-size: 14px; line-height: 25.2px;">{{ $content['body2'] }}</span>
                        </p>
                    </div>

                </td>
            </tr>
        </tbody>
    </table>

    @if ($content['paymentMethod'] == 'Bank Transfer')
        <table id="u_content_button_1" style="font-family:'Raleway',sans-serif;" role="presentation" cellpadding="0"
            cellspacing="0" width="100%" border="0">
            <tbody>
                <tr>
                    <td class="v-container-padding-padding"
                        style="overflow-wrap:break-word;word-break:break-word;padding:10px;font-family:'Raleway',sans-serif;"
                        align="left">

                        <div class="v-text-align" align="center">
                            <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;font-family:'Raleway',sans-serif;"><tr><td class="v-text-align" style="font-family:'Raleway',sans-serif;" align="center"><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="" style="height:41px; v-text-anchor:middle; width:214px;" arcsize="122%" stroke="f" fillcolor="#b0a6ef"><w:anchorlock/><center style="color:#FFFFFF;font-family:'Raleway',sans-serif;"><![endif]-->
                            <a href="{{ $content['frontUrl'] }}" target="_blank"
                                style="box-sizing: border-box;display: inline-block;font-family:'Raleway',sans-serif;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #FFFFFF; background-color: #b0a6ef; border-radius: 50px;-webkit-border-radius: 50px; -moz-border-radius: 50px; width:41%; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word; mso-border-alt: none;border-top-width: 2px; border-top-style: solid; border-left-width: 2px; border-left-style: solid; border-right-width: 2px; border-right-style: solid; border-bottom-width: 2px; border-bottom-style: solid;">
                                <span style="display:block;padding:10px;line-height:150%;"><strong>Unggah Bukti
                                        Transfer</strong></span>
                            </a>
                            <!--[if mso]></center></v:roundrect></td></tr></table><![endif]-->
                        </div>

                    </td>
                </tr>
            </tbody>
        </table>
    @elseif ($content['paymentMethod'] == 'Pihak Ketiga')
        <table id="u_content_button_1" style="font-family:'Raleway',sans-serif;" role="presentation" cellpadding="0"
            cellspacing="0" width="100%" border="0">
            <tbody>
                <tr>
                    <td class="v-container-padding-padding"
                        style="overflow-wrap:break-word;word-break:break-word;padding:10px;font-family:'Raleway',sans-serif;"
                        align="left">

                        <div class="v-text-align" align="center">
                            <!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;font-family:'Raleway',sans-serif;"><tr><td class="v-text-align" style="font-family:'Raleway',sans-serif;" align="center"><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="" style="height:41px; v-text-anchor:middle; width:214px;" arcsize="122%" stroke="f" fillcolor="#b0a6ef"><w:anchorlock/><center style="color:#FFFFFF;font-family:'Raleway',sans-serif;"><![endif]-->
                            <a href="{{ isset($data['xendit']['invoice_url']) && !empty($data['xendit']['invoice_url'])? $data['xendit']['invoice_url']: $content['frontUrl'] }}"
                                target="_blank"
                                style="box-sizing: border-box;display: inline-block;font-family:'Raleway',sans-serif;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;color: #FFFFFF; background-color: #b0a6ef; border-radius: 50px;-webkit-border-radius: 50px; -moz-border-radius: 50px; width:41%; max-width:100%; overflow-wrap: break-word; word-break: break-word; word-wrap:break-word; mso-border-alt: none;border-top-width: 2px; border-top-style: solid; border-left-width: 2px; border-left-style: solid; border-right-width: 2px; border-right-style: solid; border-bottom-width: 2px; border-bottom-style: solid;">
                                <span style="display:block;padding:10px;line-height:150%;"><strong>Selesaikan
                                        Pembayaran</strong></span>
                            </a>
                            <!--[if mso]></center></v:roundrect></td></tr></table><![endif]-->
                        </div>

                    </td>
                </tr>
            </tbody>
        </table>
    @endif

    {{-- <div>Terima kasih sudah mempercayakan Uji Aja untuk memenuhi kebutuhan belajarmu!</div> --}}
@endsection
