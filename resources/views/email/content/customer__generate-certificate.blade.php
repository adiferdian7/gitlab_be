@extends('email.template')

@section('content')
    <table id="u_content_text_1" style="font-family:'Raleway',sans-serif;" role="presentation" cellpadding="0"
        cellspacing="0" width="100%" border="0">
        <tbody>
            <tr>
                <td class="v-container-padding-padding"
                    style="overflow-wrap:break-word;word-break:break-word;padding:10px 20px;font-family:'Raleway',sans-serif;"
                    align="left">

                    <div class="v-text-align"
                        style="color: #132f40; line-height: 140%; text-align: left; word-wrap: break-word;">
                        <p style="line-height: 140%; font-size: 14px;"><span style="font-family: Rubik, sans-serif;"><span
                                    style="font-size: 16px; line-height: 22.4px;">Halo,
                                    <strong>{{ $toName }}</strong>.
                                    {!! $content['title'] !!}</span></span></p>
                    </div>

                </td>
            </tr>
        </tbody>
    </table>

    <table style="font-family:'Raleway',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%"
        border="0">
        <tbody>
            <tr>
                <td class="v-container-padding-padding"
                    style="overflow-wrap:break-word;word-break:break-word;padding:10px 20px;font-family:'Raleway',sans-serif;"
                    align="left">

                    <div class="v-text-align"
                        style="color: #333333; line-height: 180%; text-align: left; word-wrap: break-word;">
                        <p style="font-size: 14px; line-height: 180%;"><span
                                style="font-family: Raleway, sans-serif; font-size: 14px; line-height: 25.2px;">{!! $content['body'] !!}</span>
                        </p>
                    </div>

                </td>
            </tr>
        </tbody>
    </table>
@endsection
