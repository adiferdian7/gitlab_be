@extends('email.template')

@section('content')
    {{ $content }}
    <br>
    <br>
    Terima kasih,
    <br>
    <b>UjiAja.com</b>
@endsection