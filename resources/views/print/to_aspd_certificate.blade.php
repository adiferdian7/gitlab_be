<!DOCTYPE html>
<html>

<head>
    <title>Tryout Certificate</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <style>
        @page {
            margin: 0;
        }

        .skor-box {
            background-color: #F5F3FF;
            padding: 20px 20px;
            border-radius: 5px;
        }

        table {
            width: 100% !important;
        }

        table tr th,
        table tr td {
            padding: 5px 5px !important;
        }

        header {
            position: fixed;
            top: 0;
            left: 0px;
            right: 0px;
            /* background-color: lightblue; */
            background-size: 100%;
            background-position: bottom center;
            background-repeat: no-repeat;
            height: 230px;
        }

        footer {
            position: fixed;
            bottom: 0;
            left: 0px;
            right: 0px;
            /* background-color: lightblue; */
            /* height: 50px; */
        }

        main {
            padding: 25px;
        }

        main::after {
            content: "UjiAja.com";
            position: absolute;
            opacity: 0.05;
            top: 60%;
            left: 50%;
            transform: translate(-50%, -50%) rotate(-20deg);
            z-index: -1;
            font-size: 100px;
        }

    </style>
</head>

<body>
    <header style="background-image: url({{ public_path('img/cert-header.png') }})">
        <div class="text-center text-white" style="padding-top: 20px">
            <img class="img-fluid" width="80" src="{{ pengaturan('logo') }}" alt="logo">
            <div style="font-size: 30px">Sertifikat Hasil Tryout</div>
            <div style="font-size: 28px">{{ $result['detail']['nama_produk'] }}</div>
        </div>
    </header>
    <footer>
        <img class="img-fluid" style="width: 100%" src="{{ public_path('/img/cert-footer.png') }}" alt="footer">
    </footer>
    {{-- <main class="" style="background-image: url(<?php echo pengaturan('logo'); ?>)"> --}}
    <main class="" style="margin-top: 180px">
        {{-- <div class="text-center">
            <img class="img-fluid" width="80" src="{{ pengaturan('logo') }}" alt="logo">
            <div style="font-size: 30px">Sertifikat Hasil Tryout</div>
            <div style="font-size: 28px">{{ $result['detail']['nama_produk'] }}</div>
        </div> --}}
        <table class="mt-5">
            <tr class="skor">
                <td width="60%">
                    <div class="skor-date">
                        <div class="mb-2">Nama : {{ $profile->nama_lengkap }}</div>
                        <div class="mb-2">TTL : {{ $profile->tempat_lahir }},
                            {{ tgl_indo($profile->tgl_lahir) }}</div>
                        <div class="mb-2">Asal : {{ $profile->nama_sekolah }}</div>
                    </div>
                </td>
                <td>
                    <div class="text-center skor-box">
                        <div class="mb-0" style="font-size: 18px;">Jumlah Nilai</div>
                        <div class="h3 skor-val" style="font-weight: 500; font-size: 40px">
                            {{ number_format($result['detail']['sum_nilai']) }}
                        </div>
                        @if ($result['detail']['tipe_event'] == 'Masal')
                            <div class="h5 skor-val" style="font-size: 15px">
                                <small>Peringkat</small> {{ $result['detail']['ranking'] }} <small>dari</small>
                                {{ $result['detail']['jsp'] }}
                            </div>
                        @endif
                    </div>
                </td>
            </tr>
        </table>

        <div class="mt-4">
            <p>
                Telah mengikuti tryout online ASPD pada tanggal
                {{ date('d/m/Y', strtotime($result['detail']['waktu_mulai'])) }} dengan hasil sebagai berikut:
            </p>
        </div>

        <div class="hasil mt-4">
            {{-- @foreach ($result['tryout'] as $tryout)
                <table class="table table-borderless">
                    <thead>
                        <tr>
                            <th>
                                {{ $tryout['judul_edit'] }}
                                @if (!empty($tryout['kelompok_soal']))
                                    <small class="ml-1">({{ $tryout['kelompok_soal'] }})</small>
                                @endif
                            </th>
                            <th style="width: 150px; max-width: 50%">Skor</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tryout['mapel'] as $mapel)
                            <tr>
                                <td>{{ $mapel['nama_mapel'] }}</td>
                                @if ($kategori == 'UTBK')
                                    <td>{{ $mapel['ceeb'] }}</td>
                                @elseif($kategori == 'ASPD')
                                    <td>{{ $mapel['nilai_label'] }}</td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endforeach --}}

            @foreach ($result['tryout'] as $tryout)
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th colspan="{{ $result['detail']['tipe_event'] == 'Masal' ? '6' : '5' }}">
                                {{ $tryout['judul_edit'] }}
                                @if (!empty($tryout['kelompok_soal']))
                                    <small class="ml-1">({{ $tryout['kelompok_soal'] }})</small>
                                @endif
                            </th>
                            {{-- <th style="width: 150px; max-width: 50%">Skor</th> --}}
                        </tr>
                        <tr>
                            <th rowspan="2" style="text-align: center; vertical-align: middle">Mata Pelajaran</th>
                            <th colspan="3" style="text-align: center; vertical-align: middle">Jawaban</th>
                            <th rowspan="2" style="text-align: center; vertical-align: middle">Nilai</th>
                            @if ($result['detail']['tipe_event'] == 'Masal')
                                <th rowspan="2" style="text-align: center; vertical-align: middle">Status</th>
                            @endif
                        </tr>
                        <tr>
                            <th style="text-align: center; vertical-align: middle">Benar</th>
                            <th style="text-align: center; vertical-align: middle">Salah</th>
                            <th style="text-align: center; vertical-align: middle">Kosong</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $total_nilai = 0;
                        @endphp
                        @foreach ($tryout['mapel'] as $mapel)
                            @php
                                $total_nilai += $mapel['nilai'];
                            @endphp
                            <tr>
                                <td>{{ $mapel['nama_mapel'] }}</td>
                                <td style="text-align: center;">{{ $mapel['jumlah_benar'] }}</td>
                                <td style="text-align: center;">{{ $mapel['jumlah_salah'] }}</td>
                                <td style="text-align: center;">{{ $mapel['jumlah_kosong'] }}</td>
                                <td style="text-align: center;">{{ $mapel['nilai_label'] }}</td>
                                @if ($result['detail']['tipe_event'] == 'Masal')
                                    <td style="text-align: center;">{{ $mapel['status'] }}</td>
                                @endif
                            </tr>
                        @endforeach
                        @php
                            $rerata_nilai;
                        @endphp
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="4">Rata-rata</th>
                            <th colspan="{{ $result['detail']['tipe_event'] == 'Masal' ? '1' : '2' }}" style="text-align: center">{{ $result['detail']['rerata_nilai_label'] }}
                            </th>
                            @if ($result['detail']['tipe_event'] == 'Masal')
                                <th></th>
                            @endif
                        </tr>
                        <tr>
                            <th colspan="4">Jumlah Nilai
                            </th>
                            <th colspan="{{ $result['detail']['tipe_event'] == 'Masal' ? '1' : '2' }}" style="text-align: center">
                                {{ number_format($result['detail']['sum_nilai'], 2) }}</th>
                            @if ($result['detail']['tipe_event'] == 'Masal')
                                <th></th>
                            @endif
                        </tr>
                    </tfoot>
                </table>
            @endforeach

        </div>
    </main>
</body>

</html>
