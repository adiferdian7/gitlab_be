<!DOCTYPE html>
<html>

<head>
    <title>Tryout Certificate</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <style>
        @page {
            margin: 0;
        }

        .skor-box {
            background-color: #F5F3FF;
            padding: 20px 20px;
            border-radius: 5px;
        }

        table {
            width: 100% !important;
        }
        
        table tr th,
        table tr td {
            padding: 3px 8px !important;
            font-size: 13px;
        }
        table tr th {
            font-size: 14px;
        }

        header {
            position: fixed;
            top: 0;
            left: 0px;
            right: 0px;
            /* background-color: lightblue; */
            background-size: 100%;
            background-position: bottom center;
            background-repeat: no-repeat;
            height: 200px;
        }

        footer {
            position: fixed;
            bottom: 0;
            left: 0px;
            right: 0px;
            /* background-color: lightblue; */
            /* height: 50px; */
        }

        main {
            padding: 25px;
            font-size: 13px;
        }

        main::after {
            content: "UjiAja.com";
            position: absolute;
            opacity: 0.05;
            top: 60%;
            left: 50%;
            transform: translate(-50%, -50%) rotate(-20deg);
            z-index: -1;
            font-size: 100px;
        }

    </style>
</head>

<body>
    <header style="background-image: url({{ public_path('img/cert-header.png') }})">
        <div class="text-center text-white" style="padding-top: 20px">
            <img class="img-fluid" width="80" src="{{ pengaturan('logo') }}" alt="logo">
            <div style="font-size: 26px">Sertifikat Hasil Tryout</div>
            <div style="font-size: 23px">{{ $result['detail']['nama_produk'] }}</div>
        </div>
    </header>
    <footer>
        <img class="img-fluid" style="width: 100%" src="{{ public_path('/img/cert-footer.png') }}" alt="footer">
    </footer>
    {{-- <main class="" style="background-image: url(<?php echo pengaturan('logo'); ?>)"> --}}
    <main class="" style="margin-top: 130px; position: relative; z-index: 9999">
        {{-- <div class="text-center">
            <img class="img-fluid" width="80" src="{{ pengaturan('logo') }}" alt="logo">
            <div style="font-size: 30px">Sertifikat Hasil Tryout</div>
            <div style="font-size: 28px">{{ $result['detail']['nama_produk'] }}</div>
        </div> --}}
        <table class="mt-5">
            <tr class="skor">
                <td width="60%">
                    <div class="skor-date">
                        <div class="mb-2">Nama : {{ $profile->nama_lengkap }}</div>
                        <div class="mb-2">TTL : {{ $profile->tempat_lahir }},
                            {{ tgl_indo($profile->tgl_lahir) }}</div>
                        <div class="mb-2">Asal : {{ $profile->nama_sekolah }}</div>
                    </div>
                </td>
                <td>
                    <div class="text-center skor-box">
                        <div class="mb-0" style="font-size: 20px;">SKOR</div>
                        <div class="h3 skor-val" style="font-weight: 500; font-size: 35px">
                            {{ $result['detail']['ceeb_avg'] }}</div>
                        @if ($result['detail']['tipe_event'] == 'Masal')
                            <div class="h5 skor-val" style="font-size: 15px">
                                <small>Peringkat</small> {{ $result['detail']['ranking'] }} <small>dari</small>
                                {{ $result['detail']['jsp'] }}
                            </div>
                        @else
                            <div class="h5 skor-val" style="font-size: 15px">
                                dari {{ $result['detail']['jsp'] }} orang peserta
                            </div>
                        @endif
                    </div>
                </td>
            </tr>
        </table>

        <div class="mt-4">
            <p>
                Telah mengikuti tryout online UTBK pada tanggal
                {{ date('d/m/Y', strtotime($result['detail']['waktu_mulai'])) }} dengan hasil sebagai berikut:
            </p>
        </div>

        <div class="hasil mt-4">
            @foreach ($result['tryout'] as $tryout)
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th colspan="7">
                                {{ $tryout['judul_edit'] }}
                                @if (!empty($tryout['kelompok_soal']))
                                    <small class="ml-1">({{ $tryout['kelompok_soal'] }})</small>
                                @endif
                            </th>
                            {{-- <th style="width: 150px; max-width: 50%">Skor</th> --}}
                        </tr>
                        <tr>
                            <th rowspan="2" style="text-align: center; vertical-align: middle">Sub Tes</th>
                            <th colspan="3" style="text-align: center; vertical-align: middle">RAW</th>
                            <th rowspan="2" style="text-align: center; vertical-align: middle">IRT</th>
                            <th rowspan="2" style="text-align: center; vertical-align: middle">CEEB</th>
                            <th rowspan="2" style="text-align: center; vertical-align: middle">Status</th>
                        </tr>
                        <tr>
                            <th style="text-align: center; vertical-align: middle">B</th>
                            <th style="text-align: center; vertical-align: middle">S</th>
                            <th style="text-align: center; vertical-align: middle">K</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tryout['mapel'] as $mapel)
                            <tr>
                                <td>{{ $mapel['nama_mapel'] }}</td>
                                <td style="text-align: center;">{{ $mapel['raw']['t_b'] }}</td>
                                <td style="text-align: center;">{{ $mapel['raw']['t_s'] }}</td>
                                <td style="text-align: center;">{{ $mapel['raw']['t_k'] }}</td>
                                <td style="text-align: center;">{{ $mapel['irt_k'] }}</td>
                                <td style="text-align: center;">{{ $mapel['ceeb'] }}</td>
                                <td style="text-align: center;">{{ $mapel['status'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endforeach

            @if ($profile->nama_jenjang == 'SMA')
                <!-- Peluang Masuk -->
                <table class="table table-borderless">
                    <thead>
                        <tr>
                            <th colspan="3">
                                Peluang Masuk Program Studi Pilihan
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <div style="text-transform: capitalize">
                                    {{ $profile->prodi_satu->perguruan->nama_perguruan }}</div>
                            </td>
                            <td>
                                <div style="text-transform: capitalize">
                                    {{ $profile->prodi_satu->program_studi->nama_studi }}</div>
                            </td>
                            <td style="width: 150px; max-width: 50%">
                                {{ peluangLabel($result['detail']['ceeb_avg'], $profile->prodi_satu->passing_grade_prodi) }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="text-transform: capitalize">
                                    {{ $profile->prodi_dua->perguruan->nama_perguruan }}</div>
                            </td>
                            <td>
                                <div style="text-transform: capitalize">
                                    {{ $profile->prodi_dua->program_studi->nama_studi }}</div>
                            </td>
                            <td style="width: 150px; max-width: 50%">
                                {{ peluangLabel($result['detail']['ceeb_avg'], $profile->prodi_dua->passing_grade_prodi) }}
                            </td>
                        </tr>
                    </tbody>
                </table>
            @endif
        </div>

        <div class="hasil mt-4">
            @if (isset($result['rumpun']) && !empty($result['rumpun']))
                <table class="table-bordered">
                    <thead>
                        <tr>
                            <th width="10" style="text-align: center">No</th>
                            <th width="50" style="text-align: center">Kode</th>
                            <th>Rumpun</th>
                            <th width="50" style="text-align: center">Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($result['rumpun'] as $key => $item)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td style="text-align: center">{{ $item['kode'] }}</td>
                                <td>{{ $item['nama'] }}</td>
                                <td style="text-align: center">{{ $item['nilai'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>

    </main>
</body>

</html>
