

## STARTER

```
1. create database in phpmyadmin 
2. edit .env
	DB_CONNECTION=mysql
	DB_HOST=127.0.0.1
	DB_PORT=3306
	DB_DATABASE= //your database
	DB_USERNAME=root
	DB_PASSWORD=
```

```
3. 
php artisan migrate - atau - php artisan migrate:fresh

php artisan storage:link

php artisan db:seed

php artisan key:generate

```
```
4.
php artisan serve
```

## .ENV
ubah dengan nilai yang sesuai

```
APP_URL=http://localhost:8000
WEB_URL=http://localhost:7000

```

## SMTP EMAIL

```
MAIL_MAILER=smtp
MAIL_HOST=smtp.googlemail.com
MAIL_PORT=465
MAIL_USERNAME=sower.bledek@gmail.com
MAIL_PASSWORD=sower1234
MAIL_ENCRYPTION=ssl
MAIL_FROM_ADDRESS=null
MAIL_FROM_NAME="${APP_NAME}"

```


