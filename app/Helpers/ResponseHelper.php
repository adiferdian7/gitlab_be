<?php

function responseData($data, $code = 200)
{
  $response['success'] = true;
  $response['data'] = $data;
  return response()->json($response, $code);
}

function responseSuccess($data = null)
{
  $response = [
    'success' => true,
  ];

  if (!empty($data)) {
    $response['data'] = $data;
  }

  unset($data['error']);

  return response()->json($response);
}

function responseFail($errMessage = '', $code = 400)
{
  $response = [
    'success' => false,
  ];

  if(!empty($errMessage)) {
    $response['messages'] = $errMessage;
  }

  return response()->json($response, $code);
}

function responseNotFound($errMessage = '', $code = 404)
{
  $response = [
    'success' => false,
    'message' => 'Data tidak ditemukan!'
  ];

  if (!empty($errMessage)) {
    $response['message'] = $errMessage;
  } 

  return response()->json($response, $code);
}

function responseInsert($data = null, $code = 201, $message = null)
{
  $response = [
    'success' => true,
    'message' => $message ? $message : 'Data berhasil ditambah!'
  ];
  if ($data) {
    $response['data'] = $data;
  }
  return response()->json($response, $code);
}

function responseUpdate($data = null, $code = 200, $message = null)
{
  $response = [
    'success' => true,
    'message' => $message ? $message : 'Data berhasil diperbarui!'
  ];

  if ($data) {
    $response['data'] = $data;
  }
  return response()->json($response, $code);
}

function responseDeleted($data = null, $code = 200)
{
  $response = [
    'success' => true,
    'message' => 'Data berhasil dihapus!'
  ];
  if ($data) {
    $response['data'] = $data;
  }
  return response()->json($response, $code);
}