<?php

use Illuminate\Support\Facades\DB;

function peluangLabel($ceeb_avg, $pg_prodi) {
  $skorAkhir = $ceeb_avg;
  $passingGrade = $pg_prodi;

  $batasBawah = $passingGrade - ($passingGrade * 0.05);
  $batasAtas = $passingGrade + ($passingGrade * 0.05);

  if($skorAkhir >= $batasBawah && $skorAkhir <= $batasAtas) {
    return 'Sedang';
  } else if ($skorAkhir > $batasAtas) {
    return 'Tinggi';
  } else {
    return 'Rendah';
  }
}

function pengaturan($key) {
  $data = DB::table('pengaturans')->where('key', $key)->first();
  if(!empty($data)) {
    return $data->isi;
  }
  return '';
}

function tgl_indo($tanggal){
	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$pecahkan = explode('-', $tanggal);
	
	// variabel pecahkan 0 = tanggal
	// variabel pecahkan 1 = bulan
	// variabel pecahkan 2 = tahun
 
	return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}