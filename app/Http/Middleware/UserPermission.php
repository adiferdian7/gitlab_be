<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // return $next($request);
        // if(!Auth::user()) {
        //     return response()->json([
        //         'success' => false,
        //         'message' => 'Unauthenticated.'
        //     ], 401);
        // }

        $user = Auth::user();
        $requestMethod = $request->getMethod();
        $requestPath = $request->path();
        $actionName = $request->route()->getActionName();
        $actionMethod = $request->route()->getActionMethod();
        $defaultControllerName = 'App\Http\Controllers\\';
        $tempControllerPath = substr($actionName, strlen($defaultControllerName));
        $controllerPath = explode('@', $tempControllerPath)[0];
        // $controllerPath = explode('@', $actionName)[0];
        $db = DB::table('user_permissions');
        $allowedControllerPath = ['AdminController@dashboard', 'AuthController@getUser', 'NotificationController@index', 'NotificationController@triggerOpen', 'UserPermissionController@mine'];
        if (in_array($tempControllerPath, $allowedControllerPath)) {
            // echo $tempControllerPath;
            return $next($request);
        }

        if ($user->role_user == 'superAdmin') {
            return $next($request);
        } elseif ($user->role_user == 'admin') {
            $check1 = $db
                ->where('user_id', $user->id)
                ->where('user_role', $user->role_user)
                ->whereIn('request_method', [$requestMethod, 'ALL'])
                ->whereRaw("FIND_IN_SET('$controllerPath', controller_path) > 0")
                ->whereRaw("FIND_IN_SET('$actionMethod', controller_method) > 0")
                ->first();

            if ($check1) {
                return $next($request);
            } else {
                return $this->_forbid();
            }
        }

        return $next($request);
    }

    private function _forbid()
    {
        return response()->json([
            'success' => false,
            'message' => 'Unauthorized access.'
        ], 403);
    }
}
