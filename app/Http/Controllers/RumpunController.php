<?php

namespace App\Http\Controllers;

use App\Models\Klaster;
use App\Models\Mapel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\Rumpun;

class RumpunController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $limit = $request->paginate ?  $request->paginate : 10;
    $q = $request->q;
    $data = Rumpun::where('nama', 'like', '%' . $q . '%')
      ->where('kelompok', 'like', '%' . $q . '%')
      ->paginate($limit);
    return response()->json(['success' => true, 'data' => $data]);
  }

  public function indexKlaster(Request $request)
  {
    $limit = $request->paginate ?  $request->paginate : 10;
    $q = $request->q;
    $data = Rumpun::where('nama', 'like', '%' . $q . '%')
      ->where('kelompok', 'like', '%' . $q . '%')
      ->paginate($limit);

    $data = $data->toArray();

    $mapelData = Mapel::select(['id', 'kode', 'nama_mapel'])->get();
    $rumpunIdData = [];
    foreach ($data['data'] as $key => $value) {
      $rumpunIdData[] = $value['id'];
    }

    $rumpunMapelTemp = Klaster::with('mapel')->whereIn('id_rumpun', $rumpunIdData)->groupBy(['id', 'id_rumpun'])->get();

    $rumpunMapelData = [];
    foreach ($rumpunMapelTemp as $key => $value) {
      $rumpunMapelData[$value->id_rumpun][$value->id_mapel] = $value->mapel ? $value->nilai : 0;
    }

    foreach ($data['data'] as $key => $value) {
      foreach ($mapelData as $key2 => $mapelRow) {
        $data['data'][$key]['mapel'][$key2]['id'] = $mapelRow->id;
        $data['data'][$key]['mapel'][$key2]['kode'] = $mapelRow->kode;
        $data['data'][$key]['mapel'][$key2]['nama'] = $mapelRow->nama_mapel;
        if (isset($rumpunMapelData[$value['id']][$mapelRow->id])) {
          $data['data'][$key]['mapel'][$key2]['nilai'] = $rumpunMapelData[$value['id']][$mapelRow->id];
        } else {
          $data['data'][$key]['mapel'][$key2]['nilai'] = '-';
        }
      }
    }



    return response()->json(['success' => true, 'data' => $data, 'mapel' => $mapelData]);
  }


  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validate = Validator::make($request->all(), [
      'nama'   => 'required|unique:rumpun,nama,NULL,id,deleted_at,NULL',
      'kode'   => 'required|unique:rumpun,kode,NULL,id,deleted_at,NULL',
      // 'id_to_kategori'   => 'required|exists:to_kategori,id',
      'kelompok'   => 'required',
    ]);

    if ($validate->fails()) {
      return response()->json([
        'success' => false,
        'messages' => $validate->messages()
      ], 422);
    }

    // if (Rumpun::where('nama', $request->nama)->where('id_to_kategori', $request->id_to_kategori)->count()) {
    //   return response()->json([
    //     'success' => false,
    //     'messages' => ['not_unique' => ['Data already exists!']]
    //   ], 422);
    // }


    DB::beginTransaction();

    try {

      $rumpun = Rumpun::create([
        'kode' => $request->kode,
        'nama' => $request->nama,
        // 'id_to_kategori' => $request->id_to_kategori
        'kelompok' => $request->kelompok
      ]);

      DB::commit();

      return response()->json(['success' => true, 'message' => 'data berhasil di inputkan', 'data' => $rumpun], 201);
    } catch (\Exception $e) {
      DB::rollback();

      return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $data = Rumpun::findOrFail($id);

    return response()->json([
      'success' => true,
      'data' => $data
    ]);
  }

  public function showKlaster($id)
  {
    $data = Rumpun::findOrFail($id);

    $rumpunMapelTemp = Klaster::with('mapel')->where('id_rumpun', $id)->get();

    $rumpunMapelData = [];
    foreach ($rumpunMapelTemp as $key => $value) {
      $rumpunMapelData[$value->id_mapel]['id_klaster'] = $value->id;
      $rumpunMapelData[$value->id_mapel]['id_mapel'] = $value->mapel->id;
      $rumpunMapelData[$value->id_mapel]['kode'] = $value->mapel->kode;
      $rumpunMapelData[$value->id_mapel]['nama_mapel'] = $value->mapel->nama_mapel;
      $rumpunMapelData[$value->id_mapel]['nilai'] = $value->nilai;
    }

    $data->mapel = array_values($rumpunMapelData);

    return response()->json([
      'success' => true,
      'data' => $data
    ]);
  }


  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $rumpun = Rumpun::findOrFail($id);

    $validate = Validator::make($request->all(), [
      'nama'   => 'required|unique:rumpun,nama,' . $id . ',id,deleted_at,NULL',
      'kode'   => 'required|unique:rumpun,kode,' . $id . ',id,deleted_at,NULL',
      // 'id_to_kategori'   => 'required|exists:to_kategori,id',
      'kelompok'   => 'required',
    ]);

    if ($validate->fails()) {
      return response()->json([
        'success' => false,
        'messages' => $validate->messages()
      ], 422);
    }

    DB::beginTransaction();

    try {

      $rumpun->nama = $request->nama;
      $rumpun->kode = $request->kode;
      // $rumpun->id_to_kategori = $request->id_to_kategori;
      $rumpun->kelompok = $request->kelompok;
      $rumpun->save();

      DB::commit();

      return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $rumpun], 201);
    } catch (\Exception $e) {
      DB::rollback();

      return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request, $id)
  {

    $delete  = Rumpun::destroy($id);
    return response()->json([
      'success' => true,
      'messages' => 'data berhasil di hapus',
      'data' => $id,
    ]);
  }
}
