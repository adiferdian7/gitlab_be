<?php

namespace App\Http\Controllers;

use App\Models\Bundling;
use App\Models\BundlingProduct;
use Illuminate\Http\Request;
use Validator;

class BundlingController extends Controller
{
    function __construct()
    {
        $this->bundlingModel = new Bundling();
    }

    public function index(Request $request)
    {
        $data = $this->bundlingModel->getData('paginationData', $request->all(), '');
        return responseData($data);
    }

    public function detail($id = '')
    {
        $data = $this->bundlingModel->getData('detailData', ['id' => $id], '');
        if (!empty($data)) {
            return responseData($data);
        } else {
            return responseFail('Data tidak ditemukan!', 404);
        }
    }

    public function detailWithTrans($id = '')
    {
        $data = $this->bundlingModel->getData('detailWithTrans', ['id' => $id], 'detailWithTrans');
        if (!empty($data)) {
            return responseData($data);
        } else {
            return responseFail('Data tidak ditemukan!', 404);
        }
    }

    public function insert(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'name'   => 'required|unique:bundlings,name,NULL,id,deleted_at,NULL',
            'price'   => 'required',
            'desc'   => 'required',
            'products' => 'nullable|array'
        ], $this->bundlingModel->getValidationMessage());

        if ($validate->fails()) {
            return responseFail($validate->messages(), 422);
        }

        if ($request->has('products') && !empty($request->products)) {
            $result = $this->bundlingModel->insertData('insertDataWithProducts', $request->all());
        } else {
            $result = $this->bundlingModel->insertData('insertData', $request->all());
        }

        if (isset($result['error'])) {
            return responseFail($result, 500);
        } else {
            return responseInsert($result);
        }
    }

    public function update(Request $request, $id)
    {
        $bundling = Bundling::find($id);

        if (!$bundling) {
            return responseNotFound();
        }

        $validate = Validator::make($request->all(), [
            'name'   => 'required|unique:bundlings,name,' . $id . ',id,deleted_at,NULL',
            'price'   => 'required',
            'desc'   => 'required',
            'products' => 'nullable|array'
        ], $this->bundlingModel->getValidationMessage());

        if ($validate->fails()) {
            return responseFail($validate->messages(), 422);
        }

        $data = $request->all();
        $data['id'] = $id;
        $result = $this->bundlingModel->updateData('updateData', $data);

        if (isset($result['error'])) {
            return responseFail($result);
        } else {
            $bundlingProduct = new BundlingProduct();
            if ($request->has('products') && !empty($request->products)) {
                $bundlingProduct->insertData('insertBatchData', ['bundle_id' => $id, 'products' => $request->products]);
            }
            return responseUpdate($result);
        }
    }

    public function delete(Request $request, $id)
    {
        $bundling = Bundling::find($id);
        if (!$bundling) {
            return responseNotFound();
        }
        $result = $this->bundlingModel->deleteData('deleteData', ['id' => $id]);

        if (isset($result['error'])) {
            return responseFail($result);
        }
        return responseDeleted($result);
    }
}
