<?php

namespace App\Http\Controllers;

use App\Models\Kepribadian;
use App\Models\MbtiJawaban;
use App\Models\MbtiJawabanDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class MbtiJawabanDetailController extends Controller
{
    public function insert_multiple(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'jawabans'      => 'required|array',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => [
                  'jawabans' => 'Jawaban required'
                ]
            ], 422);
        }

        DB::beginTransaction();

        try {

            foreach ($request->jawabans as $jawaban) {
              $insert = MbtiJawabanDetail::create([
                  'id_mbti_jawaban'              => $jawaban['id_mbti_jawaban'],
                  'id_mbti_pertanyaan'              => $jawaban['id_mbti_pertanyaan'],
                  'jawaban_user'       => $jawaban['jawaban_user']
              ]);
            }

            $hitungan = $this->_count_test_result($insert->id_mbti_jawaban);

            if($hitungan == 'error') {
                DB::rollback();
                return response()->json(['success' => false, 'messages' => 
                [
                    "soal" => ['Permintaan tidak dapat diproses! Soal MBTI tidak lengkap. Mohon laporkan kepada kami melalui menu pengaduan.']
                ]
            ], 422);

            } else {
                DB::commit();
            }


            return response()->json(['success' => true, 'message' => 'Data MBTI-jawaban-detail berhasil di inputkan', 'data' => $hitungan], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    private function _count_test_result($id_mbti_jawaban) {

        $jawabanDetail = MbtiJawabanDetail::with(['pertanyaan'])->where('id_mbti_jawaban', $id_mbti_jawaban)->get()->toArray();

        $temp = [];
        // $temp2 = [];
        // $index = 0;
        foreach ($jawabanDetail as $key => $jawaban) {
            
            foreach ($jawaban['pertanyaan']['opsi'] as $keyopsi => $opsi) {
                $psikologi = $opsi['psikologi'];
                // $temp2[$key][$psikologi] = $opsi['id'] == $jawaban['jawaban_user'] ? 1 : 0; 
                // echo $psikologi . " "; 
                if($opsi['id'] == $jawaban['jawaban_user']) {
                    if(isset($temp[$psikologi])) {
                        // echo "+1";
                        $temp[$psikologi] += 1;
                    } else {
                        $temp[$psikologi] = 1;
                        // echo "=1";
                    }
                } else if ($opsi['id'] != $jawaban['jawaban_user'] && !isset($temp[$psikologi])) {
                    $temp[$psikologi] = 0;
                }
                // echo "<br>"; 
            }
        }

        // print_r($jawabanDetail);
        if(count($temp) < 8) {
            return 'error';
        }
        
        $temp2['Introvert'] = $temp['Introvert'] / ($temp['Introvert'] + $temp['Ekstrovert']) * 100;
        $temp2['Ekstrovert'] = $temp['Ekstrovert'] / ($temp['Ekstrovert'] + $temp['Introvert']) * 100;
        $temp2['Intuition'] = $temp['Intuition'] / ($temp['Intuition'] + $temp['Sensing']) * 100;
        $temp2['Sensing'] = $temp['Sensing'] / ($temp['Sensing'] + $temp['Intuition']) * 100;
        $temp2['Thinking'] = $temp['Thinking'] / ($temp['Thinking'] + $temp['Feeling']) * 100;
        $temp2['Feeling'] = $temp['Feeling'] / ($temp['Feeling'] + $temp['Thinking']) * 100;
        $temp2['Judging'] = $temp['Judging'] / ($temp['Judging'] + $temp['Perceiving']) * 100;
        $temp2['Perceiving'] = $temp['Perceiving'] / ($temp['Perceiving'] + $temp['Judging']) * 100;

        // $a = "I";
        // $b = "N";
        // $c = "F";
        // $d = "J";
        $a = $temp2['Introvert'] > $temp2['Ekstrovert'] ? 'I' : 'E';
        $b = $temp2['Intuition'] > $temp2['Sensing'] ? 'N' : 'S';
        $c = $temp2['Thinking'] > $temp2['Feeling'] ? 'T' : 'F';
        $d = $temp2['Judging'] > $temp2['Perceiving'] ? 'J' : 'P';

        $nama_kepribadian = "$a$b$c$d";
        $kepribadian = Kepribadian::where('nama_singkat', $nama_kepribadian)->first();

        $data = [
            [
                'label' => 'Mind',
                'bgClass' => 'bg-info',
                'psikologi' => ['Introvert', 'Extrovert'],
                'persentase' => [
                    round($temp2['Introvert']),
                    round($temp2['Ekstrovert'])
                ]
            ],
            [
                'label' => 'Energy',
                'bgClass' => 'bg-danger',
                'psikologi' => ['Intuition', 'Sensing'],
                'persentase' => [
                    round($temp2['Intuition']),
                    round($temp2['Sensing'])
                ]
            ],
            [
                'label' => 'Nature',
                'bgClass' => 'bg-success',
                'psikologi' => ['Feeling', 'Thinking'],
                'persentase' => [
                    round($temp2['Feeling']),
                    round($temp2['Thinking'])
                ]
            ],
            [
                'label' => 'Tactics',
                'bgClass' => 'bg-primary',
                'psikologi' => ['Judging', 'Perceiving'],
                'persentase' => [
                    round($temp2['Judging']),
                    round($temp2['Perceiving'])
                ]
            ]
        ];

        // print_r($jawabanDetail);
        // print_r($temp);
        // print_r($temp2);
        // print_r($data);
        // die;

        $hitungan = json_encode($data);
        MbtiJawaban::where('id', $id_mbti_jawaban)->update(['id_kepribadian'=>$kepribadian->id, 'hitungan' => $hitungan]);
        $data = MbtiJawaban::find($id_mbti_jawaban);
        return $data;
    }

}
