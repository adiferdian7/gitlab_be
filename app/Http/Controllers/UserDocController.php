<?php

namespace App\Http\Controllers;

use App\Models\UserDoc;
use Illuminate\Http\Request;
use Validator;

class UserDocController extends Controller
{
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'files'              => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $user_id = $request->user_id ?? auth()->user()->id;

            foreach ($request->files as $key => $file) {

                $check = UserDoc::where('user_id', $user_id)->where('doc_type', $file['type']);

                if($check->count() > 0)
                {
                    if(!empty($file['file'])) {
                        $check->update([
                            'doc_file' => $file['file'],
                        ]);
                    }

                } else {
                    UserDoc::create([
                        'user_id' => $user_id,
                        'doc_type' => $file['type'],
                        'doc_file' => $file['file'],
                        'doc_status' => 'Pending',
                    ]);
                }
            }

            DB::commit();

            return response()->json(['success' => true, 'message' => 'Doc create successfully!'], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 500);
        }

    }
}
