<?php

namespace App\Http\Controllers;

use App\Models\Kursus;
use App\Models\Mapel;
use App\Models\MbtiJawaban;
use App\Models\Produk;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\ProdukPaket;
use App\Models\TbMbti;
use App\Models\User;
use App\Models\Tryout;
use App\Models\Teacher;
use App\Models\Transaksi;
use App\Models\TryoutUser;
use App\Models\TryoutUserJawaban;
use App\Models\TryoutHasil;
use App\Models\Student;
use Carbon\Carbon;
use File;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->paginate ?  $request->paginate : 10;
        $q = $request->q;

        $user = auth()->user();

        $query = Produk::with([
            'list:id,id_produk,id_tryout',
            'list.tryout:id,kategori,jenis_soal,kelompok_soal,id_penjurusan,alokasi_waktu,jeda_waktu,id_jenjang',
            'list.tryout.penjurusan:id,id_kelas,nama_penjurusan',
            'list.tryout.penjurusan.kelas:id,id_jenjang,nama_kelas',
            'list.tryout.penjurusan.kelas.jenjang:id,nama_jenjang',
            'list.tryout.soal:id,id_tryout,jenis_soal,kelompok_soal,id_mapel',
            'list.tryout.soal.mapel:id,nama_mapel',
            'list.tryout.soal.pertanyaan:id,id_soal_tryout,bab_mapel,penjelasan_pertanyaan,template_pertanyaan,soal,opsi_pertanyaan,pembahasan_pertanyaan,jawaban_pertanyaan',
            'list.tryout.jenjang:id,nama_jenjang',
            'level:id,nama_level,minimal_rate_mengajar,minimal_total_mengajar',
        ]);

        $queryPaginate = Produk::select('id');

        // filter produk siswa -> general
        if (($user && $user->role_user == 'siswa') && empty($request->id_user)) {
            // $query->with(['transaksi_user'])->doesntHave('transaksi_user');
            // $queryPaginate->with(['transaksi_user'])->doesntHave('transaksi_user');
        }

        // filter produk siswa -> own
        if (($user && $user->role_user == 'siswa')
            && !empty($request->id_user)
        ) {
            $id_user = $request->id_user;
            $query->with(['transaksi_user' => function ($subquery) {
                return $subquery->orderByDesc('tanggal_transaksi');
            }])->whereHas('transaksi_user', function ($subquery) use ($id_user) {
                return $subquery->where('id_user', $id_user);
            });
            $queryPaginate->with(['transaksi_user' => function ($subquery) {
                return $subquery->orderByDesc('tanggal_transaksi');
            }])->whereHas('transaksi_user', function ($subquery) use ($id_user) {
                return $subquery->where('id_user', $id_user);
            });
        }

        // filter produk teacher -> general
        if (($user && $user->role_user == 'teacher') && empty($request->id_user)) {
            // $query->with(['tryout_user'])->doesntHave('tryout_user');
            // $queryPaginate->with(['tryout_user'])->doesntHave('tryout_user');
        }

        // filter produk teacher -> own
        if (($user && $user->role_user == 'teacher')
            && !empty($request->id_user)
        ) {
            $id_user = $request->id_user;
            $query->with(['transaksi_user' => function ($subquery) {
                return $subquery->orderByDesc('tanggal_transaksi');
            }])->whereHas('transaksi_user', function ($subquery) use ($id_user) {
                return $subquery->where('id_user', $id_user);
            });
            $queryPaginate->with(['transaksi_user' => function ($subquery) {
                return $subquery->orderByDesc('tanggal_transaksi');
            }])->whereHas('transaksi_user', function ($subquery) use ($id_user) {
                return $subquery->where('id_user', $id_user);
            });
        }

        if (!empty($request->kategori_produk)) {
            $query->where('kategori_produk', $request->kategori_produk);
            $queryPaginate->where('kategori_produk', $request->kategori_produk);
        }

        if (!empty($request->excludes_kategori)) {
            $query->whereNotIn('kategori_produk', $request->excludes_kategori);
            $queryPaginate->whereNotIn('kategori_produk', $request->excludes_kategori);
        }

        if (!empty($request->jenis_produk)) {
            $query->where('jenis_produk', $request->jenis_produk);
            $queryPaginate->where('jenis_produk', $request->jenis_produk);
        }

        if (!empty($request->status_produk)) {
            $query->where('status_produk', $request->status_produk);
            $queryPaginate->where('status_produk', $request->status_produk);
        }

        if (!empty($request->tipe_paket)) {
            $query->where('tipe_paket', $request->tipe_paket);
            $queryPaginate->where('tipe_paket', $request->tipe_paket);
        }

        if (!empty($request->is_archive)) {
            $query->where('is_archived', $request->is_archived);
            $queryPaginate->where('is_archived', $request->is_archived);
        }

        $id_jenjang = null;
        if (!empty($request->id_jenjang)) {
            $id_jenjang = $request->id_jenjang;
        }

        $query->where(function ($queryFilter) use ($q) {
            $queryFilter->where('nama_produk', 'like', '%' . $q . '%')
                ->orWhere('kategori_produk', 'like', '%' . $q . '%')
                ->orWhere('harga_produk', 'like', '%' . $q . '%');
        });

        if (!empty($id_jenjang)) {
            $query->whereHas('list.tryout', function ($filter) use ($id_jenjang) {
                $filter->where('id_jenjang', $id_jenjang);
            });
        }

        $queryPaginate->where(function ($queryFilter) use ($q) {
            $queryFilter->where('nama_produk', 'like', '%' . $q . '%')
                ->orWhere('kategori_produk', 'like', '%' . $q . '%')
                ->orWhere('harga_produk', 'like', '%' . $q . '%');
        });

        if (!empty($id_jenjang)) {
            $queryPaginate->whereHas('list.tryout', function ($filter) use ($id_jenjang) {
                $filter->where('id_jenjang', $id_jenjang);
            });
        }

        $data = $query->orderBy('produks.created_at', 'desc')->paginate($limit);

        $paginate = $queryPaginate->paginate($limit);


        $array = [];
        $tryout = [];

        foreach ($data as $key => $value) {

            $kategori = [];
            $jenis_tryout = [];
            $jumlah_soal = 0;
            $waktu  = 0;
            $penjurusan = [];
            $kelompok_soal = [];
            $jenjang = [];
            $mapel = [];

            foreach ($value->list as $row) {

                $kategori[]     = $row->tryout->kategori;
                $kelompok_soal[] = $row->tryout->kelompok_soal ?? '';
                $jenis_tryout[] = $row->tryout->jenis_soal ?? '';
                $penjurusan[]   = $row->tryout->penjurusan->kelas->jenjang->nama_jenjang ?? '';
                // $jumlah_soal   += count($row->tryout->soal);

                $jenjang[] = $row->tryout->jenjang ? $row->tryout->jenjang->nama_jenjang : '';

                foreach ($row->tryout->soal as $soal) {
                    $jumlah_soal   += count($soal->pertanyaan);
                    $mapel[] = $soal->mapel ? $soal->mapel->nama_mapel : '';
                }

                $waktu += $row->tryout->alokasi_waktu;
            }

            $is_task_start = $value->tryout_user && $value->tryout_user->count() > 0 ? true : false;
            $is_enrolled = $value->transaksi_user ? true : false;

            if ($value->transaksi_user) {
                if ($value->transaksi_user->status == 'Kadaluarsa') {
                    $is_enrolled = false;
                }
            }

            $archive =  TryoutUser::where('id_produk', $value->id)->where('id_user', auth()->user()->id)->select('is_archived')->first() == null ? 0 : TryoutUser::where('id_produk', $value->id)->where('id_user', auth()->user()->id)->select('is_archived')->first()->is_archived;

            $array[] = [
                'id'                => $value->id,
                'nama_produk'       => $value->nama_produk,
                'harga_produk'      => $value->harga_produk,
                'harga_label'        => 'Rp ' . number_format($value->harga_produk, 0, ',', '.'),
                'kategori_produk'   => $value->kategori_produk,
                'jenis_produk'   => $value->jenis_produk,
                'status_produk'     => $value->status_produk,
                'tipe_paket'        => $value->tipe_paket,
                'tanggal_mulai'     => $value->tanggal_mulai,
                'tanggal_berakhir'  => $value->tanggal_berakhir,
                'tanggal_mulai_label' => Carbon::parse($value->tanggal_mulai)->locale('id')->format('d/M/Y  H:i'),
                'tanggal_berakhir_label' => Carbon::parse($value->tanggal_berakhir)->locale('id')->format('d/M/Y  H:i'),
                'jenis_tryout'      => collect($jenis_tryout)->unique(),
                'kelompok_soal'     => collect($kelompok_soal)->unique(),
                'kategori_tryout'   => collect($kategori)->unique(),
                'waktu'             => $waktu,
                'jumlah_soal'       => $jumlah_soal,
                'penjurusan'        => collect($penjurusan)->unique(),
                'deskripsi_produk'  => $value->deskripsi_produk,
                'transaksi_user' =>  $user && $user->role_user == 'siswa' ? $value->transaksi_user : null,
                'tryout_user' =>  $user && $user->role_user == 'teacher' ? $value->tryout_user_user : null,
                'level' => $value->kategori_produk == 'UKTT' ? $value->level : null,
                'jenjang' => $jenjang,
                'mapel' => $mapel,
                'is_enrolled' => $is_enrolled,
                'is_task_start' => $is_task_start,
                'is_archive' => $archive
            ];
        }

        $arrayData = [
            'data'     => $array,
            'paginate' => $paginate,
        ];

        return response()->json([
            'success' => true,
            'messages' => 'berhasil mengambil data',
            'data' => $arrayData,
        ]);
    }

    public function gabungArray($field)
    {
        $output = [];
        array_walk_recursive($field, function ($val) use (&$output) {
            $output[] = $val;
        });
        return $output;
    }

    public function generatecsv(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'id_produk'      => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        $users = TryoutUser::select('id_user')->where('id_produk', $request->id_produk)->get();
        $headers = array(
            'Content-Type' => 'text/csv'
        );
        if (!File::exists(public_path() . "/file")) {
            File::makeDirectory(public_path() . "/file");
        }

        //csv create awal
        $filename =  public_path("file/" . "export-tryout-" . $request->id_produk . ".csv");
        $lokasi_file = "https://api.ujiaja.com/file/" . "export-tryout-" . $request->id_produk . ".csv";
        $handle = fopen($filename, 'w');


        foreach ($users as $values) {
            $id_user = $values->id_user;
            $tryouts = TryoutUserJawaban::leftjoin('tryout_users', 'tryout_users.id', 'tryout_user_jawabans.id_tryout_user')
                ->leftjoin('soal_pertanyaans', 'soal_pertanyaans.id', 'tryout_user_jawabans.id_soal_pertanyaan')
                ->leftjoin('tryouts', 'tryouts.id', 'tryout_users.id_tryout')
                ->select(
                    'tryout_users.id_produk',
                    'tryouts.kategori',
                    'tryout_user_jawabans.jawaban_user',
                    DB::raw("CASE WHEN tryout_user_jawabans.jawaban_user = replace(replace(soal_pertanyaans.jawaban_pertanyaan, '\"', ''), '\"', '') THEN 1 ELSE 0 END as jawaban")
                )
                ->where('id_user', $id_user)
                ->where('id_produk', $request->id_produk)
                ->where('kategori', $request->kategori)
                ->get();

            $data = [];
            $i = 1;

            // if ($tryouts[] == "ASPD") {
            //     return response()->json([
            //         'messages' => 'data tidak ditemukan !'
            //     ]);
            // }

            foreach ($tryouts as $value) {
                $data[$i]['nomor'] = $i++;
                $data[$i]['id_produk'] = $value->id_produk;
                $data[$i]['kategori'] = $value->kategori;
                $data[$i]['jawaban_user'] = $value->jawaban;
            }
        }


        foreach ($users as $values2) {
            $id_user2 = $values2->id_user;

            $hasil = TryoutHasil::leftjoin('tryout_users', 'tryout_users.referensi', 'tryout_hasils.referensi')
                ->select(
                    'tryout_users.id_user',
                    'tryout_hasils.riwayat_perhitungan'
                )->where('id_user', $id_user2)
                ->where('id_produk', $request->id_produk)
                ->first();

            $students[] = Student::select('nama_lengkap')->where('id_siswa', $hasil['id_user'])->first();

            // these are the headers for the csv file. Not required but good to have one incase of system didn't recongize it properly
            $jumlah_benar = array();
            $nama_mapel = array();
            $jumlah_kosong = array();
            $jumlah_salah = array();
            $total_soal = array();
            $nilai = array();

            foreach ($hasil['riwayat_perhitungan']['tryout'][0]['mapel'] as $mapel => $permapel) {

                $jumlah_benar[] = $hasil['riwayat_perhitungan']['tryout'][0]['mapel'][$mapel]['jumlah_benar'];
                $jumlah_kosong[] = $hasil['riwayat_perhitungan']['tryout'][0]['mapel'][$mapel]['jumlah_kosong'];
                $jumlah_salah[] = $hasil['riwayat_perhitungan']['tryout'][0]['mapel'][$mapel]['jumlah_salah'];
                $total_soal[] = $hasil['riwayat_perhitungan']['tryout'][0]['mapel'][$mapel]['total_soal'];
                $nilai[] = $hasil['riwayat_perhitungan']['tryout'][0]['mapel'][$mapel]['nilai'];
                $nama_mapel[] =  $hasil['riwayat_perhitungan']['tryout'][0]['mapel'][$mapel]['nama_mapel'];
            }
        }
        $ket[] = ["Jumlah Benar", "Jumlah Kosong", "Jumlah Salah", "Total Soal", "Nilai"];

        foreach ($hasil['riwayat_perhitungan']['tryout'][0]['mapel'] as $mapel => $permapel) {
            $jawaban[] = [$jumlah_benar[$mapel], $jumlah_kosong[$mapel], $jumlah_salah[$mapel], $total_soal[$mapel], round($nilai[$mapel])];
            $keterangan_header[] =  $ket;
        }

        foreach ($users as $valuess => $isi_user) {
            $field[] = $students[$valuess]->nama_lengkap;
        }

        foreach ($users as $valuess => $isi_user) {
            $isi_nilai[] = [$field[$valuess], $jawaban];
        }

        $fieldheader[] = ["No", "Nama Lengkap",  $nama_mapel];
        $fieldKet[] = ["", "", $keterangan_header];
        $outputheader = $this->gabungArray($fieldheader);
        $outputKet = $this->gabungArray($fieldKet);

        //add header on csv
        fputcsv(
            $handle,
            $outputheader
        );

        // add keterangan mapel header
        fputcsv(
            $handle,
            $outputKet
        );

        foreach ($users as $valuess => $isi_user) {
            //$output = $this->gabungArray($isi_nilai[$valuess]);
            $fieldNilai[] = [$valuess + 1,  $isi_nilai[$valuess]];
            $outputAkhir = $this->gabungArray($fieldNilai[$valuess]);
            fputcsv($handle, $outputAkhir);
        }


        fclose($handle);


        if (!$tryouts) {
            return response()->json([
                'messages' => 'data tidak ditemukan !'
            ]);
        } else {
            return response()->json([
                'success' => true,
                'messages' => 'Generate Hasil Berhasil !',
                'data' => $lokasi_file
            ]);
        }
    }






    public function indexUKTT(Request $request)
    {
        $limit = $request->paginate ?  $request->paginate : 10;
        $q = $request->q;

        $user = auth()->user();

        $query = Produk::with([
            'list:id,id_produk,id_tryout',
            'list.tryout:id,kategori,jenis_soal,kelompok_soal,id_penjurusan,alokasi_waktu,jeda_waktu,id_jenjang',
            'list.tryout.penjurusan:id,id_kelas,nama_penjurusan',
            'list.tryout.penjurusan.kelas:id,id_jenjang,nama_kelas',
            'list.tryout.penjurusan.kelas.jenjang:id,nama_jenjang',
            'list.tryout.soal:id,id_tryout,jenis_soal,kelompok_soal,id_mapel',
            'list.tryout.soal.mapel:id,nama_mapel',
            'list.tryout.soal.pertanyaan:id,id_soal_tryout,bab_mapel,penjelasan_pertanyaan,template_pertanyaan,soal,opsi_pertanyaan,pembahasan_pertanyaan,jawaban_pertanyaan',
            'list.tryout.jenjang:id,nama_jenjang',
            'level:id,nama_level,minimal_rate_mengajar,minimal_total_mengajar',
        ]);
        $queryPaginate = Produk::select('id');

        $query->where('kategori_produk', 'UKTT');
        $queryPaginate->where('kategori_produk', 'UKTT');

        if (!empty($request->jenis_produk)) {
            $query->where('jenis_produk', $request->jenis_produk);
            $queryPaginate->where('jenis_produk', $request->jenis_produk);
        }

        if (!empty($request->status_produk)) {
            $query->where('status_produk', $request->status_produk);
            $queryPaginate->where('status_produk', $request->status_produk);
        }

        if (!empty($request->tipe_paket)) {
            $query->where('tipe_paket', $request->tipe_paket);
            $queryPaginate->where('tipe_paket', $request->tipe_paket);
        }

        $id_jenjang = null;
        if (!empty($request->id_jenjang)) {
            $id_jenjang = $request->id_jenjang;
        }

        $query->where(function ($queryFilter) use ($q) {
            $queryFilter->where('nama_produk', 'like', '%' . $q . '%')
                ->orWhere('kategori_produk', 'like', '%' . $q . '%')
                ->orWhere('harga_produk', 'like', '%' . $q . '%');
        });

        $queryPaginate->where(function ($queryFilter) use ($q) {
            $queryFilter->where('nama_produk', 'like', '%' . $q . '%')
                ->orWhere('kategori_produk', 'like', '%' . $q . '%')
                ->orWhere('harga_produk', 'like', '%' . $q . '%');
        });

        if (!empty($id_jenjang)) {
            $query->whereHas('list.tryout', function ($filter) use ($id_jenjang) {
                $filter->where('id_jenjang', $id_jenjang);
            });
            $queryPaginate->whereHas('list.tryout', function ($filter) use ($id_jenjang) {
                $filter->where('id_jenjang', $id_jenjang);
            });
        } else {
            // filter produk teacher matches
            if ($user->role_user == 'teacher') {
                $check_kursus = Kursus::where('id_tentor', $user->id);
                $id_jenjang_array = [];
                $id_mapel_array = [];
                if ($check_kursus->count() > 0) {
                    $id_jenjang_array = $check_kursus->pluck('id_jenjang')->toArray();
                    $id_mapel_array = $check_kursus->pluck('id_mapel')->toArray();
                }
                $query->whereHas('list.tryout', function ($filter) use ($id_jenjang_array) {
                    $filter->whereIn('id_jenjang', $id_jenjang_array);
                })->whereHas('list.tryout.soal', function ($filter) use ($id_mapel_array) {
                    $filter->whereIn('id_mapel', $id_mapel_array);
                });
                $queryPaginate->whereHas('list.tryout', function ($filter) use ($id_jenjang_array) {
                    $filter->whereIn('id_jenjang', $id_jenjang_array);
                })->whereHas('list.tryout.soal', function ($filter) use ($id_mapel_array) {
                    $filter->whereIn('id_mapel', $id_mapel_array);
                });
            }
        }

        // if (!empty($id_jenjang)) {
        //     $queryPaginate->whereHas('list.tryout', function ($filter) use ($id_jenjang) {
        //         $filter->where('id_jenjang', $id_jenjang);
        //     });
        // }

        $data = $query->orderBy('produks.created_at', 'desc')->paginate($limit);

        $paginate = $queryPaginate->paginate($limit);


        $array = [];

        foreach ($data as $key => $value) {

            $kategori = [];
            $jenis_tryout = [];
            $jumlah_soal = 0;
            $waktu  = 0;
            $penjurusan = [];
            $kelompok_soal = [];
            $jenjang = [];
            $mapel = [];

            foreach ($value->list as $row) {

                $kategori[]     = $row->tryout->kategori;
                $kelompok_soal[] = $row->tryout->kelompok_soal ?? '';
                $jenis_tryout[] = $row->tryout->jenis_soal ?? '';
                $penjurusan[]   = $row->tryout->penjurusan->kelas->jenjang->nama_jenjang ?? '';
                // $jumlah_soal   += count($row->tryout->soal);

                $jenjang[] = $row->tryout->jenjang ? $row->tryout->jenjang->nama_jenjang : '';

                foreach ($row->tryout->soal as $soal) {
                    $jumlah_soal   += count($soal->pertanyaan);
                    $mapel[] = $soal->mapel ? $soal->mapel->nama_mapel : '';
                }

                $waktu += $row->tryout->alokasi_waktu;
            }

            $is_task_start = $value->tryout_user && $value->tryout_user->count() > 0 ? true : false;
            $is_enrolled = $value->transaksi_user ? true : false;

            if ($value->transaksi_user) {
                if ($value->transaksi_user->status == 'Kadaluarsa') {
                    $is_enrolled = false;
                }
            }

            $array[] = [
                'id'                => $value->id,
                'nama_produk'       => $value->nama_produk,
                'harga_produk'      => $value->harga_produk,
                'harga_label'        => 'Rp ' . number_format($value->harga_produk, 0, ',', '.'),
                'kategori_produk'   => $value->kategori_produk,
                'jenis_produk'   => $value->jenis_produk,
                'status_produk'     => $value->status_produk,
                'tipe_paket'        => $value->tipe_paket,
                'tanggal_mulai'     => $value->tanggal_mulai,
                'tanggal_berakhir'  => $value->tanggal_berakhir,
                'tanggal_mulai_label' => Carbon::parse($value->tanggal_mulai)->locale('id')->format('d/M/Y  H:i'),
                'tanggal_berakhir_label' => Carbon::parse($value->tanggal_berakhir)->locale('id')->format('d/M/Y  H:i'),
                'jenis_tryout'      => collect($jenis_tryout)->unique(),
                'kelompok_soal'     => collect($kelompok_soal)->unique(),
                'kategori_tryout'   => collect($kategori)->unique(),
                'waktu'             => $waktu,
                'jumlah_soal'       => $jumlah_soal,
                'penjurusan'        => collect($penjurusan)->unique(),
                'deskripsi_produk'  => $value->deskripsi_produk,
                'transaksi_user' =>  $user && $user->role_user == 'siswa' ? $value->transaksi_user : null,
                'tryout_user' =>  $user && $user->role_user == 'teacher' ? $value->tryout_user_user : null,
                'level' => $value->kategori_produk == 'UKTT' ? $value->level : null,
                'jenjang' => $jenjang,
                'mapel' => $mapel,
                'is_enrolled' => $is_enrolled,
                'is_task_start' => $is_task_start
            ];
        }




        $arrayData = [
            'data'     => $array,
            'paginate' => $paginate,
        ];
        return response()->json([
            'success' => true,
            'messages' => 'berhasil mengambil data',
            'data' => $arrayData,
        ]);
    }

    public function listOptionData(Request $request)
    {
        $limit = $request->paginate ?  $request->paginate : 10;
        $q = $request->q;

        $query = Produk::selectRaw('id, nama_produk, kategori_produk, jenis_produk');

        if (!empty($request->kategori_produk)) {
            $query->where('kategori_produk', $request->kategori_produk);
        }

        if (!empty($request->status_produk)) {
            $query->where('status_produk', $request->status_produk);
        }

        if (!empty($request->excludes_kategori)) {
            $query->whereNotIn('kategori_produk', $request->excludes_kategori);
        }

        $data = $query->paginate($limit);

        return response()->json([
            'success' => true,
            'messages' => 'berhasil mengambil data',
            'data' => $data,
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'nama_produk'     => 'required|unique:produks,nama_produk,NULL,id,deleted_at,NULL',
            'kategori_produk' => 'required',
            'jenis_produk'    => Rule::requiredIf($request->kategori_produk != 'UKTT'),
            'harga_produk'    => Rule::requiredIf($request->kategori_produk != 'UKTT'),
            'pakai_sertifikat' => Rule::requiredIf($request->kategori_produk != 'UKTT'),
            'status_produk'   => Rule::requiredIf($request->kategori_produk != 'UKTT'),
            'tipe_paket'      => Rule::requiredIf($request->kategori_produk != 'UKTT'),
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $data = [
                'nama_produk'               => $request->nama_produk,
                'kategori_produk'           => $request->kategori_produk,
                'jenis_produk'              => $request->jenis_produk,
                'harga_produk'              => $request->harga_produk,
                'pakai_sertifikat'          => $request->pakai_sertifikat,
                'status_produk'             => $request->status_produk,
                'tipe_paket'                => $request->tipe_paket,
                'tanggal_mulai'             => $request->tanggal_mulai,
                'tanggal_berakhir'          => $request->tanggal_berakhir,
                'pakai_perankingan'         => $request->pakai_perankingan,
                'perhitungan'               => $request->perhitungan,
                'deskripsi_produk'          => $request->deskripsi_produk,
                'bonus'                     => !empty($request->bonus) ? $request->bonus : null,
                'maksimal_peserta'          => !empty($request->maksimal_peserta) ? $request->maksimal_peserta : 100,

                // UKTT
                // 'id_jenjang'                => $request->id_jenjang,
                'id_uktt'                   => $request->id_uktt,
                'uktt_level'                => $request->uktt_level,
                'uktt_nilai_minimal'        => $request->uktt_nilai_minimal,
                'uktt_id_level'             => $request->uktt_id_level,
            ];

            $insert = Produk::create($data);

            if (!empty($request->bonus)) {

                $user = auth()->user();

                $mbti = TbMbti::where('status', 'Aktif')->first();

                MbtiJawaban::create([
                    'id_mbti'              => $mbti->id,
                    'id_user'                => !empty($request->id_user) ? $request->id_user : $user->id,
                    'id_kepribadian'              => null
                ]);
            }

            if (!empty($request->id_tryout)) {

                foreach ($request->id_tryout as $key => $value) {

                    ProdukPaket::create([
                        'id_produk'     => $insert->id,
                        'id_tryout'     => $value,
                    ]);
                }
            }

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di inputkan', 'data' => $insert], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tryout  $tryout
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Produk::with(['transaksi_user', 'tryout_user'])->findOrFail($id);

        $data['is_task_start'] = $data->tryout_user && $data->tryout_user->count() > 0 ? true : false;
        $data['is_task_done'] = false;

        $data['is_result_openable'] = true;

        if (
            isset($data['jenis_produk']) && $data['jenis_produk'] == 'Masal'
            && isset($data['tanggal_berakhir']) && !empty($data['tanggal_berakhir'])
        ) {
            if (time() >= strtotime($data['tanggal_berakhir'])) {
                $data['is_result_openable'] = true;
            } else {
                $data['is_result_openable'] = false;
            }
        }

        unset($data['transaksi_user']);
        unset($data['tryout_user']);

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tryout  $tryout
     * @return \Illuminate\Http\Response
     */
    public function show_detail($id)
    {
        $data = Produk::select(
            'id',
            'nama_produk',
            'harga_produk',
            'jenis_produk',
            'kategori_produk',
            'pakai_sertifikat',
            'status_produk',
            'tipe_paket',
            'tanggal_mulai',
            'tanggal_berakhir',
            'pakai_perankingan',
            'perhitungan',
            'deskripsi_produk',
            'bonus',
            'id_uktt',
            'uktt_level',
            'uktt_nilai_minimal',
            'uktt_id_level',
        )
            ->with([
                'transaksi_user' => function ($subquery) {
                    return $subquery->orderByDesc('tanggal_transaksi');
                }, 'tryout_user_user',
                'level:id,nama_level,minimal_rate_mengajar,minimal_total_mengajar,maksimal_waktu_mengajar'
            ])->findOrFail($id);

        $relasi = $data->list()->with([
            'tryout:id,judul,deskripsi,kategori,jenis_soal,kelompok_soal,template_soal,alokasi_waktu,jeda_waktu',
            'tryout.soal.mapel:id,nama_mapel',
            'tryout.soal.pertanyaan',
        ])->get();

        $user = auth()->user();

        if ($data->kategori_produk == 'UKTT' && $user->role_user == 'teacher') {
            // cek prasayarat uktt
            $prasyarat['level_tentor_terpenuhi'] = false;
            $prasyarat['uktt_sebelumnya_lulus'] = false;
            $prasyarat['pesan'] = 'Tentor belum memenuhi persyaratan.';

            $teacher = Teacher::with('level')->where('id_teacher', $user->id)->first();

            if ($teacher->id_level >= $data->uktt_id_level) {

                $tryoutUser = TryoutUser::where('id_user', $user->id)->where('id_produk', $data->id_uktt)->orderBy('created_at', 'desc')->first();

                if ($data->id_uktt != NULL && $tryoutUser && $tryoutUser->waktu_selesai != NULL) {
                    $prasyarat['level_tentor_terpenuhi'] = true;
                    $prasyarat['uktt_sebelumnya_lulus'] = true;
                    $prasyarat['pesan'] = 'Tentor memenuhi persyaratan.';
                } else if ($data->id_uktt == NULL) {
                    $prasyarat['level_tentor_terpenuhi'] = true;
                    $prasyarat['uktt_sebelumnya_lulus'] = true;
                    $prasyarat['pesan'] = 'Tentor memenuhi persyaratan.';
                } else {
                    $prasyarat['level_tentor_terpenuhi'] = true;
                    $prasyarat['uktt_sebelumnya_lulus'] = false;
                    $prasyarat['pesan'] = 'Tentor belum memenuhi persyaratan.';
                }
            }

            $data->prasyarat = $prasyarat;
        }

        if ($data->kategori_produk == 'UKTT') {
            $jenis_transaksi = 'UKTT';
        } else if (in_array($data->kategori_produk, ['UTBK', 'ASPD'])) {
            $jenis_transaksi = 'Tryout';
        }

        $total_pengerjaan = TryoutUser::where('id_produk', $id)->groupBy('referensi')->count();
        $total_transaksi = Transaksi::where('id_produk', $id)->where('jenis_transaksi', $jenis_transaksi)->whereIn('status', ['Menunggu Verifikasi', 'Sudah Diverifikasi'])->count();


        $tryout = [];
        $total_waktu = 0;
        $total_soal = 0;

        // return response()->json($data);

        foreach ($relasi as $key => $value) {

            // print_r($value);

            $soal = [];
            $total_waktu += $value->tryout->alokasi_waktu;
            $jumlah_soal  = 0;

            foreach ($value->tryout->soal as $row) {

                $count_pertanyaan = count($row->pertanyaan);
                $soal[] = [
                    'id'            => $row->id,
                    'mapel'         => $row->mapel,
                    'jenis_soal'         => $row->jenis_soal,
                    'kelompok_soal'         => $row->kelompok_soal,
                    'jumlah_soal'   => $count_pertanyaan,
                ];

                $jumlah_soal += $count_pertanyaan;
            }
            $total_soal +=  $jumlah_soal;

            $tryout[] = [
                'id'            => $value->tryout->id,
                'id_produk_paket' => $value->id,
                'judul'         => $value->tryout->judul,
                'deskripsi'     => $value->tryout->deskripsi,
                'kategori'      => $value->tryout->kategori,
                'jumlah_soal'   => $jumlah_soal,
                'jenis_soal'    => $value->tryout->jenis_soal,
                'kelompok_soal' => $value->tryout->kelompok_soal,
                'template_soal' => $value->tryout->template_soal,
                'soal'          => $soal,
                'alokasi_waktu' => $value->tryout->alokasi_waktu
            ];
        }

        $label = [
            'harga_label'        => 'Rp ' . number_format($data->harga_produk, 0, ',', '.'),
            'tanggal_mulai_label' =>  !empty($data->tanggal_mulai) ? Carbon::parse($data->tanggal_mulai)->locale('id')->format('d/M/Y  H:i') : null,
            'tanggal_berakhir_label' => !empty($data->tanggal_berakhir) ? Carbon::parse($data->tanggal_berakhir)->locale('id')->format('d/M/Y  H:i') : null,
        ];


        $array = [
            'produk' => collect($data)->merge($label),
            'total_waktu' => $total_waktu,
            'total_soal' => $total_soal,
            'tryout' => $tryout,
            'total_pengerjaan' => $total_pengerjaan,
            'total_transaksi' => $total_transaksi,
            // 'transaksi' => $data->transaksi_user,
            // 'ujian_user' => $data->tryout_user,
            // 'is_enrolled' => $data->transaksi_user ? true : false,
            // 'is_paid' => $data->transaksi_user && $data->transaksi_user->status == 'Sudah Diverifikasi',
            // 'is_task_start' => $data->tryout_user ? true : false,
            // 'is_task_done' => $data->tryout_user && !empty($data->tryout_user->waktu_selesai) ? true : false
        ];

        $tryoutUser_user = $data->tryout_user_user->toArray();

        if ($user->role_user === 'siswa') {

            // $ujian = TryoutUser::where('id_produk', $data->id)->where('id_user', $user->id)->get()->toArray();
            // dd($data->tryout_user);

            $array['transaksi'] = $data->transaksi_user;
            // $array['ujian_user'] = $data->tryout_user;
            $array['is_enrolled'] = $data->transaksi_user ? true : false;

            if ($data->transaksi_user) {
                if ($data->transaksi_user->status == 'Kadaluarsa') {
                    $array['is_enrolled'] = false;
                }
            }

            $array['is_paid'] = $data->transaksi_user && $data->transaksi_user->status == 'Sudah Diverifikasi';
            $array['is_task_start'] = $tryoutUser_user && count($tryoutUser_user) > 0 ? true : false;

            $is_task_done = false;
            $array['is_task_progress'] = false;
            $array['is_result_openable'] = true;
            $array['is_expired_test'] = false;

            if (
                $array['produk'] &&
                isset($array['produk']['jenis_produk']) && $array['produk']['jenis_produk'] == 'Masal'
                && isset($array['produk']['tanggal_berakhir']) && !empty($array['produk']['tanggal_berakhir'])
            ) {
                if (time() >= strtotime($array['produk']['tanggal_berakhir'])) {
                    $array['is_result_openable'] = true;
                    $array['is_expired_test'] = true;
                } else {
                    $array['is_result_openable'] = false;
                }
            }

            if (count($tryout) == 1 && !empty($tryoutUser_user) && !empty(@$tryoutUser_user[0]['waktu_selesai'])) {
                $is_task_done = true;
            } else
                // jika tryout dalam 1 produk lebih dari 1
                if (count($tryout) > 1) {

                    // maka cek apakah semua tryout sudah selesai dikerjakan
                    $tryout_user_selesai = array_filter($tryoutUser_user, function ($item) {
                        return $item['waktu_selesai'] !== null;
                    });

                    if ($tryoutUser_user && count($tryout_user_selesai) == count($tryout)) {
                        $is_task_done = true;
                    } elseif ($tryoutUser_user && count($tryout_user_selesai) < count($tryout)) {
                        $array['is_task_progress'] = true;
                    }
                } else {
                    if (!empty($tryoutUser_user)) {
                        $array['is_task_progress'] = true;
                    }
                }

            $array['is_task_done'] = $is_task_done;
        } else if ($user->role_user == 'teacher') {

            // if ($data->harga_produk > 0) {

            // }
            $array['transaksi'] = $data->transaksi_user;
            $array['is_enrolled'] = $data->transaksi_user ? true : false;

            if ($data->transaksi_user) {
                if ($data->transaksi_user->status == 'Kadaluarsa') {
                    $array['is_enrolled'] = false;
                }
            }

            $array['is_paid'] = $data->transaksi_user && $data->transaksi_user->status == 'Sudah Diverifikasi';

            $array['is_task_start'] = $tryoutUser_user && count($tryoutUser_user) > 0 ? true : false;

            $is_task_done = false;
            $array['is_task_progress'] = false;
            $array['is_result_openable'] = true;

            if (count($tryout) == 1 && !empty($tryoutUser_user) && !empty(@$tryoutUser_user[0]['waktu_selesai'])) {
                $is_task_done = true;
            } else
                // jika tryout dalam 1 produk lebih dari 1
                if (count($tryout) > 1) {

                    // maka cek apakah semua tryout sudah selesai dikerjakan
                    $tryout_user_selesai = array_filter($tryoutUser_user, function ($item) {
                        return $item['waktu_selesai'] !== null;
                    });

                    // count($tryout_user_selesai);
                    // // die;

                    if ($tryoutUser_user && count($tryout_user_selesai) == count($tryout)) {
                        $is_task_done = true;
                    } elseif ($tryoutUser_user && count($tryout_user_selesai) < count($tryout)) {
                        $array['is_task_progress'] = true;
                    }
                } else {
                    if (!empty($tryoutUser_user)) {
                        $array['is_task_progress'] = true;
                    }
                }

            $array['is_task_done'] = $is_task_done;
        }

        // unset($array['produk']['transaksi_user']);
        unset($array['produk']['tryout_user']);

        return response()->json([
            'success' => true,
            'data' => $array
        ]);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tryout  $tryout
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Produk::findOrFail($id);

        $validate = Validator::make($request->all(), [
            'nama_produk'     => 'required|unique:produks,nama_produk,' . $id . ',id,deleted_at,NULL',
            'kategori_produk' => 'required',
            'jenis_produk'    => Rule::requiredIf($request->kategori_produk != 'UKTT'),
            'harga_produk'    => Rule::requiredIf($request->kategori_produk != 'UKTT'),
            'pakai_sertifikat' => Rule::requiredIf($request->kategori_produk != 'UKTT'),
            'status_produk'   => Rule::requiredIf($request->kategori_produk != 'UKTT'),
            'tipe_paket'      => Rule::requiredIf($request->kategori_produk != 'UKTT'),
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {


            $update->nama_produk               = $request->nama_produk;
            $update->kategori_produk           = $request->kategori_produk;
            $update->jenis_produk              = $request->jenis_produk;
            $update->harga_produk              = $request->harga_produk;
            $update->pakai_sertifikat          = $request->pakai_sertifikat;
            $update->status_produk             = $request->status_produk;
            $update->tipe_paket                = $request->tipe_paket;
            $update->tanggal_mulai             = $request->tanggal_mulai;
            $update->tanggal_berakhir          = $request->tanggal_berakhir;
            $update->pakai_perankingan         = $request->pakai_perankingan;
            $update->perhitungan               = $request->perhitungan;
            $update->deskripsi_produk          = $request->deskripsi_produk;
            $update->bonus                     = $request->bonus;
            $update->maksimal_peserta          = $request->maksimal_peserta;

            // UKTT
            // $update->id_jenjang                = $request->id_jenjang;
            $update->id_uktt                   = $request->id_uktt;
            $update->uktt_level                = $request->uktt_level;
            $update->uktt_nilai_minimal        = $request->uktt_nilai_minimal;
            $update->uktt_id_level             = $request->uktt_id_level;

            $update->save();

            if (!empty($request->id_tryout)) {

                if ($request->kategori_produk == 'UKTT') {
                    ProdukPaket::where('id_produk', $id)->forceDelete();
                }

                foreach ($request->id_tryout as $key => $value) {

                    ProdukPaket::create([
                        'id_produk'     => $id,
                        'id_tryout'     => $value,
                    ]);
                }
            }

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $update], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tryout  $tryout
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete  = Produk::destroy($id);
        $transaksi = Transaksi::where('id_produk', $id)->whereIn('status', ['Menunggu Verifikasi', 'Sudah Diverifikasi'])->count();
        $tryout_user = TryoutUser::where('id_produk', $id)->groupBy('referensi')->count();

        if ($transaksi > 0 || $tryout_user > 0) {
            return response()->json([
                'success' => false,
                'messages' => ['error' => ['Produk yang sudah memiliki transaksi atau dikerjakan oleh siswa tidak dapat dihapus! Silakan buat baru.']],
            ], 422);
        } else {
            return response()->json([
                'success' => true,
                'messages' => 'data berhasil di hapus',
                'data' => $id,
            ]);
        }
    }


    // UJIAN

    public function get_produk_tryout(Request $request, $id_produk)
    {
        $data = Produk::select("*")->findOrFail($id_produk);


        $relasi = $data->list()->with([
            'tryout',
            'tryout.soal',
            'tryout.soal.pertanyaan' => function ($query) {
                return $query->where('soal_pertanyaans.parent_soal_pertanyaan', '=', null)->get();
            },
            'tryout.soal.pertanyaan.pertanyaan_child',
            'tryout.soal.mapel:id,nama_mapel'
        ])->get();

        $tryout = $data->tryout;

        foreach ($relasi as $key => $value) {
            $tryout[$key] = $value->tryout;
        }

        $data->tryout = $tryout;

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    public function update_start(Request $request, $id)
    {
        $update = Transaksi::findOrFail($id);

        $validate = Validator::make($request->all(), [
            'status'   => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $update->status                 = $request->status;
            if ($request->status == 'Sudah Diverifikasi') {
                $update->tanggal_konfirmasi = Carbon::now()->toDateTimeString();
            }
            $update->save();

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $update], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }
}
