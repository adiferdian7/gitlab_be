<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\TbMbti;
use App\Models\MbtiDimensi;
use App\Models\MbtiJawaban;
use App\Models\MbtiPertanyaan;
use App\Models\Transaksi;

class MbtiController extends Controller
{
    public function index(Request $request)
    {
        $limit = $request->paginate ?  $request->paginate : 10;
        $q = $request->q;
        $data = TbMbti::select('id', 'judul', 'panduan', 'deskripsi', 'harga', 'sertifikat', 'status')
            ->where('judul', 'like', '%' . $q . '%')
            ->paginate($limit);

        $data = $data->toArray();

        if (isset($data['data'])) {
            foreach ($data['data'] as $key => $mbti) {

                $dimensi = MbtiDimensi::with('pertanyaan')->where('id_mbti', $mbti['id'])->get();
                $data['data'][$key]['jumlah_soal'] = 0;

                foreach ($dimensi as $value) {
                    $data['data'][$key]['jumlah_soal'] += $value->pertanyaan ? count($value->pertanyaan) : 0;
                }
            }
        }


        return response()->json(['success' => true, 'data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'judul'            => 'required|unique:tb_mbtis,judul,NULL,id,deleted_at,NULL',
            'deskripsi'      => 'required',
            'panduan'        => 'required',
            'harga'        => 'required'
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {


            $insert = TbMbti::create([
                'judul'        => $request->judul,
                'deskripsi'    => $request->deskripsi,
                'panduan'      => $request->panduan,
                'harga'      => $request->harga,
                'status'      => $request->status ?? 'Tidak Aktif',
                'sertifikat'      => $request->sertifikat ?? 'Pakai',
            ]);

            if ($request->status && $request->status == 'Aktif') {
                $allMBTI = TbMbti::where('id', '!=', $insert->id)->update(['status' => 'Tidak Aktif']);
            }

            if (!empty($request->id_dimensi)) {
                foreach ($request->id_dimensi as $value) {
                    MbtiDimensi::create([
                        'id_mbti'       => $insert->id,
                        'id_dimensi'    => $value,
                    ]);
                }
            }

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di inputkan', 'data' => $insert], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = TbMbti::with([
            'mbti_dimensi:id,id_mbti,id_dimensi',
            'mbti_dimensi.dimensi:id,nama,psikologi_1,psikologi_2,deskripsi',
            'mbti_dimensi.pertanyaan:id,id_mbti_dimensi,pertanyaan,opsi'
        ])->select('id', 'judul', 'deskripsi', 'panduan', 'sertifikat', 'status', 'harga')->findOrFail($id);


        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = TbMbti::findOrFail($id);

        $validate = Validator::make($request->all(), [
            'judul'            => 'required|unique:tb_mbtis,judul,' . $id . ',id,deleted_at,NULL',
            'deskripsi'      => 'required',
            'panduan'        => 'required',
            'harga'        => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $update->judul        = $request->judul;
            $update->deskripsi    = $request->deskripsi;
            $update->panduan      = $request->panduan;
            $update->sertifikat      = $request->sertifikat ?? 'Pakai';
            $update->status      = $request->status ?? 'Tidak Aktif';
            $update->harga      = $request->harga;

            $allMBTI = TbMbti::where('id', '!=', $id);
            if ($request->status == 'Aktif') {
                $allMBTI->update(['status' => 'Tidak Aktif']);
            } else {
                if ($allMBTI->where('status', 'Aktif')->count() < 1) {
                    return response()->json([
                        'success' => false,
                        'messages' => ['status' => ['Minimal harus ada 1 MBTI yang Aktif!']]
                    ], 422);
                }
            }
            
            $update->save();

            if (!empty($request->id_dimensi)) {

                $update->mbti_dimensi()->forceDelete();

                foreach ($request->id_dimensi as $value) {
                    MbtiDimensi::create([
                        'id_mbti'       => $update->id,
                        'id_dimensi'    => $value,
                    ]);
                }
            }

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $update], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $delete = TbMbti::findOrFail($id);
        $delete->mbti_dimensi()->forceDelete();
        $delete->delete();

        return response()->json([
            'success' => true,
            'messages' => 'data berhasil di hapus',
            'data' => $id,
        ]);
    }

    public function update_status(Request $request, $id)
    {
        $update = TbMbti::findOrFail($id);

        $validate = Validator::make($request->all(), [
            'status'      => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $update->status        = $request->status;

            $allMBTI = TbMbti::where('id', '!=', $id);
            if ($request->status == 'Aktif') {
                $allMBTI->update(['status' => 'Tidak Aktif']);
            } else {
                if ($allMBTI->where('status', 'Aktif')->count() < 1) {
                    return response()->json([
                        'success' => false,
                        'messages' => ['status' => ['Minimal harus ada 1 MBTI yang Aktif!']]
                    ], 422);
                }
            }

            $update->save();

            DB::commit();

            return response()->json(['success' => true, 'message' => 'Status berhasil di update', 'data' => $update], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    public function show_active()
    {
        // $data = TbMbti::with(['transaksi'=>function($query){
        //     return $query->where('id_user', auth()->user()->id);
        // }, 'tes'])->where('status', 'Aktif')->first();
        
        $data = TbMbti::where('status', 'Aktif')->first();

        $transaksi = DB::table('transaksis')
        ->select(['*',
            'transaksis.id_user as id_user',
            'transaksis.id as id',
            'transaksis.status as status',
            'tb_mbtis.status as status_mbti', 
            'mbti_jawabans.deleted_at as mbti_jawaban_deleted', 'transaksis.deleted_at as transaksi_deleted'])
        ->leftJoin('tb_mbtis', 'tb_mbtis.id', '=', 'transaksis.id_produk')
        ->leftJoin('mbti_jawabans', 'mbti_jawabans.id_user', '=', 'transaksis.id_user')
        ->whereIn('jenis_transaksi', ['MBTI','Bonus MBTI'])
        ->whereNotIn('transaksis.status', ['Dibatalkan'])
        ->whereNull('transaksis.deleted_at')
        ->where('transaksis.id_user', auth()->user()->id)
        // ->where('mbti_jawabans.deleted_at', NULL)
        // ->groupBy('id_mbti')
        ->orderBy('transaksis.created_at', 'desc')
        ->get()->toArray();

        $foundNotNull = array_filter($transaksi, function($item) {
            // return $item->mbti_jawaban_deleted == NULL;
            return $item->transaksi_deleted == NULL;
        });

        if($foundNotNull) {
            $data->transaksi = array_values($foundNotNull)[0];
            if($data->transaksi->jenis_transaksi == 'Bonus MBTI') {
                $trx_induk = Transaksi::where('kode', $data->transaksi->kode)->whereNotIn('jenis_transaksi', ['MBTI', 'Bonus MBTI'])->first();
                if($trx_induk) {
                    $data->transaksi->id_induk = $trx_induk->id;
                } else {
                    $data->transaksi->id_induk = null;
                }
            } else {
                $data->transaksi->id_induk = null;
            }
        } else {
            $data->transaksi = null;
        }

        // $data->transaksi = $transaksi;

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    public function mbti_user()
    {
        $data = MbtiJawaban::with(['kepribadian'])
        ->where('id_user', auth()->user()->id)
        ->orderByDesc('created_at')
        ->first();
        
        return response()->json([
            'success' => $data ? true : false,
            'data' => $data
        ]);
    }

    public function nomor_soal(Request $request, $id_mbti)
    {
        $mbti = TbMbti::with([
            'mbti_dimensi.dimensi',
            'mbti_dimensi.pertanyaan',
        ])->findOrFail($id_mbti);


        $mbti_jawaban = MbtiJawaban::where('id_mbti', $mbti->id)->where('id_user', auth()->user()->id)->first();
        
        if(!$mbti_jawaban) {
            return response()->json([
                'success' => false,
                'messages' => 'MBTI Jawaban not found'
            ], 404);
        }
        // return response()->json($mbti);


        $mbti_dimensi = $mbti->mbti_dimensi;

        $index = 0;
        $nomor = 1;
        $data_nomor = [];
        $data_jawaban_placeholder = [];

        foreach ($mbti_dimensi as $key => $m_dimensi) {
            $m_pertanyaan = $m_dimensi->pertanyaan;
            
            $nama_dimensi = $m_dimensi->dimensi->nama;
            
            foreach ($m_pertanyaan as $keyp => $pertanyaan) {
                
                $id_pertanyaan = $pertanyaan->id;
                $data_nomor[$index]['nomor'] = $nomor;
                $data_nomor[$index]['id_mbti_pertanyaan'] = $id_pertanyaan;
                $data_nomor[$index]['nama_dimensi'] = $nama_dimensi;
                $data_nomor[$index]['pertanyaan'] = $pertanyaan->pertanyaan;
                $data_nomor[$index]['opsi_pertanyaan'] = $pertanyaan->opsi;

                // untuk placeholder aja
                $data_jawaban_placeholder[$nomor]['id_mbti_pertanyaan'] = $id_pertanyaan;
                $data_jawaban_placeholder[$nomor]['id_mbti_jawaban'] = $mbti_jawaban->id;
                $data_jawaban_placeholder[$nomor]['jawaban_user'] = null;

                $index++;
                $nomor++;
            }
        }

        return response()->json([
            'success' => true,
            'data' => [
                'pertanyaan' => $data_nomor,
                'jawaban_placeholder' => $data_jawaban_placeholder,
            ]
        ]);
    }



}
