<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\PerguruanTinggi;

class PerguruanTinggiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->paginate ?  $request->paginate : 10;
        $data = PerguruanTinggi::select('id','nama_perguruan','akreditasi')
                    ->where('nama_perguruan', 'like', '%' .$request->q. '%')
                    ->orWhere('akreditasi', 'like', '%' .$request->q. '%')
                    ->paginate($limit);

        return response()->json(['success' => true, 'data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'nama_perguruan'   => 'required|unique:perguruan_tinggi,nama_perguruan,NULL,id,deleted_at,NULL',
            'akreditasi'       => 'required',
        ]);

        if($validate->fails()) {
            return response()->json([
                'success' => false, 
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $insert = PerguruanTinggi::create([
                'nama_perguruan' => $request->nama_perguruan,
                'akreditasi'    => $request->akreditasi, 
            ]);

            DB::commit();

             return response()->json(['success' => true, 'message' => 'data berhasil di inputkan', 'data' => $insert], 201);

        } catch (\Exception $e) {
             DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = PerguruanTinggi::findOrFail($id);

        return response()->json([
            'success' => true, 
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $perguruan = PerguruanTinggi::findOrFail($id);

        $validate = Validator::make($request->all(), [
            'nama_perguruan'   => 'required|unique:perguruan_tinggi,nama_perguruan,'.$id.',id,deleted_at,NULL',
            'akreditasi' => 'required',
        ]);

        if($validate->fails()) {
            return response()->json([
                'success' => false, 
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $perguruan->nama_perguruan = $request->nama_perguruan;
            $perguruan->akreditasi = $request->akreditasi;
            $perguruan->save();

            DB::commit();

             return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $perguruan], 201);

        } catch (\Exception $e) {
             DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete  = PerguruanTinggi::destroy($id);
        return response()->json([
            'success' => true,
            'messages' => 'data berhasil di hapus',
            'data' => $id,
        ]);
    }
}
