<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Validation\Rule;
use App\Models\Bank;
use Carbon\Carbon;

class BankController extends Controller
{
    public function index(Request $request)
    {
        $limit = $request->paginate ?  $request->paginate : 10;
        $q = $request->q;
        $where = [];

        if(isset($request->aktif) && !empty($request->aktif)) {
            $where['aktif'] = $request->aktif;
        }

        $data = Bank::where($where)->paginate($limit);
        return response()->json(['success' => true, 'data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'nama_bank' => 'required',
            'nama_pemilik' => 'required',
            'nomor_rekening' => 'required',
        ]);

        if($validate->fails()) {
            return response()->json([
                'success' => false, 
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $insert = Bank::create([
                'nama_bank' 		=> $request->nama_bank,
	            'nama_pemilik' 		=> $request->nama_pemilik,
	            'nomor_rekening' 	=> $request->nomor_rekening,
	            'cabang'			=> $request->cabang,
            ]);

            DB::commit();

             return response()->json(['success' => true, 'message' => 'data berhasil di inputkan', 'data' => $insert], 201);

        } catch (\Exception $e) {
             DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Bank::findOrFail($id);

        return response()->json([
            'success' => true, 
            'data' => $data
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Bank::findOrFail($id);

        $validate = Validator::make($request->all(), [
           	'nama_bank' => 'required',
            'nama_pemilik' => 'required',
            'nomor_rekening' => 'required',
        ]);

        if($validate->fails()) {
            return response()->json([
                'success' => false, 
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

        	$update->nama_bank		= $request->nama_bank;
            $update->nama_pemilik	= $request->nama_pemilik;
            $update->nomor_rekening = $request->nomor_rekening;
            $update->cabang			= $request->cabang;
            $update->save();

            DB::commit();

             return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $update], 200);

        } catch (\Exception $e) {
             DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
      
        $delete  = Bank::destroy($id);
        return response()->json([
            'success' => true,
            'messages' => 'data berhasil di hapus',
            'data' => $id,
        ]);
    }

    public function updateMultiple(Request $request)
    {

        $validate = Validator::make($request->all(), [
           	'banks' => 'required',
        ]);

        if($validate->fails()) {
            return response()->json([
                'success' => false, 
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            foreach ($request->banks as $key => $bank) {
               Bank::where('id', $bank['id'])->update([
                   'nama_bank' => $bank['nama_bank'],
                   'nama_pemilik' => $bank['nama_pemilik'],
                   'nomor_rekening' => $bank['nomor_rekening'],
                   'cabang' => $bank['cabang'] ?? NULL,
                   'aktif' => $bank['aktif'] ?? 'Ya',
               ]);
            }

            DB::commit();

             return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $request->banks], 200);

        } catch (\Exception $e) {
             DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

}
