<?php

namespace App\Http\Controllers;

use App\Models\KursusJadwal;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class KursusJadwalController extends Controller
{
    function __construct()
    {
        $this->kursusJadwalModel = new KursusJadwal();
    }

    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'id_kursus'   => 'required|exists:kursus,id,deleted_at,NULL',
            'hari'        => ['required', Rule::in(['Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu'])],
            'jam_mulai'   => 'required',
            'jam_akhir'   => 'required',
        ], $this->kursusJadwalModel->getValidationMessage());

        if ($validate->fails()) {
            return responseFail($validate->messages(), 422);
        }

        $result = $this->kursusJadwalModel->insertData('insertData', $request->all());

        if (isset($result['error'])) {
            return responseFail($result, 500);
        } else {
            return responseInsert($result);
        }
    }

    public function update(Request $request, $id)
    {
        $req = $request->all();
        $req['id'] = $id;

        $validate = Validator::make($req, [
            'id' => 'required|exists:kursus_jadwal,id',
            'hari'        => ['required', Rule::in(['Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu'])],
            'jam_mulai'   => 'required',
            'jam_akhir'   => 'required',
        ], $this->kursusJadwalModel->getValidationMessage());

        if ($validate->fails()) {
            return responseFail($validate->messages(), 422);
        }

        $result = $this->kursusJadwalModel->updateData('updateData', $req);

        if (isset($result['error'])) {
            return responseFail($result, 500);
        } else {
            return responseUpdate($result);
        }
    }

  public function delete(Request $request, $id)
  {
    $req = $request->all();
    $req['id'] = $id;

    $validate = Validator::make($req, [
      'id' => 'required|exists:kursus_jadwal,id',
    ], $this->kursusJadwalModel->getValidationMessage());

    if ($validate->fails()) {
      return responseFail($validate->messages(), 422);
    }

    $result = $this->kursusJadwalModel->deleteData('deleteData', ['id' => $id]);

    if (isset($result['error'])) {
      return responseFail($result);
    }
    return responseDeleted($result);
  }

}
