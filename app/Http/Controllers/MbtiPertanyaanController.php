<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\MbtiPertanyaan;

class MbtiPertanyaanController extends Controller
{
    public function index(Request $request)
    {
        $limit = $request->paginate ?  $request->paginate : 10;
        $q = $request->q;
        $data = MbtiPertanyaan::paginate($limit);

        return response()->json(['success' => true, 'data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'id_mbti_dimensi' => 'required',
            'pertanyaan'      => 'required',
        ]);

        if($validate->fails()) {
            return response()->json([
                'success' => false, 
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $insert = MbtiPertanyaan::create([
                'id_mbti_dimensi' => $request->id_mbti_dimensi,
                'pertanyaan'      => $request->pertanyaan,
                'opsi'		      => $request->opsi,  
            ]);

            DB::commit();

             return response()->json(['success' => true, 'message' => 'data berhasil di inputkan', 'data' => $insert], 201);

        } catch (\Exception $e) {
             DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = MbtiPertanyaan::findOrFail($id);

        return response()->json([
            'success' => true, 
            'data' => $data
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = MbtiPertanyaan::findOrFail($id);

        $validate = Validator::make($request->all(), [
            'id_mbti_dimensi' => 'required',
            'pertanyaan'      => 'required',
        ]);

        if($validate->fails()) {
            return response()->json([
                'success' => false, 
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

                $update->id_mbti_dimensi = $request->id_mbti_dimensi;
                $update->pertanyaan      = $request->pertanyaan;
                $update->opsi            = $request->opsi;  
                $update->save();
                
            DB::commit();

             return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $update], 201);

        } catch (\Exception $e) {
             DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
      
        $delete  = MbtiPertanyaan::destroy($id);
        return response()->json([
            'success' => true,
            'messages' => 'data berhasil di hapus',
            'data' => $id,
        ]);
    }
}
