<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Models\cmsContent1;
use App\Models\cmsContent2;
use App\Models\cmsContent3;
use App\Models\cmsContent4;
use App\Models\cmsContent5;
use App\Models\cmsContent6;
use App\Models\cmsContent7;
use App\Models\cmsContent8;
use App\Models\cmsContent9;

class contentController extends Controller
{
    public function getContentHalamanUtama()
    {
        $content1 = cmsContent1::limit(10)->get();
        $content2 = cmsContent2::limit(10)->get();
        $content3 = cmsContent3::limit(10)->get();
        $content4 = cmsContent4::limit(10)->get();
        $content5 = cmsContent5::limit(10)->get();
        $content6 = cmsContent6::limit(10)->get();
        $content7 = cmsContent7::limit(10)->get();
        $content8 = cmsContent8::limit(10)->get();
        $content9 = cmsContent9::limit(10)->get();

        $data = json_encode([
            'dataContent1' => $content1,
            'dataContent2' => $content2,
            'dataContent3' => $content3,
            'dataContent4' => $content4,
            'dataContent5' => $content5,
            'dataContent6' => $content6,
            'dataContent7' => $content7,
            'dataContent8' => $content8,
            'dataContent9' => $content9,
        ]);

        return response()->json(['success' => true, 'message' => 'data berhasil didapatkan', 'data' => json_decode($data)], 201);
    }

    public function ContentHalamanUtama(Request $request)
    {
        DB::beginTransaction();

        try {

            if (isset($request['konten1'])) {
                $validate = Validator::make($request['konten1'], [
                    'banner'   => 'required|image:jpeg,png,jpg,gif,svg|max:5120',
                    'gambar'   => 'required|image:jpeg,png,jpg,gif,svg|max:5120',
                    'judul'   => 'required',
                    'text'   => 'required',
                    'sub_content'   => 'required',
                ]);

                if ($validate->fails()) {
                    return response()->json([
                        'success' => false,
                        'messages' => $validate->messages()
                    ], 422);
                }

                $bannerLink = '';
                $gambarLink = '';

                $uploadFolder = 'landingPage';
                $banner = $request->file('konten1.banner');
                $gambar = $request->file('konten1.gambar');
                $bannerLink = $banner->store($uploadFolder, 'public');
                $gambarLink = $gambar->store($uploadFolder, 'public');

                $dataContent1[] = [
                    'gambar' => $gambarLink,
                    'banner' => $bannerLink,
                    'judul' => $request['konten1']['judul'],
                    'text' => $request['konten1']['text'],
                    'sub_content' => json_encode($request['konten1']['sub_content']),
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString(),
                ];

                $id = $request['konten1']['id'];
                if (isset($id)) {
                    cmsContent1::where('id', $id)->update($dataContent1);
                } else {
                    cmsContent1::insert($dataContent1);
                }
            } else {
                $dataContent1 = null;
            }

            if (isset($request['konten2'])) {
                $dataContent2 = [];
                foreach ($request['konten2']['data'] as $key => $val) {
                    $validate = Validator::make($val, [
                        'carousel'   => 'required|image:jpeg,png,jpg,gif,svg|max:5120',
                        'judul'   => 'required',
                        'text'   => 'required',
                    ]);

                    if ($validate->fails()) {
                        return response()->json([
                            'success' => false,
                            'messages' => $validate->messages()
                        ], 422);
                    }

                    $carouselLink = '';
                    $uploadFolder = 'landingPage';
                    $carousel = $val['carousel'];
                    $carouselLink = $carousel->store($uploadFolder, 'public');
                    $dataContent = [
                        'carousel' => $carouselLink,
                        'judul' => $val['judul'],
                        'text' => $val['text'],
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString(),
                    ];
                    array_push($dataContent2, $dataContent);
                    $id = $val['id'];
                    if (isset($id)) {
                        cmsContent2::where('id', $id)->update($dataContent);
                    } else {
                        cmsContent2::insert($dataContent);
                    }
                }
            } else {
                $dataContent2 = null;
            }

            if (isset($request['konten3'])) {
                $dataContent3 = [];
                for ($i = 0; $i < count($request['konten3']['data']); $i++) {
                    $validate = Validator::make($request['konten3']['data'][$i], [
                        'id_content'   => 'required',
                        'judul'   => 'required',
                        'text'   => 'required',
                    ]);

                    if ($validate->fails()) {
                        return response()->json([
                            'success' => false,
                            'messages' => $validate->messages()
                        ], 422);
                    };

                    $dataContent3[] = [
                        'id_content' => $request['konten3']['data'][$i]['id_content'],
                        'sub_judul' => isset($request['konten3']['data'][$i]['sub_judul']) ? $request['konten3']['data'][$i]['sub_judul'] : null,
                        'judul' => $request['konten3']['data'][$i]['judul'],
                        'text' => $request['konten3']['data'][$i]['text'],
                        'tombol' => isset($request['konten3']['data'][$i]['tombol']) ? $request['konten3']['data'][$i]['tombol'] : null,
                        'link' => isset($request['konten3']['data'][$i]['link']) ? $request['konten3']['data'][$i]['link'] : null,
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString(),
                    ];
                    
                    $id = $request['konten3']['data'][$i]['id'];
                    if (isset($id)) {
                        cmsContent3::where('id', $id)->update($dataContent3);
                    } else {
                        cmsContent3::insert($dataContent3);
                    }
                }
            } else {
                $dataContent3 = null;
            }

            if (isset($request['konten4'])) {
                $dataContent4 = [];
                for ($i = 0; $i < count($request['konten4']['data']); $i++) {
                    $validate = Validator::make($val, [
                        'gambar'   => 'image:jpeg,png,jpg,gif,svg|max:5120',
                        'id_content'   => 'required',
                        'judul'   => 'required',
                        'text'   => 'required',
                    ]);

                    if ($validate->fails()) {
                        return response()->json([
                            'success' => false,
                            'messages' => $validate->messages()
                        ], 422);
                    };

                    $gambarLink = '';
                    $uploadFolder = 'landingPage';
                    $gambar = isset($val['gambar']) ? $val['gambar'] : "";
                    $gambarLink = ($gambar == "") ? "" : $gambar->store($uploadFolder, 'public');

                    $dataContent = [
                        'id_content' => $request['konten4']['data'][$i]['id_content'],
                        'judul' => $request['konten4']['data'][$i]['judul'],
                        'text' => $request['konten4']['data'][$i]['text'],
                        'tombol' => isset($request['konten4']['data'][$i]['tombol']) ? $request['konten4']['data'][$i]['tombol'] : null,
                        'link' => isset($request['konten4']['data'][$i]['link']) ? $request['konten4']['data'][$i]['link'] : null,
                        'gambar' => $gambarLink,
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString(),
                    ];
                    array_push($dataContent4, $dataContent);
                    $id = $request['konten4']['data'][$i]['id'];
                    if (isset($id)) {
                        cmsContent4::where('id', $id)->update($dataContent);
                    } else {
                        cmsContent4::insert($dataContent);
                    }
                }
            } else {
                $dataContent4 = null;
            }

            if (isset($request['konten5'])) {

                $validate = Validator::make($request['konten5'], [
                    'gambar'   => 'required|image:jpeg,png,jpg,gif,svg|max:5120',
                    'judul'   => 'required',
                    'text'   => 'required',
                    'tombol'   => 'required',
                    'link'   => 'required',
                ]);

                if ($validate->fails()) {
                    return response()->json([
                        'success' => false,
                        'messages' => $validate->messages()
                    ], 422);
                };

                $gambarLink = '';

                $uploadFolder = 'landingPage';
                $gambar = $request->file('konten5.gambar');
                $gambarLink = $gambar->store($uploadFolder, 'public');

                $dataContent5 = [
                    'judul' => $request['konten5']['judul'],
                    'text' => $request['konten5']['text'],
                    'tombol' => $request['konten5']['tombol'],
                    'link' => $request['konten5']['link'],
                    'gambar' => $gambarLink,
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString(),
                ];

                $id = $request['konten5']['id'];
                if (isset($id)) {
                    cmsContent5::where('id', $id)->update($dataContent5);
                } else {
                    cmsContent5::insert($dataContent5);
                }
            } else {
                $dataContent5 = null;
            }

            if (isset($request['konten6'])) {
                $validate = Validator::make($request['konten6'], [
                    'judul'   => 'required',
                    'sub_judul'   => 'required',
                    'text1'   => 'required',
                    'text2'   => 'required',
                    'tombol'   => 'required',
                    'link'   => 'required',
                ]);

                if ($validate->fails()) {
                    return response()->json([
                        'success' => false,
                        'messages' => $validate->messages()
                    ], 422);
                };

                $dataContent6[] = [
                    'judul' => $request['konten6']['judul'],
                    'text1' => $request['konten6']['text1'],
                    'sub_judul' => $request['konten6']['sub_judul'],
                    'text2' => $request['konten6']['text2'],
                    'tombol' => $request['konten6']['tombol'],
                    'link' => $request['konten6']['link'],
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString(),
                ];

                $id = $request['konten6']['id'];
                if (isset($id)) {
                    cmsContent6::where('id', $id)->update($dataContent6);
                } else {
                    cmsContent6::insert($dataContent6);
                }
            } else {
                $dataContent6 = null;
            }

            if (isset($request['konten7'])) {
                $validate = Validator::make($request['konten7'], [
                    'gambar'   => 'required|image:jpeg,png,jpg,gif,svg|max:5120',
                    'judul'   => 'required',
                    'sub_judul'   => 'required',
                    'text1'   => 'required',
                    'text2'   => 'required',
                    'tombol'   => 'required',
                    'link'   => 'required',
                ]);

                if ($validate->fails()) {
                    return response()->json([
                        'success' => false,
                        'messages' => $validate->messages()
                    ], 422);
                };

                $gambarLink = '';

                $uploadFolder = 'landingPage';
                $gambar = $request->file('konten7.gambar');
                $gambarLink = $gambar->store($uploadFolder, 'public');
                $dataContent7[] = [
                    'judul' => $request['konten7']['judul'],
                    'text1' => $request['konten7']['text1'],
                    'sub_judul' => $request['konten7']['sub_judul'],
                    'text2' => $request['konten7']['text2'],
                    'tombol' => $request['konten7']['tombol'],
                    'gambar' => $gambarLink,
                    'link' => $request['konten7']['link'],
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString(),
                ];
                $id = $request['konten7']['id'];
                if (isset($id)) {
                    cmsContent7::where('id', $id)->update($dataContent7);
                } else {
                    cmsContent7::insert($dataContent7);
                }
            } else {
                $dataContent7 = null;
            }

            if (isset($request['konten8'])) {
                $dataContent8 = [];
                for ($i = 0; $i < count($request['konten8']['data']); $i++) {
                    $validate = Validator::make($val, [
                        'foto'   => 'image:jpeg,png,jpg,gif,svg|max:5120',
                        'id_content'   => 'required',
                        'text'   => 'required',
                    ]);

                    if ($validate->fails()) {
                        return response()->json([
                            'success' => false,
                            'messages' => $validate->messages()
                        ], 422);
                    };

                    $fotoLink = '';
                    $uploadFolder = 'landingPage';
                    $foto = isset($val['foto']) ? $val['foto'] : null;
                    $fotoLink = ($foto == null) ? null : $foto->store($uploadFolder, 'public');

                    $dataContent = [
                        'judul' => isset($request['konten8']['data'][$i]['judul']) ? $request['konten8']['data'][$i]['judul'] : null,
                        'text' => $request['konten8']['data'][$i]['text'],
                        'foto' => $fotoLink,
                        'id_content' => $request['konten8']['data'][$i]['id_content'],
                        'nama' => isset($request['konten8']['data'][$i]['nama']) ? $request['konten8']['data'][$i]['nama'] : null,
                        'jurusan' => isset($request['konten8']['data'][$i]['jurusan']) ? $request['konten8']['data'][$i]['jurusan'] : null,
                        'created_at' => Carbon::now()->toDateTimeString(),
                        'updated_at' => Carbon::now()->toDateTimeString(),
                    ];
                    array_push($dataContent8, $dataContent);
                    $id = $request['konten8']['data'][$i]['id'];
                    if (isset($id)) {
                        cmsContent8::where('id', $id)->update($dataContent8);
                    } else {
                        cmsContent8::insert($dataContent8);
                    }
                }
            } else {
                $dataContent8 = null;
            }

            if (isset($request['konten9'])) {
                $validate = Validator::make($request['konten9'], [
                    'gambar'   => 'required|image:jpeg,png,jpg,gif,svg|max:5120',
                    'judul'   => 'required',
                    'text'   => 'required',
                    'tombol'   => 'required',
                    'link'   => 'required',
                ]);

                if ($validate->fails()) {
                    return response()->json([
                        'success' => false,
                        'messages' => $validate->messages()
                    ], 422);
                };

                $gambarLink = '';

                $uploadFolder = 'landingPage';
                $gambar = $request->file('konten9.gambar');
                $gambarLink = $gambar->store($uploadFolder, 'public');

                $dataContent9[] = [
                    'gambar' => $gambarLink,
                    'judul' => $request['konten9']['judul'],
                    'text' => $request['konten9']['text'],
                    'tombol' => $request['konten9']['tombol'],
                    'link' => $request['konten9']['link'],
                    'created_at' => Carbon::now()->toDateTimeString(),
                    'updated_at' => Carbon::now()->toDateTimeString(),
                ];
                $id = $request['konten9']['id'];
                if (isset($id)) {
                    cmsContent9::where('id', $id)->update($dataContent9);
                } else {
                    cmsContent9::insert($dataContent9);
                }
            } else {
                $dataContent9 = null;
            }

            $data = json_encode([
                'dataContent1' => $dataContent1,
                'dataContent2' => $dataContent2,
                'dataContent3' => $dataContent3,
                'dataContent4' => $dataContent4,
                'dataContent5' => $dataContent5,
                'dataContent6' => $dataContent6,
                'dataContent7' => $dataContent7,
                'dataContent8' => $dataContent8,
                'dataContent9' => $dataContent9,
            ]);
            DB::commit();
            return response()->json(['success' => true, 'message' => 'data berhasil diinputkan', 'data' => json_decode($data)], 201);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['success' => false, 'messages' => $e->getMessage(), 'line' => $e->getLine()], 400);
        }
    }

    public function deleteContentHalamanUtama(Request $request, $id)
    {
        $reqData =  $request->all();
        $reqData['id'] = $id;

        $validator = Validator::make($reqData, [
            'id'   => 'required',
        ]);

        if ($validator->fails()) {
            return responseFail($validator->messages());
        }

        $result = new cmsContent1();
        $result->where('id', $id)->delete();

        if (isset($result['error'])) {
            return responseFail($result);
        }
        return responseDeleted($result, 200);
    }
}
