<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\Mapel;

class MapelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->paginate ?  $request->paginate : 10;
        $data = Mapel::select('id', 'kode', 'nama_mapel', /* 'kelompok' */)->where('nama_mapel', 'like', '%' . $request->q . '%')->latest()->paginate($limit);
        return response()->json(['success' => true, 'data' => $data]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'nama_mapel'   => 'required|unique:mapels,nama_mapel,NULL,id,deleted_at,NULL',
            'kode'   => 'required|unique:mapels,kode,NULL,id,deleted_at,NULL',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $mapel = Mapel::create([
                'nama_mapel' => $request->nama_mapel,
                'kode' => $request->kode,
                // 'kelompok' => $request->kelompok,
            ]);

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di inputkan', 'data' => $mapel], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Mapel::findOrFail($id);

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mapel = Mapel::findOrFail($id);

        $validate = Validator::make($request->all(), [
            'nama_mapel'   => 'required|unique:mapels,nama_mapel,' . $id . ',id,deleted_at,NULL',
            'kode'   => 'required|unique:mapels,kode,' . $id . ',id,deleted_at,NULL',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $mapel->nama_mapel = $request->nama_mapel;
            $mapel->kode = $request->kode;
            // $mapel->kelompok = $request->kelompok;
            $mapel->save();

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $mapel], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete  = Mapel::destroy($id);
        return response()->json([
            'success' => true,
            'messages' => 'data berhasil di hapus',
            'data' => $id,
        ]);
    }
}
