<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\Penjurusan;
use Illuminate\Validation\Rule;

class PenjurusanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->paginate ?  $request->paginate : 10;
        $q = $request->q;

        $data = Penjurusan::with([
            'kelas:id,id_jenjang,nama_kelas',
            'kelas.jenjang:id,nama_jenjang'
        ])
            ->where('nama_penjurusan', 'like', '%' . $q . '%')
            ->orWhereHas('kelas', function ($query) use ($q) {
                $query->where('nama_kelas', 'like', '%' . $q . '%');
            })
            ->orWhereHas('kelas.jenjang', function ($query) use ($q) {
                $query->where('nama_jenjang', 'like', '%' . $q . '%');
            })
            ->select('id', 'id_kelas', 'nama_penjurusan')
            ->paginate($limit);
        return response()->json(['success' => true, 'data' => $data]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->nama_penjurusan != '-') {
            // $validate = Validator::make($request->all(), [
            // 'nama_penjurusan'   => 'required|unique:penjurusans,nama_penjurusan,NULL,id,deleted_at,NULL',
            // 'id_kelas'   => 'required',
            // ]);

            $id_kelas = $request->id_kelas;
            $nama_penjurusan = $request->nama_penjurusan;

            $validate = Validator::make($request->all(), [
                'id_kelas'          => 'required',
                'nama_penjurusan'   => [
                    Rule::unique('penjurusans')->where(function ($query) use ($id_kelas, $nama_penjurusan) {
                        return $query->where('nama_penjurusan', $nama_penjurusan)
                            ->where('id_kelas', $id_kelas)
                            ->whereNull('deleted_at');
                    })
                ],
            ]);
        } else {
            $validate = Validator::make($request->all(), [
                'nama_penjurusan'   => 'required',
                'id_kelas'   => 'required',
            ]);
        }

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $penjurusan = Penjurusan::create([
                'nama_penjurusan' => $request->nama_penjurusan,
                'id_kelas'  => $request->id_kelas,
            ]);

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di inputkan', 'data' => $penjurusan], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Penjurusan::with([
            'kelas:id,id_jenjang,nama_kelas',
            'kelas.jenjang:id,nama_jenjang'
        ])->findOrFail($id);

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $penjurusan = Penjurusan::findOrFail($id);

        if ($request->nama_penjurusan != '-') {
            // $validate = Validator::make($request->all(), [
            //     'nama_penjurusan'   => 'required|unique:penjurusans,nama_penjurusan,'.$id.',id,deleted_at,NULL',
            //     'id_kelas'   => 'required',
            // ]);

            $id_kelas = $request->id_kelas;
            $nama_penjurusan = $request->nama_penjurusan;

            $validate = Validator::make($request->all(), [
                'id_kelas'          => 'required',
                'nama_penjurusan'   => [
                    Rule::unique('penjurusans')->where(function ($query) use ($id_kelas, $nama_penjurusan, $id) {
                        return $query->where('nama_penjurusan', $nama_penjurusan)
                            ->where('id', '!=', $id)
                            ->where('id_kelas', $id_kelas)
                            ->whereNull('deleted_at');
                    })
                ],
            ]);
        } else {
            $validate = Validator::make($request->all(), [
                'nama_penjurusan'   => 'required',
                'id_kelas'   => 'required',
            ]);
        }

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $penjurusan->nama_penjurusan = $request->nama_penjurusan;
            $penjurusan->id_kelas  = $request->id_kelas;

            $penjurusan->save();

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $penjurusan], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete  = Penjurusan::destroy($id);
        return response()->json([
            'success' => true,
            'messages' => 'data berhasil di hapus',
            'data' => $id,
        ]);
    }
}
