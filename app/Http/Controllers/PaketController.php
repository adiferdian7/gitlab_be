<?php

namespace App\Http\Controllers;

use App\Models\ProdukPaket;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class PaketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->paginate ?  $request->paginate : 10;
        $data = ProdukPaket::with([
                        'tryout', 
                        'tryout.soal',
                        'tryout.soal.pertanyaan:id,id_soal_tryout,bab_mapel,penjelasan_pertanyaan,template_pertanyaan,soal,opsi_pertanyaan,pembahasan_pertanyaan',
                        'produk'
                            ])
                            ->select('id', 'id_tryout', 'id_produk')
                            ->paginate($limit);
        return response()->json(['success' => true, 'data' => $data]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'id_produk'   => 'required',
            'id_tryout'   => 'required',
        ]);

        if($validate->fails()) {
            return response()->json([
                'success' => false, 
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $insert = ProdukPaket::create([
                'id_produk'     => $request->id_produk,
                'id_tryout'     => $request->id_tryout,
            ]);

            DB::commit();

             return response()->json(['success' => true, 'message' => 'data berhasil di inputkan', 'data' => $insert], 201);

        } catch (\Exception $e) {
             DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ProdukPaket::with([
                        'tryout', 
                        'tryout.soal',
                        'tryout.soal.pertanyaan:id,id_soal_tryout,bab_mapel,penjelasan_pertanyaan,template_pertanyaan,soal,opsi_pertanyaan,pembahasan_pertanyaan',
                        'produk'
                            ])->findOrFail($id);

        return response()->json([
            'success' => true, 
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = ProdukPaket::findOrFail($id);

        $validate = Validator::make($request->all(), [
            'id_produk'   => 'required',
            'id_tryout'   => 'required',
        ]);

        if($validate->fails()) {
            return response()->json([
                'success' => false, 
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $update->id_produk    = $request->id_produk;
            $update->id_tryout    = $request->id_tryout;
            $update->save();

            DB::commit();

             return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $update], 201);

        } catch (\Exception $e) {
             DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete  = ProdukPaket::destroy($id);
        if($delete) {
            return response()->json([
                'success' => true,
                'message' => 'data berhasil di hapus',
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'data gagal di hapus',
            ], 400);
        }
    }
}
