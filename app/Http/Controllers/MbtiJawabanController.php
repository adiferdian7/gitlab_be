<?php

namespace App\Http\Controllers;

use App\Models\MbtiJawaban;
use App\Models\Transaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class MbtiJawabanController extends Controller
{
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'id_mbti'      => 'required',
            // 'id_user'     => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $user = auth()->user();

            $insert = MbtiJawaban::create([
                'id_mbti'              => $request->id_mbti,
                'id_user'                => !empty($request->id_user) ? $request->id_user : $user->id,
                'id_kepribadian'              => $request->id_kepribadian
            ]);

            DB::commit();

            return response()->json(['success' => true, 'message' => 'Data MBTI jawaban berhasil di inputkan', 'data' => $insert], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    public function destroy(Request $request, $id)
    {
        $delete = MbtiJawaban::findOrFail($id);
        // $delete->mbti_dimensi()->forceDelete();
        $delete->delete();

        $transaksi = Transaksi::where('id_user', auth()->user()->id)
        ->whereIn('jenis_transaksi', ['MBTI', 'Bonus MBTI'])->first();

        $transaksi->update(['status'=>'Dibatalkan', 'alasan_pembatalan'=>'Reset MBTI']);
        $transaksi->delete();
        

        return response()->json([
            'success' => true,
            'messages' => 'data berhasil di hapus',
            'data' => $id,
        ]);
    }
}
