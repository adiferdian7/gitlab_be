<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\Klaster;

class KlasterController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $limit = $request->paginate ?  $request->paginate : 10;
    $q = $request->q;
    $data = Klaster::with([
      'rumpun:id,nama,id_to_kategori',
      'rumpun.kategori:id,kategori,kelompok,penjurusan,jenjang,kelas',
      'mapel:id,kode,nama_mapel'
    ])
      ->where('nilai', 'like', '%' . $q . '%')
      ->orWhereHas('rumpun', function ($query) use ($q) {
        $query->where('nama', 'like', '%' . $q . '%');
      })
      ->orWhereHas('mapel', function ($query) use ($q) {
        $query->where('nama_mapel', 'like', '%' . $q . '%');
      })
      ->paginate($limit);
    return response()->json(['success' => true, 'data' => $data]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validate = Validator::make($request->all(), [
      'id_rumpun'   => 'required|exists:rumpun,id',
      'id_mapel'   => 'required|exists:mapels,id',
      'nilai' => 'required'
    ]);

    if ($validate->fails()) {
      return response()->json([
        'success' => false,
        'messages' => $validate->messages()
      ], 422);
    }

    DB::beginTransaction();

    try {

      $klaster = Klaster::create([
        'id_rumpun' => $request->id_rumpun,
        'id_mapel' => $request->id_mapel,
        'nilai' => $request->nilai,
      ]);

      DB::commit();

      return response()->json(['success' => true, 'message' => 'data berhasil di inputkan', 'data' => $klaster], 201);
    } catch (\Exception $e) {
      DB::rollback();

      return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
    }
  }

  public function storeBulk(Request $request)
  {
    $validate = Validator::make($request->all(), [
      'id_rumpun'   => 'required|exists:rumpun,id',
      'mapels.*.id'   => 'required|exists:mapels,id',
      'mapels.*.nilai'   => 'required',
    ]);

    if ($validate->fails()) {
      return response()->json([
        'success' => false,
        'messages' => $validate->messages()
      ], 422);
    }

    if (!isset($request->is_edit) || (isset($request->is_edit) && $request->is_edit !== true)) {
      $check = Klaster::where('id_rumpun', $request->id_rumpun)->count();
      if ($check > 0) {
        return response()->json([
          'success' => false,
          'messages' => ['not_unique' => ['Klaster untuk Rumpun ini sudah ditambah! Silakan edit Klaster.']]
        ], 422);
      }
    }

    DB::beginTransaction();

    try {

      $bulkData = [];
      // foreach ($request->mapels as $key => $value) {
      //   $check = Klaster::where('id_rumpun', $request->id_rumpun)->where('id_mapel', $value['id']);
      //   if ($check->count() == 0) {
      //     $bulkData[$key] = [
      //       'id_rumpun' => $request->id_rumpun,
      //       'id_mapel' => $value['id'],
      //       'nilai' => $value['nilai'],
      //       'created_at' => date('Y-m-d H:i:s')
      //     ];
      //   } else {
      //     $check->update([
      //       'nilai' => $value['nilai'],
      //       'updated_at' => date('Y-m-d H:i:s')
      //     ]);
      //   }
      // }

      Klaster::where('id_rumpun', $request->id_rumpun)->delete();
      foreach ($request->mapels as $key => $value) {
        $bulkData[$key] = [
          'id_rumpun' => $request->id_rumpun,
          'id_mapel' => $value['id'],
          'nilai' => $value['nilai'],
          'created_at' => date('Y-m-d H:i:s')
        ];
      }

      if (!empty($bulkData)) {
        Klaster::insert($bulkData);
      }

      DB::commit();

      return response()->json(['success' => true, 'message' => 'data berhasil di inputkan', 'data' => $bulkData], 201);
    } catch (\Exception $e) {
      DB::rollback();

      return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $data = Klaster::with([
      'rumpun:id,nama,id_to_kategori',
      'rumpun.kategori:id,kategori,kelompok,penjurusan,jenjang,kelas',
      'mapel:id,kode,nama_mapel'
    ])->findOrFail($id);

    return response()->json([
      'success' => true,
      'data' => $data
    ]);
  }


  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $klaster = Klaster::findOrFail($id);

    $validate = Validator::make($request->all(), [
      'id_rumpun'   => 'required|exists:rumpun,id',
      'id_mapel'   => 'required|exists:mapels,id',
      'nilai' => 'required'
    ]);

    if ($validate->fails()) {
      return response()->json([
        'success' => false,
        'messages' => $validate->messages()
      ], 422);
    }

    $check = Klaster::where('id_rumpun', $request->id_rumpun)->where('id_mapel', $request->id_mapel)->whereNotIn('id', [$id]);
    if ($check->count() > 0) {
      return response()->json([
        'success' => false,
        'messages' => ['not_unique' => ['Data already exists!']]
      ], 422);
    }

    DB::beginTransaction();

    try {

      $klaster->nilai = $request->nilai;
      $klaster->id_rumpun = $request->id_rumpun;
      $klaster->id_mapel = $request->id_mapel;
      $klaster->save();

      DB::commit();

      return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $klaster], 201);
    } catch (\Exception $e) {
      DB::rollback();

      return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request, $id)
  {

    $detail = Klaster::findOrFail($id);
    $detail::destroy($id);
    return response()->json([
      'success' => true,
      'messages' => 'data berhasil di hapus',
      'data' => $detail,
    ]);
  }
}
