<?php

namespace App\Http\Controllers;

use App\Models\Level;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;
use Validator;

class LevelController extends Controller
{
  function __construct()
  {
    $this->levelModel = new Level();
  }

  public function index(Request $request)
  {
    $data = $this->levelModel->getData('data', []);

    return responseData($data);
  }

  public function detail($key)
  {
  }

  public function updateMultiple(Request $request)
  {
    $reqData =  $request->all();

    foreach ($reqData as $key => $dataRow) {
      $this->levelModel->updateData('updateData', $dataRow);
    }

    return responseUpdate([], 200, 'Level berhasil diperbarui!');
  }
}
