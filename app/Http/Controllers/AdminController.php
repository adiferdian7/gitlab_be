<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Admin;
use Illuminate\Support\Str;

use App\Mail\MySendMail;
use App\Mail\RegisterAdminEmail;
use App\Mail\TestMail;
use App\Models\Kursus;
use App\Models\Transaksi;
use App\Models\Tryout;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller
{
    public function index(Request $request)
    {
        $limit = $request->paginate ?  $request->paginate : 10;
        $q = $request->q ?? "";

        $data = Admin::with(['user'])
            ->where(
                function ($query) use ($q) {
                    $query->where('username', 'like',  '%' . $q . '%');
                    $query->orWhere('nama_lengkap', 'like',  '%' . $q . '%');
                    $query->orWhere('email', 'like',  '%' . $q . '%');
                    $query->orWhere('nomor_telephone', 'like',  '%' . $q . '%');
                }
            )
            ->paginate($limit);
        return response()->json([
            'success' => true,
            'messages' => 'berhasil mengambil data',
            'data' => $data,
        ]);
    }

    public function dashboard()
    {
        if ($this->user()->role_user != 'superAdmin' && $this->user()->role_user != 'admin') {
            return responseFail('Unauthenticated.', 403);
        }

        $total_tryout = DB::table('tryouts')->whereNull('deleted_at')->count();
        $total_kursus = DB::table('kursus')->whereNull('deleted_at')->count();
        $total_transaksi = DB::table('transaksis')->whereNull('deleted_at')->count();

        $data = [
            'total_tryout' => $total_tryout,
            'total_kursus' => $total_kursus,
            'total_transaksi' => $total_transaksi,
        ];

        return responseData($data);
    }

    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'username'          => 'required|unique:users',
            'nama_lengkap'      => 'required',
            'email'             => 'required|unique:admins|unique:users',
            'nomor_telephone'   => 'required',
            'password'          => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }


        DB::beginTransaction();

        $random = Str::random(40);

        try {

            $dataUser = [
                'username'   => $request->username,
                'password'   => Hash::make($request->password),
                'role_user'  => 'admin',
                'email'      => $request->email,
                'token'      => $random,
                'verifikasi' => 1,
            ];

            $insertUser = User::create($dataUser);

            $insert = Admin::create([
                'id_admin'          => $insertUser->id,
                'username'          => $request->username,
                'nama_lengkap'      => $request->nama_lengkap,
                'email'             => $request->email,
                'nomor_telephone'   => $request->nomor_telephone,
            ]);

            DB::commit();

            $data = [
                'admin' => ['users' => $insertUser, 'detail' => $insert]
            ];

            $mailData = [
                'token' => $insertUser->token,
                'password' => $request->password,
                'username' => $request->username,
            ];

            Mail::to($request->email)->send(new RegisterAdminEmail($mailData));

            return response()->json([
                'success' => true,
                'messages' => 'insert admin success',
                'data' => $data
            ], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'data' => $e->getMessage()], 500);
        }
    }


    public function create(Request $request)
    {

        if (!$request->user()->role_user == 'superAdmin') {
            return response()->json(['success' => false, 'message' => 'You are not superAdmin!'], 403);
        }

        $validate = Validator::make($request->all(), [
            'username'          => 'required|unique:users',
            'nama_lengkap'      => 'required',
            'email'             => 'required|unique:admins|unique:users',
            'nomor_telephone'   => 'required',
            'password'          => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }


        DB::beginTransaction();

        $random = Str::random(40);

        try {

            $dataUser = [
                'username'   => $request->username,
                'password'   => Hash::make($request->password),
                'role_user'  => 'admin',
                'email'      => $request->email,
                'token'      => $random,
                'verifikasi' => 1,
            ];

            $insertUser = User::create($dataUser);

            $insert = Admin::create([
                'id_admin'          => $insertUser->id,
                'username'          => $request->username,
                'nama_lengkap'      => $request->nama_lengkap,
                'email'             => $request->email,
                'nomor_telephone'   => $request->nomor_telephone,
            ]);

            $data = [
                'admin' => ['users' => $insertUser, 'detail' => $insert]
            ];

            $mailData = [
                'token' => $insertUser->token,
                'password' => $request->password,
                'username' => $request->username,
            ];

            if (!empty($request->permissions)) {
                $dataInsert = [];
                $index = 0;
                DB::table('user_permissions')->where('user_id', $insertUser->id)->delete();
                foreach ($request->permissions as $key => $permission) {
                    foreach ($permission['actions'] as $key2 => $action) {
                        if ($action['allow'] == true) {
                            $dataInsert[$index]['user_id'] = $insertUser->id;
                            $dataInsert[$index]['user_role'] = 'admin';
                            $dataInsert[$index]['module_name'] = $permission['label'];
                            $dataInsert[$index]['action_name'] = $action['label'];
                            $dataInsert[$index]['request_path'] = NULL;
                            $dataInsert[$index]['request_method'] = $action['r_method'];
                            $dataInsert[$index]['controller_path'] = is_array($action['c_name']) ? implode(",", $action['c_name']) : $action['c_name'];
                            $dataInsert[$index]['controller_method'] = is_array($action['c_method']) ? implode(",", $action['c_method']) : $action['c_method'];
                            $dataInsert[$index]['created_at'] = date('Y-m-d H:i:s');
                            $index++;
                        }
                    }
                }

                DB::table('user_permissions')->insert($dataInsert);
            }

            DB::commit();

            // Mail::to($request->email)->send(new RegisterAdminEmail($mailData));

            $testMail = new TestMail([
                'subject' => 'Email Anda telah didaftarkan sebagai Administrator UjiAja.com!',
                'to' => $insertUser->email,
                'toName' => $insert->nama_lengkap,
                'content' => '',
                'token' => $insertUser->token,
                'view' => 'email.content.admin__register-success',
                'data' => $mailData
            ]);

            Mail::send($testMail);

            return response()->json([
                'success' => true,
                'messages' => 'insert admin success',
                'data' => $data
            ], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'data' => $e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Admin::with(['user'])->findOrFail($id);

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $admin = Admin::findOrFail($id);
        $user  = User::findOrFail($admin->id_admin);

        $validate = Validator::make($request->all(), [
            'username'              => 'required|unique:users,username,' . $id,
            'nama_lengkap'          => 'required',
            'email'                 => 'required|unique:users,email,' . $id,
            'nomor_telephone'       => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $admin->nomor_telephone      = $request->nomor_telephone;
            $admin->email                = $request->email;
            $admin->nama_lengkap         = $request->nama_lengkap;

            // if ($request->verifikasi && !empty($request->verifikasi)) {
            //     $admin->verifikasi       = $request->verifikasi;
            // }

            $admin->save();


            $user->username    = $request->username;
            $user->email       = $request->email;

            if ($request->verifikasi && !empty($request->verifikasi)) {
                $user->verifikasi       = $request->verifikasi;
            }

            if ($request->password && !empty($request->password)) {
                $user->password       = Hash::make($request->password);
            }

            $user->save();

            if (!empty($request->permissions)) {
                $dataInsert = [];
                $index = 0;
                DB::table('user_permissions')->where('user_id', $id)->delete();
                foreach ($request->permissions as $key => $permission) {
                    foreach ($permission['actions'] as $key2 => $action) {
                        if ($action['allow'] == true) {
                            $dataInsert[$index]['user_id'] = $id;
                            $dataInsert[$index]['user_role'] = 'admin';
                            $dataInsert[$index]['module_name'] = $permission['label'];
                            $dataInsert[$index]['action_name'] = $action['label'];
                            $dataInsert[$index]['request_path'] = NULL;
                            $dataInsert[$index]['request_method'] = $action['r_method'];
                            $dataInsert[$index]['controller_path'] = is_array($action['c_name']) ? implode(",", $action['c_name']) : $action['c_name'];
                            $dataInsert[$index]['controller_method'] = is_array($action['c_method']) ? implode(",", $action['c_method']) : $action['c_method'];
                            $dataInsert[$index]['created_at'] = date('Y-m-d H:i:s');
                            $index++;
                        }
                    }
                }

                DB::table('user_permissions')->insert($dataInsert);
            }

            DB::commit();

            return response()->json([
                'success' => true,
                'messages' => 'update admin success',
                'data' => $admin,
            ], 200);
        } catch (\Exception $e) {

            DB::rollback();

            return response()->json([
                'success' => false,
                'messages' => $e->getMessage()
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin = Admin::findOrFail($id);
        $delete = Admin::destroy($id);
        $user   = User::destroy($admin->id_admin);
        return response()->json([
            'success' => true,
            'messages' => 'data berhasil di hapus',
            'data' => $id,
        ]);
    }

    public function undo(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }


        DB::beginTransaction();

        try {
            $admin =  Admin::withTrashed()->findOrFail($request->id);
            Admin::withTrashed()->find($request->id)->restore();
            User::withTrashed()->find($admin->id_admin)->restore();

            DB::commit();

            return response()->json([
                'success' => true,
                'messages' => 'data berhasil di pulihkan',
            ]);
        } catch (\Exception $e) {

            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }
}
