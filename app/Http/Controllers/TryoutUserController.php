<?php

namespace App\Http\Controllers;

use App\Mail\TestMail;
use App\Models\Notification;
use App\Models\OrangTua;
use App\Models\Produk;
use App\Models\ProgramStudiPerguruanTinggi;
use App\Models\SoalPertanyaan;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\Tryout;
use App\Models\TryoutHasil;
use App\Models\TryoutUser;
use App\Models\TryoutUserJawaban;
use App\Models\Klaster;
use App\Models\Penjurusan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Validator;
use PDF;

class TryoutUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->paginate ?  $request->paginate : 10;
        $q = $request->q;
        $data = TryoutUser::with([
            'produk:id,nama_produk,kategori_produk',
            'user:id,username,email',
            'user.siswa:id_siswa,nama_lengkap',
            'user.guru:id_teacher,nama_lengkap',
        ])
            ->select(['id', 'referensi', 'id_produk', 'id_user', 'waktu_mulai', 'waktu_selesai']);

        if (isset($request->id_parent) && !empty($request->id_parent)) {
            $orangtua = OrangTua::where('id_user', $request->id_parent)->first();
            if ($orangtua) {
                $id_user =  $orangtua->id_orang_tua;
                $data->where(
                    function ($query) use ($q, $id_user) {
                        $query->where('id_user', $id_user);
                    }
                );
            }
        }


        $data->where(function ($query) use ($q) {

            $query->where('referensi', 'like', '%' . $q . '%')
                ->orWhere('waktu_mulai', 'like', '%' . $q . '%')
                ->orWhere('waktu_selesai', 'like', '%' . $q . '%');
            $query->orWhereHas('produk', function ($filter) use ($q) {
                $filter->where('nama_produk', 'like', '%' . $q . '%');
            });
            $query->orWhereHas('user.siswa', function ($filter) use ($q) {
                $filter->where('nama_lengkap', 'like', '%' . $q . '%');
            });
            $query->orWhereHas('user.guru', function ($filter) use ($q) {
                $filter->where('nama_lengkap', 'like', '%' . $q . '%');
            });
        });

        if ($request->kategori_produk) {
            $data->whereHas('produk', function ($query) use ($request) {
                $query->whereIn('kategori_produk', $request->kategori_produk);
            });
        }

        $array = $data->has('produk')->groupBy('referensi')->paginate($limit);

        return response()->json([
            'success' => true,
            'messages' => 'Berhasil mengambil data TryoutUser',
            'data' => $array,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'id_produk'      => 'required',
            'id_tryout'     => 'required',
            'referensi'     => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        $user = auth()->user();
        $id_user = !empty($request->id_user) ? $request->id_user : $user->id;

        $sudah_ada = TryoutUser::where('id_user', $id_user)
            ->where('id_produk', $request->id_produk)
            ->where('id_tryout', $request->id_tryout)
            ->where('referensi', $request->referensi)
            ->first();

        // if ($sudah_ada) {
        //     return response()->json([
        //         'success' => false,
        //         'messages' => [
        //             'error' => ['Invalid request! Duplicate data not allowed.'],
        //         ]
        //     ], 422);
        // }

        DB::beginTransaction();

        try {

            // cek TU user & id_produk yg sama
            $tu_before = TryoutUser::where('id_user', $id_user)
                ->where('id_produk', $request->id_produk)
                ->where('referensi', $request->referensi)
                ->first();

            if (!empty($tu_before)) {
                $kode = $tu_before->referensi;
            } else {
                $kode = $request->referensi;
            }

            if ($sudah_ada) {
                $sudah_ada->update([
                    'referensi' => $kode,
                    'id_user'                => $id_user,
                    'id_produk'              => $request->id_produk,
                    'id_tryout'              => $request->id_tryout,
                    'waktu_selesai'          => date("Y-m-d H:i:s"),
                ]);
                $insert = $sudah_ada->refresh();
            } else {
                $insert = TryoutUser::create([
                    'referensi' => $kode,
                    'id_user'                => $id_user,
                    'id_produk'              => $request->id_produk,
                    'id_tryout'              => $request->id_tryout,
                    'waktu_mulai'            => date("Y-m-d H:i:s"),
                    'waktu_selesai'          => null
                ]);
            }

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data Tryout user berhasil di inputkan', 'data' => $insert], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TryoutUser  $tryoutUser
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = TryoutUser::findOrFail($id);
        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    public function show_ujian_per_tryout_and_produk(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'id_produk'      => 'required',
            'id_tryout'     => 'required',
            'referensi'     => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        $id_produk = $request->id_produk;
        $id_tryout = $request->id_tryout;
        $referensi = $request->referensi;
        $user = auth()->user();
        $id_user = !empty($request->id_user) ? $request->id_user : $user->id;

        $data = TryoutUser::where('id_produk', $id_produk)
            ->where('id_tryout', $id_tryout)
            ->where('id_user', $id_user)
            ->where('referensi', $referensi)
            ->first();

        $produk = Produk::findOrFail($id_produk)->list()->with(['tryout'])->get();

        // return response()->json($produk);

        $tryout = [];

        foreach ($produk as $key => $value) {
            array_push($tryout, $value->tryout);
        }

        if ($data) {

            foreach ($tryout as $key => $value) {
                $tryout[$key]['number'] = $key + 1;
                if ($value->id_tryout == $id_tryout) {
                    $tryout_user = $data;
                    $tryout[$key]['waktu_mulai_test'] = $tryout_user->waktu_mulai;
                } else {
                    $tryout_user = TryoutUser::where('id_produk', $id_produk)
                        ->where('id_tryout', $value->id)
                        ->where('id_user', $this->userId())
                        ->first();

                    if ($tryout_user) {
                        $tryout[$key]['waktu_mulai_test'] = $tryout_user['waktu_mulai'];
                        $tryout[$key]['waktu_selesai_test'] = $tryout_user['waktu_selesai'];
                    } else {
                        $tryout[$key]['waktu_mulai_test'] = NULL;
                    }
                }

                if (!empty($tryout_user->waktu_selesai)) {
                    $tryout[$key]['is_tryout_done'] = true;
                } else {
                    $tryout[$key]['is_tryout_done'] = false;
                }
            }

            $data->list_tryout = $tryout;

            return response()->json([
                'success' => true,
                'message' => 'Data ujian per tryout and produk',
                'data' => $data
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Data not found',
                'data' => $data
            ], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TryoutUser  $tryoutUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = TryoutUser::findOrFail($id);

        // $validate = Validator::make($request->all(), [
        //     'waktu_mulai'      => 'required',
        //     'waktu_selesai'     => 'required',
        // ]);

        // if ($validate->fails()) {
        //     return response()->json([
        //         'success' => false,
        //         'messages' => $validate->messages()
        //     ], 422);
        // }

        DB::beginTransaction();

        try {

            if (!empty($request->id_user)) {
                $update->id_user = $request->id_user;
            }
            if (!empty($request->id_produk)) {
                $update->id_produk = $request->id_produk;
            }
            if (!empty($request->id_tryout)) {
                $update->id_tryout = $request->id_tryout;
            }
            if (!empty($request->waktu_mulai)) {
                $update->waktu_mulai = Carbon::parse($request->waktu_mulai);
            }
            if (!empty($request->waktu_berakhir)) {
                $update->waktu_berakhir = Carbon::parse($request->waktu_berakhir);
            }
            if (!empty($request->temp_jawaban_user)) {
                $update->temp_jawaban_user = $request->temp_jawaban_user;
            }

            $update->save();

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $update], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TryoutUser  $tryoutUser
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = TryoutUser::find($id);

        if ($data) {
            $delete = $data->delete();
            return response()->json([
                'success' => true,
                'messages' => 'Data berhasil dihapus',
                'data' => $delete,
            ]);
        } else {
            return response()->json([
                'success' => false,
                'messages' => 'Data tidak ditemukan'
            ], 404);
        }
    }


    public function uktt_detail_and_jawaban(Request $request)
    {

        $id_user = !empty($request->id_user) ? $request->id_user : $this->userId();
        $id_produk = $request->id_produk;

        $tryoutUser = TryoutUser::with([
            'tentor:id_teacher,nama_lengkap,email',
            // 'jawabans',
            'produk:id,kategori_produk,uktt_id_level',
            'produk.level:id,nama_level'
        ])
            ->where('id_user', $id_user)
            ->where('id_produk', $id_produk)
            ->whereHas('produk', function ($query) {
                $query->where('kategori_produk', 'UKTT');
            })
            ->first();

        if (empty($tryoutUser)) {
            return responseFail('TryoutUser not found', 404);
        }

        $tryout = Tryout::with([
            'soal', 'soal.pertanyaan' => function ($query) {
                return $query->where('soal_pertanyaans.parent_soal_pertanyaan', '=', null)->get();
            }, 'soal.pertanyaan.pertanyaan_child',
            'soal.mapel:id,nama_mapel'
        ])->find($tryoutUser->id_tryout);

        if (empty($tryoutUser)) {
            return responseFail('Tryout not found', 404);
        }

        $list_soal = $tryout->soal;

        // ambil detail produk
        $produk = Produk::find($id_produk);

        if ($produk->kategori_produk == 'UKTT') {

            $teacher = Teacher::where('id_teacher', $id_user)->first();
            if (!$teacher->id_level) {
                $teacher->id_level = 0;
            }
            if (!empty($teacher) && $teacher->id_level < $produk->uktt_id_level && $tryoutUser->produk->level) {
                $teacher->update(['id_level' => $produk->uktt_id_level, 'nama_level' => $tryoutUser->produk->level->nama_level]);
            }
        }

        // perhitungan skor nilai
        $perhitungan_skor = $produk->perhitungan;

        $nilai_correct = isset($perhitungan_skor['correct']) ? $perhitungan_skor['correct'] : 1;
        $nilai_wrong = isset($perhitungan_skor['wrong']) ? $perhitungan_skor['wrong'] : 0;
        $nilai_unfilled = isset($perhitungan_skor['unfilled']) ? $perhitungan_skor['unfilled'] : 0;


        $index = 0;
        $nomor = 1;
        $data_nomor = [];
        $data_pencocokan = [];
        $data_skor = [
            'total_benar' => 0,
            'total_kosong' => 0,
            'total_salah' => 0,
            'total_skor_benar' => 0,
            'total_skor_kosong' => 0,
            'total_skor_salah' => 0,
            'total_skor_akhir' => 0,
        ];

        foreach ($list_soal as $key => $soal) {
            $pertanyaan_soal = $soal->pertanyaan;

            $nama_mapel = $soal->mapel->nama_mapel;
            $id_soal_tryout = $soal->id;

            $nomor_awal = $nomor;
            $jumlah_soal = SoalPertanyaan::where('id_soal_tryout', $id_soal_tryout)->count();
            $nomor_akhir = $nomor_awal + ($jumlah_soal - 1);
            $range_nomor = $nomor_awal . " - " . $nomor_akhir;

            foreach ($pertanyaan_soal as $keyp => $pertanyaan) {

                $pertanyaan_child = $pertanyaan->pertanyaan_child;

                $data_nomor[$index]['nomor'] = $nomor;
                $data_nomor[$index]['id_tryout'] = $soal->id_tryout;
                $data_nomor[$index]['id_soal_tryout'] = $pertanyaan->id_soal_tryout;
                $data_nomor[$index]['id_soal_pertanyaan'] = $pertanyaan->id;
                $data_nomor[$index]['induk_pertanyaan'] = $pertanyaan->parent_soal_pertanyaan;
                $data_nomor[$index]['nama_mapel'] = $nama_mapel;
                $data_nomor[$index]['bab'] = $pertanyaan->bab_mapel;
                // $data_nomor[$index]['range_nomor'] = $range_nomor;
                // $data_nomor[$index]['jumlah_soal'] = $jumlah_soal;
                // $data_nomor[$index]['penjelasan_pertanyaan'] = $pertanyaan->penjelasan_pertanyaan;
                $data_nomor[$index]['soal_pertanyaan'] = $pertanyaan->soal;
                $data_nomor[$index]['opsi_pertanyaan'] = $pertanyaan->opsi_pertanyaan;
                $data_nomor[$index]['jawaban_pertanyaan'] = $pertanyaan->jawaban_pertanyaan;
                // $data_nomor[$index]['pembahasan_pertanyaan'] = $pertanyaan->pembahasan_pertanyaan;

                // jawaban user
                $dataJawabanUser = TryoutUserJawaban::with(['tryout_user'])
                    ->where('id_soal_tryout', $pertanyaan->id_soal_tryout)
                    ->where('id_soal_pertanyaan', $pertanyaan->id)
                    ->whereHas('tryout_user', function ($query) use ($id_user) {
                        $query->where('id_user', $id_user);
                    })->first();

                $data_nomor[$index]['jawaban_user'] = @$dataJawabanUser['jawaban_user'];


                // pencocokan jawaban
                $data_pencocokan[$index]['nomor'] = $nomor;
                if (@$dataJawabanUser['jawaban_user'] == $pertanyaan->jawaban_pertanyaan) {
                    $data_nomor[$index]['koreksi_jawaban'] = 'Benar';
                    $data_pencocokan[$index]['hasil'] = 'Benar';
                    $data_pencocokan[$index]['skor'] = $nilai_correct;
                    $data_skor['total_benar'] += 1;
                    $data_skor['total_skor_benar'] += $nilai_correct;
                } else if (empty(@$dataJawabanUser['jawaban_user']) || @$dataJawabanUser['jawaban_user'] == NULL) {
                    $data_nomor[$index]['koreksi_jawaban'] = 'Kosong';
                    $data_pencocokan[$index]['hasil'] = 'Kosong';
                    $data_pencocokan[$index]['skor'] = $nilai_unfilled;
                    $data_skor['total_kosong'] += 1;
                    $data_skor['total_skor_kosong'] += $nilai_unfilled;
                } else {
                    $data_nomor[$index]['koreksi_jawaban'] = 'Salah';
                    $data_pencocokan[$index]['hasil'] = 'Salah';
                    $data_pencocokan[$index]['skor'] = $nilai_wrong;
                    $data_skor['total_salah'] += 1;
                    $data_skor['total_skor_salah'] += $nilai_wrong;
                }

                $index++;
                $nomor++;

                foreach ($pertanyaan_child as $keypc => $child) {

                    $data_nomor[$index]['nomor'] = $nomor;
                    $data_nomor[$index]['id_tryout'] = $soal->id_tryout;
                    $data_nomor[$index]['id_soal_tryout'] = $child->id_soal_tryout;
                    $data_nomor[$index]['id_soal_pertanyaan'] = $child->id;
                    $data_nomor[$index]['induk_pertanyaan'] = $child->parent_soal_pertanyaan;
                    $data_nomor[$index]['nama_mapel'] = $soal->mapel->nama_mapel;
                    $data_nomor[$index]['bab'] = $child->bab_mapel;
                    // $data_nomor[$index]['range_nomor'] = $range_nomor;
                    // $data_nomor[$index]['jumlah_soal'] = $jumlah_soal;
                    // $data_nomor[$index]['penjelasan_pertanyaan'] = $pertanyaan->penjelasan_pertanyaan;
                    $data_nomor[$index]['soal_pertanyaan'] = $child->soal;
                    $data_nomor[$index]['opsi_pertanyaan'] = $child->opsi_pertanyaan;
                    $data_nomor[$index]['jawaban_pertanyaan'] = $child->jawaban_pertanyaan;
                    // $data_nomor[$index]['pembahasan_pertanyaan'] = $child->pembahasan_pertanyaan;

                    // jawaban user
                    $dataJawabanUser = TryoutUserJawaban::with(['tryout_user'])
                        ->where('id_soal_tryout', $child->id_soal_tryout)
                        ->where('id_soal_pertanyaan', $child->id)
                        ->whereHas('tryout_user', function ($query) use ($id_user) {
                            $query->where('id_user', $id_user);
                        })->first();

                    $data_nomor[$index]['jawaban_user'] = $dataJawabanUser['jawaban_user'];

                    // pencocokan jawaban
                    $data_pencocokan[$index]['nomor'] = $nomor;
                    if (@$dataJawabanUser['jawaban_user'] == $child->jawaban_pertanyaan) {
                        $data_nomor[$index]['koreksi_jawaban'] = 'Benar';
                        $data_pencocokan[$index]['hasil'] = 'Benar';
                        $data_pencocokan[$index]['skor'] = $nilai_correct;
                        $data_skor['total_benar'] += 1;
                        $data_skor['total_skor_benar'] += $nilai_correct;
                    } else if (empty(@$dataJawabanUser['jawaban_user']) || @$dataJawabanUser['jawaban_user'] == NULL) {
                        $data_nomor[$index]['koreksi_jawaban'] = 'Kosong';
                        $data_pencocokan[$index]['hasil'] = 'Kosong';
                        $data_pencocokan[$index]['skor'] = $nilai_unfilled;
                        $data_skor['total_kosong'] += 1;
                        $data_skor['total_skor_kosong'] += $nilai_unfilled;
                    } else {
                        $data_nomor[$index]['koreksi_jawaban'] = 'Salah';
                        $data_pencocokan[$index]['hasil'] = 'Salah';
                        $data_pencocokan[$index]['skor'] = $nilai_wrong;
                        $data_skor['total_salah'] += 1;
                        $data_skor['total_skor_salah'] += $nilai_wrong;
                    }

                    $nomor++;
                    $index++;
                }
            }
        }

        $akumulasi_benar = $nilai_correct * ($nomor - 1);
        $akumulasi_skor = $data_skor['total_skor_benar'] + $data_skor['total_skor_kosong'] + $data_skor['total_skor_salah'];
        $skor_akhir = round($akumulasi_skor / $akumulasi_benar, 4) * 100;


        $data['detail'] = $tryoutUser;
        $data['soal'] = $data_nomor;
        $data['pencocokan'] = $data_pencocokan;
        $data['skor'] = $data_skor;
        $data['skor']['total_skor_akumulasi'] = $akumulasi_skor;
        $data['skor']['skor_akhir'] = $skor_akhir > 0 ? $skor_akhir : 0.00;
        return responseData($data);
    }

    public function uktt_reset(Request $request, $id)
    {
        $data = TryoutUser::with(['produk:id,nama_produk,kategori_produk'])->find($id);

        if ($data != NULL) {
            if ($data->produk && $data->produk->kategori_produk == 'UKTT') {
                $data->delete();
                return responseData([
                    'message' => 'Data deleted successfully!'
                ]);
            } else {
                return responseFail([
                    'message' => ['Forbidden!']
                ], 422);
            }
        } else {
            return responseFail([
                'message' => ['Data fail to delete!']
            ], 422);
        }
    }

    public function to_reset(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'id_user'      => 'required',
            'referensi'     => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        $data = TryoutUser::with(['produk:id,nama_produk,kategori_produk'])
            ->where('referensi', $request->referensi)
            ->where('id_user', $request->id_user);

        if ($data != NULL) {
            $find = $data->first();
            if ($find->produk && $find->produk->kategori_produk !== 'UKTT' && $request->password == '123@456') {
                $data->delete();
                return responseData([
                    'message' => 'Data deleted successfully!'
                ]);
            } else {
                return responseFail([
                    'message' => ['Forbidden!']
                ], 422);
            }
        } else {
            return responseFail([
                'message' => ['Data fail to delete!']
            ], 422);
        }
    }


    public function tryout_riwayat_pengerjaan(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'id_produk'      => 'required',
            'referensi'     => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        $excludes = ['superadmin', 'superAdmin', 'admin'];
        if (!in_array($this->user()->role_user, $excludes)) {
            $id_user = !empty($request->id_user) ? $request->id_user : $this->userId();
        } else {
            $id_user = $request->id_user;
        }

        $id_produk = $request->id_produk;
        $referensi = $request->referensi;

        // $tryoutUser = TryoutUser::with([
        //     'student:id_siswa,nama_lengkap,email',
        //     // 'jawabans',
        //     'produk:id,nama_produk,kategori_produk',
        // ])
        //     ->where('id_user', $id_user)
        //     ->where('id_produk', $id_produk)
        //     ->where('referensi', $referensi)
        //     ->whereHas('produk', function ($query) {
        //         $query->whereIn('kategori_produk', ['UTBK', 'ASPD']);
        //     })
        //     ->get();

        // if ($tryoutUser->count() == 0) {
        //     return responseFail('Tryout not found', 404);
        // }

        // ambil detail produk
        $produk = Produk::find($id_produk);

        if (empty($produk)) {
            return response()->json(['success' => false, 'message' => 'Data produk tidak ditemukan!'], 400);
        }

        if ($produk->jenis_produk == 'Perorangan') {
            $tryoutUser = TryoutUser::with([
                // 'tentor:id_teacher,nama_lengkap,email',
                // 'jawabans',
                'produk:id,nama_produk,kategori_produk',
            ])
                ->where('id_user', $id_user)
                ->where('id_produk', $id_produk)
                ->where('referensi', $referensi)
                ->whereHas('produk', function ($query) {
                    $query->where('jenis_produk', 'Perorangan')->whereIn('kategori_produk', ['UTBK', 'ASPD']);
                })
                ->has('produk')
                ->orderBy('waktu_mulai', 'asc')
                ->get();

            if ($tryoutUser->count() == 0) {
                return responseFail('Tryout not found', 404);
            }

            // jumlah seluruh peserta
            $jsp = TryoutUser::where('id_produk', $id_produk)
                // ->where('id_tryout', $tryout->id)
                ->whereNotNull('waktu_selesai')
                ->groupBy(['id_user'])->get()->count();
        } else {
            $tryoutUser = TryoutUser::with([
                // 'tentor:id_teacher,nama_lengkap,email',
                // 'jawabans',
                'produk:id,nama_produk,kategori_produk',
            ])
                ->where('id_user', $id_user)
                ->where('id_produk', $id_produk)
                ->where('referensi', $referensi)
                ->whereHas('produk', function ($query) {
                    $query->where('jenis_produk', 'Masal')->whereIn('kategori_produk', ['UTBK', 'ASPD']);
                })
                ->has('produk')
                ->orderBy('waktu_mulai', 'asc')
                ->get();

            if ($tryoutUser->count() == 0) {
                return responseFail('Tryout not found', 404);
            }

            $tanggal_mulai_event = $produk->tanggal_mulai;
            $tanggal_berakhir_event = $produk->tanggal_berakhir;

            // jumlah seluruh peserta
            $jsp = TryoutUser::where('id_produk', $id_produk)
                // ->where('waktu_mulai', $tanggal_mulai_event)
                // ->where('waktu_selesai', $tanggal_berakhir_event)
                ->whereNotNull('waktu_selesai')
                ->groupBy(['id_user'])->get()->count();
        }


        $jsp = $jsp < 1 ? 1 : $jsp;

        $detail = [];
        $id_tryouts = [];

        foreach ($tryoutUser as $key => $tryoutUserRow) {
            $detail['referensi'] = $tryoutUserRow->referensi;
            $detail['id_user'] = $tryoutUserRow->id_user;
            $detail['id_produk'] = $tryoutUserRow->id_produk;
            $detail['nama_produk'] = $tryoutUserRow->produk ? $tryoutUserRow->produk->nama_produk : '-';

            if ($key == 0) {
                $detail['waktu_mulai'] = $tryoutUserRow->waktu_mulai;
            } else if ($key == ($tryoutUser->count() - 1)) {
                $detail['waktu_selesai'] = $tryoutUserRow->waktu_selesai;
            }

            $id_tryouts[] = $tryoutUserRow->id_tryout;
        }

        // cek riwayat
        // $riwayat = TryoutHasil::where('referensi', $detail['referensi'])->first();

        // // return responseData($riwayat);

        // if (!empty($riwayat) && $riwayat->jumlah_peserta == $jsp && $riwayat->riwayat_pengerjaan !== NULL) {
        //     return responseData($riwayat->riwayat_pengerjaan);
        // }

        $dataTryouts = Tryout::with([
            'soal', 'soal.pertanyaan' => function ($query) {
                return $query->where('soal_pertanyaans.parent_soal_pertanyaan', '=', null)->get();
            }, 'soal.pertanyaan.pertanyaan_child',
            'soal.mapel:id,nama_mapel'
        ])->whereIn('id', $id_tryouts)->orderBy('jenis_soal', 'DESC')->get();

        $datas = [];
        foreach ($dataTryouts as $key1 => $tryout) {

            $datas[$key1]['id'] = $tryout->id;
            $datas[$key1]['judul'] = $tryout->judul;
            if ($tryout->jenis_soal == 'TPS') {
                $datas[$key1]['judul_edit'] = 'Tes Potensi Skolastik';
            } else if ($tryout->jenis_soal == 'TKA') {
                $datas[$key1]['judul_edit'] = 'Tes Kompetensi Akademik';
            }
            if ($produk->kategori_produk == 'ASPD') {
                $datas[$key1]['judul_edit'] = $tryout->judul;
            }
            $datas[$key1]['kategori'] = $tryout->kategori;
            $datas[$key1]['jenis_soal'] = $tryout->jenis_soal;
            $datas[$key1]['kelompok_soal'] = $tryout->kelompok_soal;

            $index = 0;
            $nomor = 1;
            $data_nomor = [];

            $list_soal = $tryout->soal;

            foreach ($list_soal as $key => $soal) {
                $pertanyaan_soal = $soal->pertanyaan;

                $nama_mapel = $soal->mapel->nama_mapel;
                $id_soal_tryout = $soal->id;

                foreach ($pertanyaan_soal as $keyp => $pertanyaan) {

                    $pertanyaan_child = $pertanyaan->pertanyaan_child;

                    $data_nomor[$index]['nomor'] = $nomor;
                    $data_nomor[$index]['id_tryout'] = $soal->id_tryout;
                    $data_nomor[$index]['id_soal_tryout'] = $pertanyaan->id_soal_tryout;
                    $data_nomor[$index]['id_soal_pertanyaan'] = $pertanyaan->id;
                    $data_nomor[$index]['induk_pertanyaan'] = $pertanyaan->parent_soal_pertanyaan;
                    $data_nomor[$index]['nama_mapel'] = $nama_mapel;
                    $data_nomor[$index]['bab'] = $pertanyaan->bab_mapel;
                    // $data_nomor[$index]['range_nomor'] = $range_nomor;
                    // $data_nomor[$index]['jumlah_soal'] = $jumlah_soal;
                    $data_nomor[$index]['penjelasan_pertanyaan'] = $pertanyaan->penjelasan_pertanyaan;
                    $data_nomor[$index]['soal_pertanyaan'] = $pertanyaan->soal;
                    $data_nomor[$index]['opsi_pertanyaan'] = $pertanyaan->opsi_pertanyaan;
                    $data_nomor[$index]['jawaban_pertanyaan'] = $pertanyaan->jawaban_pertanyaan;
                    $data_nomor[$index]['pembahasan_pertanyaan'] = $pertanyaan->pembahasan_pertanyaan;

                    // jawaban user
                    $dataJawabanUser = TryoutUserJawaban::with(['tryout_user'])
                        ->where('id_soal_tryout', $pertanyaan->id_soal_tryout)
                        ->where('id_soal_pertanyaan', $pertanyaan->id)
                        ->whereHas('tryout_user', function ($query) use ($id_user, $referensi) {
                            $query->where('id_user', $id_user)->where('referensi', $referensi);
                        })->has('tryout_user')
                        ->first();

                    $data_nomor[$index]['jawaban_user'] = @$dataJawabanUser['jawaban_user'];


                    // pencocokan jawaban
                    // $data_pencocokan[$index]['nomor'] = $nomor;
                    if (@$dataJawabanUser['jawaban_user'] == $pertanyaan->jawaban_pertanyaan) {
                        $data_nomor[$index]['koreksi_jawaban'] = 'Benar';
                        // $data_pencocokan[$index]['hasil'] = 'Benar';
                        // $data_pencocokan[$index]['skor'] = $nilai_correct;
                        // $data_skor['total_benar'] += 1;
                        // $data_skor['total_skor_benar'] += $nilai_correct;
                    } else if (empty(@$dataJawabanUser['jawaban_user']) || @$dataJawabanUser['jawaban_user'] == NULL) {
                        $data_nomor[$index]['koreksi_jawaban'] = 'Kosong';
                        // $data_pencocokan[$index]['hasil'] = 'Kosong';
                        // $data_pencocokan[$index]['skor'] = $nilai_unfilled;
                        // $data_skor['total_kosong'] += 1;
                        // $data_skor['total_skor_kosong'] += $nilai_unfilled;
                    } else {
                        $data_nomor[$index]['koreksi_jawaban'] = 'Salah';
                        // $data_pencocokan[$index]['hasil'] = 'Salah';
                        // $data_pencocokan[$index]['skor'] = $nilai_wrong;
                        // $data_skor['total_salah'] += 1;
                        // $data_skor['total_skor_salah'] += $nilai_wrong;
                    }

                    $index++;
                    $nomor++;

                    foreach ($pertanyaan_child as $keypc => $child) {

                        $data_nomor[$index]['nomor'] = $nomor;
                        $data_nomor[$index]['id_tryout'] = $soal->id_tryout;
                        $data_nomor[$index]['id_soal_tryout'] = $child->id_soal_tryout;
                        $data_nomor[$index]['id_soal_pertanyaan'] = $child->id;
                        $data_nomor[$index]['induk_pertanyaan'] = $child->parent_soal_pertanyaan;
                        $data_nomor[$index]['nama_mapel'] = $soal->mapel->nama_mapel;
                        $data_nomor[$index]['bab'] = $child->bab_mapel;
                        // $data_nomor[$index]['range_nomor'] = $range_nomor;
                        // $data_nomor[$index]['jumlah_soal'] = $jumlah_soal;
                        $data_nomor[$index]['penjelasan_pertanyaan'] = $pertanyaan->penjelasan_pertanyaan;
                        $data_nomor[$index]['soal_pertanyaan'] = $child->soal;
                        $data_nomor[$index]['opsi_pertanyaan'] = $child->opsi_pertanyaan;
                        $data_nomor[$index]['jawaban_pertanyaan'] = $child->jawaban_pertanyaan;
                        $data_nomor[$index]['pembahasan_pertanyaan'] = $child->pembahasan_pertanyaan;

                        // jawaban user
                        $dataJawabanUser = TryoutUserJawaban::with(['tryout_user'])
                            ->where('id_soal_tryout', $child->id_soal_tryout)
                            ->where('id_soal_pertanyaan', $child->id)
                            ->whereHas('tryout_user', function ($query) use ($id_user, $referensi) {
                                $query->where('id_user', $id_user)->where('referensi', $referensi);
                            })->has('tryout_user')
                            ->first();


                        $data_nomor[$index]['jawaban_user'] = @$dataJawabanUser['jawaban_user'];

                        // pencocokan jawaban
                        // $data_pencocokan[$index]['nomor'] = $nomor;
                        if (@$dataJawabanUser['jawaban_user'] == $child->jawaban_pertanyaan) {
                            $data_nomor[$index]['koreksi_jawaban'] = 'Benar';
                            // $data_pencocokan[$index]['hasil'] = 'Benar';
                            // $data_pencocokan[$index]['skor'] = $nilai_correct;
                            // $data_skor['total_benar'] += 1;
                            // $data_skor['total_skor_benar'] += $nilai_correct;
                        } else if (empty(@$dataJawabanUser['jawaban_user']) || @$dataJawabanUser['jawaban_user'] == NULL) {
                            $data_nomor[$index]['koreksi_jawaban'] = 'Kosong';
                            // $data_pencocokan[$index]['hasil'] = 'Kosong';
                            // $data_pencocokan[$index]['skor'] = $nilai_unfilled;
                            // $data_skor['total_kosong'] += 1;
                            // $data_skor['total_skor_kosong'] += $nilai_unfilled;
                        } else {
                            $data_nomor[$index]['koreksi_jawaban'] = 'Salah';
                            // $data_pencocokan[$index]['hasil'] = 'Salah';
                            // $data_pencocokan[$index]['skor'] = $nilai_wrong;
                            // $data_skor['total_salah'] += 1;
                            // $data_skor['total_skor_salah'] += $nilai_wrong;
                        }

                        $nomor++;
                        $index++;
                    }
                }
            }

            $datas[$key1]['soal'] = $data_nomor;
        }

        $data = [
            'detail' => $detail,
            'tryout' => $datas,
        ];

        // $hasil = TryoutHasil::where('referensi', $detail['referensi']);

        // if ($hasil->count() > 0) {
        //     // $dataUpdate = $hasil->first()->riwayat_pengerjaan == NULL ? json_encode($data) : json_encode($hasil);
        //     $hasil->first()->update(['riwayat_pengerjaan' => $data, 'jumlah_peserta' => $jsp]);
        // } else {
        // TryoutHasil::insert([
        //     'referensi' => $detail['referensi'],
        //     'jumlah_peserta' => $jsp,
        //     'riwayat_pengerjaan' =>  json_encode($data),
        // ]);
        // }

        return responseData($data);
    }

    public function tryout_hasil_pengerjaan(Request $request)
    {

        $excludes = ['superadmin', 'superAdmin', 'admin'];
        if (!in_array($this->user()->role_user, $excludes)) {
            $id_user = !empty($request->id_user) ? $request->id_user : $this->userId();
        } else {
            $id_user = $request->id_user;
        }
        $id_produk = $request->id_produk;
        $referensi = $request->referensi;

        // ambil detail produk
        $produk = Produk::find($id_produk);

        if (empty($produk)) {
            return response()->json(['success' => false, 'message' => 'Data produk tidak ditemukan!'], 400);
        }

        if (!empty($produk) && $produk->kategori_produk != 'UTBK') {
            return response()->json(['success' => false, 'message' => 'Data produk bukan UTBK!'], 400);
        }

        if ($produk->jenis_produk == 'Perorangan') {
            $tryoutUser = TryoutUser::with([
                'student:id_siswa,nama_lengkap,email',
                // 'jawabans',
                'produk:id,nama_produk,kategori_produk',
            ])
                ->where('id_user', $id_user)
                ->where('id_produk', $id_produk)
                ->where('referensi', $referensi)
                ->whereHas('produk', function ($query) {
                    $query->where('jenis_produk', 'Perorangan')->whereIn('kategori_produk', ['UTBK', 'ASPD']);
                })
                ->get();

            if ($tryoutUser->count() == 0) {
                return responseFail('Tryout not found', 404);
            }

            // jumlah seluruh peserta
            $jsp = TryoutUser::where('id_produk', $id_produk)
                // ->where('id_tryout', $tryout->id)
                ->whereNotNull('waktu_selesai')
                ->groupBy(['id_user'])->get()->count();
        } else {
            $tryoutUser = TryoutUser::with([
                // 'tentor:id_teacher,nama_lengkap,email',
                // 'jawabans',
                'produk:id,nama_produk,kategori_produk',
            ])
                ->where('id_user', $id_user)
                ->where('id_produk', $id_produk)
                ->where('referensi', $referensi)
                ->whereHas('produk', function ($query) {
                    $query->where('jenis_produk', 'Masal')->whereIn('kategori_produk', ['UTBK', 'ASPD']);
                })
                ->get();

            if ($tryoutUser->count() == 0) {
                return responseFail('Tryout not found', 404);
            }

            $tanggal_mulai_event = $produk->tanggal_mulai;
            $tanggal_berakhir_event = $produk->tanggal_berakhir;

            // jumlah seluruh peserta
            $jsp = TryoutUser::where('id_produk', $id_produk)
                // ->where('waktu_mulai', '>=', $tanggal_mulai_event)
                // ->where('waktu_selesai', '<=', $tanggal_berakhir_event)
                ->whereNotNull('waktu_selesai')
                ->groupBy(['id_user'])->get()->count();
        }

        $jsp = $jsp < 1 ? 1 : $jsp;


        $detail = [];
        $id_tryouts = [];

        foreach ($tryoutUser as $key => $tryoutUserRow) {
            $detail['referensi'] = $tryoutUserRow->referensi;
            $detail['id_user'] = $tryoutUserRow->id_user;
            $detail['id_produk'] = $tryoutUserRow->id_produk;
            $detail['nama_produk'] = $tryoutUserRow->produk ? $tryoutUserRow->produk->nama_produk : '-';
            $detail['tipe_event'] = $produk->jenis_produk;
            $detail['kategori_produk'] = $produk->kategori_produk;

            if ($produk->jenis_produk == 'Masal') {
                $detail['mulai_event'] = $tanggal_mulai_event;
                $detail['berakhir_event'] = $tanggal_berakhir_event;
            }

            if ($key == 0) {
                $detail['waktu_mulai'] = $tryoutUserRow->waktu_mulai;
            }
            $detail['waktu_selesai'] = $tryoutUserRow->waktu_selesai;
            $detail['jsp'] = $jsp;
            $detail['ceeb_sum'] = 0;
            $detail['ceeb_avg'] = 0;

            $id_tryouts[] = $tryoutUserRow->id_tryout;
        }

        // cek riwayat
        $riwayat = TryoutHasil::where('referensi', $detail['referensi'])->first();

        if ((!empty($riwayat) && $riwayat->jumlah_peserta == $jsp && $riwayat->riwayat_perhitungan !== NULL && !$request->refresh) || ($request->has('refresh') && $request->refresh == false)) {
            if (isset($riwayat->riwayat_perhitungan['detail']) && @$riwayat->riwayat_perhitungan['detail']['kategori_produk'] == 'UTBK') {
                // return responseData($riwayat->riwayat_perhitungan);
            }
        }

        // perhitungan skor nilai
        $perhitungan_skor = $produk->perhitungan;

        $nilai_correct = isset($perhitungan_skor['correct']) ? $perhitungan_skor['correct'] : 1;
        $nilai_wrong = isset($perhitungan_skor['wrong']) ? $perhitungan_skor['wrong'] : 0;
        $nilai_unfilled = isset($perhitungan_skor['unfilled']) ? $perhitungan_skor['unfilled'] : 0;

        $data_skor = [];

        $dataTryouts = Tryout::with([
            'soal', 'soal.pertanyaan' => function ($query) {
                return $query->where('soal_pertanyaans.parent_soal_pertanyaan', '=', null)->get();
            }, 'soal.pertanyaan.pertanyaan_child',
            'soal.mapel:id,nama_mapel,kode'
        ])->whereIn('id', $id_tryouts)->orderBy('jenis_soal', 'DESC')->get();

        if ($tryoutUser->count() == 0) {
            return responseFail('Tryout not found', 404);
        }

        $datas = [];
        $array_ceeb = [];
        $jsp = $jsp > 0 ? $jsp : 1;
        foreach ($dataTryouts as $key1 => $tryout) {

            $datas[$key1]['id'] = $tryout->id;
            $datas[$key1]['judul'] = $tryout->judul;
            if ($tryout->jenis_soal == 'TPS') {
                $datas[$key1]['judul_edit'] = 'Nilai Tes Potensi Skolastik';
            } else if ($tryout->jenis_soal == 'TKA') {
                $datas[$key1]['judul_edit'] = 'Nilai Tes Kompetensi Akademik';
            }
            $datas[$key1]['kategori'] = $tryout->kategori;
            $datas[$key1]['jenis_soal'] = $tryout->jenis_soal;
            $datas[$key1]['kelompok_soal'] = $tryout->kelompok_soal;

            $index = 0;
            $nomor = 1;

            $list_soal = $tryout->soal;

            $mapel = [];

            // // jumlah seluruh peserta
            // $jsp = TryoutUser::where('id_produk', $id_produk)
            //     ->where('id_tryout', $tryout->id)
            //     ->whereNotNull('waktu_selesai')
            //     ->groupBy('id_user')->get()->count();
            // echo 'JPS:' . $jsp;
            // echo '<br>';

            $ceeb_rank = [];

            foreach ($list_soal as $key => $soal) {
                $pertanyaan_soal = $soal->pertanyaan;

                $id_mapel = $soal->id_mapel;
                $nama_mapel = $soal->mapel->nama_mapel;
                $kode_mapel = $soal->mapel->kode;
                $id_soal_tryout = $soal->id;

                $mapel[$key]['id_mapel'] = $id_mapel;
                $mapel[$key]['nama_mapel'] = $nama_mapel;
                $mapel[$key]['kode_mapel'] = $kode_mapel;
                $mapel[$key]['total_soal'] = 0;
                $mapel[$key]['jsp'] = 1;
                $mapel[$key]['raw'] = [
                    'b' => 0,
                    's' => 0,
                    'k' => 0,
                    't_b' => 0,
                    't_s' => 0,
                    't_k' => 0,
                ];

                $mapel[$key]['irt_k'] = 0;

                $irt_k_lain = [];
                foreach ($pertanyaan_soal as $keyp => $pertanyaan) {

                    // $jpjb = 0; // jumlah peserta jawab benar
                    $jpjb = TryoutUserJawaban::where('jawaban_user', $pertanyaan->jawaban_pertanyaan)
                        ->where('id_soal_pertanyaan', $pertanyaan->id)
                        ->whereHas('tryout_user', function ($query) use ($id_produk) {
                            $query->where('id_produk', $id_produk);
                        })
                        ->has('tryout_user')
                        ->groupBy(['id_soal_pertanyaan'])
                        ->get()->count();


                    $pi = $jpjb / $jsp; // tingkat kesulitan
                    $ni = 1 - $pi; // nilai IRT tiap nomor
                    // echo $ni;
                    // echo "<br>";

                    $id =  $pertanyaan->id;

                    $mapel[$key]['jsp'] = $jsp;
                    // $mapel[$key]['jpjb'][$id] = $jpjb;
                    // $mapel[$key]['pi'][$id] = $pi;
                    // $mapel[$key]['ni'][$id] = $ni;

                    // $this->__get_sum_irt_all_siswa([
                    //     'jps' => $jps,
                    //     'jpjb' => $jpjb,
                    //     'pi' =>
                    // ]);

                    $pertanyaan_child = $pertanyaan->pertanyaan_child;

                    // jawaban user
                    $dataJawabanUser = TryoutUserJawaban::with(['tryout_user:id,id_user'])
                        ->select(['jawaban_user'])
                        ->where('id_soal_tryout', $pertanyaan->id_soal_tryout)
                        ->where('id_soal_pertanyaan', $pertanyaan->id)
                        ->whereHas('tryout_user', function ($query) use ($id_user, $id_produk) {
                            $query->where('id_user', $id_user)
                                ->where('id_produk', $id_produk)->groupBy('referensi');
                        })
                        ->has('tryout_user')
                        ->first();

                    $jawaban_user = @$dataJawabanUser['jawaban_user'];

                    // pencocokan jawaban
                    if ($jawaban_user == $pertanyaan->jawaban_pertanyaan) {
                        $mapel[$key]['raw']['b'] += $nilai_correct;
                        $mapel[$key]['raw']['t_b'] += 1;
                        $mapel[$key]['irt'][$id] = $ni;
                    } else if (empty($jawaban_user) || $jawaban_user == NULL) {
                        $mapel[$key]['raw']['k'] += $nilai_unfilled;
                        $mapel[$key]['raw']['t_k'] += 1;
                        $mapel[$key]['irt'][$id] = 0;
                    } else {
                        $mapel[$key]['raw']['s'] += $nilai_wrong;
                        $mapel[$key]['raw']['t_s'] += 1;
                        $mapel[$key]['irt'][$id] = 0;
                    }

                    $mapel[$key]['irt_k'] += $mapel[$key]['irt'][$id];

                    // echo $detail['tipe_event'];
                    // die;

                    if ($detail['tipe_event'] != 'Masal') {
                        $dataJawabanUserLain = TryoutUserJawaban::with(['tryout_user:id,id_user,id_tryout'])
                            // ->select(['id_tryout_user','jawaban_user'])
                            ->where('id_soal_tryout', $pertanyaan->id_soal_tryout)
                            ->where('id_soal_pertanyaan', $pertanyaan->id)
                            ->whereHas('tryout_user', function ($query) use ($id_user, $id_produk) {
                                $query->whereNotIn('id_user', [$id_user])
                                    ->where('id_produk', $id_produk)
                                    ->groupBy('id_user');
                            })
                            ->has('tryout_user')
                            ->groupBy('id_tryout_user')
                            ->get();
                    } else {
                        $dataJawabanUserLain = TryoutUserJawaban::with(['tryout_user:id,id_user,id_tryout'])
                            // ->select(['id_tryout_user','jawaban_user'])
                            ->where('id_soal_tryout', $pertanyaan->id_soal_tryout)
                            ->where('id_soal_pertanyaan', $pertanyaan->id)
                            ->whereHas('tryout_user', function ($query) use ($id_user, $id_produk, $tanggal_mulai_event, $tanggal_berakhir_event) {
                                $query
                                    // ->where('waktu_mulai', '>=', $tanggal_mulai_event)
                                    // ->where('waktu_selesai', '<=', $tanggal_berakhir_event)
                                    ->whereNotNull('waktu_selesai')
                                    ->whereNotIn('id_user', [$id_user])
                                    ->where('id_produk', $id_produk)
                                    ->groupBy('id_user');
                            })
                            ->has('tryout_user')
                            ->groupBy('id_tryout_user')->get();
                    }

                    foreach ($dataJawabanUserLain as $jul => $jawabanUserLain) {
                        $idu = $jawabanUserLain->tryout_user->id_user;
                        $idsp = $jawabanUserLain->id_soal_pertanyaan;
                        if ($jawabanUserLain->jawaban_user == $pertanyaan->jawaban_pertanyaan) {
                            $irt_k_lain[$idu][$idsp] = $ni;
                        } else {
                            $irt_k_lain[$idu][$idsp] = 0;
                        }
                    }
                    // print_r($irt_k_lain);

                    $nomor++;
                    $index++;

                    $mapel[$key]['total_soal'] += 1;

                    foreach ($pertanyaan_child as $keypc => $child) {

                        // $jpjb = 0; // jumlah peserta jawab benar
                        $jpjb = TryoutUserJawaban::where('jawaban_user', $child->jawaban_pertanyaan)
                            ->where('id_soal_pertanyaan', $child->id)
                            ->whereHas('tryout_user', function ($query) use ($id_produk) {
                                $query->where('id_produk', $id_produk);
                            })
                            ->has('tryout_user')
                            ->groupBy(['id_soal_pertanyaan'])
                            ->get()->count();


                        $pi = $jpjb / $jsp; // tingkat kesulitan
                        $ni = 1 - $pi; // nilai IRT tiap nomor

                        // echo $ni;
                        // echo "<br>";

                        $id = $child->id;

                        $mapel[$key]['jsp'] = $jsp;
                        // $mapel[$key]['jpjb'][$id] = $jpjb;
                        // $mapel[$key]['pi'][$id] = $pi;

                        // jawaban user
                        $dataJawabanUser = TryoutUserJawaban::with(['tryout_user'])
                            ->select(['jawaban_user'])
                            ->where('id_soal_tryout', $child->id_soal_tryout)
                            ->where('id_soal_pertanyaan', $child->id)
                            ->whereHas('tryout_user', function ($query) use ($id_user, $id_produk) {
                                $query->where('id_user', $id_user)
                                    ->where('id_produk', $id_produk);
                            })
                            ->has('tryout_user')
                            ->first();

                        $jawaban_user = @$dataJawabanUser['jawaban_user'];

                        // pencocokan jawaban
                        if ($jawaban_user == $child->jawaban_pertanyaan) {
                            $mapel[$key]['raw']['b'] += $nilai_correct;
                            $mapel[$key]['raw']['t_b'] += 1;
                            $mapel[$key]['irt'][$id] = $ni;
                        } else if (empty($jawaban_user) || $jawaban_user == NULL) {
                            $mapel[$key]['raw']['k'] += $nilai_unfilled;
                            $mapel[$key]['raw']['t_k'] += 1;
                            $mapel[$key]['irt'][$id] = 0;
                        } else {
                            $mapel[$key]['raw']['s'] += $nilai_wrong;
                            $mapel[$key]['raw']['t_s'] += 1;
                            $mapel[$key]['irt'][$id] = 0;
                        }

                        $mapel[$key]['irt_k'] += $mapel[$key]['irt'][$id];

                        if ($detail['tipe_event'] != 'Masal') {
                            $dataJawabanUserLain = TryoutUserJawaban::with(['tryout_user:id,id_user,id_tryout'])
                                // ->select(['id_tryout_user','jawaban_user'])
                                ->where('id_soal_tryout', $child->id_soal_tryout)
                                ->where('id_soal_pertanyaan', $child->id)
                                ->whereHas('tryout_user', function ($query) use ($id_user, $id_produk) {
                                    $query->whereNotIn('id_user', [$id_user])
                                        ->where('id_produk', $id_produk)
                                        ->groupBy('id_user');
                                })
                                ->has('tryout_user')
                                ->groupBy('id_tryout_user')
                                ->get();
                        } else {
                            $dataJawabanUserLain = TryoutUserJawaban::with(['tryout_user:id,id_user,id_tryout'])
                                // ->select(['id_tryout_user','jawaban_user'])
                                ->where('id_soal_tryout', $child->id_soal_tryout)
                                ->where('id_soal_pertanyaan', $child->id)
                                ->whereHas('tryout_user', function ($query) use ($id_user, $tanggal_mulai_event, $tanggal_berakhir_event) {
                                    $query
                                        // ->where('waktu_mulai', '>=', $tanggal_mulai_event)
                                        // ->where('waktu_selesai', '<=', $tanggal_berakhir_event)
                                        ->whereNotNull('waktu_selesai')
                                        ->whereNotIn('id_user', [$id_user])
                                        ->groupBy('id_user');
                                })
                                ->has('tryout_user')
                                ->groupBy('id_tryout_user')
                                ->get();
                        }

                        // print_r($dataJawabanUserLain->toArray());

                        foreach ($dataJawabanUserLain as $jul => $jawabanUserLain) {
                            $idu = $jawabanUserLain->tryout_user->id_user;
                            $idsp = $jawabanUserLain->id_soal_pertanyaan;
                            if ($jawabanUserLain->jawaban_user == $child->jawaban_pertanyaan) {
                                $irt_k_lain[$idu][$idsp] = $ni;
                            } else {
                                $irt_k_lain[$idu][$idsp] = 0;
                            }
                        }

                        // print_r($irt_k_lain);


                        $nomor++;
                        $index++;

                        $mapel[$key]['total_soal'] += 1;
                    }
                }

                // print_r($irt_k_lain);
                // die;
                // echo $nama_mapel; echo " -- ";
                foreach ($irt_k_lain as $idusr => $irtk_val) {
                    $irtk_lain[$idusr] = array_sum($irtk_val);
                }
                // echo $mapel[$key]['irt_k'];
                $irtk_lain[$id_user] = $mapel[$key]['irt_k'];
                $mapel[$key]['irt_avg'] = $this->getAverage($irtk_lain);
                // $mapel[$key]['irt_avg'] = 1;

                // print_r($irtk_lain);
                // die;

                $stdev = $this->stdev($irtk_lain);
                $stdev =  $stdev > 0 ? $stdev : 1;
                $mapel[$key]['ceeb'] = (($mapel[$key]['irt_k'] -  $mapel[$key]['irt_avg']) /  $stdev) * 100 + 500;
                $array_ceeb[] = $mapel[$key]['ceeb'];

                $mapel[$key]['irt_k'] = round($mapel[$key]['irt_k'], 3);
                $mapel[$key]['ceeb'] = round($mapel[$key]['ceeb'], 2);
                // $mapel[$key]['ceeb'] = $this->formatRupiah($mapel[$key]['ceeb'], false, 2);

                // echo ">====";
                // echo $nama_mapel; echo " ==> ";
                // echo $mapel[$key]['ceeb'];
                // echo $mapel[$key]['irt_k'] -  $mapel[$key]['irt_avg'];
                // echo "<==== <br>";

                // perankingan
                if ($produk->jenis_produk == 'Masal') {
                    $ceeb_all = [];
                    $i_ceeb = 0;
                    foreach ($irtk_lain as $idus => $value) {
                        // $stdev =  $stdev > 0 ? $stdev : 1;
                        $ceeb_temp = (($value -  $mapel[$key]['irt_avg']) /  $stdev) * 100 + 500;
                        $ceeb_all[$i_ceeb]['user'] = $idus;
                        $ceeb_all[$i_ceeb]['ceeb'] = round($ceeb_temp, 3);
                        $i_ceeb++;
                    }
                    // print_r($ceeb_all);
                    foreach ($ceeb_all as $ceeb_all_key => $ceeb_all_val) {
                        $ceeb_id_user = $ceeb_all_val['user'];
                        $filter = array_filter($ceeb_all, function ($item) use ($ceeb_id_user) {
                            return $item['user'] == $ceeb_id_user;
                        });
                        // print_r($filter);
                        if (!empty($filter)) {
                            $filteredData = array_values($filter);
                            if (!isset($ceeb_rank[$ceeb_id_user])) {
                                $ceeb_rank[$ceeb_id_user] = [];
                                $ceeb_rank[$ceeb_id_user]['id_user'] = $ceeb_id_user;
                                $ceeb_rank[$ceeb_id_user]['ceeb'] = $filteredData[0]['ceeb'];
                            } else {
                                $ceeb_rank[$ceeb_id_user]['ceeb'] += $filteredData[0]['ceeb'];
                            }
                        }
                    }
                }

                $mapel[$key]['status'] = '-';
                unset($mapel[$key]['irt']);
                unset($mapel[$key]['irt_avg']);
            }

            if ($produk->jenis_produk == 'Masal') {
                $ceeb_sort_rank_desc = collect($ceeb_rank)->sortBy('ceeb')->reverse()->toArray();
                // print_r($ceeb_sort_rank_desc);
                $ceeb_rank = [];
                $cr_index = 0;
                $cr_rank = 1;
                foreach ($ceeb_sort_rank_desc as $value) {
                    $ceeb_rank[$cr_index] = $value;
                    $ceeb_rank[$cr_index]['rank'] = $cr_rank;
                    $cr_index++;
                    $cr_rank++;
                }
                $filter = array_filter($ceeb_rank, function ($item) use ($id_user) {
                    return $item['id_user'] == $id_user;
                });
                if (!empty($filter)) {
                    $filteredData = array_values($filter);
                    $detail['ranking'] = $filteredData[0]['rank'];
                } else {
                    $detail['ranking'] = 0;
                }
            }

            $datas[$key1]['mapel'] = $mapel;
        }

        $detail['ceeb_sum'] = round(array_sum($array_ceeb), 2);
        $detail['ceeb_avg'] = round($detail['ceeb_sum'] / count($array_ceeb), 2);

        if ($produk->jenis_produk == 'Masal') {
            // Quarter
            $tryout_hasils = DB::table('tryout_users')
                ->join('tryout_hasils', 'tryout_users.referensi', '=', 'tryout_hasils.referensi')
                ->where('id_produk', $id_produk)
                ->where('waktu_mulai', '>=', $produk->tanggal_mulai)
                ->where('waktu_selesai', '<=', $produk->tanggal_berakhir)
                ->whereNotNull('waktu_selesai')
                ->groupBy(['id_user'])->get();

            $ceeb_mapels = [];
            foreach ($tryout_hasils as $k => $v) {
                $riwayat_perhitungan = json_decode($v->riwayat_perhitungan);
                if ($riwayat_perhitungan != null && $riwayat_perhitungan->tryout) {
                    foreach ($riwayat_perhitungan->tryout as $tryoutRow) {
                        foreach ($tryoutRow->mapel as $mapelRow) {
                            $ceeb_mapels[$tryoutRow->id][$mapelRow->id_mapel][] = $mapelRow->ceeb;
                        }
                    }
                }
            }

            $ceeb_mapels_sorted = [];
            $ceeb_mapels_result = [];
            foreach ($ceeb_mapels as $tryout_id => $mapels) {
                foreach ($mapels as $mapel_id => $ceeb) {
                    sort($ceeb);
                    $ceeb_mapels_sorted[$tryout_id][$mapel_id] = $ceeb;
                    $sorted_data = $ceeb_mapels_sorted[$tryout_id][$mapel_id];
                    $count = count($ceeb);
                    $q1 = ceil(0.25 * $count) - 1;
                    $q2 = ceil(0.5 * $count) - 1;
                    $q3 = ceil(0.75 * $count) - 1;
                    $q4 = ceil(1 * $count) - 1;
                    $ceeb_mapels_result[$tryout_id][$mapel_id]['q1'] = $sorted_data[$q1];
                    $ceeb_mapels_result[$tryout_id][$mapel_id]['q2'] = $sorted_data[$q2];
                    $ceeb_mapels_result[$tryout_id][$mapel_id]['q3'] = $sorted_data[$q3];
                    $ceeb_mapels_result[$tryout_id][$mapel_id]['q4'] = $sorted_data[$q4];
                }
            }

            // print_r($ceeb_mapels_result);

            $ceeb_mapels_result = array_values($ceeb_mapels_result);
            if (!empty($ceeb_mapels_result)) {
                foreach ($datas as $key1 => $dataRow) {
                    $ceeb_mapels_result_row = array_values($ceeb_mapels_result[$key1]);
                    foreach ($dataRow['mapel'] as $key2 => $mapelRow) {
                        $status = '-';
                        if ($mapelRow['ceeb'] == $ceeb_mapels_result_row[$key2]['q2']) {
                            $status = 'Q1';
                        } else if ($mapelRow['ceeb'] >= $ceeb_mapels_result_row[$key2]['q1'] && $mapelRow['ceeb'] < $ceeb_mapels_result_row[$key2]['q2']) {
                            $status = 'Antara Q1 dan Q2';
                        } else if ($mapelRow['ceeb'] == $ceeb_mapels_result_row[$key2]['q2']) {
                            $status = 'Q2';
                        } else if ($mapelRow['ceeb'] >= $ceeb_mapels_result_row[$key2]['q2'] && $mapelRow['ceeb'] < $ceeb_mapels_result_row[$key2]['q3']) {
                            $status = 'Antara Q2 dan Q3';
                        } else if ($mapelRow['ceeb'] == $ceeb_mapels_result_row[$key2]['q3']) {
                            $status = 'Q3';
                        } else if ($mapelRow['ceeb'] >= $ceeb_mapels_result_row[$key2]['q3'] && $mapelRow['ceeb'] < $ceeb_mapels_result_row[$key2]['q4']) {
                            $status = 'Diatas Q3';
                        }
                        $datas[$key1]['mapel'][$key2]['status'] = $status;
                    }
                }
            }
        }

        $data = [
            'detail' => $detail,
            'tryout' => $datas,
        ];

        $tryoutHasil = DB::table('tryout_users')
            ->join('tryout_hasils', 'tryout_hasils.referensi', '=', 'tryout_users.referensi')
            ->join('produks', 'produks.id', '=', 'tryout_users.id_produk')
            ->where('id_user', $id_user)
            ->where('kategori_produk', 'UTBK')
            ->selectRaw("kategori_produk, DATE(waktu_selesai) as date, riwayat_perhitungan")
            ->orderBy('waktu_selesai', 'ASC')
            ->groupBy('tryout_users.referensi')
            ->get();

        $mapelData = [];
        foreach ($tryoutHasil as $key => $value) {
            if ($value->riwayat_perhitungan) {
                $riwayat = json_decode($value->riwayat_perhitungan);
                foreach ($riwayat->tryout as $key2 => $tryout) {
                    foreach ($tryout->mapel as $key3 => $mapel) {
                        $mapelData[$mapel->id_mapel]['id'] = $mapel->id_mapel;
                        $mapelData[$mapel->id_mapel]['kode'] = $mapel->kode_mapel ?? 'N/A';
                        $mapelData[$mapel->id_mapel]['nama'] = $mapel->nama_mapel ?? 'N/A';
                        $mapelData[$mapel->id_mapel]['ceeb'][] = $mapel->ceeb;
                    }
                }
            }
        }

        $mapelIDs = [];
        foreach ($mapelData as $key => $value) {
            $mapelIDs[] = $value['id'];
        }

        $rumpunKlasterData = DB::table('rumpun')
            ->selectRaw('rumpun.id AS id, rumpun.kode as kode, rumpun.nama as nama, id_mapel, nama_mapel, nilai')
            ->join('klaster', 'klaster.id_rumpun', '=', 'rumpun.id')
            ->join('mapels', 'mapels.id', '=', 'klaster.id_mapel')
            // ->whereIn('id_mapel', $mapelIDs)
            ->groupBy('rumpun.id')
            ->groupBy('id_mapel')
            ->orderBy('rumpun.id')
            ->orderBy('id_mapel')
            ->get()->toArray();


        $rumpunKlasterGroupData = [];
        foreach ($rumpunKlasterData as $key => $value) {
            $rumpunKlasterGroupData[$key]['id_rumpun'] = $value->id;
            $rumpunKlasterGroupData[$key]['nama_rumpun'] = $value->nama;
            $rumpunKlasterGroupData[$key]['kode_rumpun'] = $value->kode;
            $rumpunKlasterGroupData[$key]['id_mapel'] = $value->id_mapel;
            $rumpunKlasterGroupData[$key]['nama_mapel'] = $value->nama_mapel;
            $rumpunKlasterGroupData[$key]['nilai_klaster'] = $value->nilai;
        }

        $nilaiData = [];
        foreach ($rumpunKlasterGroupData as $key => $value) {
            $filterData = array_filter($mapelData, function ($item) use ($value) {
                return $item['id'] == $value['id_mapel'];
            });

            $nilai_rumpun = 0;
            if (!empty($filterData)) {
                $filterData = array_values($filterData)[0];
                $nilai_klaster = $value['nilai_klaster'];
                $ceeb = $this->getAverage($filterData['ceeb']);

                // Nilai CEEB rer Mapel /100 * nilai Klaster setiap Rumpun Mapel
                $nilai_rumpun = ($ceeb / 100) * $nilai_klaster;
            }

            $nilaiData[$value['id_rumpun']]['nama'] = $value['nama_rumpun'];
            $nilaiData[$value['id_rumpun']]['kode'] = $value['kode_rumpun'];
            $nilaiData[$value['id_rumpun']]['nilai'][] = $nilai_rumpun;
        }

        foreach ($nilaiData as $key => $value) {
            $resultDataTemp[$key]['nama'] = $value['nama'];
            $resultDataTemp[$key]['kode'] = $value['kode'];
            $resultDataTemp[$key]['nilai'] = $this->getAverage($value['nilai']);
        }

        usort($resultDataTemp, function ($a, $b) {
            return $b['nilai'] <=> $a['nilai'];
        });

        foreach ($resultDataTemp as $key => $value) {
            $data['rumpun'][$key]['nama'] = $value['nama'];
            $data['rumpun'][$key]['kode'] = $value['kode'];
            $data['rumpun'][$key]['nilai'] = $value['nilai'];
            $data['rumpun'][$key]['ranking'] = $key + 1;
        }

        $hasil = TryoutHasil::where('referensi', $detail['referensi']);

        if ($hasil->count() > 0) {
            $dataUpdate = $hasil->first()->riwayat_perhitungan == NULL ? json_encode($data) : json_encode($hasil);
            $hasil->first()->update(['riwayat_perhitungan' => $data, 'jumlah_peserta' => $jsp]);
        } else {

            TryoutHasil::insert([
                'referensi' => $detail['referensi'],
                'jumlah_peserta' => $jsp,
                'riwayat_perhitungan' =>  json_encode($data),
            ]);
        }

        return responseData($data);
    }

    public function tryout_hasil_pengerjaan_aspd(Request $request)
    {

        $excludes = ['superadmin', 'superAdmin', 'admin'];
        if (!in_array($this->user()->role_user, $excludes)) {
            $id_user = !empty($request->id_user) ? $request->id_user : $this->userId();
        } else {
            $id_user = $request->id_user;
        }
        $id_produk = $request->id_produk;

        // ambil detail produk
        $produk = Produk::find($id_produk);

        if (empty($produk)) {
            return response()->json(['success' => false, 'message' => 'Data produk tidak ditemukan!'], 400);
        }

        if (!empty($produk) && $produk->kategori_produk != 'ASPD') {
            return response()->json(['success' => false, 'message' => 'Data produk bukan ASPD!'], 400);
        }

        if ($produk->jenis_produk == 'Perorangan') {
            $tryoutUser = TryoutUser::with([
                'student:id_siswa,nama_lengkap,email',
                // 'jawabans',
                'produk:id,nama_produk,kategori_produk',
            ])
                ->where('id_user', $id_user)
                ->where('id_produk', $id_produk)
                ->whereHas('produk', function ($query) {
                    $query->where('jenis_produk', 'Perorangan')->whereIn('kategori_produk', ['UTBK', 'ASPD']);
                })
                ->get();

            if ($tryoutUser->count() == 0) {
                return responseFail('Tryout not found', 404);
            }

            // jumlah seluruh peserta
            $jsp = TryoutUser::where('id_produk', $id_produk)
                // ->where('id_tryout', $tryout->id)
                ->whereNotNull('waktu_selesai')
                ->groupBy(['id_user'])->get()->count();
        } else {
            $tryoutUser = TryoutUser::with([
                // 'tentor:id_teacher,nama_lengkap,email',
                // 'jawabans',
                'produk:id,nama_produk,kategori_produk',
            ])
                ->where('id_user', $id_user)
                ->where('id_produk', $id_produk)
                ->whereHas('produk', function ($query) {
                    $query->where('jenis_produk', 'Masal')->whereIn('kategori_produk', ['UTBK', 'ASPD']);
                })
                ->get();

            if ($tryoutUser->count() == 0) {
                return responseFail('Tryout not found', 404);
            }

            $tanggal_mulai_event = $produk->tanggal_mulai;
            $tanggal_berakhir_event = $produk->tanggal_berakhir;

            // jumlah seluruh peserta
            $jsp = TryoutUser::where('id_produk', $id_produk)
                ->where('waktu_mulai', '>=', $tanggal_mulai_event)
                ->where('waktu_selesai', '<=', $tanggal_berakhir_event)
                ->whereNotNull('waktu_selesai')
                ->groupBy(['id_user'])->get()->count();
        }

        $jsp = $jsp < 1 ? 1 : $jsp;


        $detail = [];
        $id_tryouts = [];

        foreach ($tryoutUser as $key => $tryoutUserRow) {
            $detail['referensi'] = $tryoutUserRow->referensi;
            $detail['id_user'] = $tryoutUserRow->id_user;
            $detail['id_produk'] = $tryoutUserRow->id_produk;
            $detail['nama_produk'] = $tryoutUserRow->produk ? $tryoutUserRow->produk->nama_produk : '-';
            $detail['tipe_event'] = $produk->jenis_produk;
            $detail['kategori_produk'] = $produk->kategori_produk;

            if ($produk->jenis_produk == 'Masal') {
                $detail['mulai_event'] = $tanggal_mulai_event;
                $detail['berakhir_event'] = $tanggal_berakhir_event;
            }

            if ($key == 0) {
                $detail['waktu_mulai'] = $tryoutUserRow->waktu_mulai;
            }
            $detail['waktu_selesai'] = $tryoutUserRow->waktu_selesai;
            $detail['jsp'] = $jsp;

            $id_tryouts[] = $tryoutUserRow->id_tryout;
        }

        // cek riwayat
        $riwayat = TryoutHasil::where('referensi', $detail['referensi'])->first();

        if ((!empty($riwayat) && $riwayat->jumlah_peserta == $jsp && $riwayat->riwayat_perhitungan !== NULL && !$request->refresh) || ($request->has('refresh') && $request->refresh == false)) {
            if (isset($riwayat->riwayat_perhitungan['detail']) && @$riwayat->riwayat_perhitungan['detail']['kategori_produk'] == 'ASPD') {
                // return responseData($riwayat->riwayat_perhitungan);
            }
        }

        // perhitungan skor nilai
        $perhitungan_skor = $produk->perhitungan;

        $nilai_correct = isset($perhitungan_skor['correct']) ? $perhitungan_skor['correct'] : 1;
        $nilai_wrong = isset($perhitungan_skor['wrong']) ? $perhitungan_skor['wrong'] : 0;
        $nilai_unfilled = isset($perhitungan_skor['unfilled']) ? $perhitungan_skor['unfilled'] : 0;

        $data_skor = [];

        $dataTryouts = Tryout::with([
            'soal', 'soal.pertanyaan' => function ($query) {
                return $query->where('soal_pertanyaans.parent_soal_pertanyaan', '=', null)->get();
            }, 'soal.pertanyaan.pertanyaan_child',
            'soal.mapel:id,nama_mapel,kode'
        ])->whereIn('id', $id_tryouts)->orderBy('jenis_soal', 'DESC')->get();

        if ($tryoutUser->count() == 0) {
            return responseFail('Tryout not found', 404);
        }

        $sumNilai = 0;
        $countMapel = 0;
        $rerataNilai = 0;

        $datas = [];
        $jsp = $jsp > 0 ? $jsp : 1;
        foreach ($dataTryouts as $key1 => $tryout) {

            $datas[$key1]['id'] = $tryout->id;
            $datas[$key1]['judul'] = $tryout->judul;
            $datas[$key1]['judul_edit'] = $tryout->judul;

            $datas[$key1]['kategori'] = $tryout->kategori;
            $datas[$key1]['jenis_soal'] = $tryout->jenis_soal;
            $datas[$key1]['kelompok_soal'] = $tryout->kelompok_soal;

            $index = 0;
            $nomor = 1;

            $list_soal = $tryout->soal;

            $mapel = [];

            // // jumlah seluruh peserta
            // $jsp = TryoutUser::where('id_produk', $id_produk)
            //     ->where('id_tryout', $tryout->id)
            //     ->whereNotNull('waktu_selesai')
            //     ->groupBy('id_user')->get()->count();
            // echo 'JPS:' . $jsp;
            // echo '<br>';

            foreach ($list_soal as $key => $soal) {
                $pertanyaan_soal = $soal->pertanyaan;

                $id_mapel = $soal->id_mapel;
                $nama_mapel = $soal->mapel->nama_mapel;
                $kode_mapel = $soal->mapel->kode;
                $id_soal_tryout = $soal->id;

                $mapel[$key]['id_mapel'] = $id_mapel;
                $mapel[$key]['nama_mapel'] = $nama_mapel;
                $mapel[$key]['kode_mapel'] = $kode_mapel;
                $mapel[$key]['status'] = '-';
                $mapel[$key]['jsp'] = 1;
                $mapel[$key]['jumlah_benar'] = 0;
                $mapel[$key]['jumlah_kosong'] = 0;
                $mapel[$key]['jumlah_salah'] = 0;
                $mapel[$key]['total_soal'] = 0;

                foreach ($pertanyaan_soal as $keyp => $pertanyaan) {

                    $id =  $pertanyaan->id;

                    $mapel[$key]['jsp'] = $jsp;
                    // $mapel[$key]['jpjb'][$id] = $jpjb;
                    // $mapel[$key]['pi'][$id] = $pi;
                    // $mapel[$key]['ni'][$id] = $ni;

                    // $this->__get_sum_irt_all_siswa([
                    //     'jps' => $jps,
                    //     'jpjb' => $jpjb,
                    //     'pi' =>
                    // ]);

                    $pertanyaan_child = $pertanyaan->pertanyaan_child;

                    // jawaban user
                    $dataJawabanUser = TryoutUserJawaban::with(['tryout_user:id,id_user'])
                        ->select(['jawaban_user'])
                        ->where('id_soal_tryout', $pertanyaan->id_soal_tryout)
                        ->where('id_soal_pertanyaan', $pertanyaan->id)
                        ->whereHas('tryout_user', function ($query) use ($id_user) {
                            $query->where('id_user', $id_user);
                        })->first();

                    $jawaban_user = isset($dataJawabanUser->jawaban_user) ? $dataJawabanUser->jawaban_user : null;

                    // pencocokan jawaban
                    if ($jawaban_user == $pertanyaan->jawaban_pertanyaan) {
                        $mapel[$key]['jumlah_benar'] += 1;
                    } else if (empty($jawaban_user) || $jawaban_user == null || $jawaban_user = '') {
                        $mapel[$key]['jumlah_kosong'] += 1;
                    } else {
                        $mapel[$key]['jumlah_salah'] += 1;
                    }

                    $mapel[$key]['total_soal'] += 1;
                    // echo $detail['tipe_event'];
                    // die;

                    $nomor++;
                    $index++;

                    foreach ($pertanyaan_child as $keypc => $child) {

                        $id = $child->id;

                        $mapel[$key]['jsp'] = $jsp;
                        // $mapel[$key]['jpjb'][$id] = $jpjb;
                        // $mapel[$key]['pi'][$id] = $pi;

                        // jawaban user
                        $dataJawabanUser = TryoutUserJawaban::with(['tryout_user'])
                            ->select(['jawaban_user'])
                            ->where('id_soal_tryout', $child->id_soal_tryout)
                            ->where('id_soal_pertanyaan', $child->id)
                            ->whereHas('tryout_user', function ($query) use ($id_user) {
                                $query->where('id_user', $id_user);
                            })->first();

                        // $jawaban_user = @$dataJawabanUser['jawaban_user'];

                        // // pencocokan jawaban
                        // if ($jawaban_user == $child->jawaban_pertanyaan) {
                        //     $mapel[$key]['jumlah_benar'] += 1;
                        // } else if (empty($jawaban_user) || $jawaban_user == NULL) {
                        //     $mapel[$key]['jumlah_kosong'] += 1;
                        // } else {
                        //     $mapel[$key]['jumlah_salah'] += 1;
                        // }

                        $jawaban_user = isset($dataJawabanUser->jawaban_user) ? $dataJawabanUser->jawaban_user : null;

                        // pencocokan jawaban
                        if ($jawaban_user == $child->jawaban_pertanyaan) {
                            $mapel[$key]['jumlah_benar'] += 1;
                        } else if (empty($jawaban_user) || $jawaban_user == null || $jawaban_user = '') {
                            $mapel[$key]['jumlah_kosong'] += 1;
                        } else {
                            $mapel[$key]['jumlah_salah'] += 1;
                        }

                        $mapel[$key]['total_soal'] += 1;

                        $nomor++;
                        $index++;
                    }
                }

                $total_soal = $mapel[$key]['total_soal'] > 0 ? $mapel[$key]['total_soal'] : 1;
                $mapel[$key]['nilai'] =  ($mapel[$key]['jumlah_benar'] / $total_soal) * 100;
                $sumNilai += $mapel[$key]['nilai'];
                $mapel[$key]['nilai_label'] = number_format($mapel[$key]['nilai'], 2, ',', '.');
                $countMapel++;
            }

            $datas[$key1]['mapel'] = $mapel;
        }

        $rerataNilai =  $sumNilai / $countMapel;
        $detail['sum_nilai'] = $sumNilai;
        $detail['rerata_nilai'] = $rerataNilai;
        $detail['rerata_nilai_label'] = number_format($rerataNilai, 2, ',', '.');

        if ($produk->jenis_produk == 'Masal') {
            // TryoutUser::where('id_produk', $id_produk)
            //     ->where('waktu_mulai', '>=', $tanggal_mulai_event)
            //     ->where('waktu_selesai', '<=', $tanggal_berakhir_event)
            //     ->whereNotNull('waktu_selesai')
            //     ->groupBy(['id_user'])->get()->count();
            $tryout_hasils = DB::table('tryout_users')
                ->join('tryout_hasils', 'tryout_users.referensi', '=', 'tryout_hasils.referensi')
                ->where('id_produk', $id_produk)
                ->where('waktu_mulai', '>=', $produk->tanggal_mulai)
                ->where('waktu_selesai', '<=', $produk->tanggal_berakhir)
                ->whereNotNull('waktu_selesai')
                ->groupBy(['id_user'])->get();

            $array_rerata = [];
            foreach ($tryout_hasils as $k => $v) {
                $riwayat_perhitungan = json_decode($v->riwayat_perhitungan);
                if ($riwayat_perhitungan != null) {
                    $rerata_nilai = $riwayat_perhitungan->detail->rerata_nilai;
                    $id_user = $riwayat_perhitungan->detail->id_user;
                    $array_rerata[$k]['user'] = $id_user;
                    $array_rerata[$k]['rerata'] = $rerata_nilai;
                } else {
                    $array_rerata[$k]['user'] = $v->id_user;
                    $array_rerata[$k]['rerata'] = 0;
                }
            }

            $array_rerata = collect($array_rerata)->sortBy('rerata')->reverse()->toArray();

            $aspd_rank = [];
            $ar_index = 0;
            $user_rank = 1;
            foreach ($array_rerata as $value) {
                $aspd_rank[$ar_index] = $value;
                $aspd_rank[$ar_index]['rank'] = $user_rank;
                $ar_index++;
                $user_rank++;
            }
            // print_r($aspd_rank);
            $filter = array_filter($aspd_rank, function ($item) use ($id_user) {
                return $item['user'] == $id_user;
            });
            if (!empty($filter)) {
                $filteredData = array_values($filter);
                $detail['ranking'] = $filteredData[0]['rank'];
            } else {
                $detail['ranking'] = 1;
            }
        }

        if ($produk->jenis_produk == 'Masal') {
            // Quarter
            $tryout_hasils = DB::table('tryout_users')
                ->join('tryout_hasils', 'tryout_users.referensi', '=', 'tryout_hasils.referensi')
                ->where('id_produk', $id_produk)
                ->where('waktu_mulai', '>=', $produk->tanggal_mulai)
                ->where('waktu_selesai', '<=', $produk->tanggal_berakhir)
                ->whereNotNull('waktu_selesai')
                ->groupBy(['id_user'])->get();

            $nilai_mapels = [];
            foreach ($tryout_hasils as $k => $v) {
                $riwayat_perhitungan = json_decode($v->riwayat_perhitungan);
                if ($riwayat_perhitungan != null && $riwayat_perhitungan->tryout) {
                    foreach ($riwayat_perhitungan->tryout as $tryoutRow) {
                        foreach ($tryoutRow->mapel as $mapelRow) {
                            $nilai_mapels[$tryoutRow->id][$mapelRow->id_mapel][] = $mapelRow->nilai;
                            $nilai_mapels[$tryoutRow->id][$mapelRow->id_mapel][] = 150;
                            $nilai_mapels[$tryoutRow->id][$mapelRow->id_mapel][] = 250;
                            $nilai_mapels[$tryoutRow->id][$mapelRow->id_mapel][] = 800;
                            $nilai_mapels[$tryoutRow->id][$mapelRow->id_mapel][] = 600;
                            $nilai_mapels[$tryoutRow->id][$mapelRow->id_mapel][] = 160;
                            $nilai_mapels[$tryoutRow->id][$mapelRow->id_mapel][] = 350;
                        }
                    }
                }
            }

            // print_r($nilai_mapels); die;


            $nilai_mapels_sorted = [];
            $nilai_mapels_result = [];
            foreach ($nilai_mapels as $tryout_id => $mapels) {
                foreach ($mapels as $mapel_id => $nilai) {
                    sort($nilai);
                    $nilai_mapels_sorted[$tryout_id][$mapel_id] = $nilai;
                    $sorted_data = $nilai_mapels_sorted[$tryout_id][$mapel_id];
                    $count = count($nilai);
                    $q1 = ceil(0.25 * $count) - 1;
                    $q2 = ceil(0.5 * $count) - 1;
                    $q3 = ceil(0.75 * $count) - 1;
                    $q4 = ceil(1 * $count) - 1;
                    $nilai_mapels_result[$tryout_id][$mapel_id]['q1'] = $sorted_data[$q1];
                    $nilai_mapels_result[$tryout_id][$mapel_id]['q2'] = $sorted_data[$q2];
                    $nilai_mapels_result[$tryout_id][$mapel_id]['q3'] = $sorted_data[$q3];
                    $nilai_mapels_result[$tryout_id][$mapel_id]['q4'] = $sorted_data[$q4];
                }
            }

            // print_r($nilai_mapels_result);
            // print_r($nilai_mapels_result);

            $nilai_mapels_result = array_values($nilai_mapels_result);
            if (!empty($nilai_mapels_result)) {
                foreach ($datas as $key1 => $dataRow) {
                    $nilai_mapels_result_row = array_values($nilai_mapels_result[$key1]);
                    foreach ($dataRow['mapel'] as $key2 => $mapelRow) {
                        $status = '-';
                        if ($mapelRow['nilai'] == $nilai_mapels_result_row[$key2]['q2']) {
                            $status = 'Q1';
                        } else if ($mapelRow['nilai'] >= $nilai_mapels_result_row[$key2]['q1'] && $mapelRow['nilai'] < $nilai_mapels_result_row[$key2]['q2']) {
                            $status = 'Antara Q1 dan Q2';
                        } else if ($mapelRow['nilai'] == $nilai_mapels_result_row[$key2]['q2']) {
                            $status = 'Q2';
                        } else if ($mapelRow['nilai'] >= $nilai_mapels_result_row[$key2]['q2'] && $mapelRow['nilai'] < $nilai_mapels_result_row[$key2]['q3']) {
                            $status = 'Antara Q2 dan Q3';
                        } else if ($mapelRow['nilai'] == $nilai_mapels_result_row[$key2]['q3']) {
                            $status = 'Q3';
                        } else if ($mapelRow['nilai'] >= $nilai_mapels_result_row[$key2]['q3'] && $mapelRow['nilai'] < $nilai_mapels_result_row[$key2]['q4']) {
                            $status = 'Diatas Q3';
                        }
                        $datas[$key1]['mapel'][$key2]['status'] = $status;
                    }
                }
            }
        }


        $data = [
            'detail' => $detail,
            'tryout' => $datas,
        ];

        $hasil = TryoutHasil::where('referensi', $detail['referensi']);

        if ($hasil->count() > 0) {
            // $dataUpdate = $hasil->first()->riwayat_perhitungan == NULL ? json_encode($data) : json_encode($hasil);
            $hasil->first()->update(['riwayat_perhitungan' => $data, 'jumlah_peserta' => $jsp]);
        } else {
            TryoutHasil::insert([
                'referensi' => $detail['referensi'],
                'jumlah_peserta' => $jsp,
                'riwayat_perhitungan' =>  json_encode($data),
            ]);
        }

        return responseData($data);
    }

    public function tryout_persentase_skor(Request $request)
    {
        $id_user = !empty($request->id_user) ? $request->id_user : $this->userId();
        $id_jenjang = $request->id_jenjang;
        $id_kelas = $request->id_kelas;
        $id_penjurusan = $request->id_penjurusan;
        $where = [];
        if (isset($request->id_penjurusan) && !empty($id_penjurusan)) {
            $where['id_penjurusan'] = $request->id_penjurusan;
        } else if (isset($request->id_kelas) && !empty($id_kelas)) {
            $findPenjurusan = Penjurusan::where('id_kelas', $request->id_kelas)->first();
            if ($findPenjurusan) {
                $where['id_penjurusan'] = $findPenjurusan->id;
            }
        } else if (isset($request->id_jenjang) && !empty($id_jenjang)) {
            $where['id_jenjang'] = $request->id_jenjang;
        }

        // ambil data tryout yang pernah diikut
        // $references = TryoutUser::with(['produk'])->where('id_user', $id_user)
        //     ->whereHas('produk', function ($subquery) {
        //         $subquery->where('kategori_produk', 'UTBK');
        //     })->groupBy('referensi')->get();

        // ambil data tryout yang pernah diikut
        $references = TryoutUser::join('produks', 'produks.id', '=', 'tryout_users.id_produk')
            // ->join('produk_pakets', 'produk_pakets.id_produk', '=', 'produks.id')
            ->join('tryouts', 'tryouts.id', '=', 'tryout_users.id_tryout')
            ->where('id_user', $id_user)
            ->where('kategori_produk', 'UTBK')
            ->where($where)
            ->groupBy('referensi')->get();


        if (count($references) < 1) {
            return responseFail('Not yet participated!', 400);
        }

        $ceeb_users = [];
        foreach ($references as $key => $dataRow) {
            $hasil = TryoutHasil::where('referensi', $dataRow->referensi)->first();
            if ($hasil) {
                $riwayat = $hasil->riwayat_perhitungan;
                $ceeb = @$riwayat['detail']['ceeb_avg'] ?? 0;
                $ceeb_users[] = $ceeb;
            }
        }

        /* PERSENTASE SKOR UTAMA */

        $average = $this->getAverage($ceeb_users);

        $student = Student::where('id_siswa', $id_user)->first();
        $pg1_data = ProgramStudiPerguruanTinggi::join('program_studi', 'program_studi.id', '=', 'program_studi_and_perguruan_tinggi.id_program_studi')->find($student->id_prodi_bind_perguruan);
        if ($pg1_data == NULL) {
            $pg1 = -1;
        } else {
            $pg1 = (int) $pg1_data->passing_grade_prodi;
        }
        $pg2_data = ProgramStudiPerguruanTinggi::join('program_studi', 'program_studi.id', '=', 'program_studi_and_perguruan_tinggi.id_program_studi')->find($student->id_prodi_bind_perguruan_2);
        if ($pg2_data == NULL) {
            $pg2 = -1;
        } else {
            $pg2 = (int) $pg2_data->passing_grade_prodi;
        }

        $toleransi = 0.25;

        $id_prodis = [];
        $id_perguruans = [];
        $id_rumpuns = [];
        $toleransiKebawah = [];
        $toleransiKeatas = [];
        if (!empty($pg1)) {
            $id_prodis[] = $pg1_data->id_program_studi;
            $id_perguruans[] = $pg1_data->id_perguruan_tinggi;
            $id_rumpuns[] = $pg1_data->id_rumpun;
            // $toleransiKebawah[] = $pg1 - ($pg1 * $toleransi);
            // $toleransiKeatas[] = $pg1 + ($pg1 * $toleransi);
        }
        if (!empty($pg2)) {
            $id_prodis[] = $pg2_data->id_program_studi;
            $id_perguruans[] = $pg2_data->id_perguruan_tinggi;
            $id_rumpuns[] = $pg2_data->id_rumpun;
            // $toleransiKebawah[] = $pg2 - ($pg2 * $toleransi);
            // $toleransiKeatas[] = $pg2 + ($pg2 * $toleransi);
        }

        $toleransiKebawah[] = $average - ($average * $toleransi);
        $toleransiKeatas[] = $average + ($average * $toleransi);

        $toleransiKebawah = min($toleransiKebawah);
        $toleransiKeatas = max($toleransiKeatas);
        if (!empty($id_rumpuns)) {
            $id_rumpuns = collect($id_rumpuns)->unique();
        }
        // print_r($id_perguruans);
        // print_r($id_rumpuns);
        // print_r($toleransiKebawah);
        // print_r($toleransiKeatas);

        /* DAPATKAN REKOMENDASI DARI PT LAIN */

        $ptLain = DB::table('program_studi_and_perguruan_tinggi')
            ->join('perguruan_tinggi', 'perguruan_tinggi.id', '=', 'program_studi_and_perguruan_tinggi.id_perguruan_tinggi')
            ->whereIn('id_program_studi', $id_prodis)
            ->whereNotIn('id_perguruan_tinggi', $id_perguruans)
            ->whereRaw('passing_grade_prodi >= ' . $toleransiKebawah)
            ->whereRaw('passing_grade_prodi <= ' . $toleransiKeatas)
            ->groupBy('id_perguruan_tinggi')
            ->select(['nama_perguruan', 'passing_grade_prodi'])
            ->limit(5)
            ->orderBy('passing_grade_prodi', 'ASC')
            ->get();

        // print_r($ptLain);

        /* DAPATKAN REKOMENDASI DARI PRODI LAIN PT SAMA */
        $prodiLain = DB::table('program_studi_and_perguruan_tinggi')
            ->join('perguruan_tinggi', 'perguruan_tinggi.id', '=', 'program_studi_and_perguruan_tinggi.id_perguruan_tinggi')
            ->join('program_studi', 'program_studi.id', '=', 'program_studi_and_perguruan_tinggi.id_program_studi')
            ->whereIn('id_perguruan_tinggi', $id_perguruans)
            ->whereNotIn('id_program_studi', $id_prodis)
            ->whereRaw('passing_grade_prodi >= ' . $toleransiKebawah)
            ->whereRaw('passing_grade_prodi <= ' . $toleransiKeatas)
            ->groupBy('id_perguruan_tinggi')
            ->select(['nama_perguruan', 'passing_grade_prodi', 'nama_studi'])
            ->limit(5)
            ->orderBy('passing_grade_prodi', 'ASC')
            ->get();

        /* DAPATKAN REKOMENDASI DARI PRODI LAIN PT SAMA atau PT BEDA */
        $prodiPerRumpun = DB::table('program_studi_and_perguruan_tinggi')
            ->join('perguruan_tinggi', 'perguruan_tinggi.id', '=', 'program_studi_and_perguruan_tinggi.id_perguruan_tinggi')
            ->join('program_studi', 'program_studi.id', '=', 'program_studi_and_perguruan_tinggi.id_program_studi')
            ->whereIn('id_rumpun', $id_rumpuns)
            ->whereNotIn('id_program_studi', $id_prodis)
            ->whereRaw('passing_grade_prodi >= ' . $toleransiKebawah)
            ->whereRaw('passing_grade_prodi <= ' . $toleransiKeatas)
            ->groupBy('id_perguruan_tinggi')
            ->select(['nama_perguruan', 'passing_grade_prodi', 'nama_studi', 'rumpun'])
            ->limit(5)
            ->orderBy('passing_grade_prodi', 'ASC')
            ->get();



        $resultData = [
            'avg_score' => $average,
            'pg1' => $pg1,
            'pg1_percent' => round(($average / $pg1) * 100, 2),
            'pg2' => $pg2,
            'pg2_percent' => round(($average / $pg2) * 100, 2),
            'potencies' => [
                'univ' => $ptLain ?? [],
                'prodi' => $prodiLain ?? [],
                'rumpun' => $prodiPerRumpun ?? []
            ]
        ];

        return responseData($resultData);
    }

    public function tryout_persentase_skor_aspd(Request $request)
    {
        $id_user = !empty($request->id_user) ? $request->id_user : $this->userId();
        $id_jenjang = $request->id_jenjang;
        $id_kelas = $request->id_kelas;
        $id_penjurusan = $request->id_penjurusan;
        $where = [];
        if (isset($request->id_penjurusan) && !empty($id_penjurusan)) {
            $where['tryouts.id_penjurusan'] = $request->id_penjurusan;
        } else if (isset($request->id_kelas) && !empty($id_kelas)) {
            $findPenjurusan = Penjurusan::where('id_kelas', $request->id_kelas)->first();
            if ($findPenjurusan) {
                $where['tryouts.id_penjurusan'] = $findPenjurusan->id;
            }
        } else if (isset($request->id_jenjang) && !empty($id_jenjang)) {
            $where['tryouts.id_jenjang'] = $request->id_jenjang;
        }

        // ambil data tryout yang pernah diikut
        $references = TryoutUser::/* with(['produk'])-> */where('id_user', $id_user)
            // ->whereHas('produk', function ($subquery) {
            //     $subquery->where('kategori_produk', 'ASPD');
            // })
            ->join('produks', 'produks.id', '=', 'tryout_users.id_produk')
            // ->join('produk_pakets', 'produk_pakets.id_produk', '=', 'produks.id')
            ->join('tryouts', 'tryouts.id', '=', 'tryout_users.id_tryout')
            ->where('tryout_users.id_user', $id_user)
            ->where('produks.kategori_produk', 'ASPD')
            ->where($where)
            ->groupBy('referensi')->get();

        if (count($references) < 1) {
            return responseFail('Not yet participated!', 400);
        }

        $nilai_users = [];
        foreach ($references as $key => $dataRow) {
            $hasil = TryoutHasil::where('referensi', $dataRow->referensi)->first();
            if ($hasil) {
                $riwayat = $hasil->riwayat_perhitungan;
                $nilai = @$riwayat['detail']['rerata_nilai'] ?? 0;
                $nilai_users[] = $nilai;
            }
        }

        $average = $this->getAverage($nilai_users);

        $student = Student::where('id_siswa', $id_user)->first();
        $percent = 0;
        $nilai_sempurna = 400;
        if (in_array($student->nama_jenjang, ['SMA', 'SMK', 'MA'])) {
            $nilai_sempurna = 400;
        } elseif (in_array($student->nama_jenjang, ['SMP', 'MTS'])) {
            $nilai_sempurna = 400;
        } else {
            $nilai_sempurna = 300;
        }
        $percent = round(($average / $nilai_sempurna) * 100, 2);

        return responseData([
            'perpect_score' => $nilai_sempurna,
            'avg_score' => $average,
            'percent_score' => $percent,
        ]);
    }

    public function tryout_generate_certificate(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'id_produk'      => 'required',
            'referensi'     => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        $excludes = ['superadmin', 'superAdmin', 'admin'];
        if (!in_array($this->user()->role_user, $excludes)) {
            $id_user = !empty($request->id_user) ? $request->id_user : $this->userId();
        } else {
            $id_user = $request->id_user;
        }
        $id_produk = $request->id_produk;
        $referensi = $request->referensi;

        // ambil detail produk
        $produk = Produk::find($id_produk);

        if (empty($produk)) {
            return response()->json(['success' => false, 'message' => 'Data produk tidak ditemukan!'], 400);
        }

        // if (!empty($produk) && $produk->kategori_produk != 'UTBK') {
        //     return response()->json(['success' => false, 'message' => 'Data produk bukan UTBK!'], 400);
        // }

        if ($produk->jenis_produk == 'Perorangan') {
            $tryoutUser = TryoutUser::with([
                'student:id_siswa,nama_lengkap,email',
                'produk:id,nama_produk,kategori_produk',
            ])
                ->where('id_user', $id_user)
                ->where('id_produk', $id_produk)
                ->where('referensi', $referensi)
                ->whereHas('produk', function ($query) {
                    $query->where('jenis_produk', 'Perorangan')->whereIn('kategori_produk', ['UTBK', 'ASPD']);
                })
                ->get();

            if ($tryoutUser->count() == 0) {
                return responseFail('Tryout not found', 404);
            }
        } else {
            $tryoutUser = TryoutUser::with([
                'produk:id,nama_produk,kategori_produk',
            ])
                ->where('id_user', $id_user)
                ->where('id_produk', $id_produk)
                ->where('referensi', $referensi)
                ->whereHas('produk', function ($query) {
                    $query->where('jenis_produk', 'Masal')->whereIn('kategori_produk', ['UTBK', 'ASPD']);
                })
                ->get();
            if ($tryoutUser->count() == 0) {
                return responseFail('Tryout not found', 404);
            }
        }

        // cek riwayat
        $riwayat = TryoutHasil::where('referensi', $request->referensi)->first();

        if (!empty($riwayat)) {

            $profile = Student::where('id_siswa', $id_user)->first();
            $data['result'] = $riwayat->riwayat_perhitungan;
            $data['profile'] = $profile;
            $data['kategori'] = $produk->kategori_produk;

            $file_name = "cert-" . $data['result']['detail']['referensi'] . ".pdf";
            $lokasi_file = asset("file/$file_name");

            try {

                if ($produk->kategori_produk == 'UTBK') {
                    $pdf = PDF::loadView('print/to_certificate', $data);
                } else if ($produk->kategori_produk == 'ASPD') {
                    $pdf = PDF::loadView('print/to_aspd_certificate', $data);
                }
                // $pdf->setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif', 'isRemoteEnabled' => true]);
                $pdf->setPaper('a4', 'portrait')->setWarnings(false);
                $pdf->save(public_path('file/' . $file_name));

                if ($request->has('send_to_email') && $request->send_to_email == 1) {

                    // buat notifikasi
                    $notificationModel = new Notification();

                    $notificationModel->insertData('insertData', [
                        'recipientId' => $id_user,
                        'recipientType' => 3, // student
                        'type' => 2, // tryout
                        'title' => 'Sertifikat hasil tryout-mu berhasil dibuat!',
                        'body' => "Cek Email Kamu yaa..",
                        'data' => [
                            'download_url' => $lokasi_file
                        ]
                    ]);

                    $testMail = new TestMail([
                        'subject' => "Sertifikat hasil tryout-mu berhasil dibuat!",
                        'to' => $profile->email,
                        'toName' => $profile->nama_lengkap,
                        'content' => [
                            'title' => "Ini adalah sertitikat hasil tryout <b>$produk->kategori_produk</b>-mu!",
                            'body' => 'Cek lampiran pada email ini ya..',
                        ],
                        // 'data' => [
                        //     'download_url' => $lokasi_file
                        // ],,
                        'attachmentFile' => public_path('file/' . $file_name),
                        'view' => 'email.content.customer__generate-certificate'
                    ]);

                    Mail::send($testMail);

                    $response = [
                        'code' => 200,
                        'success' => true,
                        'message' => 'Sertifikat berhasil dikirimkan ke Email kamu!',
                        'data' => $lokasi_file,

                    ];
                } else {
                    $response = [
                        'code' => 200,
                        'success' => true,
                        'message' => 'Generate Sertifikat berhasil!',
                        'data' => $lokasi_file

                    ];
                }
            } catch (\Throwable $th) {
                throw $th;
                $response = [
                    'code' => 500,
                    'success' => false,
                    'message' => 'Generate Sertifikat gagal!',
                    'error' => $th,
                ];
            }

            return response()->json($response, $response['code']);
        } else {
            return responseFail('Tryout result not found.');
        }
    }

    public function tryout_hasil_per_pengerjaan(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'kategori'      => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        $kategori = $request->kategori;
        $id_jenjang = $request->id_jenjang;
        $id_kelas = $request->id_kelas;
        $id_penjurusan = $request->id_penjurusan;
        $where = [];
        if (isset($request->id_penjurusan) && !empty($id_penjurusan)) {
            $where['id_penjurusan'] = $request->id_penjurusan;
        } else if (isset($request->id_kelas) && !empty($id_kelas)) {
            $findPenjurusan = Penjurusan::where('id_kelas', $request->id_kelas)->first();
            if ($findPenjurusan) {
                $where['id_penjurusan'] = $findPenjurusan->id;
            }
        } else if (isset($request->id_jenjang) && !empty($id_jenjang)) {
            $where['id_jenjang'] = $request->id_jenjang;
        }

        $role_user = auth()->user()->role_user;
        $id_user = auth()->user()->id;
        if ($role_user == 'parent') {
            $id_user = OrangTua::where('id_user', $id_user)->first()->id_orang_tua;
        }


        $tryoutHasil = DB::table('tryout_users')
            ->join('tryout_hasils', 'tryout_hasils.referensi', '=', 'tryout_users.referensi')
            ->join('produks', 'produks.id', '=', 'tryout_users.id_produk')
            // ->join('produk_pakets', 'produk_pakets.id_produk', '=', 'produks.id')
            ->join('tryouts', 'tryouts.id', '=', 'tryout_users.id_tryout')
            ->where('id_user', $id_user)
            ->where('kategori_produk', $kategori)
            ->where($where)
            ->selectRaw("kategori_produk, DATE(waktu_selesai) as date, riwayat_perhitungan")
            ->orderBy('waktu_selesai', 'ASC')
            ->groupBy('tryout_users.referensi')
            ->limit(7)
            ->get();

        $resultData = [
            'kategori' => $kategori,
            'label' => [],
            'data' => [],
            'kenaikan' => '0.00'
        ];

        foreach ($tryoutHasil as $key => $value) {
            $resultData['label'][$key] = $value->date;
            if ($value->riwayat_perhitungan) {
                $riwayat = json_decode($value->riwayat_perhitungan);
                if ($kategori == 'UTBK') {
                    $resultData['data'][$key] = $riwayat->detail->ceeb_avg;
                } elseif ($kategori == 'ASPD') {
                    $resultData['data'][$key] = $riwayat->detail->rerata_nilai > 0 ? $riwayat->detail->rerata_nilai : 0;
                }
            } else {
                $resultData['data'][$key] = 0;
            }
        }

        // print_r($resultData); die;

        if (count($resultData['data']) >= 2) {
            $lastIndex = count($resultData['data']) - 1;
            $secondLastNilai = $resultData['data'][$lastIndex - 1];
            $lastNilai = $resultData['data'][$lastIndex];
            // $lastNilai = 700;
            $res = (($lastNilai - $secondLastNilai) / ($secondLastNilai > 0 ? $secondLastNilai : 1)) * 100;
            $resultData['kenaikan'] = number_format($res, 2);
        }

        // dd($tryoutHasil);

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Data hasil TO per pengerjaan per tanggal.',
            'data' => $resultData
        ]);
    }

    public function tryout_hasil_nilai_per_mapel(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'kategori'      => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        $kategori = $request->kategori;
        $id_jenjang = $request->id_jenjang;
        $id_kelas = $request->id_kelas;
        $id_penjurusan = $request->id_penjurusan;
        $where = [];
        if (isset($request->id_penjurusan) && !empty($id_penjurusan)) {
            $where['id_penjurusan'] = $request->id_penjurusan;
        } else if (isset($request->id_kelas) && !empty($id_kelas)) {
            $findPenjurusan = Penjurusan::where('id_kelas', $request->id_kelas)->first();
            if ($findPenjurusan) {
                $where['id_penjurusan'] = $findPenjurusan->id;
            }
        } else if (isset($request->id_jenjang) && !empty($id_jenjang)) {
            $where['id_jenjang'] = $request->id_jenjang;
        }

        $role_user = auth()->user()->role_user;
        $id_user = auth()->user()->id;
        if ($role_user == 'parent') {
            $id_user = OrangTua::where('id_user', $id_user)->first()->id_orang_tua;
        }

        $tryoutHasil = DB::table('tryout_users')
            ->join('tryout_hasils', 'tryout_hasils.referensi', '=', 'tryout_users.referensi')
            ->join('produks', 'produks.id', '=', 'tryout_users.id_produk')
            // ->join('produk_pakets', 'produk_pakets.id_produk', '=', 'produks.id')
            ->join('tryouts', 'tryouts.id', '=', 'tryout_users.id_tryout')
            ->where('id_user', $id_user)
            ->where('kategori_produk', $kategori)
            ->where($where)
            ->selectRaw("kategori_produk, DATE(waktu_selesai) as date, riwayat_perhitungan")
            ->orderBy('waktu_selesai', 'ASC')
            ->groupBy('tryout_users.referensi')
            ->limit(7)
            ->get();

        $mapelData = [];
        $mapelTotalSoalData = [];
        $mapelJumlahBenarData = [];
        $jenisSoal = [];
        foreach ($tryoutHasil as $key => $value) {
            if ($value->riwayat_perhitungan) {
                $riwayat = json_decode($value->riwayat_perhitungan);
                foreach ($riwayat->tryout as $key2 => $tryout) {
                    $jenis_soal = $tryout->jenis_soal ?? 0;
                    foreach ($tryout->mapel as $key3 => $mapel) {
                        // print_r($mapel);
                        // $per_mapel_total_soal = 0;
                        // if (isset($mapel->total_soal)) {
                        //     $per_mapel_total_soal = $mapel->total_soal;
                        // } else {
                        // $per_mapel_total_soal = $mapel->raw->t_b + $mapel->raw->t_s + $mapel->raw->t_k;
                        // }
                        if ($kategori == 'UTBK') {
                            $per_mapel_total_soal = $mapel->raw->t_b + $mapel->raw->t_s + $mapel->raw->t_k;
                            $mapelJumlahBenarData[$jenis_soal][$mapel->id_mapel][] = $mapel->raw->t_b;
                        } else {
                            $per_mapel_total_soal = $mapel->jumlah_benar + $mapel->jumlah_salah + $mapel->jumlah_kosong;
                            $mapelJumlahBenarData[$jenis_soal][$mapel->id_mapel][] = $mapel->jumlah_benar;
                        }
                        $mapelTotalSoalData[$jenis_soal][$mapel->id_mapel][] = $per_mapel_total_soal;

                        $mapelData[$jenis_soal][$mapel->id_mapel]['id'] = $mapel->id_mapel;
                        $mapelData[$jenis_soal][$mapel->id_mapel]['kode'] = $mapel->kode_mapel ?? 'N/A';
                        $mapelData[$jenis_soal][$mapel->id_mapel]['nama'] = $mapel->nama_mapel ?? 'N/A';
                    }
                }
            }
        }

        // print_r($mapelTotalSoalData);
        // print_r($mapelJumlahBenarData);

        $resultData = [];
        $labelData = [];
        $capaianTertinggiData = [];
        $totalSoalData = [];
        $rerataData = [];
        $resultMapelData = [];
        foreach ($mapelData as $jenis => $jenisData) {
            $i = 0;
            foreach ($jenisData as $id_mapel => $perJenisValue) {
                $labelData[$id_mapel] = $perJenisValue['kode'];
                $capaianTertinggiData[$id_mapel] = max($mapelJumlahBenarData[$jenis][$id_mapel]);
                $rerataData[$id_mapel] = $this->getAverage($mapelJumlahBenarData[$jenis][$id_mapel]);
                // $totalSoalData[$id_mapel] = $this->getAverage($mapelTotalSoalData[$jenis][$id_mapel]);
                $totalSoalData[$id_mapel] = max($mapelTotalSoalData[$jenis][$id_mapel]);

                $resultMapelData[$id_mapel]['nama'] = $perJenisValue['nama'];
                $resultMapelData[$id_mapel]['kode'] = $perJenisValue['kode'];
                $resultMapelData[$id_mapel]['persentase'] = $rerataData[$id_mapel] / $totalSoalData[$id_mapel] * 100;

                $mapelRankingData = array_values($resultMapelData);
                usort($mapelRankingData, function ($a, $b) {
                    return $b['persentase'] <=> $a['persentase'];
                });

                $resultData[$jenis] = [
                    'kategori' => $kategori,
                    'label' => array_values($labelData),
                    'nilai_tertinggi' => array_values($capaianTertinggiData),
                    'rerata' => array_values($rerataData),
                    'total_soal' => array_values($totalSoalData),
                    'mapels' => $mapelRankingData,
                ];

                $i++;
            }
        }

        // print_r($mapelData);
        // print_r($mapelTotalSoalData);

        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Data hasil nilai capaian tertinggi dan rata-rata per mapel.',
            'data' => $resultData
        ]);
    }

    public function tryout_hasil_nilai_perumpun(Request $request)
    {
        // $validate = Validator::make($request->all(), [
        //     'kategori'      => 'required',
        // ]);

        // if ($validate->fails()) {
        //     return response()->json([
        //         'success' => false,
        //         'messages' => $validate->messages()
        //     ], 422);
        // }

        $kategori = 'UTBK';

        $role_user = auth()->user()->role_user;
        $id_user = auth()->user()->id;
        if ($role_user == 'parent') {
            $id_user = OrangTua::where('id_user', $id_user)->first()->id_orang_tua;
        }

        $tryoutHasil = DB::table('tryout_users')
            ->join('tryout_hasils', 'tryout_hasils.referensi', '=', 'tryout_users.referensi')
            ->join('produks', 'produks.id', '=', 'tryout_users.id_produk')
            ->where('id_user', $id_user)
            ->where('kategori_produk', $kategori)
            ->selectRaw("kategori_produk, DATE(waktu_selesai) as date, riwayat_perhitungan")
            ->orderBy('waktu_selesai', 'ASC')
            ->groupBy('tryout_users.referensi')
            ->get();

        $resultData = [];

        $mapelData = [];
        foreach ($tryoutHasil as $key => $value) {
            if ($value->riwayat_perhitungan) {
                $riwayat = json_decode($value->riwayat_perhitungan);
                foreach ($riwayat->tryout as $key2 => $tryout) {
                    foreach ($tryout->mapel as $key3 => $mapel) {
                        $mapelData[$mapel->id_mapel]['id'] = $mapel->id_mapel;
                        $mapelData[$mapel->id_mapel]['kode'] = $mapel->kode_mapel ?? 'N/A';
                        $mapelData[$mapel->id_mapel]['nama'] = $mapel->nama_mapel ?? 'N/A';
                        $mapelData[$mapel->id_mapel]['ceeb'][] = $mapel->ceeb;
                    }
                }
            }
        }
        // print_r($mapelData);

        $mapelIDs = [];
        foreach ($mapelData as $key => $value) {
            $mapelIDs[] = $value['id'];
        }

        // print_r($mapelIDs);

        // $rumpunKlasterData = DB::table('klaster')->leftJoin('rumpun', 'rumpun.id', '=', 'klaster.id_rumpun')
        $rumpunKlasterData = DB::table('rumpun')
            ->selectRaw('rumpun.id AS id, rumpun.kode as kode, rumpun.nama as nama, id_mapel, nama_mapel, nilai')
            ->join('klaster', 'klaster.id_rumpun', '=', 'rumpun.id')
            ->join('mapels', 'mapels.id', '=', 'klaster.id_mapel')
            // ->whereIn('id_mapel', $mapelIDs)
            ->groupBy('rumpun.id')
            ->groupBy('id_mapel')
            ->orderBy('rumpun.id')
            ->orderBy('id_mapel')
            ->get()->toArray();

        // print_r($rumpunKlasterData);

        $rumpunKlasterGroupData = [];
        foreach ($rumpunKlasterData as $key => $value) {
            $rumpunKlasterGroupData[$key]['id_rumpun'] = $value->id;
            $rumpunKlasterGroupData[$key]['nama_rumpun'] = $value->nama;
            $rumpunKlasterGroupData[$key]['kode_rumpun'] = $value->kode;
            $rumpunKlasterGroupData[$key]['id_mapel'] = $value->id_mapel;
            $rumpunKlasterGroupData[$key]['nama_mapel'] = $value->nama_mapel;
            $rumpunKlasterGroupData[$key]['nilai_klaster'] = $value->nilai;
        }

        $nilaiData = [];
        foreach ($rumpunKlasterGroupData as $key => $value) {
            $filterData = array_filter($mapelData, function ($item) use ($value) {
                return $item['id'] == $value['id_mapel'];
            });

            $nilai_rumpun = 0;
            if (!empty($filterData)) {
                $filterData = array_values($filterData)[0];
                $nilai_klaster = $value['nilai_klaster'];
                $ceeb = $this->getAverage($filterData['ceeb']);

                // Nilai CEEB rer Mapel /100 * nilai Klaster setiap Rumpun Mapel (dengan pembulatan)
                $nilai_rumpun = round(($ceeb / 100) * $nilai_klaster);
            }

            $nilaiData[$value['id_rumpun']]['nama'] = $value['nama_rumpun'];
            $nilaiData[$value['id_rumpun']]['kode'] = $value['kode_rumpun'];
            $nilaiData[$value['id_rumpun']]['nilai'][] = $nilai_rumpun;
        }
        // print_r($nilaiData);

        foreach ($nilaiData as $key => $value) {
            $resultDataTemp[$key]['nama'] = $value['nama'];
            $resultDataTemp[$key]['kode'] = $value['kode'];
            $resultDataTemp[$key]['nilai'] = $this->getAverage($value['nilai']);
        }
        // print_r($resultDataTemp);
        usort($resultDataTemp, function ($a, $b) {
            return $b['nilai'] <=> $a['nilai'];
        });
        // print_r($resultDataTemp);

        foreach ($resultDataTemp as $key => $value) {
            $resultData['grafik']['code'][$key] = $value['kode'];
            $resultData['grafik']['value'][$key] = $value['nilai'];
            $resultData['rank'][$key]['nama'] = $value['nama'];
            $resultData['rank'][$key]['kode'] = $value['kode'];
            $resultData['rank'][$key]['ranking'] = $key + 1;
        }


        return response()->json([
            'code' => 200,
            'success' => true,
            'message' => 'Data hasil nilai perumpun.',
            'data' => $resultData
        ]);
    }

    // fungsi untuk melihat tryout yang di arsipkan
    public function showArchived(Request $request)
    {
        $limit = $request->paginate ?  $request->paginate : 10;
        $q = $request->q;
        $data = TryoutUser::with([
            'produk:id,nama_produk,kategori_produk',
            'user:id,username,email',
            'user.siswa:id_siswa,nama_lengkap',
            'user.guru:id_teacher,nama_lengkap',
        ])->select(['id', 'referensi', 'id_produk', 'id_user', 'waktu_mulai', 'waktu_selesai', 'is_archived']);

        $data->where(function ($query) use ($q) {
            $query->where('referensi', 'like', '%' . $q . '%')
                ->orWhere('waktu_mulai', 'like', '%' . $q . '%')
                ->orWhere('waktu_selesai', 'like', '%' . $q . '%');
            $query->orWhereHas('produk', function ($filter) use ($q) {
                $filter->where('nama_produk', 'like', '%' . $q . '%');
            });
            $query->orWhereHas('user.siswa', function ($filter) use ($q) {
                $filter->where('nama_lengkap', 'like', '%' . $q . '%');
            });
            $query->orWhereHas('user.guru', function ($filter) use ($q) {
                $filter->where('nama_lengkap', 'like', '%' . $q . '%');
            });
        });

        if ($request->kategori_produk) {
            $data->whereHas('produk', function ($query) use ($request) {
                $query->whereIn('kategori_produk', $request->kategori_produk);
            });
        }

        $array = $data->has('produk')->where('is_archived', true)->groupBy('referensi')->paginate($limit);

        return response()->json([
            'success' => true,
            'messages' => 'Berhasil Menampilkan data Try Out',
            'data' => $array,
        ]);
    }

    // fungsi untuk mengarsipkan tryout user
    public function doArchive($id)
    {
        $tryout = TryoutUser::where('id_produk', $id)->where('id_user', auth()->user()->id)->firstOrFail();
        $tryout->is_archived = !$tryout->is_archived;
        $archive = $tryout->save();

        return response()->json([
            'code' => 200,
            'success' => $archive,
            'message' => 'Try Out Berhasil Di Arsipkan.',
            'data' => $tryout
        ], 200);
    }
}
