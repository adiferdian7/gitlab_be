<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $validationMessage = [
        'required' => ':Attribute diperlukan.',
        'unique' => ':Attribute sudah terdaftar.',
        'exists' => ':Attribute tidak terdaftar.',
    ];

    public function user()
    {
        return Auth::user();
    }

    public function userId()
    {
        return Auth::user()->id;
    }

    public function validationMessage()
    {
        return $this->validationMessage;
    }

    public function formatRupiah($num, $currency = false, $dec = 0)
    {
        $num_format = number_format($num, $dec, ',', '.');
        if (!$currency) {
            return $num_format;
        }
        return 'Rp ' . $num_format;
    }

    public function generateCodePad($num, $prefix = '')
    {
        return $prefix . str_pad($num, 5, 0, STR_PAD_LEFT);
    }

    public function getAverage($arr)
    {
        $a = array_filter($arr);
        $average = 0;
        if (count($a)) {
            $average = array_sum($a) / count($a);
        }
        return $average;
    }

    // Function to calculate square of value - mean
    public function stdev_square($x, $mean)
    {
        return pow($x - $mean, 2);
    }

    // Function to calculate standard deviation (uses sd_square)    
    public function stdev($array)
    {
        $countMinOne = count($array) - 1 < 1 ? 1 : count($array) - 1;
        // square root of sum of squares devided by N-1
        return sqrt(array_sum(array_map(function ($x, $mean) {
            return pow($x - $mean, 2);
        }, $array, array_fill(0, count($array), (array_sum($array) / count($array))))) / $countMinOne);
    }
}
