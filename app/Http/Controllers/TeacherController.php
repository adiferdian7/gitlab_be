<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Teacher;
use Illuminate\Support\Str;

use App\Mail\MySendMail;
use App\Mail\TestMail;
use App\Models\Notification;
use App\Models\UserDoc;
use Illuminate\Support\Facades\Mail;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->paginate ?  $request->paginate : 10;
        $q = $request->q ?? "";

        $data = Teacher::with(['user'])->where(
            function ($query) use ($q) {
                // $query->where('username', 'like',  '%' . $q . '%');
                $query->orWhere('nama_lengkap', 'like',  '%' . $q . '%');
                $query->orWhere('email', 'like',  '%' . $q . '%');
                $query->orWhere('nomor_telephone', 'like',  '%' . $q . '%');
            }
        )
            ->paginate($limit);
        return response()->json([
            'success' => true,
            'messages' => 'berhasil mengambil data',
            'data' => $data,
        ]);
    }


    public function cek(Request $request)
    {
        $user = $request->user();
        if ($user->role_user != 'teacher') {
            return response()->json(['success' => false, 'messages' => 'maaf anda bukan role Teacher'], 404);
        }

        $teacher = Teacher::with('user_doc')->where('id_teacher', $user->id)->first();

        if (
            $teacher->tanggal_lahir == null || $teacher->alamat_lengkap == null || $teacher->tempat_lahir == null || $teacher->jenis_kelamin == null ||
            $teacher->id_provinsi == null || $teacher->id_kota == null || $teacher->id_kecamatan == null ||
            count($teacher->user_doc) < 2
        ) {

            return response()->json(['success' => false, 'messages' => 'Profil belum dilengkapi']);
        }

        if ($teacher->verifikasi == 2) {
            return response()->json(['success' => true, 'messages' => 'Profil sudah dilengkapi dan diverifikasi']);
        } else {
            return response()->json(['success' => true, 'messages' => 'Profil sudah dilengkapi tapi belum diverifikasi']);
        }
    }

    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'username'              => 'required|unique:users',
            'nama_lengkap'          => 'required',
            'email'                 => 'required|unique:teachers|unique:users',
            'nomor_telephone'       => 'required',
            'password'              => 'required',
            'info'                  => 'required',

        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }


        DB::beginTransaction();
        $random = Str::random(40);

        try {

            $dataUser = [
                'username'   => $request->username,
                'password'   => Hash::make($request->password),
                'role_user'  => 'teacher',
                'token'      => $random,
                'email'      => $request->email,
                'verifikasi' => 1,
            ];

            $insertUser = User::create($dataUser);

            $insertTeacher = Teacher::create([
                'id_teacher'          => $insertUser->id,
                'nama_lengkap'        => $request->nama_lengkap,
                'email'               => $request->email,
                'nomor_telephone'     => $request->nomor_telephone,
                'info'                => $request->info,
                'verifikasi'          => 1,
                'id_level' => 1, // BASE
                'nama_level' => 'BASE'
            ]);

            // DB::commit();

            // $mailData = [
            //     'token' => $insertUser->token,
            // ];

            // Mail::to($request->email)->send(new MySendMail($mailData));

            $notificationModel = new Notification();
            $notificationModel->insertData('insertBatchData', [
                [
                    'recipientId' => '',
                    'recipientType' => 4, // superadmin + admin
                    'type' => 0, // registration
                    'title' => 'Ada Pendaftar Baru!',
                    'body' => 'Ada Tentor baru telah mendaftar. Yuk cek detailnya!',
                    'data' => [
                        'user' => $insertUser,
                        'detail' => $insertTeacher,
                    ],
                ],
                [
                    'recipientId' => $insertUser->id,
                    'recipientType' => 2, // teacher
                    'type' => 0, // registration
                    'title' => 'Selamat datang!',
                    'body' => "Terima kasih telah mendaftar di UjiAja.com. Yuk lengkapi profile kamu sekarang!",
                    'data' => [
                        'user' => $insertUser,
                        'detail' => $insertTeacher,
                    ],
                ]
            ]);

            $testMail = new TestMail([
                'subject' => 'Pendaftaran akun Tentor berhasil!',
                'to' => $insertUser->email,
                'toName' => $insertTeacher->nama_lengkap,
                'content' => '',
                'token' => $insertUser->token,
                'view' => 'email.content.customer__register-success'
            ]);

            Mail::send($testMail);

            DB::commit();

            return response()->json(['success' => true, 'data' => $dataUser], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 500);
        }
    }


    public function verifikasi(Request $request, $id)
    {

        DB::beginTransaction();

        try {

            $user = User::find($id);
            $user->verifikasi = 2;
            $user->save();

            $patner = Teacher::find($id);
            $patner->verifikasi = 2;
            $patner->save();

            DB::commit();
            return response()->json(['success' => true, 'message' => 'verifikasi berhasil']);
        } catch (\Exception $e) {

            DB::rollback();
            return response()->json(['success' => false, 'messages' => $e->getMessage()]);
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Teacher::with('user', 'user_doc')->findOrFail($id);

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $teacher = Teacher::findOrFail($id);
        $user  = User::findOrFail($teacher->id_teacher);

        $validate = Validator::make($request->all(), [
            'username'              => 'required|unique:users,username,' . $id,
            'nama_lengkap'          => 'required',
            'email'                 => 'required|unique:users,email,' . $id,
            'nomor_telephone'           => 'required',
            'info'                      => 'required',
            'tempat_lahir'              => 'required',
            'tgl_lahir'                 => 'required',
            'jenis_kelamin'             => 'required',
            'alamat'                    => 'required',
            'id_provinsi'               => 'required',
            'id_kota'                   => 'required',
            'id_kecamatan'              => 'required',
            'pendidikan_terakhir'       => 'required',

        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        // if($request->foto){
        //     $validator = Validator::make($request->all(), [
        //         'foto' => 'required|image:jpeg,png,jpg,gif,svg|max:2048'
        //     ]);

        //     if ($validator->fails()) {
        //         return response()->json([
        //             'success' => false, 
        //             'messages' => $validate->messages()
        //         ], 422);
        //     }
        // }

        DB::beginTransaction();

        try {

            $date = date('Y-m-d', strtotime($request->tgl_lahir));

            if (!empty($request->foto)) {

                // $uploadFolder = 'users';
                // $image = $request->file('foto');
                // $image_uploaded_path = $image->store($uploadFolder, 'public');
                // $urlUpload = Storage::disk('public')->url($image_uploaded_path);

                $teacher->foto = $request->foto;
            }

            $teacher->pendidikan_terakhir  = $request->pendidikan_terakhir;
            // $teacher->agama                = $request->agama;
            $teacher->tanggal_lahir        = $date;
            $teacher->tempat_lahir         = $request->tempat_lahir;
            $teacher->alamat_lengkap       = $request->alamat;
            $teacher->nomor_telephone      = $request->nomor_telephone;
            $teacher->email                = $request->email;
            $teacher->nama_lengkap         = $request->nama_lengkap;
            $teacher->jenis_kelamin        = $request->jenis_kelamin;
            $teacher->info                 = $request->info;
            $teacher->id_provinsi          = $request->id_provinsi;
            $teacher->id_kota              = $request->id_kota;
            $teacher->id_kecamatan         = $request->id_kecamatan;

            $teacher->save();

            if ($request->verifikasi && !empty($request->verifikasi)) {
                $user->verifikasi       = $request->verifikasi;
            }

            if (!empty($request->password)) {
                $user->password = Hash::make($request->password);
            }

            $user->username    = $request->username;
            $user->email       = $request->email;
            $user->save();

            if (!empty($request->user_docs)) {
                foreach ($request->user_docs as $key => $doc) {

                    $check = UserDoc::where('user_id', $user->id)->where('doc_type', $doc['doc_type']);

                    if ($check->count() > 0) {
                        if (!empty($doc['doc_file'])) {
                            $check->update([
                                'doc_file' => $doc['doc_file'],
                            ]);
                        }
                    } else {
                        UserDoc::create([
                            'user_id' => $user->id,
                            'doc_label' => $doc['doc_label'],
                            'doc_type' => $doc['doc_type'],
                            'doc_file' => $doc['doc_file'],
                            'doc_status' => 'Pending',
                        ]);
                    }
                }
            }

            DB::commit();

            return response()->json([
                'success' => true,
                'messages' => 'update teacher success',
                'data' => $teacher,
            ], 200);
        } catch (\Exception $e) {

            DB::rollback();

            return response()->json([
                'success' => false,
                'messages' => $e->getMessage()
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $teacher = Teacher::findOrFail($id);
        $delete = Teacher::destroy($id);
        $user   = User::destroy($teacher->id_teacher);
        return response()->json([
            'success' => true,
            'messages' => 'data berhasil di hapus',
            'data' => $id,
        ]);
    }

    public function undo(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }


        DB::beginTransaction();

        try {
            $teacher =  Teacher::withTrashed()->findOrFail($request->id);
            Teacher::withTrashed()->find($request->id)->restore();
            User::withTrashed()->find($teacher->id_teacher)->restore();

            DB::commit();

            return response()->json([
                'success' => true,
                'messages' => 'data berhasil di pulihkan',
            ]);
        } catch (\Exception $e) {

            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }
}
