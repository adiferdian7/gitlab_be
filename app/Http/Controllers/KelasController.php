<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\Kelas;
use Illuminate\Validation\Rule;

class KelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->paginate ?  $request->paginate : 10;
        $q = $request->q;
        $data = Kelas::with(['jenjang:id,nama_jenjang'])
            ->select('id', 'id_jenjang', 'nama_kelas')
            ->where('nama_kelas', 'like', '%' . $q . '%')
            ->orWhereHas('jenjang', function ($query) use ($q) {
                $query->where('nama_jenjang', 'like', '%' . $q . '%');
            })
            ->paginate($limit);
        return response()->json(['success' => true, 'data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id_jenjang = $request->id_jenjang;
        $nama_kelas = $request->nama_kelas;
        $validate = Validator::make($request->all(), [
            // 'nama_kelas'   => 'required|unique:kelas,nama_kelas,NULL,id,deleted_at,NULL',
            'nama_kelas'   => [
                Rule::unique('kelas')->where(function ($query) use ($id_jenjang, $nama_kelas) {
                    return $query->where('nama_kelas', $nama_kelas)
                        ->where('id_jenjang', $id_jenjang)
                        ->whereNull('deleted_at');
                })
            ],
            'id_jenjang'   => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $kelas = Kelas::create([
                'nama_kelas' => $request->nama_kelas,
                'id_jenjang' => $request->id_jenjang
            ]);

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di inputkan', 'data' => $kelas], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Kelas::findOrFail($id);

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kelas = Kelas::findOrFail($id);

        $id_jenjang = $request->id_jenjang;
        $nama_kelas = $request->nama_kelas;
        $validate = Validator::make($request->all(), [
            // 'nama_kelas'   => 'required|unique:kelas,nama_kelas,' . $id . ',id,deleted_at,NULL',
            'nama_kelas' => Rule::unique('kelas')->where(function ($query) use ($id_jenjang, $nama_kelas, $id) {
                return $query->where('nama_kelas', $nama_kelas)
                    ->whereNotIn('id', $id)
                    ->where('id_jenjang', $id_jenjang)
                    ->whereNull('deleted_at');
            }),
            'id_jenjang'   => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $kelas->nama_kelas = $request->nama_kelas;
            $kelas->id_jenjang = $request->id_jenjang;
            $kelas->save();

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $kelas], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {

        $delete  = Kelas::destroy($id);
        return response()->json([
            'success' => true,
            'messages' => 'data berhasil di hapus',
            'data' => $id,
        ]);
    }
}
