<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\SuperAdmin;
use Illuminate\Support\Str;

use App\Mail\MySendMail;

use Illuminate\Support\Facades\Mail;

class SuperAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = SuperAdmin::all();
         return response()->json([
            'success' => true,
            'messages' => 'berhasil mengambil data',
            'data' => $data,
        ]);
    }


    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'username'          => 'required|unique:users',
            'nama_lengkap'      => 'required',
            'email'             => 'required|unique:super_admins',
            'nomor_telephone'   => 'required',
            'password'          => 'required', 

        ]);

        if($validate->fails()) {
            return response()->json([
                'success' => false, 
                'messages' => $validate->messages()
            ], 422);
        }


        DB::beginTransaction();

        $random = Str::random(40);

        try {

            $dataUser = [
                'username'   => $request->username,
                'password'   => Hash::make($request->password),
                'role_user'  => 'superAdmin',
                'token'      => $random,
                'email'      => $request->email,
                'verifikasi' => 2,
            ];

            $insertUser = User::create($dataUser);

            SuperAdmin::create([
                'id_super_admin'    => $insertUser->id,
                'username'          => $request->username,
                'nama_lengkap'      => $request->nama_lengkap,
                'email'             => $request->email,
                'nomor_telephone'   => $request->nomor_telephone,
            ]);

            DB::commit();

            $mailData = [
                'token' => $insertUser->token,
            ];

            Mail::to($request->email)->send(new MySendMail($mailData));
            
            return response()->json(['success' => true,'message' => 'success create super Admin', 'data' => $insertUser]);

        } catch (\Exception $e) {
             DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $data = SuperAdmin::findOrFail($id);

        return response()->json([
            'success' => true, 
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $admin = SuperAdmin::findOrFail($id);
        $user  = User::findOrFail($admin->id_super_admin);

        $validate = Validator::make($request->all(), [
            'username'              => 'required|unique:users,username,'.$id,
            'nama_lengkap'          => 'required',
            'email'                 => 'required|unique:users,email,'.$id,
            'nomor_telephone'       => 'required',
        ]);

        if($validate->fails()) {
            return response()->json([
                'success' => false, 
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try{

            $admin->nomor_telephone      = $request->nomor_telephone;
            $admin->email                = $request->email;
            $admin->nama_lengkap         = $request->nama_lengkap;

            $admin->save();


            $user->username    = $request->username;
            $user->email       = $request->email;
            $user->save();
            
            DB::commit();

            return response()->json([
                'success' => true, 
                'messages' => 'update super admin success',
                'data' => $admin,
            ], 200);

        } catch (\Exception $e) {

            DB::rollback();

            return response()->json([
                'success' => false, 
                'messages' => $e->getMessage()
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin = SuperAdmin::findOrFail($id);
        $delete = SuperAdmin::destroy($id);
        $user   = User::destroy($admin->id_super_admin);
        return response()->json([
            'success' => true,
            'messages' => 'data berhasil di hapus',
            'data' => $id,
        ]);
    }

    public function undo(Request $request) {
        $validate = Validator::make($request->all(), [
            'id' => 'required', 
        ]);

        if($validate->fails()) {
            return response()->json([
                'success' => false, 
                'messages' => $validate->messages()
            ], 422);
        }


        DB::beginTransaction();

        try {
            $admin =  SuperAdmin::withTrashed()->findOrFail($request->id);
            SuperAdmin::withTrashed()->find($request->id)->restore();
            User::withTrashed()->find($admin->id_super_admin)->restore();

            DB::commit();

            return response()->json([
                'success' => true,
                'messages' => 'data berhasil di pulihkan',
            ]);

        } catch (\Exception $e) {

            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }
}
