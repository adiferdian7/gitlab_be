<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Validator;
use App\Models\ProgramStudiPerguruanTinggi as ModelData;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ProgramStudiPerguruanTinggiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->paginate ?  $request->paginate : 10;
        $q = $request->q;
        $data = ModelData::with([
            'program_studi:id,nama_studi,deskripsi,id_penjurusan,id_mapel',
            'program_studi.mapel:id,nama_mapel',
            'program_studi.penjurusan:id,id_kelas,nama_penjurusan',
            'program_studi.penjurusan.kelas:id,nama_kelas',
            'perguruan:id,nama_perguruan,akreditasi'
        ])
            ->select('id', 'id_program_studi', 'id_perguruan_tinggi', 'akreditasi_program_studi', 'passing_grade_prodi');

        if (!empty($request->id_program_studi)) {
            $data = $data->where(function ($query) use ($request) {
                return $query->where('id_program_studi', $request->id_program_studi);
            });
        }

        if (!empty($request->id_prodi_bind_perguruan)) {
            if (!empty($request->id_program_studi)) {
                $data = $data->where(function ($query) use ($request) {
                    return $query->where('id_program_studi', $request->id_program_studi);
                });
            } else {
                $data = $data->where(function ($query) use ($request) {
                    return $query->where('id', $request->id_prodi_bind_perguruan);
                });
            }
        }

        $data->where(function ($query) use ($q) {
            $query = $query->WhereHas('program_studi', function ($query) use ($q) {
                $query->orWhere('nama_studi', 'like', '%' . $q . '%');
                $query->orWhere('deskripsi', 'like', '%' . $q . '%');
            })
                ->orWhereHas('program_studi.mapel', function ($query) use ($q) {
                    $query->where('nama_mapel', 'like', '%' . $q . '%');
                })
                ->orWhereHas('program_studi.penjurusan', function ($query) use ($q) {
                    $query->where('nama_penjurusan', 'like', '%' . $q . '%');
                })
                ->orWhereHas('program_studi.penjurusan.kelas', function ($query) use ($q) {
                    $query->where('nama_kelas', 'like', '%' . $q . '%');
                })
                ->orWhereHas('perguruan', function ($query) use ($q) {
                    $query->orWhere('nama_perguruan', 'like', '%' . $q . '%');
                    $query->orWhere('akreditasi', 'like', '%' . $q . '%');
                });
        });

        $data = $data->has('perguruan')->paginate($limit);

        return response()->json(['success' => true, 'data' => $data]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'id_program_studi'   => 'required',
            'id_perguruan_tinggi' => 'required',
            'akreditasi_program_studi' => 'required',
            'passing_grade_prodi' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $insert = ModelData::create([
                'id_program_studi'   => $request->id_program_studi,
                'id_perguruan_tinggi' => $request->id_perguruan_tinggi,
                'akreditasi_program_studi' => $request->akreditasi_program_studi,
                'passing_grade_prodi' => $request->passing_grade_prodi,
            ]);

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di inputkan', 'data' => $insert], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ModelData::with([
            'program_studi:id,nama_studi,akreditasi,deskripsi,id_penjurusan,id_mapel',
            'program_studi.mapel:id,nama_mapel',
            'program_studi.penjurusan:id,id_kelas,nama_penjurusan',
            'program_studi.penjurusan.kelas:id,nama_kelas',
            'perguruan:id,nama_perguruan,akreditasi'
        ])->findOrFail($id);

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = ModelData::findOrFail($id);

        $validate = Validator::make($request->all(), [
            'id_program_studi'   => 'required',
            'id_perguruan_tinggi' => 'required',
            'akreditasi_program_studi' => 'required',
            'passing_grade_prodi' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $update->id_program_studi    = $request->id_program_studi;
            $update->id_perguruan_tinggi = $request->id_perguruan_tinggi;
            $update->akreditasi_program_studi = $request->akreditasi_program_studi;
            $update->passing_grade_prodi = $request->passing_grade_prodi;
            $update->save();

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $update], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete  = ModelData::destroy($id);
        return response()->json([
            'success' => true,
            'messages' => 'data berhasil di hapus',
            'data' => $id,
        ]);
    }


    // Import

    public function import(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'file' => 'required|file|mimes:xls,xlsx'
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        $file = $request->file('file');

        DB::beginTransaction();

        try {
            $spreadsheet = IOFactory::load($file->getRealPath());
            $sheet        = $spreadsheet->getActiveSheet();
            $row_limit    = $sheet->getHighestDataRow();
            $column_limit = $sheet->getHighestDataColumn();
            $row_range    = range(5, $row_limit);
            $column_range = range('A', $column_limit);

            $penjurusan = $sheet->getCell('B1')->getValue();
            $cek_penjurusan = DB::table('penjurusans')->where('nama_penjurusan', $penjurusan)->first();
            $id_pj = NULL;
            if ($cek_penjurusan) {
                $id_pj = $cek_penjurusan->id;
            }

            $total_count = 2;
            // $error_count = 0;
            $success_count = 0;
            $dataTransTemp = array();
            $dataTrans = array();
            $dataPerguruan = array();
            $dataProdi = array();
            foreach ($row_range as $row) {

                $id_perguruan = $sheet->getCell('A' . $row)->getValue();
                // $nama_perguruan = ucwords(strtolower($sheet->getCell('D' . $row)->getValue()));
                $nama_perguruan = $sheet->getCell('D' . $row)->getValue();
                // cek perguruan
                $count_perguruan = DB::table('perguruan_tinggi')->where('nama_perguruan', $nama_perguruan)->count();
                if ($count_perguruan < 1) {
                    $filterDataPerguruan = array_filter($dataPerguruan, function ($item) use ($id_perguruan) {
                        return $item['id'] == $id_perguruan;
                    });
                    if (empty($filterDataPerguruan)) {
                        $dataPerguruan[] = [
                            'id' => $id_perguruan,
                            'nama_perguruan' => $nama_perguruan,
                            'akreditasi' => '-',
                            'created_at' => date('Y-m-d H:i:s')
                        ];
                    }
                }
                $id_prodi = NULL;
                $id_plh = $sheet->getCell('B' . $row)->getValue();
                // $id_plh_explode = explode($id_perguruan, $id_plh);
                // if (isset($id_plh_explode[1])) {
                //     $id_prodi = $id_plh_explode[1];
                // }
                // cek prodi
                // $nama_studi = ucwords(strtolower($sheet->getCell('C' . $row)->getValue()));
                $nama_studi = $sheet->getCell('C' . $row)->getValue();
                $count_prodi = DB::table('program_studi')->where('nama_studi', $nama_studi)->count();
                if ($count_prodi < 1) {
                    $filterDataProdi = array_filter($dataProdi, function ($item) use ($nama_studi) {
                        return $item['nama_studi'] == $nama_studi;
                    });
                    if (empty($filterDataProdi)) {
                        $nama_rumpun = $sheet->getCell('F' . $row)->getValue();
                        $detailRumpun = DB::table('rumpun')->where('nama', $nama_rumpun)->first();
                        $id_rumpun = null;
                        $rumpun = null;
                        if ($detailRumpun) {
                            $id_rumpun = $detailRumpun->id;
                            $rumpun = $detailRumpun->nama;
                        }
                        $dataProdi[] = [
                            // 'id' => $id_prodi,
                            'nama_studi' => $nama_studi,
                            'deskripsi' => '-',
                            'alasan' => '-',
                            'prospek' => '-',
                            'id_penjurusan' => $id_pj,
                            'kelompok' => $penjurusan == 'IPA' ? 'SAINTEK' : 'SOSHUM',
                            'slug' => Str::slug($nama_studi, '-'),
                            'id_rumpun' => $id_rumpun,
                            'rumpun' => $rumpun,
                            'created_at' => date('Y-m-d H:i:s')
                        ];
                    }
                }

                // data trans
                $pg = $sheet->getCell('E' . $row)->getValue();
                $pg_convert = str_replace(',', '.', $pg);
                $dataTransTemp[] = [
                    'id' => $id_plh,
                    'id_perguruan_tinggi' => $id_perguruan,
                    'id_program_studi' => $id_prodi, //NULL
                    'temp_nama_prodi' => $nama_studi,
                    'akreditasi_program_studi' => '-',
                    'passing_grade_prodi' => $pg_convert,
                ];
                $total_count++;
            }    
            
            if (!empty($dataPerguruan)) {
                DB::table('perguruan_tinggi')->insert($dataPerguruan);
            }
            if (!empty($dataProdi)) {
                DB::table('program_studi')->insert($dataProdi);
            }

            foreach ($dataTransTemp as $key => $dataTransRow) {

                $temp_nama_prodi = $dataTransRow['temp_nama_prodi'];

                $prodi = DB::table('program_studi')->where('nama_studi', $temp_nama_prodi);

                if ($prodi->count() > 0) {
                    $prodiData =  $prodi->first();
                    $id_prodi = $prodiData->id;
                    $id_perguruan = $dataTransRow['id_perguruan_tinggi'];
                    $id_plh = $dataTransRow['id'];
                    // cek prodiTrans
                    $count_prodiTrans = DB::table('program_studi_and_perguruan_tinggi')
                        ->where('id', $id_plh)
                        ->where('id_perguruan_tinggi', $id_perguruan)
                        ->where('id_program_studi', $id_prodi)
                        ->count();

                    if ($count_prodiTrans < 1) {
                        unset($dataTransRow['temp_nama_prodi']);
                        $dataTransRow['id_program_studi'] = $id_prodi;
                        $dataTrans[] = $dataTransRow;
                        $success_count++;
                    }
                }
            }

            // print_r($dataTrans); die;

            if (!empty($dataTrans)) {
                DB::table('program_studi_and_perguruan_tinggi')->insert($dataTrans);
            }

            DB::commit();
            
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'messages' => $e->getMessage()], 500);
        }
        return response()->json(
            [
                'success' => true,
                'message' => "Data berhasil diimport!",
                'data' => [
                    'total_count' => $total_count,
                    'success_count' => $success_count,
                ]
            ],
            200
        );
    }
}
