<?php

namespace App\Http\Controllers;

use App\Models\Material;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Validator;

class MaterialController extends Controller
{
    function __construct()
    {
        $this->materialModel = new Material();
    }

    public function index(Request $request)
    {
        $data = $this->materialModel->getData('paginationData', $request->all(), '');
        return responseData($data);
    }

    public function detail($id = '')
    {
        $data = $this->materialModel->getData('detailData', ['id' => $id], '');
        if (!empty($data)) {
            return responseData($data);
        } else {
            return responseFail('Data tidak ditemukan!', 404);
        }
    }

    public function detailWithTrans($id = '')
    {
        $data = $this->materialModel->getData('detailWithTrans', ['id' => $id], '');
        if (!empty($data)) {
            return responseData($data);
        } else {
            return responseFail('Data tidak ditemukan!', 404);
        }
    }

    public function insert(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'title'   => 'required|unique:materials,title,NULL,id,deleted_at,NULL',
            'price'   => 'required',
            'desc'   => 'required',
            'published'   => ['required', Rule::in([1, 0])],
            'cover_image' => 'nullable|image|file|max:1024|mimes:jpg,jpeg,gif,png',
            'file' => 'required|file|max:10240|mimes:ppt,doc,docx,xls,xlsx,pdf,zip',
        ], $this->materialModel->getValidationMessage());

        if ($validate->fails()) {
            return responseFail($validate->messages(), 422);
        }

        $coverImageLink = '';
        $fileLink = '';

        // file upload
        $uploadFolder = 'materials';
        $file = $request->file('file');
        $fileLink = $file->store($uploadFolder, 'public');

        // cover image upload
        if ($request->hasFile('cover_image')) {
            $image = $request->file('cover_image');
            $coverImageLink = $image->store($uploadFolder, 'public');
        }

        $insertData = [
            'title' => $request->input('title'),
            'price' => $request->input('price'),
            'desc' => $request->input('desc'),
            'cover_image_link' => $coverImageLink,
            'file_link' => $fileLink,
        ];

        $result = $this->materialModel->insertData('insertData', $insertData);

        if (isset($result['error'])) {
            return responseFail($result, 500);
        } else {
            return responseInsert($result);
        }
    }

    public function update(Request $request, $id)
    {
        $material = Material::find($id);

        if (!$material) {
            return responseNotFound();
        }

        $validate = Validator::make($request->all(), [
            'title'   => 'required|unique:materials,title,' . $id . ',id,deleted_at,NULL',
            'price'   => 'required',
            'desc'   => 'required',
            'published'   => ['required', Rule::in([1, 0])],
            'cover_image' => 'nullable|image|file|max:1024|mimes:jpg,jpeg,gif,png',
            'file' => 'nullable|file|max:10240|mimes:ppt,doc,docx,xls,xlsx,pdf,zip',
        ], $this->materialModel->getValidationMessage());

        if ($validate->fails()) {
            return responseFail($validate->messages(), 422);
        }

        $updateData = $request->all();
        $updateData['id'] = $id;

        $deleteFolder = storage_path('app/public/');
        $uploadFolder = 'materials';
        if ($request->hasFile('file')) {
            // delete old file
            if (file_exists($deleteFolder . $material->file_link) && !empty($material->file_link)) {
                unlink($deleteFolder . $material->file_link);
            }

            // file upload
            $file = $request->file('file');
            $updateData['file_link'] = $file->store($uploadFolder, 'public');
        }

        if ($request->hasFile('cover_image')) {
            // delete old cover image
            if (file_exists($deleteFolder . $material->cover_image_link)  && !empty($material->cover_image_link)) {
                unlink($deleteFolder . $material->cover_image_link);
            }

            // cover image upload
            $image = $request->file('cover_image');
            $updateData['cover_image_link'] = $image->store($uploadFolder, 'public');
        }

        $result = $this->materialModel->updateData('updateData', $updateData);

        if (isset($result['error'])) {
            return responseFail($result);
        } else {
            return responseUpdate($result);
        }
    }

    public function delete(Request $request, $id)
    {
        $material = Material::find($id);
        if (!$material) {
            return responseNotFound();
        }
        $result = $this->materialModel->deleteData('deleteData', ['id' => $id]);

        if (isset($result['error'])) {
            // delete related file
            $deleteFolder = storage_path('app/public/');
            if (file_exists($deleteFolder . $material->file_link) && !empty($material->file_link)) {
                unlink($deleteFolder . $material->file_link);
            }
            if (file_exists($deleteFolder . $material->file_link) && !empty($material->file_link)) {
                unlink($deleteFolder . $material->file_link);
            }
            return responseFail($result);
        }
        return responseDeleted($result);
    }
}
