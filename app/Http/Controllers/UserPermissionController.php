<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class UserPermissionController extends Controller
{

    protected $permissionList = [
        // Dashboard
        // [
        //     'label' => 'Dashboard',
        //     'actions' => [
        //         [
        //             'label' => 'View',
        //             'r_method' => 'GET',
        //             'c_name' => 'AdminController',
        //             'c_method' => 'dashboard',
        //             'allow' => true,
        //         ]
        //     ]
        // ],
        // Jenjang
        [
            'label' => 'Jenjang',
            'actions' => [
                [
                    'label' => 'List',
                    'r_method' => 'GET',
                    'c_name' => 'JenjangController',
                    'c_method' => 'index',
                    'allow' => true,
                ],
                [
                    'label' => 'Detail',
                    'r_method' => 'GET',
                    'c_name' => 'JenjangController',
                    'c_method' => 'show',
                    'allow' => true,
                ],
                [
                    'label' => 'Create',
                    'r_method' => 'POST',
                    'c_name' => 'JenjangController',
                    'c_method' => 'store',
                    'allow' => true,
                ],
                [
                    'label' => 'Edit',
                    'r_method' => 'PUT',
                    'c_name' => 'JenjangController',
                    'c_method' => 'update',
                    'allow' => true,
                ],
                [
                    'label' => 'Delete',
                    'r_method' => 'DELETE',
                    'c_name' => 'JenjangController',
                    'c_method' => 'destroy',
                    'allow' => true,
                ]
            ]
        ],
        // Kelas
        [
            'label' => 'Kelas',
            'actions' => [
                [
                    'label' => 'List',
                    'r_method' => 'GET',
                    'c_name' => 'KelasController',
                    'c_method' => 'index',
                    'allow' => true,
                ],
                [
                    'label' => 'Detail',
                    'r_method' => 'GET',
                    'c_name' => 'KelasController',
                    'c_method' => 'show',
                    'allow' => true,
                ],
                [
                    'label' => 'Create',
                    'r_method' => 'POST',
                    'c_name' => 'KelasController',
                    'c_method' => 'store',
                    'allow' => true,
                ],
                [
                    'label' => 'Edit',
                    'r_method' => 'PUT',
                    'c_name' => 'KelasController',
                    'c_method' => 'update',
                    'allow' => true,
                ],
                [
                    'label' => 'Delete',
                    'r_method' => 'DELETE',
                    'c_name' => 'KelasController',
                    'c_method' => 'destroy',
                    'allow' => true,
                ]
            ]
        ],
        // Penjurusan
        [
            'label' => 'Penjurusan',
            'actions' => [
                [
                    'label' => 'List',
                    'r_method' => 'GET',
                    'c_name' => 'PenjurusanController',
                    'c_method' => 'index',
                    'allow' => true,
                ],
                [
                    'label' => 'Detail',
                    'r_method' => 'GET',
                    'c_name' => 'PenjurusanController',
                    'c_method' => 'show',
                    'allow' => true,
                ],
                [
                    'label' => 'Create',
                    'r_method' => 'POST',
                    'c_name' => 'PenjurusanController',
                    'c_method' => 'store',
                    'allow' => true,
                ],
                [
                    'label' => 'Edit',
                    'r_method' => 'PUT',
                    'c_name' => 'PenjurusanController',
                    'c_method' => 'update',
                    'allow' => true,
                ],
                [
                    'label' => 'Delete',
                    'r_method' => 'DELETE',
                    'c_name' => 'PenjurusanController',
                    'c_method' => 'destroy',
                    'allow' => true,
                ]
            ]
        ],
        // Mapel
        [
            'label' => 'Mapel',
            'actions' => [
                [
                    'label' => 'List',
                    'r_method' => 'GET',
                    'c_name' => 'MapelController',
                    'c_method' => 'index',
                    'allow' => true,
                ],
                [
                    'label' => 'Detail',
                    'r_method' => 'GET',
                    'c_name' => 'MapelController',
                    'c_method' => 'show',
                    'allow' => true,
                ],
                [
                    'label' => 'Create',
                    'r_method' => 'POST',
                    'c_name' => 'MapelController',
                    'c_method' => 'store',
                    'allow' => true,
                ],
                [
                    'label' => 'Edit',
                    'r_method' => 'PUT',
                    'c_name' => 'MapelController',
                    'c_method' => 'update',
                    'allow' => true,
                ],
                [
                    'label' => 'Delete',
                    'r_method' => 'DELETE',
                    'c_name' => 'MapelController',
                    'c_method' => 'destroy',
                    'allow' => true,
                ]
            ]
        ],
        // Perguruan Tinggi
        [
            'label' => 'Perguruan Tinggi',
            'actions' => [
                [
                    'label' => 'List',
                    'r_method' => 'GET',
                    'c_name' => 'PerguruanTinggiController',
                    'c_method' => 'index',
                    'allow' => true,
                ],
                [
                    'label' => 'Detail',
                    'r_method' => 'GET',
                    'c_name' => 'PerguruanTinggiController',
                    'c_method' => 'show',
                    'allow' => true,
                ],
                [
                    'label' => 'Create',
                    'r_method' => 'POST',
                    'c_name' => 'PerguruanTinggiController',
                    'c_method' => 'store',
                    'allow' => true,
                ],
                [
                    'label' => 'Edit',
                    'r_method' => 'PUT',
                    'c_name' => 'PerguruanTinggiController',
                    'c_method' => 'update',
                    'allow' => true,
                ],
                [
                    'label' => 'Delete',
                    'r_method' => 'DELETE',
                    'c_name' => 'PerguruanTinggiController',
                    'c_method' => 'destroy',
                    'allow' => true,
                ]
            ]
        ],
        // Program Studi
        [
            'label' => 'Program Studi',
            'actions' => [
                [
                    'label' => 'List',
                    'r_method' => 'GET',
                    'c_name' => 'ProgramStudiController',
                    'c_method' => 'index',
                    'allow' => true,
                ],
                [
                    'label' => 'Detail',
                    'r_method' => 'GET',
                    'c_name' => 'ProgramStudiController',
                    'c_method' => 'show',
                    'allow' => true,
                ],
                [
                    'label' => 'Create',
                    'r_method' => 'POST',
                    'c_name' => 'ProgramStudiController',
                    'c_method' => 'store',
                    'allow' => true,
                ],
                [
                    'label' => 'Edit',
                    'r_method' => 'PUT',
                    'c_name' => 'ProgramStudiController',
                    'c_method' => 'update',
                    'allow' => true,
                ],
                [
                    'label' => 'Delete',
                    'r_method' => 'DELETE',
                    'c_name' => 'ProgramStudiController',
                    'c_method' => 'destroy',
                    'allow' => true,
                ]
            ]
        ],
        // Tryout Master
        [
            'label' => 'Tryout & UKTT - Data Master',
            'actions' => [
                [
                    'label' => 'List',
                    'r_method' => 'GET',
                    'c_name' => 'TryoutController',
                    'c_method' => 'index',
                    'allow' => true,
                ],
                [
                    'label' => 'Detail',
                    'r_method' => 'GET',
                    'c_name' => 'TryoutController',
                    'c_method' => ['show', 'show_detail'],
                    'allow' => true,
                ],
                [
                    'label' => 'Create',
                    'r_method' => 'POST',
                    'c_name' => 'TryoutController',
                    'c_method' => 'store',
                    'allow' => false,
                ],
                [
                    'label' => 'Edit',
                    'r_method' => 'PUT',
                    'c_name' => 'TryoutController',
                    'c_method' => 'update',
                    'allow' => false,
                ],
                [
                    'label' => 'Delete',
                    'r_method' => 'DELETE',
                    'c_name' => 'TryoutController',
                    'c_method' => 'destroy',
                    'allow' => false,
                ]
            ]
        ],
        // Tryout Soal
        [
            'label' => 'Tryout & UKTT - Data Soal',
            'actions' => [
                [
                    'label' => 'List',
                    'r_method' => 'GET',
                    'c_name' => 'SoalTryoutController',
                    'c_method' => 'index',
                    'allow' => true,
                ],
                [
                    'label' => 'Detail',
                    'r_method' => 'GET',
                    'c_name' => 'SoalTryoutController',
                    'c_method' => 'show',
                    'allow' => true,
                ],
                [
                    'label' => 'Create',
                    'r_method' => 'POST',
                    'c_name' => 'SoalTryoutController',
                    'c_method' => ['store', 'multiInsert'],
                    'allow' => false,
                ],
                [
                    'label' => 'Edit',
                    'r_method' => 'PUT',
                    'c_name' => 'SoalTryoutController',
                    'c_method' => 'update',
                    'allow' => false,
                ],
                [
                    'label' => 'Delete',
                    'r_method' => 'DELETE',
                    'c_name' => 'SoalTryoutController',
                    'c_method' => 'destroy',
                    'allow' => false,
                ]
            ]
        ],
        // Tryout Pertanyaan Soal
        [
            'label' => 'Tryout & UKTT - Pertanyaan Soal',
            'actions' => [
                [
                    'label' => 'List',
                    'r_method' => 'GET',
                    'c_name' => 'SoalPertanyaanController',
                    'c_method' => 'index',
                    'allow' => true,
                ],
                [
                    'label' => 'Detail',
                    'r_method' => 'GET',
                    'c_name' => 'SoalPertanyaanController',
                    'c_method' => 'show',
                    'allow' => true,
                ],
                [
                    'label' => 'Create',
                    'r_method' => 'POST',
                    'c_name' => 'SoalPertanyaanController',
                    'c_method' => 'store',
                    'allow' => false,
                ],
                [
                    'label' => 'Edit',
                    'r_method' => 'PUT',
                    'c_name' => 'SoalPertanyaanController',
                    'c_method' => 'update',
                    'allow' => false,
                ],
                [
                    'label' => 'Delete',
                    'r_method' => 'DELETE',
                    'c_name' => 'SoalPertanyaanController',
                    'c_method' => 'destroy',
                    'allow' => false,
                ]
            ]
        ],
        // Produk/Event (Tryout & UKTT)
        [
            'label' => 'Tryout & UKTT - Produk/Event',
            'actions' => [
                [
                    'label' => 'List',
                    'r_method' => 'GET',
                    'c_name' => ['ProdukController', 'PaketController'],
                    'c_method' => 'index',
                    'allow' => true,
                ],
                [
                    'label' => 'Detail',
                    'r_method' => 'GET',
                    'c_name' => ['ProdukController', 'PaketController'],
                    'c_method' => ['show', 'show_detail'],
                    'allow' => true,
                ],
                [
                    'label' => 'Create',
                    'r_method' => 'POST',
                    'c_name' => ['ProdukController', 'PaketController'],
                    'c_method' => 'store',
                    'allow' => false,
                ],
                [
                    'label' => 'Edit',
                    'r_method' => 'PUT',
                    'c_name' => ['ProdukController', 'PaketController'],
                    'c_method' => 'update',
                    'allow' => false,
                ],
                [
                    'label' => 'Delete',
                    'r_method' => 'DELETE',
                    'c_name' => ['ProdukController', 'PaketController'],
                    'c_method' => 'destroy',
                    'allow' => false,
                ]
            ]
        ],
        // Tryout - Hasil Pengerjaan
        [
            'label' => 'Tryout Riwayat',
            'actions' => [
                [
                    'label' => 'View Hasil Pengerjaan',
                    'r_method' => 'GET',
                    'c_name' => 'TryoutUserController',
                    'c_method' => 'tryout_hasil_pengerjaan',
                    'allow' => false,
                ],
                [
                    'label' => 'View Riwayat Pengerjaan',
                    'r_method' => 'GET',
                    'c_name' => 'TryoutUserController',
                    'c_method' => 'tryout_riwayat_pengerjaan',
                    'allow' => false,
                ],
            ]
        ],
        // MBTI - Data Dimensi
        [
            'label' => 'MBTI - Data Dimensi',
            'actions' => [
                [
                    'label' => 'List',
                    'r_method' => 'GET',
                    'c_name' => 'DimensiController',
                    'c_method' => 'index',
                    'allow' => true,
                ],
                [
                    'label' => 'Detail',
                    'r_method' => 'GET',
                    'c_name' => 'DimensiController',
                    'c_method' => 'show',
                    'allow' => true,
                ],
                [
                    'label' => 'Create',
                    'r_method' => 'POST',
                    'c_name' => 'DimensiController',
                    'c_method' => 'store',
                    'allow' => false,
                ],
                [
                    'label' => 'Edit',
                    'r_method' => 'PUT',
                    'c_name' => 'DimensiController',
                    'c_method' => 'update',
                    'allow' => false,
                ],
                [
                    'label' => 'Delete',
                    'r_method' => 'DELETE',
                    'c_name' => 'DimensiController',
                    'c_method' => 'destroy',
                    'allow' => false,
                ]
            ]
        ],
        // MBTI - Data Kepribadian
        [
            'label' => 'MBTI - Data Kepribadian',
            'actions' => [
                [
                    'label' => 'List',
                    'r_method' => 'GET',
                    'c_name' => 'KepribadianController',
                    'c_method' => 'index',
                    'allow' => true,
                ],
                [
                    'label' => 'Detail',
                    'r_method' => 'GET',
                    'c_name' => 'KepribadianController',
                    'c_method' => 'show',
                    'allow' => true,
                ],
                [
                    'label' => 'Create',
                    'r_method' => 'POST',
                    'c_name' => 'KepribadianController',
                    'c_method' => 'store',
                    'allow' => false,
                ],
                [
                    'label' => 'Edit',
                    'r_method' => 'PUT',
                    'c_name' => 'KepribadianController',
                    'c_method' => 'update',
                    'allow' => false,
                ],
                [
                    'label' => 'Delete',
                    'r_method' => 'DELETE',
                    'c_name' => 'KepribadianController',
                    'c_method' => 'destroy',
                    'allow' => false,
                ]
            ]
        ],
        // MBTI - Data Utama
        [
            'label' => 'MBTI - Data Utama',
            'actions' => [
                [
                    'label' => 'List',
                    'r_method' => 'GET',
                    'c_name' => 'MbtiController',
                    'c_method' => 'index',
                    'allow' => true,
                ],
                [
                    'label' => 'Detail',
                    'r_method' => 'GET',
                    'c_name' => 'MbtiController',
                    'c_method' => ['show', 'show_active'],
                    'allow' => true,
                ],
                [
                    'label' => 'Create',
                    'r_method' => 'POST',
                    'c_name' => 'MbtiController',
                    'c_method' => 'store',
                    'allow' => false,
                ],
                [
                    'label' => 'Edit',
                    'r_method' => 'PUT',
                    'c_name' => 'MbtiController',
                    'c_method' => ['update', 'update_status'],
                    'allow' => false,
                ],
                [
                    'label' => 'Delete',
                    'r_method' => 'DELETE',
                    'c_name' => 'MbtiController',
                    'c_method' => 'destroy',
                    'allow' => false,
                ]
            ]
        ],
        // MBTI - Data Soal & Pertanyaan
        [
            'label' => 'MBTI - Data Soal & Pertanyaan',
            'actions' => [
                [
                    'label' => 'List',
                    'r_method' => 'GET',
                    'c_name' => ['MbtiPertanyaanController', 'MbtiDimensiController'],
                    'c_method' => 'index',
                    'allow' => true,
                ],
                [
                    'label' => 'Detail',
                    'r_method' => 'GET',
                    'c_name' => ['MbtiPertanyaanController', 'MbtiDimensiController'],
                    'c_method' => 'show',
                    'allow' => true,
                ],
                [
                    'label' => 'Create',
                    'r_method' => 'POST',
                    'c_name' => ['MbtiPertanyaanController', 'MbtiDimensiController'],
                    'c_method' => 'store',
                    'allow' => false,
                ],
                [
                    'label' => 'Edit',
                    'r_method' => 'PUT',
                    'c_name' => ['MbtiPertanyaanController', 'MbtiDimensiController'],
                    'c_method' => ['update', 'update_status'],
                    'allow' => false,
                ],
                [
                    'label' => 'Delete',
                    'r_method' => 'DELETE',
                    'c_name' => ['MbtiPertanyaanController', 'MbtiDimensiController'],
                    'c_method' => 'destroy',
                    'allow' => false,
                ]
            ]
        ],
        // Bank
        [
            'label' => 'Bank',
            'actions' => [
                [
                    'label' => 'List',
                    'r_method' => 'GET',
                    'c_name' => 'BankController',
                    'c_method' => 'index',
                    'allow' => false,
                ],
                // [
                //     'label' => 'Detail',
                //     'r_method' => 'GET',
                //     'c_name' => 'BankController',
                //     'c_method' => 'show',
                //     'allow' => true,
                // ],
                // [
                //     'label' => 'Create',
                //     'r_method' => 'POST',
                //     'c_name' => 'BankController',
                //     'c_method' => 'store',
                //     'allow' => false,
                // ],
                [
                    'label' => 'Edit',
                    'r_method' => 'PUT',
                    'c_name' => 'BankController',
                    'c_method' => ['update', 'updateMultiple'],
                    'allow' => false,
                ],
                // [
                //     'label' => 'Delete',
                //     'r_method' => 'DELETE',
                //     'c_name' => 'BankController',
                //     'c_method' => 'destroy',
                //     'allow' => false,
                // ]
            ]
        ],
        // Kelas Kursus
        [
            'label' => 'Kelas Kursus',
            'actions' => [
                [
                    'label' => 'List',
                    'r_method' => 'GET',
                    'c_name' => 'KursusController',
                    'c_method' => 'index',
                    'allow' => true,
                ],
                [
                    'label' => 'Detail',
                    'r_method' => 'GET',
                    'c_name' => 'KursusController',
                    'c_method' => 'detail',
                    'allow' => true,
                ],
                [
                    'label' => 'Active/Deactive',
                    'r_method' => 'PUT',
                    'c_name' => 'KursusController',
                    'c_method' => 'toggleActive',
                    'allow' => false,
                ],
                [
                    'label' => 'Verification',
                    'r_method' => 'PUT',
                    'c_name' => 'KursusController',
                    'c_method' => 'toggleVerification',
                    'allow' => false,
                ],
            ]
        ],
        // Transaksi & Pembayaran
        [
            'label' => 'Transaksi - Pembayaran Produk',
            'actions' => [
                [
                    'label' => 'List',
                    'r_method' => 'GET',
                    'c_name' => 'TransaksiController',
                    'c_method' => 'index',
                    'allow' => true,
                ],
                [
                    'label' => 'Detail',
                    'r_method' => 'GET',
                    'c_name' => 'TransaksiController',
                    'c_method' => 'show',
                    'allow' => true,
                ],
                [
                    'label' => 'Update Status',
                    'r_method' => 'PUT',
                    'c_name' => 'TransaksiController',
                    'c_method' => 'update_status',
                    'allow' => false,
                ]
            ]
        ],
        // Transaksi Penarikan Data
        [
            'label' => 'Transaksi - Penarikan Data',
            'actions' => [
                [
                    'label' => 'List',
                    'r_method' => 'GET',
                    'c_name' => 'PenarikanController',
                    'c_method' => 'historyPenarikan',
                    'allow' => true,
                ],
                [
                    'label' => 'Proses Permintaan',
                    'r_method' => 'PUT',
                    'c_name' => 'PenarikanController',
                    'c_method' => 'prosesPenarikan',
                    'allow' => true,
                ],
                [
                    'label' => 'Kirim/Upload Bukti',
                    'r_method' => 'PUT',
                    'c_name' => 'PenarikanController',
                    'c_method' => 'kirimDanaPenarikan',
                    'allow' => false,
                ]
            ]
        ],
        // User Siswa
        [
            'label' => 'Data Siswa',
            'actions' => [
                [
                    'label' => 'List',
                    'r_method' => 'GET',
                    'c_name' => 'StudentController',
                    'c_method' => 'index',
                    'allow' => true,
                ],
                [
                    'label' => 'Detail',
                    'r_method' => 'GET',
                    'c_name' => 'StudentController',
                    'c_method' => 'show',
                    'allow' => true,
                ],
            ]
        ],
        // User Tentor
        [
            'label' => 'Data Tentor',
            'actions' => [
                [
                    'label' => 'List',
                    'r_method' => 'GET',
                    'c_name' => 'TeacherController',
                    'c_method' => 'index',
                    'allow' => true,
                ],
                [
                    'label' => 'Detail',
                    'r_method' => 'GET',
                    'c_name' => 'TeacherController',
                    'c_method' => 'show',
                    'allow' => true,
                ],
                [
                    'label' => 'Verification',
                    'r_method' => 'PUT',
                    'c_name' => 'TeacherController',
                    'c_method' => 'verifikasi',
                    'allow' => false,
                ],
            ]
        ],
        // Pengaduan
        [
            'label' => 'Pengaduan',
            'actions' => [
                [
                    'label' => 'List',
                    'r_method' => 'GET',
                    'c_name' => 'PengaduanController',
                    'c_method' => 'index',
                    'allow' => true,
                ],
                [
                    'label' => 'Detail',
                    'r_method' => 'GET',
                    'c_name' => 'PengaduanController',
                    'c_method' => 'detail',
                    'allow' => true,
                ],
                [
                    'label' => 'Update Status',
                    'r_method' => 'PUT',
                    'c_name' => 'PengaduanController',
                    'c_method' => 'updateStatus',
                    'allow' => false,
                ],
                [
                    'label' => 'Update Priority',
                    'r_method' => 'PUT',
                    'c_name' => 'PengaduanController',
                    'c_method' => 'updatePrioritas',
                    'allow' => false,
                ],
            ]
        ],
        // Pengaturan
        [
            'label' => 'Pengaturan',
            'actions' => [
                [
                    'label' => 'View',
                    'r_method' => 'GET',
                    'c_name' => 'PengaturanController',
                    'c_method' => 'index',
                    'allow' => true,
                ],
                [
                    'label' => 'Edit',
                    'r_method' => 'PUT',
                    'c_name' => 'PengaturanController',
                    'c_method' => 'updateMultiple',
                    'allow' => false,
                ]
            ]
        ],
        // Level
        [
            'label' => 'Level',
            'actions' => [
                [
                    'label' => 'View',
                    'r_method' => 'GET',
                    'c_name' => 'LevelController',
                    'c_method' => 'index',
                    'allow' => false,
                ],
                [
                    'label' => 'Edit',
                    'r_method' => 'PUT',
                    'c_name' => 'LevelController',
                    'c_method' => 'updateMultiple',
                    'allow' => false,
                ]
            ]
        ],
    ];

    public function list()
    {

        if ($this->user()->role_user != 'superAdmin') {
            return responseFail('Unauthorized.', 403);
        }

        return responseData($this->permissionList, 200);
    }

    public function mine(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'user_id'  => 'nullable|exists:users,id',
        ]);
        $response = [];
        foreach ($this->permissionList as $key => $permission) {
            $response[$key] = $permission;
            foreach ($permission['actions'] as $key2 => $action) {

                $c_name = is_array($action['c_name']) ? implode(',', $action['c_name']) : $action['c_name'];
                $c_method = is_array($action['c_method']) ? implode(',', $action['c_method']) : $action['c_method'];

                if (empty($request->user_id) && $this->user()->role_user == 'superAdmin') {
                    $response[$key]['actions'][$key2]['allow'] = true;
                } else {
                    $check = DB::table('user_permissions')->where('user_id', $request->user_id ?? $this->userId())
                        ->where('user_role', 'admin')
                        ->where('request_method', $action['r_method'])
                        ->whereIn('controller_path', [$c_name])
                        ->whereIn('controller_method', [$c_method])->count();
                    if ($check == 0) {
                        $response[$key]['actions'][$key2]['allow'] = false;
                    } else {
                        $response[$key]['actions'][$key2]['allow'] = true;
                    }
                }
            }
        }

        return responseData($response, 200);
    }

    public function assign(Request $request)
    {

        $validate = Validator::make($request->all(), [
            'user_id'  => 'required|exists:users,id',
            // 'user_role'  => 'required',
            'permissions'  => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $user_role = User::find($request->user_id)->role_user;

            // if (!empty($request->permissions)) {
            $dataInsert = [];
            $index = 0;
            // print_r($request->permissions); die;
            DB::table('user_permissions')->where('user_id', $request->user_id)->delete();
            foreach ($request->permissions as $key => $permission) {
                foreach ($permission['actions'] as $key2 => $action) {                    
                    if ($action['allow'] == true) {
                        $dataInsert[$index]['user_id'] = $request->user_id;
                        $dataInsert[$index]['user_role'] = $user_role;
                        $dataInsert[$index]['module_name'] = $permission['label'];
                        $dataInsert[$index]['action_name'] = $action['label'];
                        $dataInsert[$index]['request_path'] = NULL;
                        $dataInsert[$index]['request_method'] = $action['r_method'];
                        $dataInsert[$index]['controller_path'] = is_array($action['c_name']) ? implode(",", $action['c_name']) : $action['c_name'];
                        $dataInsert[$index]['controller_method'] = is_array($action['c_method']) ? implode(",", $action['c_method']) : $action['c_method'];
                        $dataInsert[$index]['created_at'] = date('Y-m-d H:i:s');

                        $index++;
                    }
                }
            }

            // print_r($dataInsert);
            // die;

            DB::table('user_permissions')->insert($dataInsert);

            // }

            DB::commit();

            return response()->json(['success' => true, 'message' => 'Permission assigned successfully!'], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 500);
        }
    }
}
