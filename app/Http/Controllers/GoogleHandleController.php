<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Mail\TestMail;
use App\Models\Student;
use App\Models\Teacher;
use Illuminate\Support\Str;
use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class GoogleHandleController extends Controller
{
    public function googleLogin(Request $request, $type = null)
    {
        $user = User::where('email', $request->email)->first();

        if ($user) {

            if ($user->google_id != $request->sub) {
                $user->google_id = $request->sub;
                $user->save();
            }

            if ($user->verifikasi === 1) {
                return response()->json(['success' => false, 'message' => 'email belum di verifikasi'], 422);
            } else {
                $token =  $user->createToken(date('Y-m-d H:i:s'))->plainTextToken;

                $response = [
                    'user'  => $user,
                    'token' => $token,
                ];

                return response()->json(['success' => true, 'message' => 'user success login', 'data' => $response]);
            }
        } else {
            return response()->json(['success' => false, 'message' => 'User Tidak Ditemukan'], 200);
        }
    }

    public function googleSignup(Request $request, $type = 'siswa')
    {
        $notificationModel = new Notification();

        DB::beginTransaction();

        try {

            $random = Str::random(40);

            $insertUser = User::create([
                'username'   => str_replace(" ", "_", $request->name),
                'password'   => Hash::make(Str::random(8)),
                'role_user'  => $type,
                'token'      => $random,
                'email'      => $request->email,
                'verifikasi' => $request->email_verified ? 2 : 1,
            ]);

            if ($type == 'siswa') {

                $insertStudent = Student::create([
                    'id_siswa'          => $insertUser->id,
                    'nama_lengkap'      => $request->name,
                    'email'             => $request->email,
                    'nomor_telephone'   => null,
                    'info'              => null,
                ]);

                $data = [
                    'siswa' => ['users' => $insertUser, 'data' => $insertStudent]
                ];

                $notificationModel->insertData('insertData', [
                    'recipientId' => '',
                    'recipientType' => 4, // superadmin + admin
                    'type' => 0, // registration
                    'title' => 'Ada Pendaftar Baru!',
                    'body' => 'Ada Siswa baru telah mendaftar. Yuk cek detailnya!',
                    'data' => [
                        'user' => $insertUser,
                        'detail' => $insertStudent,
                    ],
                ]);

                $notificationModel->insertData('insertData', [
                    'recipientId' => $insertUser->id,
                    'recipientType' => 3, // student
                    'type' => 0, // registration
                    'title' => 'Selamat datang!',
                    'body' => "Terima kasih telah mendaftar di UjiAja.com. Yuk lengkapi profile kamu sekarang!",
                    'data' => [
                        'user' => $insertUser,
                        'detail' => $insertStudent,
                    ],
                ]);

                $testMail = new TestMail([
                    'subject'   => 'Pendaftaran akun Siswa berhasil!',
                    'to'        => $insertUser->email,
                    'toName'    => $insertStudent->nama_lengkap,
                    'content'   => '',
                    'token'     => $insertUser->token,
                    'view'      => 'email.content.customer__register-success'
                ]);
            }

            if ($type == 'teacher') {
                $insertTeacher = Teacher::create([
                    'id_teacher'          => $insertUser->id,
                    'nama_lengkap'        => $request->name,
                    'email'               => $request->email,
                    'nomor_telephone'     => null,
                    'info'                => null,
                    'verifikasi'          => 1,
                    'id_level'            => 1, // BASE
                    'nama_level'          => 'BASE'
                ]);

                $data = $insertTeacher;

                $notificationModel->insertData('insertBatchData', [
                    [
                        'recipientId' => '',
                        'recipientType' => 4, // superadmin + admin
                        'type' => 0, // registration
                        'title' => 'Ada Pendaftar Baru!',
                        'body' => 'Ada Tentor baru telah mendaftar. Yuk cek detailnya!',
                        'data' => [
                            'user' => $insertUser,
                            'detail' => $insertTeacher,
                        ],
                    ],
                    [
                        'recipientId' => $insertUser->id,
                        'recipientType' => 2, // teacher
                        'type' => 0, // registration
                        'title' => 'Selamat datang!',
                        'body' => "Terima kasih telah mendaftar di UjiAja.com. Yuk lengkapi profile kamu sekarang!",
                        'data' => [
                            'user' => $insertUser,
                            'detail' => $insertTeacher,
                        ],
                    ]
                ]);

                $testMail = new TestMail([
                    'subject' => 'Pendaftaran akun Tentor berhasil!',
                    'to' => $insertUser->email,
                    'toName' => $insertTeacher->nama_lengkap,
                    'content' => '',
                    'token' => $insertUser->token,
                    'view' => 'email.content.customer__register-success'
                ]);
            }

            DB::commit();

            // if ($testMail) {
            //     Mail::send($testMail);
            // }

            return response()->json(['success' => true, 'message' => 'success register', 'data' => $data], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage(), 'data' => $e->getMessage()], 400);
        }
    }
}
