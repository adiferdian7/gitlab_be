<?php

namespace App\Http\Controllers;

use App\Models\SoalPertanyaan;
use App\Models\SoalTryout;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

use Carbon\Carbon;

class SoalTryoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->paginate ?  $request->paginate : 10;
        $q = $request->q;

        $data = SoalTryout::with([
            'tryout:id,judul,deskripsi,kategori,panduan_pengerjaan',
            'mapel:id,nama_mapel',
            'pertanyaan:id,id_soal_tryout,bab_mapel,penjelasan_pertanyaan,template_pertanyaan,soal,opsi_pertanyaan,pembahasan_pertanyaan,jawaban_pertanyaan'
        ])
            ->select('id', 'id_tryout', 'jenis_soal', 'kelompok_soal', 'id_mapel', 'jeda_waktu_antar_mapel', 'alokasi_waktu_per_mapel')
            ->paginate($limit);

        return response()->json([
            'success' => true,
            'messages' => 'berhasil mengambil data',
            'data' => $data,
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'id_tryout'     => 'required',

        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }


        DB::beginTransaction();

        try {

            $data = [
                'id_tryout'     => $request->id_tryout,
                'id_mapel'      => $request->id_mapel,
                'kelompok_soal' => $request->kelompok_soal,
                'jenis_soal'    => $request->jenis_soal,
                'jeda_waktu_antar_mapel' => $request->jeda_waktu_antar_mapel,
                'alokasi_waktu_per_mapel' => $request->alokasi_waktu_per_mapel,
            ];


            $insert = SoalTryout::create($data)->load('mapel');
            $new_soal = [];

            if ($request->has('jumlah_soal') && $request->jumlah_soal > 0) {

                $soalPertanyaanInsert = [];

                for ($i = 0; $i < $request->jumlah_soal; $i++) {
                    $soalPertanyaanInsert[$i] = [
                        'id_soal_tryout' => $insert->id,
                        'bab_mapel'             => json_encode(['Bab Test']),
                        'penjelasan_pertanyaan' => '',
                        'template_pertanyaan'   => $request->template_pertanyaan ?? 'Pilihan Ganda',
                        'soal'                  => "Pertanyaannya...",
                        'gambar'                => null,
                        'opsi_pertanyaan'       => json_encode([
                            ['uuid' => uniqid(), 'option' => 'Opsi 1']
                        ]),
                        'pembahasan_pertanyaan' => null,
                        'parent_soal_pertanyaan' => null,
                        'jawaban_pertanyaan'    => null,
                        'created_at' => date('Y-m-d H:i:s'),
                    ];
                }

                SoalPertanyaan::insert($soalPertanyaanInsert);
                $new_soal = SoalPertanyaan::where('id_soal_tryout', $insert->id)->get();
            }

            DB::commit();

            return response()->json(['success' => true, 'message' => 'daasdasata berhasil di inputkan', 'data' => $insert, 'new_soal' => $new_soal], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }


    public function multiInsert(Request $request)
    {
        $validate = Validator::make($request->all(), [
            '*.id_tryout'     => 'required',

        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }


        DB::beginTransaction();

        try {

            $data = [];

            foreach ($request->all() as $key => $value) {

                $data[] = [
                    'id_tryout'     => $value['id_tryout'],
                    'id_mapel'      => $value['id_mapel'] ?? null,
                    'kelompok_soal' => $value['kelompok_soal'] ?? null,
                    'jenis_soal'    => $value['jenis_soal'] ?? null,
                    'jeda_waktu_antar_mapel' => $value['jeda_waktu_antar_mapel'] ?? null,
                    'alokasi_waktu_per_mapel' => $value['alokasi_waktu_per_mapel'] ?? null,
                    'created_at'    => Carbon::now()->toDateTimeString(),
                    'updated_at'    => Carbon::now()->toDateTimeString(),
                ];
            }


            $insert = SoalTryout::insert($data);

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di inputkan', 'data' => $data], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tryout  $tryout
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = SoalTryout::findOrFail($id);

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tryout  $tryout
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = SoalTryout::findOrFail($id);

        $validate = Validator::make($request->all(), [
            'id_tryout'     => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {


            $update->id_tryout     = $request->id_tryout;
            $update->id_mapel      = $request->id_mapel;
            $update->kelompok_soal = $request->kelompok_soal;
            $update->jenis_soal    = $request->jenis_soal;
            $update->jeda_waktu_antar_mapel = $request->jeda_waktu_antar_mapel;
            $update->alokasi_waktu_per_mapel = $request->alokasi_waktu_per_mapel;

            $update->save();

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $update], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tryout  $tryout
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SoalPertanyaan::where('id_soal_tryout', $id)->delete();
        $delete  = SoalTryout::destroy($id);
        return response()->json([
            'success' => true,
            'messages' => 'data berhasil di hapus',
            'data' => $id,
        ]);
    }
}
