<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\Admin;
use App\Models\SuperAdmin;
use App\Models\OrangTua;
use Carbon\Carbon;
use Illuminate\Support\Str;

use App\Mail\ResetPassword;
use App\Mail\TestMail;
use App\Models\Kepribadian;
use App\Models\MbtiJawaban;
use App\Models\Notification;
use App\Models\Transaksi;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    public function login(Request $request)
    {

        $validate = Validator::make($request->all(), [
            'username'          => 'required',
            'password'          => 'required',
        ]);

        $date = Carbon::now();

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        $user = User::where('username', $request->username)
            ->orWhere('email', $request->username)->first();


        if (!$user || !Hash::check($request->password, $user->password)) {

            $siswa = Student::where('email', $request->username)->first();

            return response()->json(['success' => false, 'message' => 'Username dan sandi tidak cocok.'], 422);
        } else {

            if ($user->verifikasi === 1) {

                return response()->json(['success' => false, 'message' => 'email belum di verifikasi'], 422);
            } else {
                $token =  $user->createToken($date)->plainTextToken;

                $response = [
                    'user'  => $user,
                    'token' => $token,
                ];

                return response()->json(['success' => true, 'data' => $response]);
            }
        }
    }


    public function getUser(Request $request)
    {

        $user = $request->user();

        // cek bonus mbti
        $count_bonus_mbti = DB::table('transaksis')->leftJoin('produks', 'produks.id', '=', 'transaksis.id_produk')
            ->where('id_user', $user->id)
            ->whereIn('jenis_transaksi', ['MBTI', 'Bonus MBTI'])
            ->whereNotNull('bonus')
            ->whereNull('transaksis.deleted_at')
            ->count();

        $mbti_jawaban = MbtiJawaban::where('id_user', $user->id)->first();

        if ($mbti_jawaban) {
            $kepribadian = Kepribadian::where('id', $mbti_jawaban->id_kepribadian)->first();
        } else {
            $kepribadian = null;
        }

        // return response()->json($mbti_jawaban);


        switch ($user->role_user) {
            case 'siswa':

                $data = Student::with(['parent'])->where('id_siswa', $user->id)->first();

                break;

            case 'parent':

                $data = OrangTua::with('siswa')->where('id_user', $user->id)->first();

                break;
            case 'teacher':
                $data = Teacher::with(['user_doc', 'level'])->where('id_teacher', $user->id)->first();

                if ($data) {
                    $data->total_mengajar = DB::table('teachers')
                        ->leftJoin('kursus', 'kursus.id_tentor', '=', 'teachers.id_teacher')
                        ->join('kursus_siswa', 'kursus_siswa.id_kursus', '=', 'kursus.id')
                        ->where('status_dikelas', 'Sesi Selesai')
                        ->groupBy('kursus.id_tentor')->count();
                    $data->total_kursus = DB::table('kursus')->where('id_tentor', $user->id)->count();
                }

                break;
            case 'admin':
                $data = Admin::where('id_admin', $user->id)->first();
                break;
            case 'superAdmin':
                $data = SuperAdmin::where('id_super_admin', $user->id)->first();
                break;
            default:
                return response()->json([
                    'success' => false,
                    'message' => 'user tidak terdaftar'
                ], 404);
                break;
        }

        $data->bonus_mbti = $count_bonus_mbti;
        $data->nama_kepribadian = $kepribadian ? $kepribadian->nama_singkat : null;

        $output = [
            'user' => $user,
            'detail' => $data,
        ];

        return response()->json(['success' => true, 'data' => $output]);
    }


    public function logout(Request $request)
    {

        $request->user()->tokens()->delete();

        return response()->json(['success' => true, 'message' => 'Success Logout']);
    }


    public function resetPassword(Request $request)
    {

        $validate = Validator::make($request->all(), [
            'email' => 'required',
            'url'   => 'required'
        ]);


        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        $userCek = User::where('email', $request->email)->first();

        if (!$userCek) {

            return response()->json([
                'success' => false,
                'message' => 'email tidak terdaftar',
            ], 422);
        } else {

            if ($userCek->verifikasi == 1) {
                return response()->json([
                    'success' => false,
                    'message' => 'email belum di verifikasi',
                ], 422);
            } else {

                $userCek->token = Str::random(10);
                $userCek->save();

                $user =  User::where('email', $request->email)->first();

                $mailData = [
                    'url'   => $request->url ? $request->url : env('WEB_URL') . '?token=' . $user->token,
                ];

                try {

                    // Mail::to($request->email)->send(new ResetPassword($mailData));
                    $notificationModel = new Notification();

                    $recipientType = 0;

                    if($user->role_user == 'siswa') {
                        $recipientType = 3;
                    } elseif ($user->role_user == 'teacher') {
                        $recipientType = 2;
                    } elseif ($user->role_user == 'admin') {
                        $recipientType = 1;
                    }

                    $notificationModel->insertData('insertData', [
                        'recipientId' => $user->id,
                        'recipientType' => $recipientType, 
                        'type' => 0, // registration
                        'title' => 'Permintaan Ubah Password berhasil dikirim!',
                        'body' => "Permintaan Anda telah kami kirimkan via email! Silakan cek untuk lakukan ubah password.",
                        'data' => [
                            'user' => $user,
                            'url' => $mailData['url']
                        ],
                    ]);

                    $testMail = new TestMail([
                        'subject' => 'Permintaan Ubah Password',
                        'to' => $user->email,
                        'toName' => $user->username,
                        'content' => '',
                        'token' => $user->token,
                        'view' => 'email.content.customer__request-reset',
                        'data' => [
                            'url' => $mailData['url']
                        ]
                    ]);

                    Mail::send($testMail);

                    return response()->json([
                        'success' => true,
                        'messages' => 'verifikasi telah dikirim ke email',
                        'data' => $mailData['url']
                    ], 200);
                } catch (\Exception $e) {

                    return response()->json([
                        'success' => false,
                        'messages' => $e->getMessage()
                    ], 400);
                }
            }
        }
    }

    public function activePassword(Request $request)
    {

        $validate = Validator::make($request->all(), [
            'token' => 'required',
            'new_password' => 'required',
        ]);


        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        $user = User::where('token', $request->token)->first();

        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'token telah usang',
            ], 400);
        }

        try {
            $user->password = Hash::make($request->new_password);
            $user->token = Str::random(10);
            $user->save();

            return response()->json([
                'success' => true,
                'message' => 'password berhasil di ubah',
            ]);
        } catch (\Exception $e) {

            return response()->json([
                'success' => false,
                'messages' => $e->getMessage()
            ], 400);
        }
    }
}
