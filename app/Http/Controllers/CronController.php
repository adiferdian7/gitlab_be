<?php

namespace App\Http\Controllers;

use App\Models\Transaksi;
use Illuminate\Http\Request;

class CronController extends Controller
{
    public function expiredTransactionCheck() {
        $allTransaksi = Transaksi::where('status', 'Menunggu Pembayaran')
        ->orWhere('status', '')
        ->get();
        foreach ($allTransaksi as $key => $transaksi) {
            if(time() > strtotime($transaksi->batas_pembayaran)) {
                Transaksi::find($transaksi->id)->update(['status' => 'Kadaluarsa']);
            }
        }
        return response()->json('ok');
    }
}
