<?php

namespace App\Http\Controllers;

use App\Models\Kursus;
use App\Models\KursusUlasan;
use App\Models\Teacher;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class KursusUlasanController extends Controller
{
  function __construct()
  {
    $this->kursusUlasanModel = new KursusUlasan();
  }

  public function index(Request $request)
  {
    $reqData = $request->all();
    if (in_array($request->user()->role_user, ['superAdmin', 'admin'])) {
      $data = $this->kursusUlasanModel->getData('paginationData', $reqData, 'paginationData');
    } else {
      if (in_array($request->user()->role_user, ['teacher', 'tentor'])) {
        $reqData['id_tentor'] = $this->userId();
      }
      $data = $this->kursusUlasanModel->getData('paginationData', $reqData, 'memberPaginationData');
    }
    return responseData($data);
  }

  public function store(Request $request)
  {
    $validate = Validator::make($request->all(), [
      'id_kursus'   => 'required|exists:kursus,id,deleted_at,NULL',
      'id_siswa'   => 'required|exists:students,id_siswa,deleted_at,NULL',
      'nilai'   => 'required',
    ], $this->kursusUlasanModel->getValidationMessage());

    if ($validate->fails()) {
      return responseFail($validate->messages(), 422);
    }

    $result = $this->kursusUlasanModel->insertData('insertData', $request->all());

    if (isset($result['error'])) {
      return responseFail($result, 500);
    } else {
      
      $kursusModel = new Kursus();
      $kursusDetail = $kursusModel->getData('detailData', ['id' => $request->id_kursus]);

      // ambil data kursus
      $kursusUlasanData = $this->kursusUlasanModel->getData('paginationData', ['id_kursus' => $request->id_kursus]);

      $totalNilai = 0;
      foreach ($kursusUlasanData as $row) {
        $totalNilai += $row['nilai'];
      }

      $rerataNilai = 0;
      if(count($kursusUlasanData) > 0) {
        // rata2 = sum data / total data
        $rerataNilai = round($totalNilai / count($kursusUlasanData), 1);
      }

      // update rate mengajar
      Teacher::where('id_teacher', $kursusDetail['id_tentor'])->update([
        'rate_mengajar' => $rerataNilai
      ]);

      return responseInsert($result);
    }
  }

  public function detail($id = '')
  {
    $data = $this->kursusUlasanModel->getData('detailData', ['id' => $id]);
    if (!empty($data)) {
      return responseData($data);
    } else {
      return responseFail('Data tidak ditemukan!', 404);
    }
  }

  public function update(Request $request, $id)
  {
    $req = $request->all();
    $req['id'] = $id;

    $validate = Validator::make($req, [
      'id' => 'required|exists:kursus_ulasan,id',
      // 'id_kursus'   => 'required|exists:kursus,id,deleted_at,NULL',
      // 'id_siswa'   => 'required|exists:students,id_siswa,deleted_at,NULL',
      'nilai'   => 'required',
    ], $this->kursusUlasanModel->getValidationMessage());

    if ($validate->fails()) {
      return responseFail($validate->messages(), 422);
    }

    $result = $this->kursusUlasanModel->updateData('updateData', $req);

    if (isset($result['error'])) {
      return responseFail($result, 500);
    } else {
      return responseInsert($result);
    }
  }

  public function tentorReplyUlasan(Request $request, $id)
  {
    $req = $request->all();
    $req['id'] = $id;

    $validate = Validator::make($req, [
      'id' => 'required|exists:kursus_ulasan,id',
      // 'id_kursus'   => 'required|exists:kursus,id,deleted_at,NULL',
      // 'id_siswa'   => 'required|exists:students,id_siswa,deleted_at,NULL',
      'balasan'   => 'required',
    ], $this->kursusUlasanModel->getValidationMessage());

    $errorMessage = $validate->messages();
    if ($validate->fails()) {
      return responseFail($errorMessage, 422);
    }

    if(auth()->user()->role_user != 'teacher') {
      $errorMessage = ['error' => ['Akses dilarang!']];
      return responseFail($errorMessage, 422);
    }

    $detail = $this->kursusUlasanModel->getData('detailData', $req);

    if(auth()->user()->id != $detail->kursus->tentor->id_teacher) {
      $errorMessage = ['error' => ['Akses dilarang!']];
      return responseFail($errorMessage, 422);
    }
    
    $result = $this->kursusUlasanModel->updateData('tentorReplyUlasan', $req);

    if (isset($result['error'])) {
      return responseFail($result, 500);
    } else {
      return responseUpdate($result);
    }
  }

  public function delete(Request $request, $id)
  {
    $req = $request->all();
    $req['id'] = $id;

    $validate = Validator::make($req, [
      'id' => 'required|exists:kursus_ulasan,id',
    ], $this->kursusUlasanModel->getValidationMessage());

    if ($validate->fails()) {
      return responseFail($validate->messages(), 422);
    }

    $result = $this->kursusUlasanModel->deleteData('deleteData', ['id' => $id]);

    if (isset($result['error'])) {
      return responseFail($result);
    }
    return responseDeleted($result);
  }
}
