<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Student;
use App\Models\OrangTua;
use Illuminate\Support\Str;

use App\Mail\MySendMail;
use App\Mail\TestMail;
use App\Models\Notification;
use Illuminate\Support\Facades\Mail;

use App\Utils\Wilayah;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $limit = $request->paginate ?  $request->paginate : 10;
        $q = $request->q ?? "";

        $data  = Student::with([
            'parent:id_orang_tua,id_user,nama_lengkap,email,nomor_telephone',
            'penjurusan:id,id_kelas,nama_penjurusan',
            'penjurusan.kelas:id,id_jenjang,nama_kelas',
            'penjurusan.kelas.jenjang:id,nama_jenjang'
        ])
            ->select(
                'id_siswa',
                'nama_lengkap',
                'email',
                'nomor_telephone',
                'info',
                'foto',
                'tempat_lahir',
                'tgl_lahir',
                'jenis_kelamin',
                'alamat',
                'id_penjurusan',
                'id_provinsi',
                'id_kota',
                'id_kecamatan',
                'nama_sekolah',
            )
            ->where(
                function ($query) use ($q) {
                    // $query->where('username', 'like',  '%' . $q . '%');
                    $query->orWhere('nama_lengkap', 'like',  '%' . $q . '%');
                    $query->orWhere('email', 'like',  '%' . $q . '%');
                    $query->orWhere('nomor_telephone', 'like',  '%' . $q . '%');
                }
            )
            ->orderByDesc('created_at')
            ->paginate($limit);


        return response()->json([
            'success' => true,
            'messages' => 'berhasil mengambil data',
            'data' => $data,
        ]);
    }

    public function dashboard()
    {
        $id_user = auth()->user()->id;
        $total_tryout = DB::table('transaksis')->where('id_user', $id_user)
        ->whereIn('jenis_transaksi', ['Tryout', 'Bundling - Product'])
        ->whereNull('deleted_at')->groupBy('id_produk')->count();
        $total_kursus = DB::table('transaksis')->where('id_user', $id_user)
        ->whereIn('jenis_transaksi', ['Les Privat'])
        ->whereNull('deleted_at')->groupBy('id_produk')->count();

        $data = [
            'total_tryout' => $total_tryout,
            'total_kursus' => $total_kursus,
        ];

        return responseData($data);
    }

    public function cek(Request $request)
    {
        $user = $request->user();
        if ($user->role_user != 'siswa') {
            return response()->json(['success' => false, 'messages' => 'maaf anda bukan role siswa'], 404);
        }

        $students = Student::find($user->id);

        if (
            $students->tgl_lahir == null || $students->alamat == null
            || $students->tempat_lahir == null
            || $students->jenis_kelamin == null
            // || $students->id_prodi_bind_perguruan == null
            // || $students->id_prodi_bind_perguruan_2 == null
        ) {

            return response()->json(['success' => false, 'messages' => 'profil belum di lengkapi']);
        }

        return response()->json(['success' => true, 'messages' => 'profil sudah dilengkapi']);
    }

    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'username'                  => 'required|unique:users',
            'nama_lengkap'              => 'required',
            'email'                     => 'required|unique:students|unique:users',
            'nomor_telephone'           => 'required',
            'password'                  => 'required',
            'info'                      => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }


        DB::beginTransaction();

        try {


            $insertUser = User::create([
                'username'   => $request->username,
                'password'   => Hash::make($request->password),
                'role_user'  => 'siswa',
                'token'      => Str::random(40),
                'email'     => $request->email,
                'verifikasi' => 1,
            ]);

            $insertStudent = Student::create([
                'id_siswa'          => $insertUser->id,
                'nama_lengkap'      => $request->nama_lengkap,
                'email'             => $request->email,
                'nomor_telephone'   => $request->nomor_telephone,
                'info'              => $request->info,
            ]);

            DB::commit();

            $data = [
                'siswa' => ['users' => $insertUser, 'data' => $insertStudent]
            ];

            // $mailData = [
            //     'token' => $insertUser->token,
            // ];

            // Mail::to($request->email)->send(new MySendMail($mailData));
            $notificationModel = new Notification();
            $notificationModel->insertData('insertData', [
                'recipientId' => '',
                'recipientType' => 4, // superadmin + admin
                'type' => 0, // registration
                'title' => 'Ada Pendaftar Baru!',
                'body' => 'Ada Siswa baru telah mendaftar. Yuk cek detailnya!',
                'data' => [
                    'user' => $insertUser,
                    'detail' => $insertStudent,
                ],
            ]);

            $notificationModel->insertData('insertData', [
                'recipientId' => $insertUser->id,
                'recipientType' => 3, // student
                'type' => 0, // registration
                'title' => 'Selamat datang!',
                'body' => "Terima kasih telah mendaftar di UjiAja.com. Yuk lengkapi profile kamu sekarang!",
                'data' => [
                    'user' => $insertUser,
                    'detail' => $insertStudent,
                ],
            ]);

            $testMail = new TestMail([
                'subject' => 'Pendaftaran akun Siswa berhasil!',
                'to' => $insertUser->email,
                'toName' => $insertStudent->nama_lengkap,
                'content' => '',
                'token' => $insertUser->token,
                'view' => 'email.content.customer__register-success'
            ]);

            Mail::send($testMail);

            return response()->json(['success' => true, 'message' => 'siswa success register', 'data' => $data], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Student::with([
            'parent:id_orang_tua,id_user,nama_lengkap,email,nomor_telephone',
            'prodi_satu:id,id_program_studi,id_perguruan_tinggi,akreditasi_program_studi,passing_grade_prodi',
            'prodi_satu.program_studi:id,nama_studi',
            'prodi_satu.perguruan:id,nama_perguruan',
            'prodi_dua:id,id_program_studi,id_perguruan_tinggi,akreditasi_program_studi,passing_grade_prodi',
            'prodi_dua.program_studi:id,nama_studi',
            'prodi_dua.perguruan:id,nama_perguruan',
        ])->findOrFail($id);

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $siswa = Student::findOrFail($id);
        $user  = User::findOrFail($siswa->id_siswa);
        $validate = Validator::make($request->all(), [
            'username'                  => 'required|unique:users,username,' . $id,
            'nama_lengkap'              => 'required',
            'email'                     => 'required|email|unique:users,email,' . $id,
            'nomor_telephone'           => 'required',
            'info'                      => 'required',
            'tempat_lahir'              => 'required',
            'tgl_lahir'                 => 'required',
            'jenis_kelamin'             => 'required',
            'alamat'                    => 'required',
            'id_provinsi'               => 'required',
            'id_kota'                   => 'required',
            'id_kecamatan'              => 'required',
            'id_jenjang'              => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        // if($request->foto){
        //     $validator = Validator::make($request->all(), [
        //         'foto' => 'required|image:jpeg,png,jpg,gif,svg|max:2048'
        //     ]);

        //     if ($validator->fails()) {
        //         return response()->json([
        //             'success' => false, 
        //             'messages' => $validate->messages()
        //         ], 422);
        //     }
        // }

        DB::beginTransaction();

        try {


            $tgl = date('Y-m-d', strtotime($request->tgl_lahir));

            if (!empty($request->foto)) {

                // $uploadFolder = 'users';
                // $image = $request->file('foto');
                // $image_uploaded_path = $image->store($uploadFolder, 'public');
                // $urlUpload = Storage::disk('public')->url($image_uploaded_path);

                $siswa->foto = $request->foto;
            }

            $siswa->nama_lengkap    = $request->nama_lengkap;
            $siswa->email           = $request->email;
            $siswa->nomor_telephone = $request->nomor_telephone;
            $siswa->info            = $request->info;
            $siswa->tempat_lahir    = $request->tempat_lahir;
            $siswa->tgl_lahir       = $tgl;
            $siswa->jenis_kelamin   = $request->jenis_kelamin;
            $siswa->alamat          = $request->alamat;
            $siswa->id_provinsi     = $request->id_provinsi;
            $siswa->id_kota         = $request->id_kota;
            $siswa->id_kecamatan    = $request->id_kecamatan;
            $siswa->id_penjurusan   = $request->id_penjurusan;
            $siswa->nama_sekolah    = $request->nama_sekolah;
            $siswa->nama_provinsi   = $request->nama_provinsi;
            $siswa->nama_kota       = $request->nama_kota;
            $siswa->nama_kecamatan  = $request->nama_kecamatan;
            $siswa->id_jenjang  = $request->id_jenjang;
            $siswa->nama_jenjang  = $request->nama_jenjang;

            if ($siswa->nama_jenjang == 'SMA' || $siswa->nama_jenjang == 'SMK' || $siswa->nama_jenjang == 'MA' || strpos($request->nama_jenjang, 'SMA') || strpos($request->nama_jenjang, 'SMK') || strpos($request->nama_jenjang, 'MA')) {
                $siswa->id_prodi_bind_perguruan  = $request->id_prodi_bind_perguruan;
                $siswa->id_prodi_bind_perguruan_2  = $request->id_prodi_bind_perguruan_2;
            } else {
                $siswa->id_prodi_bind_perguruan  = null;
                $siswa->id_prodi_bind_perguruan_2  = null;
            }

            $siswa->save();

            if (!empty($request->password)) {
                $user->password = Hash::make($request->password);
            }

            $user->username    = $request->username;
            $user->email       = $request->email;
            $user->save();

            DB::commit();

            return response()->json([
                'success' => true,
                'messages' => 'update siswa success',
                'data' => $siswa,
            ], 200);
        } catch (\Exception $e) {

            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $siswa = Student::findOrFail($id);
        $delete = Student::destroy($id);
        $user   = User::destroy($siswa->id_siswa);
        return response()->json([
            'success' => true,
            'messages' => 'data berhasil di hapus',
            'data' => $id,
        ]);
    }

    public function undo(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }


        DB::beginTransaction();

        try {

            $student =  Student::withTrashed()->findOrFail($request->id);
            Student::withTrashed()->find($request->id)->restore();
            User::withTrashed()->find($student->id_siswa)->restore();

            DB::commit();

            return response()->json([
                'success' => true,
                'messages' => 'data berhasil di pulihkan',
            ]);
        } catch (\Exception $e) {

            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }
}
