<?php

namespace App\Http\Controllers;

use App\Models\BundlingProduct;
use Illuminate\Http\Request;

class BundlingProductController extends Controller
{
  function __construct()
  {
    $this->bundlingProductModel = new BundlingProduct();
  }

  public function delete(Request $request, $id)
  {
    $bundling = $this->bundlingProductModel->find($id);
    if (!$bundling) {
      return responseNotFound();
    }
    $result = $this->bundlingProductModel->deleteData('deleteData', ['id' => $id]);

    if (isset($result['error'])) {
      return responseFail($result);
    }
    return responseDeleted($result);
  }
}
