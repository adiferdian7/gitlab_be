<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\Dimensi;

class DimensiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->paginate ?  $request->paginate : 10;
        $q = $request->q;
        $data = Dimensi::where('nama', 'like', '%' .$q. '%')
                    ->paginate($limit);
                    
        return response()->json(['success' => true, 'data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'nama'          => 'required|unique:dimensis,nama,NULL,id,deleted_at,NULL',
            'deskripsi'     => 'required',
            'psikologi_1'   => 'required',
            'psikologi_2'   => 'required'

        ]);

        if($validate->fails()) {
            return response()->json([
                'success' => false, 
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $insert = Dimensi::create([
                'nama'      => $request->nama,
                'psikologi_1' => $request->psikologi_1,
                'psikologi_2' => $request->psikologi_2,
                'deskripsi' => $request->deskripsi, 
            ]);

            DB::commit();

             return response()->json(['success' => true, 'message' => 'data berhasil di inputkan', 'data' => $insert], 201);

        } catch (\Exception $e) {
             DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Dimensi::findOrFail($id);

        return response()->json([
            'success' => true, 
            'data' => $data
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Dimensi::findOrFail($id);

         $validate = Validator::make($request->all(), [
            'nama'          => 'required|unique:dimensis,nama,'.$id.',id,deleted_at,NULL',
            'deskripsi'     => 'required',
            'psikologi_1'   => 'required',
            'psikologi_2'   => 'required'
        ]);

        if($validate->fails()) {
            return response()->json([
                'success' => false, 
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $update->nama           = $request->nama;
            $update->psikologi_1    = $request->psikologi_1;
            $update->psikologi_2    = $request->psikologi_2;
            $update->deskripsi      = $request->deskripsi;
            $update->save();

            DB::commit();

             return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $update], 201);

        } catch (\Exception $e) {
             DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $delete  = Dimensi::destroy($id);

        return response()->json([
            'success' => true,
            'messages' => 'data berhasil di hapus',
            'data' => $id,
        ]);
    }
}
