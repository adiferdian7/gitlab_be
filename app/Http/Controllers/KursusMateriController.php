<?php

namespace App\Http\Controllers;

use App\Models\KursusMateri;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;

class KursusMateriController extends Controller
{
  function __construct()
  {
    $this->kursusMateriModel = new KursusMateri();
  }

  public function index(Request $request)
  {
    $data = $this->kursusMateriModel->getData('data', $request->all());
    return responseData($data);
  }

  public function indexPagination(Request $request)
  {
    $data = $this->kursusMateriModel->getData('paginationData', $request->all());
    return responseData($data);
  }


  public function store(Request $request)
  {
    $validate = Validator::make($request->all(), [
      'id_kursus'   => 'required|exists:kursus,id,deleted_at,NULL',
      'judul_materi'   => 'required|unique:kursus,nama_kursus,NULL,id,deleted_at,NULL',
      'deskripsi_materi'   => 'required',
      'link_file_materi'   => Rule::requiredIf($request->link_video_materi == null || $request->link_file_materi == null),
    ], $this->kursusMateriModel->getValidationMessage());

    if ($validate->fails()) {
      return responseFail($validate->messages(), 422);
    }

    $result = $this->kursusMateriModel->insertData('insertData', $request->all());

    if (isset($result['error'])) {
      return responseFail($result, 500);
    } else {
      return responseInsert($result);
    }
  }

  public function detail($id = '')
  {
    $data = $this->kursusMateriModel->getData('detailData', ['id' => $id]);
    if (!empty($data)) {
      return responseData($data);
    } else {
      return responseFail('Data tidak ditemukan!', 404);
    }
  }

  public function update(Request $request, $id)
  {
    $req = $request->all();
    $req['id'] = $id;

    $validate = Validator::make($req, [
      'id' => 'required|exists:kursus_materi,id',
      'id_kursus'   => 'required|exists:kursus,id,deleted_at,NULL',
      'judul_materi'   => 'required|unique:kursus,nama_kursus,NULL,id,deleted_at,NULL',
      'deskripsi_materi'   => 'required',
      'link_file_materi'   => Rule::requiredIf($request->link_video_materi == null || $request->link_file_materi == null),
    ], $this->kursusMateriModel->getValidationMessage());

    if ($validate->fails()) {
      return responseFail($validate->messages(), 422);
    }

    $result = $this->kursusMateriModel->updateData('updateData', $req);

    if (isset($result['error'])) {
      return responseFail($result, 500);
    } else {
      return responseUpdate($result);
    }
  }

  public function delete(Request $request, $id)
  {
    $req = $request->all();
    $req['id'] = $id;

    $validate = Validator::make($req, [
      'id' => 'required|exists:kursus_jadwal,id',
    ], $this->kursusMateriModel->getValidationMessage());

    if ($validate->fails()) {
      return responseFail($validate->messages(), 422);
    }

    $result = $this->kursusMateriModel->deleteData('deleteData', ['id' => $id]);

    if (isset($result['error'])) {
      return responseFail($result);
    }
    return responseDeleted($result);
  }
}
