<?php

namespace App\Http\Controllers;

use App\Models\KategoriTo;
use App\Models\Penjurusan;
use App\Models\SoalPertanyaan;
use App\Models\SoalTryout;
use App\Models\Tryout;
use App\Models\TryoutUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class TryoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->paginate ?  $request->paginate : 10;
        $q = $request->q;
        $data = Tryout::with([
            'jenjang:id,nama_jenjang',
            'penjurusan:id,id_kelas,nama_penjurusan',
            'penjurusan.kelas:id,id_jenjang,nama_kelas',
            'penjurusan.kelas.jenjang:id,nama_jenjang',
            'soal:id,id_tryout,id_mapel',
            'soal.mapel:id,nama_mapel',
            // 'soal.pertanyaan:id,id_soal_tryout,bab_mapel,penjelasan_pertanyaan,template_pertanyaan,soal,opsi_pertanyaan,pembahasan_pertanyaan,jawaban_pertanyaan',
            'soal.pertanyaan:id,id_soal_tryout',
        ]);

        if (!empty($request->filter)) {

            $data->where('kategori', $request->filter);
        }

        if (!empty($request->excludes)) {
            $data->whereNotIn('kategori', $request->excludes);
        }

        $data->where(function ($query) use ($q) {

            $query->where('judul', 'like', '%' . $q . '%')
                ->orWhere('kategori', 'like', '%' . $q . '%')
                ->orWhere('deskripsi', 'like', '%' . $q . '%')
                ->orWhere('jenis_soal', 'like', '%' . $q . '%')
                ->orWhere('kelompok_soal', 'like', '%' . $q . '%')
                ->orWhere('template_soal', 'like', '%' . $q . '%');

            $query->orWhereHas('penjurusan', function ($filter) use ($q) {
                $filter->where('nama_penjurusan', 'like', '%' . $q . '%');
            });
            $query->orWhereHas('penjurusan.kelas', function ($filter) use ($q) {
                $filter->where('nama_kelas', 'like', '%' . $q . '%');
            });
            $query->orWhereHas('penjurusan.kelas.jenjang', function ($filter) use ($q) {
                $filter->where('nama_jenjang', 'like', '%' . $q . '%');
            });
        });

        $data->selectRaw('*, SUBSTR(panduan_pengerjaan, 1, 100) as panduan_pengerjaan');

        $array = $data->latest()->paginate($limit);

        return response()->json([
            'success' => true,
            'messages' => 'berhasil mengambil data',
            'data' => $array,
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'judul'         => 'required|unique:tryouts,judul,NULL,id,deleted_at,NULL',
            'kategori'      => 'required',
            'deskripsi'     => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        $id_penjurusan = $request->id_penjurusan;
        $id_kelas = $request->id_kelas;
        $id_jenjang = $request->id_jenjang;
        $kategoriTo =  DB::table('to_kategori');
        $kategoriToDetail = null;
        $id_to_kategori = null;
        if ($request->kategori == 'UTBK') {
            $kategoriToDetail = $kategoriTo->where('kategori', $request->kategori)
                ->where('kelompok', $request->kelompok_soal)->first();
        } else {
            $penjurusanDetail = Penjurusan::with(['kelas.jenjang'])->where('id', $id_penjurusan)->first();
            if ($penjurusanDetail && empty($id_jenjang)) {
                $id_jenjang = $penjurusanDetail->kelas && $penjurusanDetail->kelas->jenjang ? $penjurusanDetail->kelas->jenjang->id : null;
            }

            if ($request->kategori == 'ASPD') {
                $kategoriToDetail = $kategoriTo->where('kategori', $request->kategori)
                    ->where('id_jenjang', $id_jenjang)->first();
            } else if ($request->kategori == 'PAS' || $request->kategori == 'PAT') {
                if ($penjurusanDetail && $penjurusanDetail->nama_penjurusan != '-' && $penjurusanDetail->nama_penjurusan != null) {
                    $kategoriToDetail = $kategoriTo->where('kategori', $request->kategori)
                        ->where('id_jenjang', $id_jenjang)
                        ->where('penjurusan', $penjurusanDetail->nama_penjurusan)
                        ->first();
                } else {
                    $kategoriToDetail = $kategoriTo->where('kategori', $request->kategori)
                        ->where('id_jenjang', $id_jenjang)
                        ->first();
                }
            }

            if ($penjurusanDetail && $kategoriToDetail && empty($id_kelas)) {
                $id_kelas = $penjurusanDetail->kelas ? $penjurusanDetail->kelas->id : null;
            }
        }
        if ($kategoriToDetail) {
            $id_to_kategori = $kategoriToDetail->id;
        }

        try {

            $insert = Tryout::create([
                'judul'                 => $request->judul,
                'kategori'              => $request->kategori,
                'deskripsi'             => $request->deskripsi,
                'jenis_soal'            => $request->jenis_soal,
                'kelompok_soal'         => $request->kelompok_soal,
                'template_soal'         => $request->template_soal,
                'id_penjurusan'         => $request->id_penjurusan,
                'panduan_pengerjaan'    => $request->panduan_pengerjaan,
                'jeda_waktu'            => $request->jeda_waktu ?? null,
                'alokasi_waktu'         => $request->alokasi_waktu ?? null,
                'subexam_breaks'        => $request->subexam_breaks ?? null,
                'id_jenjang'            => $id_jenjang,
                'id_to_kategori'        => $id_to_kategori
            ]);

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di inputkan', 'data' => $insert], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tryout  $tryout
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Tryout::with([
            'jenjang:id,nama_jenjang',
            'soal', 'soal.pertanyaan',
            'soal.mapel:id,nama_mapel',
            'penjurusan:id,id_kelas,nama_penjurusan',
            'penjurusan.kelas:id,id_jenjang,nama_kelas',
            'penjurusan.kelas.jenjang:id,nama_jenjang'
        ])->findOrFail($id);
        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    /**
     * Menampilkan detail versi lengkap
     *
     * @param  \App\Models\Tryout  $tryout
     * @return \Illuminate\Http\Response
     */
    public function show_detail($id)
    {
        $dataTryout = Tryout::with([
            'soal', 'soal.pertanyaan' => function ($query) {
                return $query->where('soal_pertanyaans.parent_soal_pertanyaan', '=', null)->get();
            }, 'soal.pertanyaan.pertanyaan_child',

            'penjurusan:id,id_kelas,nama_penjurusan',
            'penjurusan.kelas:id,id_jenjang,nama_kelas',
            'penjurusan.kelas.jenjang:id,nama_jenjang',
            'soal.mapel:id,nama_mapel',
            'jenjang:id,nama_jenjang',
        ])->findOrFail($id);

        // $data = [];
        // $data['id'] = $dataTryout->id;
        // $data['judul'] = $dataTryout->judul;
        // $data['deskripsi'] = $dataTryout->deskripsi;
        // $data['jenis_soal'] = $dataTryout->jenis_soal;
        // $data['kelompok_soal'] = $dataTryout->kelompok_soal;
        // $data['template_soal'] = $dataTryout->template_soal;
        // $data['id_penjurusan'] = $dataTryout->id_penjurusan;
        // $data['panduan_pengerjaan'] = $dataTryout->panduan_pengerjaan;
        // $data['alokasi_waktu'] = $dataTryout->alokasi_waktu;
        // $data['jeda_waktu'] = $dataTryout->jeda_waktu;
        // $data['id_jenjang'] = $dataTryout->id_jenjang;
        // $data['created_at'] = $dataTryout->created_at;
        // $data['updated_at'] = $dataTryout->updated_at;
        // $data['deleted_at'] = $dataTryout->deleted_at;
        // $data['soal'] = [];
        $nomorGlobal = 1;
        $nomorSubtest = [];
        foreach ($dataTryout->soal as $key => $perSubtest) {
            // $data['soal'][$key] = $perSubtest;
            // $nomor = 1;
            foreach ($perSubtest->pertanyaan as $key2 => $pertanyaan) {
                $pertanyaan->nomor = $nomorGlobal;
                // $data['soal'][$key]['pertanyaan'] =  $pertanyaan;

                // $nomor++;
                $nomorSubtest[$key][] = $nomorGlobal;
                $nomorGlobal++;
                foreach ($pertanyaan->pertanyaan_child as $key3 => $pertanyaan_child) {
                    $pertanyaan_child->nomor = $nomorGlobal;
                    // $data['soal'][$key]['pertanyaan'][$key2]['nomor'] = $nomor;

                    // $nomor++;
                    $nomorSubtest[$key][] = $nomorGlobal;
                    $nomorGlobal++;
                }
            }
        }

        return response()->json([
            'success' => true,
            'totalNomor' => $nomorGlobal - 1,
            'nomorSubtest' => $nomorSubtest,
            'data' => $dataTryout,
        ]);
    }

    public function list_soal($id)
    {
        $dataTryout = Tryout::with([
            'soal', 'soal.pertanyaan' => function ($query) {
                return $query->where('soal_pertanyaans.parent_soal_pertanyaan', '=', null)->get();
            }, 'soal.pertanyaan.pertanyaan_child',
            // 'penjurusan:id,id_kelas,nama_penjurusan',
            // 'penjurusan.kelas:id,id_jenjang,nama_kelas',
            // 'penjurusan.kelas.jenjang:id,nama_jenjang',
            // 'soal.mapel:id,nama_mapel',
            // 'jenjang:id,nama_jenjang',
        ])->findOrFail($id);
        $nomorGlobal = 1;
        $nomorSubtest = [];
        foreach ($dataTryout->soal as $key => $perSubtest) {
            // $data['soal'][$key] = $perSubtest;
            // $nomor = 1;
            foreach ($perSubtest->pertanyaan as $key2 => $pertanyaan) {
                $pertanyaan->nomor = $nomorGlobal;
                // $data['soal'][$key]['pertanyaan'] =  $pertanyaan;

                // $nomor++;
                $nomorSubtest[$key][] = $nomorGlobal;
                $nomorGlobal++;
                foreach ($pertanyaan->pertanyaan_child as $key3 => $pertanyaan_child) {
                    $pertanyaan_child->nomor = $nomorGlobal;
                    // $data['soal'][$key]['pertanyaan'][$key2]['nomor'] = $nomor;

                    // $nomor++;
                    $nomorSubtest[$key][] = $nomorGlobal;
                    $nomorGlobal++;
                }
            }
        }

        return response()->json([
            'success' => true,
            'totalNomor' => $nomorGlobal - 1,
            'nomorSubtest' => $nomorSubtest,
            'data' => $dataTryout->soal,
        ]);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tryout  $tryout
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Tryout::findOrFail($id);

        $validate = Validator::make($request->all(), [
            'judul'         => 'required|unique:tryouts,judul,' . $id . ',id,deleted_at,NULL',
            'kategori'      => 'required',
            'deskripsi'     => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        $id_penjurusan = $request->id_penjurusan;
        $id_kelas = $request->id_kelas;
        $id_jenjang = $request->id_jenjang;
        $kategoriTo =  DB::table('to_kategori');
        $kategoriToDetail = null;
        $id_to_kategori = null;
        if ($request->kategori == 'UTBK') {
            $kategoriToDetail = $kategoriTo->where('kategori', $request->kategori)
                ->where('kelompok', $request->kelompok_soal)->first();
        } else {
            $penjurusanDetail = Penjurusan::with(['kelas.jenjang'])->where('id', $id_penjurusan)->first();
            if ($penjurusanDetail && empty($id_jenjang)) {
                $id_jenjang = $penjurusanDetail->kelas && $penjurusanDetail->kelas->jenjang ? $penjurusanDetail->kelas->jenjang->id : null;
            }

            if ($request->kategori == 'ASPD') {
                $kategoriToDetail = $kategoriTo->where('kategori', $request->kategori)
                    ->where('id_jenjang', $id_jenjang)->first();
            } else if ($request->kategori == 'PAS' || $request->kategori == 'PAT') {
                if ($penjurusanDetail && $penjurusanDetail->nama_penjurusan != '-' && $penjurusanDetail->nama_penjurusan != null) {
                    $kategoriToDetail = $kategoriTo->where('kategori', $request->kategori)
                        ->where('id_jenjang', $id_jenjang)
                        ->where('penjurusan', $penjurusanDetail->nama_penjurusan)
                        ->first();
                } else {
                    $kategoriToDetail = $kategoriTo->where('kategori', $request->kategori)
                        ->where('id_jenjang', $id_jenjang)
                        ->first();
                }
            }

            if ($penjurusanDetail && $kategoriToDetail && empty($id_kelas)) {
                $id_kelas = $penjurusanDetail->kelas ? $penjurusanDetail->kelas->id : null;
            }
        }
        if ($kategoriToDetail) {
            $id_to_kategori = $kategoriToDetail->id;
        }

        try {

            $update->judul                 = $request->judul;
            $update->kategori              = $request->kategori;
            $update->deskripsi             = $request->deskripsi;
            $update->jenis_soal            = $request->jenis_soal;
            $update->kelompok_soal         = $request->kelompok_soal;
            $update->template_soal         = $request->template_soal;
            $update->id_penjurusan         = $id_penjurusan;
            // $update->panduan_pengerjaan    = $request->panduan_pengerjaan;
            $update->jeda_waktu    = $request->jeda_waktu;
            $update->alokasi_waktu    = $request->alokasi_waktu;
            $update->id_jenjang    = $id_jenjang;
            $update->id_to_kategori    = $id_to_kategori;

            $update->save();

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $update], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    public function updatePanduan(Request $request, $id)
    {
        $update = Tryout::findOrFail($id);

        $validate = Validator::make($request->all(), [
            // 'judul'         => 'required|unique:tryouts,judul,' . $id . ',id,deleted_at,NULL',
            // 'kategori'      => 'required',
            'panduan_pengerjaan'     => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            // $update->judul                 = $request->judul;
            // $update->kategori              = $request->kategori;
            // $update->deskripsi             = $request->deskripsi;
            // $update->jenis_soal            = $request->jenis_soal;
            // $update->kelompok_soal         = $request->kelompok_soal;
            // $update->template_soal         = $request->template_soal;
            // $update->id_penjurusan         = $request->id_penjurusan;
            $update->panduan_pengerjaan    = $request->panduan_pengerjaan;
            // $update->jeda_waktu    = $request->jeda_waktu;
            // $update->alokasi_waktu    = $request->alokasi_waktu;
            // $update->id_jenjang    = $request->id_jenjang;

            $update->save();

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $update], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tryout  $tryout
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tryout = Tryout::findOrFail($id);

        $tryout->soal()->with(['pertanyaan' => function ($query) {
            return $query->delete();
        }])->get();

        $tryout->soal()->delete();

        $delete = $tryout->delete();

        return response()->json([
            'success' => true,
            'messages' => 'data berhasil di hapus',
            'data' => $delete,
        ]);
    }

    public function nomor_soal(Request $request, $id_tryout)
    {
        $tryout = Tryout::with([
            'soal', 'soal.pertanyaan' => function ($query) {
                return $query->where('soal_pertanyaans.parent_soal_pertanyaan', '=', null)->get();
            }, 'soal.pertanyaan.pertanyaan_child',
            'soal.mapel:id,nama_mapel'
        ])->findOrFail($id_tryout);

        $list_soal = $tryout->soal;

        $index = 0;
        $nomor = 1;
        $data_nomor = [];
        $data_jawaban_placeholder = [];
        $data_temp_jawaban_user = [];
        $last_saved_data_time = null;

        $cekTryoutUser = TryoutUser::where('id_tryout', $id_tryout)
            ->where('id_user', auth()->user()->id)->orderBy('created_at', 'DESC')->first();

        if ($cekTryoutUser !== null && $cekTryoutUser->temp_jawaban_user !== null) {
            $data_temp_jawaban_user = json_decode($cekTryoutUser->temp_jawaban_user, true);
            $last_saved_data_time = $cekTryoutUser->updated_at;
        }

        foreach ($list_soal as $key => $soal) {
            $pertanyaan_soal = $soal->pertanyaan;

            $nama_mapel = $soal->mapel->nama_mapel;
            $id_soal_tryout = $soal->id;

            $nomor_awal = $nomor;
            $jumlah_soal = SoalPertanyaan::where('id_soal_tryout', $id_soal_tryout)->count();
            $nomor_akhir = $nomor_awal + ($jumlah_soal - 1);
            $range_nomor = $nomor_awal . " - " . $nomor_akhir;

            foreach ($pertanyaan_soal as $keyp => $pertanyaan) {

                $pertanyaan_child = $pertanyaan->pertanyaan_child;

                $data_nomor[$index]['nomor'] = $nomor;
                $data_nomor[$index]['id_tryout'] = $soal->id_tryout;
                $data_nomor[$index]['id_soal_tryout'] = $pertanyaan->id_soal_tryout;
                $data_nomor[$index]['id_soal_pertanyaan'] = $pertanyaan->id;
                $data_nomor[$index]['induk_pertanyaan'] = $pertanyaan->parent_soal_pertanyaan;
                $data_nomor[$index]['nama_mapel'] = $nama_mapel;
                $data_nomor[$index]['bab'] = $pertanyaan->bab_mapel;
                $data_nomor[$index]['range_nomor'] = $range_nomor;
                $data_nomor[$index]['jumlah_soal'] = $jumlah_soal;
                $data_nomor[$index]['penjelasan_pertanyaan'] = $pertanyaan->penjelasan_pertanyaan;
                $data_nomor[$index]['soal_pertanyaan'] = $pertanyaan->soal;
                $data_nomor[$index]['opsi_pertanyaan'] = $pertanyaan->opsi_pertanyaan;
                $data_nomor[$index]['pembahasan_pertanyaan'] = $pertanyaan->pembahasan_pertanyaan;
                // $data_nomor[$index]['jawaban_user'] = ""; // untuk placeholder aja

                // untuk placeholder aja
                $data_jawaban_placeholder[$nomor]['id_soal_tryout'] = $pertanyaan->id_soal_tryout;
                $data_jawaban_placeholder[$nomor]['id_soal_pertanyaan'] = $pertanyaan->id;

                if (isset($data_temp_jawaban_user[$nomor]) && isset($data_temp_jawaban_user[$nomor]['jawaban_user'])) {
                    $data_jawaban_placeholder[$nomor]['jawaban_user'] = $data_temp_jawaban_user[$nomor]['jawaban_user'];
                } else {
                    $data_jawaban_placeholder[$nomor]['jawaban_user'] = null;
                }
                // $data_jawaban_placeholder[$nomor]['jawaban_user'] = null;

                $index++;
                $nomor++;

                foreach ($pertanyaan_child as $keypc => $child) {

                    $data_nomor[$index]['nomor'] = $nomor;
                    $data_nomor[$index]['id_tryout'] = $soal->id_tryout;
                    $data_nomor[$index]['id_soal_tryout'] = $child->id_soal_tryout;
                    $data_nomor[$index]['id_soal_pertanyaan'] = $child->id;
                    $data_nomor[$index]['induk_pertanyaan'] = $child->parent_soal_pertanyaan;
                    $data_nomor[$index]['nama_mapel'] = $soal->mapel->nama_mapel;
                    $data_nomor[$index]['bab'] = $child->bab_mapel;
                    $data_nomor[$index]['range_nomor'] = $range_nomor;
                    $data_nomor[$index]['jumlah_soal'] = $jumlah_soal;
                    $data_nomor[$index]['penjelasan_pertanyaan'] = $pertanyaan->penjelasan_pertanyaan;
                    $data_nomor[$index]['soal_pertanyaan'] = $child->soal;
                    $data_nomor[$index]['opsi_pertanyaan'] = $child->opsi_pertanyaan;
                    $data_nomor[$index]['pembahasan_pertanyaan'] = $child->pembahasan_pertanyaan;
                    // $data_nomor[$index]['jawaban_user'] = ""; // untuk placeholder aja

                    // untuk placeholder aja
                    $data_jawaban_placeholder[$nomor]['id_soal_tryout'] = $child->id_soal_tryout;
                    $data_jawaban_placeholder[$nomor]['id_soal_pertanyaan'] = $child->id;

                    if (isset($data_temp_jawaban_user[$nomor]) && isset($data_temp_jawaban_user[$nomor]['jawaban_user'])) {
                        $data_jawaban_placeholder[$nomor]['jawaban_user'] = $data_temp_jawaban_user[$nomor]['jawaban_user'];
                    } else {
                        $data_jawaban_placeholder[$nomor]['jawaban_user'] = null;
                    }

                    $nomor++;
                    $index++;
                }
            }
        }

        return response()->json([
            'success' => true,
            'data' => [
                'soal' => $data_nomor,
                'jawaban_placeholder' => $data_jawaban_placeholder,
                'last_saved_data_time' => $last_saved_data_time
            ]
        ]);
    }
}
