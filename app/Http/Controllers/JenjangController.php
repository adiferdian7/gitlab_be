<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\Jenjang;

class JenjangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->paginate ?  $request->paginate : 10;
        $data = Jenjang::select('id','nama_jenjang')->where('nama_jenjang', 'like', '%' .$request->q. '%')->paginate($limit);
        return response()->json(['success' => true, 'data' => $data]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'nama_jenjang'   => 'required|unique:jenjangs,nama_jenjang,NULL,id,deleted_at,NULL',
        ]);

        if($validate->fails()) {
            return response()->json([
                'success' => false, 
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $jenjang = Jenjang::create([
                'nama_jenjang' => $request->nama_jenjang, 
            ]);

            DB::commit();

             return response()->json(['success' => true, 'message' => 'data berhasil di inputkan', 'data' => $jenjang], 201);

        } catch (\Exception $e) {
             DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Jenjang::findOrFail($id);

        return response()->json([
            'success' => true, 
            'data' => $data
        ]);
    }

  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $jenjang =  Jenjang::findOrFail($id);

        $validate = Validator::make($request->all(), [
            'nama_jenjang'   => 'required|unique:jenjangs,nama_jenjang,'.$id.',id,deleted_at,NULL',
        ]);

        if($validate->fails()) {
            return response()->json([
                'success' => false, 
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $jenjang->nama_jenjang = $request->nama_jenjang;
            $jenjang->save();

            DB::commit();

             return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $jenjang], 201);

        } catch (\Exception $e) {
             DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $delete  = Jenjang::destroy($id);
        return response()->json([
            'success' => true,
            'messages' => 'data berhasil di hapus',
            'data' => $id,
        ]);
    }
}
