<?php

namespace App\Http\Controllers;

use App\Models\ProdukPaket;
use App\Models\TryoutUser;
use App\Models\TryoutUserJawaban;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class TryoutUserJawabanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'id_tryout_user'   => 'required',
            'id_soal_tryout'   => 'required',
            'id_soal_pertanyaan'   => 'required',
            'jawaban_user'   => 'required'
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $insert = TryoutUserJawaban::create([
                'id_tryout_user'     => $request->id_tryout_user,
                'id_soal_tryout'     => $request->id_soal_tryout,
                'id_soal_pertanyaan'     => $request->id_soal_pertanyaan,
                'jawaban_user'     => $request->jawaban_user
            ]);

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di inputkan', 'data' => $insert], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TryoutUserJawaban  $tryoutUserJawaban
     * @return \Illuminate\Http\Response
     */
    public function show(TryoutUserJawaban $tryoutUserJawaban)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TryoutUserJawaban  $tryoutUserJawaban
     * @return \Illuminate\Http\Response
     */
    public function edit(TryoutUserJawaban $tryoutUserJawaban)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TryoutUserJawaban  $tryoutUserJawaban
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TryoutUserJawaban $tryoutUserJawaban)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TryoutUserJawaban  $tryoutUserJawaban
     * @return \Illuminate\Http\Response
     */
    public function destroy(TryoutUserJawaban $tryoutUserJawaban)
    {
        //
    }

    public function insert_multiple(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'id_tryout_user'   => 'required|exists:tryout_users,id',
            'jawabans'   => 'required'
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        // return response()->json($request->jawabans);

        DB::beginTransaction();

        try {

            $insertData = [];
            $jawabanArray = array_values($request->jawabans);
            foreach ($jawabanArray as $key => $value) {
                // $insert = TryoutUserJawaban::create([]);
                $cek = TryoutUserJawaban::where('id_tryout_user', $request->id_tryout_user)
                    ->where('id_soal_tryout', $value['id_soal_tryout'])
                    ->where('id_soal_pertanyaan', $value['id_soal_pertanyaan'])
                    ->count();

                if ($cek < 1) {
                    $insertData[$key] = [
                        'id_tryout_user'     => $request->id_tryout_user,
                        'id_soal_tryout'     => $value['id_soal_tryout'],
                        'id_soal_pertanyaan'     => $value['id_soal_pertanyaan'],
                        'jawaban_user'     => isset($value['jawaban_user']) && !empty($value['jawaban_user']) ? $value['jawaban_user'] : null
                    ];
                }
            }

            // return response()->json($insertData);

            if(!empty($insertData)) {
                TryoutUserJawaban::insert($insertData);
            }

            $cek = TryoutUserJawaban::where('id_tryout_user', $request->id_tryout_user);            
            $list = $cek->get();

            $tryout_user = TryoutUser::where('id', $request->id_tryout_user)->first();
            if (!empty($request->waktu_selesai_ujian)) {
                // $waktu_selesai = Carbon::parse($request->waktu_selesai_ujian);
                $waktu_selesai = isset($request->waktu_selesai_ujian) && !empty($request->waktu_selesai_ujian) ? $request->waktu_selesai_ujian : date("Y-m-d H:i:s");
            } else {
                $waktu_selesai = date("Y-m-d H:i:s");
            }
            $update = $tryout_user->update(['waktu_selesai' => $waktu_selesai, 'temp_jawaban_user' => null]);

            $tryout_user = $tryout_user->refresh();
            $id_produk = $tryout_user->id_produk;
            $tryout_user = TryoutUser::where('id_produk', $id_produk)
            ->where('referensi', $tryout_user->referensi)
            ->get()->toArray();

            $produkPaket = ProdukPaket::with(['tryout'])->where('id_produk', $id_produk)->get();
            $list_tryout = array_map(function ($item) {
                return $item['tryout'];
            }, $produkPaket->toArray());

            $tryout_user_selesai = array_filter($tryout_user, function ($item) {
                return $item['waktu_selesai'] !== null;
            });

            $is_task_done = false;
            if (count($tryout_user_selesai) == count($list_tryout)) {
                $is_task_done = true;
            }

            DB::commit();

            return response()->json([
                'success' => true, 'message' => 'data berhasil di inputkan masal',
                'data' => ['list_jawaban' => $list, 'is_task_done' => $is_task_done]
            ], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }
}
