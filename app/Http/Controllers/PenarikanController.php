<?php

namespace App\Http\Controllers;

use App\Models\Penarikan;
use App\Models\Pendapatan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;

class PenarikanController extends Controller
{
    function __construct()
    {
        $this->penarikanModel = new Penarikan();
        $this->pendapatanModel = new Pendapatan();
    }

    public function createPenarikan(Request $request)
    {
        $reqData = $request->all();
        if ($this->user()->role_user == 'teacher') {
            $reqData['id_tentor'] = $this->userId();
        } else {
            return responseFail(['role_user' => ['Forbidden action!']], 422);
        }

        $validate = Validator::make($reqData, [
            'id_tentor'   => 'required|exists:teachers,id_teacher,deleted_at,NULL',
            'nominal_penarikan' => 'required|integer',
            'nama_rekening' => 'required|string',
            'bank_rekening' => 'required|string',
            'bank_rekening' => 'required|string',
            'nomor_rekening' => 'required|string',
            'password' => 'required',
        ], $this->penarikanModel->getValidationMessage());

        if ($validate->fails()) {
            return responseFail($validate->messages(), 422);
        }

        // cek saldo
        if ($this->__cek_saldo() < $reqData['nominal_penarikan']) {
            return responseFail(['saldo' => ['Saldo tidak mencukupi!']], 422);
        }

        if (Hash::check($reqData['password'], $this->user()->getAuthPassword()) == false) {
            return responseFail(['password' => ['Password tidak cocok!']], 422);
        }

        $reqData['kode'] = "PN-UJJ" . date("Ymd") . "-" . date("His");
        $result = $this->penarikanModel->insertData('insertData', $reqData);

        if (isset($result['error'])) {
            return responseFail($result, 500);
        } else {
            return responseInsert($result);
        }
    }

    private function __cek_saldo()
    {
        $dataPendapatan = $this->pendapatanModel->getData('dataByTentor', ['id_tentor' => $this->userId()], 'dataByTentor');
        $dataPenarikan = $this->penarikanModel->getData('dataByTentor', ['id_tentor' => $this->userId(), 'status_penarikan' => ''], 'dataByTentor');

        $totalPendapatan = 0;
        foreach ($dataPendapatan as $dataPendapatanRow) {
            $totalPendapatan += $dataPendapatanRow['netto_pendapatan'];
        }

        $totalPenarikan = 0;
        foreach ($dataPenarikan as $dataPenarikanRow) {
            $totalPenarikan += $dataPenarikanRow['nominal_penarikan'];
        }

        $totalSaldo = $totalPendapatan - $totalPenarikan;

        return $totalSaldo;
    }

    public function historyPenarikan(Request $request)
    {
        $reqData = $request->all();
        if ($this->user()->role_user == 'teacher') {
            $reqData['id_tentor'] = $this->userId();
        } else if ($this->user()->role_user == 'siswa') {
            return responseFail(['role_user' => ['Forbidden action!']], 422);
        }

        $data = $this->penarikanModel->getData('paginationData', $reqData, 'paginationData');

        return responseData($data);
    }

    public function prosesPenarikan(Request $request, $id)
    {
        if (!in_array($this->user()->role_user, ['superAdmin', 'superadmin', 'admin'])) {
            return responseFail('Unauthorized', 403);
        }

        $result = $this->penarikanModel->updateData('updateStatus', ['status_penarikan' => 'Proses', 'id' => $id]);

        if (isset($result['error'])) {
            return responseFail($result);
        }
        return responseUpdate($result, 200, 'Penarikan diproses!');
    }

    public function kirimDanaPenarikan(Request $request, $id)
    {
        if (!in_array($this->user()->role_user, ['superAdmin', 'superadmin', 'admin'])) {
            return responseFail('Unauthorized', 403);
        }

        $result = $this->penarikanModel->updateData(
            'updateData',
            [
                'status_penarikan' => 'Selesai',
                'bukti_transfer' => $request->bukti_transfer,
                'id' => $id
            ]
        );

        if (isset($result['error'])) {
            return responseFail($result);
        }

        return responseUpdate($result, 200, 'Dana dikirim dan penarikan selesai!');
    }
}
