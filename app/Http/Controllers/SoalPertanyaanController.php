<?php

namespace App\Http\Controllers;

use App\Models\SoalPertanyaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class SoalPertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {
        $limit = $request->paginate ?  $request->paginate : 10;
        $data = SoalPertanyaan::select(
            'id',
            'id_soal_tryout',
            'bab_mapel',
            'penjelasan_pertanyaan',
            'template_pertanyaan',
            'soal',
            'opsi_pertanyaan',
            'pembahasan_pertanyaan',
            'jawaban_pertanyaan',
            'parent_soal_pertanyaan'
        )
            ->where('bab_mapel', 'like', '%' . $request->q . '%')
            ->paginate($limit);

        return response()->json([
            'success' => true,
            'messages' => 'berhasil mengambil data',
            'data' => $data,
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'id_soal_tryout'        => 'required',
            'bab_mapel'             => 'required',
            'template_pertanyaan'   => 'required',
            'soal'                  => 'required',
            // 'pembahasan_pertanyaan' => 'required',

        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }


        $data = [
            'id_soal_tryout'        => $request->id_soal_tryout,
            'bab_mapel'             => $request->bab_mapel,
            'penjelasan_pertanyaan' => $request->penjelasan_pertanyaan,
            'template_pertanyaan'   => $request->template_pertanyaan,
            'soal'                  => $request->soal,
            'gambar'                => $request->gambar,
            'opsi_pertanyaan'       => $request->opsi_pertanyaan,
            'pembahasan_pertanyaan' => $request->pembahasan_pertanyaan,
            'parent_soal_pertanyaan' => $request->parent_soal_pertanyaan,
            'jawaban_pertanyaan'    => $request->jawaban_pertanyaan,
        ];



        DB::beginTransaction();

        try {


            $insert = SoalPertanyaan::create($data);

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di inputkan', 'data' => $insert], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tryout  $tryout
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = SoalPertanyaan::findOrFail($id);

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tryout  $tryout
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = SoalPertanyaan::findOrFail($id);

        $validate = Validator::make($request->all(), [
            'id_soal_tryout'        => 'required',
            'bab_mapel'             => 'required',
            'template_pertanyaan'   => 'required',
            'soal'                  => 'required',
            // 'pembahasan_pertanyaan' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $update->id_soal_tryout = $request->id_soal_tryout;
            $update->bab_mapel              = $request->bab_mapel;
            $update->penjelasan_pertanyaan  = $request->penjelasan_pertanyaan;
            $update->template_pertanyaan    = $request->template_pertanyaan;
            $update->soal                   = $request->soal;
            $update->gambar                 = $request->gambar;
            $update->opsi_pertanyaan        = $request->opsi_pertanyaan;
            $update->pembahasan_pertanyaan  = $request->pembahasan_pertanyaan;
            $update->parent_soal_pertanyaan = $request->parent_soal_pertanyaan;
            $update->jawaban_pertanyaan     = $request->jawaban_pertanyaan;

            $update->save();

            if ($request->has('parent_soal_pertanyaan') && empty($request->parent_soal_pertanyaan)) {
                SoalPertanyaan::where('parent_soal_pertanyaan', $id)->update([
                    'bab_mapel' => $request->bab_mapel,
                    'penjelasan_pertanyaan' => $request->penjelasan_pertanyaan
                ]);
            }

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $update], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    public function updateBabMapel(Request $request, $id)
    {
        $update = SoalPertanyaan::findOrFail($id);

        $validate = Validator::make($request->all(), [
            // 'id_soal_tryout'        => 'required',
            'bab_mapel'             => 'required',
            // 'template_pertanyaan'   => 'required',
            // 'soal'                  => 'required',
            // 'pembahasan_pertanyaan' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            // $update->id_soal_tryout = $request->id_soal_tryout;
            $update->bab_mapel              = $request->bab_mapel;
            // $update->penjelasan_pertanyaan  = $request->penjelasan_pertanyaan;
            // $update->template_pertanyaan    = $request->template_pertanyaan;
            // $update->soal                   = $request->soal;
            // $update->gambar                 = $request->gambar;
            // $update->opsi_pertanyaan        = $request->opsi_pertanyaan;
            // $update->pembahasan_pertanyaan  = $request->pembahasan_pertanyaan;
            // $update->parent_soal_pertanyaan = $request->parent_soal_pertanyaan;
            // $update->jawaban_pertanyaan     = $request->jawaban_pertanyaan;

            $update->save();

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $update], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tryout  $tryout
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete  = SoalPertanyaan::destroy($id);
        return response()->json([
            'success' => true,
            'messages' => 'data berhasil di hapus',
            'data' => $id,
        ]);
    }
}
