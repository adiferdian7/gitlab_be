<?php

namespace App\Http\Controllers;

use App\Mail\TestMail;
use App\Models\Pengaduan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;
use Validator;

class PengaduanController extends Controller
{
  function __construct()
  {
    $this->pengaduanModal = new Pengaduan();
  }

  public function index(Request $request)
  {
    $data = $this->pengaduanModal->getData('paginationData', $request->all(), 'paginationData');

    return responseData($data);
  }

  public function detail($id)
  {
    $data = $this->pengaduanModal->getData('detailData', ['id' => $id], '');

    return responseData($data);
  }

  public function create(Request $request)
  {
    $reqData =  $request->all();
    if (!in_array($this->user()->role_user, ['teacher', 'siswa', 'orangtua', 'parent'])) {
      return responseFail('Unauthorized.', 403);
    }

    $validator = Validator::make($reqData, [
      'subjek' => 'required',
      'permasalahan' => 'required',
    ]);

    if ($validator->fails()) {
      return responseFail($validator->messages());
    }

    $insertData = $reqData;
    $insertData['id_user'] = $reqData['id_user'] ?? $this->userId();
    $insertData['id_admin'] = NULL;
    $insertData['status'] = $reqData['buka'] ?? 'Buka';
    $insertData['prioritas'] = $reqData['prioritas'] ?? 'Sedang';
    $insertData['kode'] = time();
    $insertData['tgl_buka'] = date("Y-m-d H:i:s");
    $insertData['tgl_ditutup'] = NULL;

    $result = $this->pengaduanModal->insertData('createData', $insertData);

    if (isset($result['error'])) {
      return responseFail($result);
    }

    $kode = $this->generateCodePad($result->id, 'UJP');
    $result = $this->pengaduanModal->updateData('updateData', ['id' => $result->id, 'kode' => $kode]);
    $user = $this->user();
    $testMail = new TestMail([
      'to' => $user->email,
      'toName' => $user->username,
      'content' => 'Dear, ' . $user->username . '. Pengaduan Anda berhasil dikirimkan.'
    ]);
    
    Mail::send($testMail);

    return responseInsert($result);
  }

  public function update(Request $request, $id)
  {
    $reqData =  $request->all();
    $reqData['id'] = $id;
    $validator = Validator::make($reqData, [
      'id'   => 'required|exists:pengaduans,id,deleted_at,NULL',
      'subjek' => 'required',
      'permasalahan' => 'required',
      'status' => ['nullable', Rule::in('Buka', 'Ditutup')],
      'prioritas' => ['nullable', Rule::in('Rendah', 'Sedang', 'Tinggi')]
    ], $this->validationMessage);

    if ($validator->fails()) {
      return responseFail($validator->messages());
    }

    $result = $this->pengaduanModal->updateData('updateData', $reqData);

    if (isset($result['error'])) {
      return responseFail($result);
    }
    return responseUpdate($result, 200, 'Status berhasil diperbarui!');
  }

  public function updateStatus(Request $request, $id)
  {
    $reqData =  $request->all();
    $reqData['id'] = $id;

    $validator = Validator::make($reqData, [
      'id'   => 'required|exists:pengaduans,id,deleted_at,NULL',
      'status' => ['required', Rule::in('Buka', 'Ditutup')]
    ], $this->validationMessage);

    if ($validator->fails()) {
      return responseFail($validator->messages());
    }

    $result = $this->pengaduanModal->updateData('updateStatus', $reqData);

    if (isset($result['error'])) {
      return responseFail($result);
    }
    return responseUpdate($result, 200, 'Status berhasil diperbarui!');
  }

  public function updatePrioritas(Request $request, $id)
  {
    $reqData =  $request->all();
    $reqData['id'] = $id;

    $validator = Validator::make($reqData, [
      'id'   => 'required|exists:pengaduans,id,deleted_at,NULL',
      'prioritas' => ['required', Rule::in('Rendah', 'Sedang', 'Tinggi')]
    ], $this->validationMessage);

    if ($validator->fails()) {
      return responseFail($validator->messages());
    }

    $result = $this->pengaduanModal->updateData('updatePrioritas', $reqData);

    if (isset($result['error'])) {
      return responseFail($result);
    }
    return responseUpdate($result, 200, 'Prioritas berhasil diperbarui!');
  }

  public function assignAdmin(Request $request, $id)
  {
    $reqData =  $request->all();
    $reqData['id'] = $id;

    $validator = Validator::make($reqData, [
      'id'   => 'required|exists:pengaduans,id,deleted_at,NULL',
      'id_admin'   => 'required|exists:admins,id_admin,deleted_at,NULL',
    ], $this->validationMessage);

    if ($validator->fails()) {
      return responseFail($validator->messages());
    }

    $result = $this->pengaduanModal->updateData('assignAdmin', $reqData);

    if (isset($result['error'])) {
      return responseFail($result);
    }
    return responseUpdate($result, 200, 'Status berhasil diperbarui!');
  }

  public function delete(Request $request, $id)
  {
    $reqData =  $request->all();
    $reqData['id'] = $id;

    $validator = Validator::make($reqData, [
      'id'   => 'required|exists:pengaduans,id,deleted_at,NULL',
    ], $this->validationMessage);

    if ($validator->fails()) {
      return responseFail($validator->messages());
    }

    $result = $this->pengaduanModal->deleteData('deleteData', $reqData);

    if (isset($result['error'])) {
      return responseFail($result);
    }
    return responseDeleted($result, 200);
  }

}
