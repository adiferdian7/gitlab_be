<?php

namespace App\Http\Controllers;

use App\Models\Penarikan;
use App\Models\Pendapatan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Validator;

class PendapatanController extends Controller
{
  function __construct()
  {
    $this->pendapatanModel = new Pendapatan();
    $this->penarikanModel = new Penarikan();
  }

  function getPendapatanByTentor()
  {

    $dataPendapatan = $this->pendapatanModel->getData('dataByTentor', ['id_tentor' => $this->userId()], 'dataByTentor');
    $dataPenarikan = $this->penarikanModel->getData('dataByTentor', ['id_tentor' => $this->userId(), 'status_penarikan' => ''], 'dataByTentor');

    $totalPendapatan = 0;
    foreach ($dataPendapatan as $dataPendapatanRow) {
      $totalPendapatan += $dataPendapatanRow['netto_pendapatan'];
    }

    $totalPenarikan = 0;
    foreach ($dataPenarikan as $dataPenarikanRow) {
      $totalPenarikan += $dataPenarikanRow['nominal_penarikan'];
    }

    $totalSaldo = $totalPendapatan - $totalPenarikan;

    $data = [
      'totalPendapatan' => $this->formatRupiah($totalPendapatan),
      'totalPendapatanNum' => $totalPendapatan,
      'totalPenarikan' => $this->formatRupiah($totalPenarikan),
      'totalPenarikanNum' => $totalPenarikan,
      'totalSaldo' => $this->formatRupiah($totalSaldo),
      'totalSaldoNum' => $totalSaldo,
    ];

    return responseData($data);
  }

  function getPendapatanByTentorGrafikBulanan()
  {
    $dataPendapatan = $this->pendapatanModel->getData('dataByTentorPerBulan', ['id_tentor' => $this->userId()], '')->toArray();

    $dataPenarikan = $this->penarikanModel->getData('dataByTentorPerBulan', ['id_tentor' => $this->userId()], '')->toArray();

    $dataGrafik = [
      'pendapatan' => [],
      'penarikan' => []
    ];

    // pendapatan
    $index = 0;
    for ($bulan = 1; $bulan <= 12; $bulan++) {

      $filterPendapatan = array_values(array_filter($dataPendapatan, function ($item) use ($bulan) {
        return $item['bulan'] == $bulan;
      }));

      $filterPenarikan = array_values(array_filter($dataPenarikan, function ($item) use ($bulan) {
        return $item['bulan'] == $bulan;
      }));

      if (isset($filterPendapatan[0])) {
        $dataGrafik['pendapatan'][$index] = $filterPendapatan[0]['pendapatan'];
      } else {
        $dataGrafik['pendapatan'][$index] = 0;
      }

      if (isset($filterPenarikan[0])) {
        $dataGrafik['penarikan'][$index] = $filterPenarikan[0]['penarikan'];
      } else {
        $dataGrafik['penarikan'][$index] = 0;
      }

      $index++;
    }

    return responseData($dataGrafik);
  }

  function pendapatanPerKursusTentor()
  {
    $data = $this->pendapatanModel->getData('dataByTentorPerkursus', ['id_tentor' => $this->userId()], 'dataByTentorPerkursus');
    return responseData($data);
  }

  function pendapatanPerKursusTentorDetail($id)
  {
    $data = $this->pendapatanModel->getData('detailDataByTentorPerkursus', ['id_tentor' => $this->userId(), 'id_kursus' => $id], 'detailDataByTentorPerkursus');
    return responseData($data);
  }

  function pendapatanPerKursusTentorDetailList($id)
  {
    $data = $this->pendapatanModel->getData('dataByTentorPerkursusDetailList', ['id_tentor' => $this->userId(), 'id_kursus' => $id], 'dataByTentorPerkursusDetailList');
    return responseData($data);
  }
}
