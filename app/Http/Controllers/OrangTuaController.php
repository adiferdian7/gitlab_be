<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Student;
use App\Models\OrangTua;
use Illuminate\Support\Str;

use App\Mail\MySendMail;
use App\Mail\TestMail;
use App\Models\Notification;
use Illuminate\Support\Facades\Mail;

class OrangTuaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->paginate ?  $request->paginate : 10;
        $data = OrangTua::with(['siswa'])->paginate($limit);
        return response()->json([
            'success' => true,
            'messages' => 'berhasil mengambil data',
            'data' => $data,
        ]);
    }

    public function cek(Request $request)
    {
        $user = $request->user();
        if ($user->role_user != 'parent') {
            return response()->json(['success' => false, 'messages' => 'maaf anda bukan role parent'], 404);
        }

        $orangTua = OrangTua::find($user->id);

        if ($orangTua && $orangTua->nama_lengkap == null) {

            return response()->json(['success' => false, 'messages' => 'profil belum di lengkapi']);
        }

        return response()->json(['success' => true, 'messages' => 'profil sudah dilengkapi']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->user()->role_user == 'siswa') {

            $validate = Validator::make($request->all(), [
                'username'                  => 'required|unique:users',
                'nama_lengkap'              => 'required',
                'email'                     => 'required|unique:students|unique:users',
                'nomor_telephone'           => 'required',
                'password'                  => 'required',
            ]);
        } else {
            $validate = Validator::make($request->all(), [
                'username'                  => 'required|unique:users',
                'nama_lengkap'              => 'required',
                'email'                     => 'required|unique:students|unique:users',
                'nomor_telephone'           => 'required',
                'password'                  => 'required',
                'id_siswa'                  => 'required|unique:orang_tua,id_orang_tua',
            ]);
        }


        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }


        if ($request->user()->role_user == 'siswa') {
            $siswa = Student::findOrFail($request->user()->id);
        } else {
            $siswa = Student::findOrFail($request->id_siswa);
        }

        DB::beginTransaction();

        try {


            $insertUser = User::create([
                'username'   => $request->username,
                'password'   => Hash::make($request->password),
                'role_user'  => 'parent',
                'token'      => Str::random(40),
                'email'     => $request->email,
                'verifikasi' => 1,
            ]);


            $insert = OrangTua::create([
                'id_user'           => $insertUser->id,
                'id_orang_tua'      => $siswa->id_siswa,
                'nama_lengkap'      => $request->nama_lengkap,
                'email'             => $request->email,
                'nomor_telephone'   => $request->nomor_telephone,
            ]);

            DB::commit();

            $data = [
                'users' => $insertUser,
                'data' => $insert
            ];

            $mailData = [
                'token' => $insertUser->token,
            ];

            // Mail::to($insert->email)->send(new MySendMail($mailData));
            $notificationModel = new Notification();
            $notificationModel->insertData('insertData', [
                'recipientId' => '',
                'recipientType' => 4, // superadmin + admin
                'type' => 0, // registration
                'title' => 'Ada Pendaftar Baru!',
                'body' => 'Ada Siswa baru telah mendaftar. Yuk cek detailnya!',
                'data' => [
                    'user' => $insertUser,
                    'detail' => $insert,
                    'siswa' => $siswa
                ],
            ]);

            $notificationModel->insertData('insertData', [
                'recipientId' => $insertUser->id,
                'recipientType' => 5, // student
                'type' => 0, // registration
                'title' => 'Selamat datang!',
                'body' => "Anda telah didaftarkan sebagai orangtua oleh $siswa->nama_lengkap.",
                'data' => [
                    'user' => $insertUser,
                    'detail' => $insert,
                    'siswa' => $siswa
                ],
            ]);

            $testMail = new TestMail([
                'subject' => 'Anda telah didaftarkan sebagai Orangtua di UjiAja.com!',
                'to' => $insertUser->email,
                'toName' => $insert->nama_lengkap,
                'content' => '',
                'token' => $insertUser->token,
                'view' => 'email.content.parent__register-success',
                'data' => [
                    'siswa' => $siswa,
                ]
            ]);

            Mail::send($testMail);

            return response()->json(['success' => true, 'messages' => 'orang tua success register', 'data' => $data]);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    public function dashboard()
    {
        if ($this->user()->role_user != 'parent') {
            return responseFail('Unauthenticated.', 403);
        }

        $orangtua = OrangTua::where('id_user', $this->userId())->first();

        $total_tryout = DB::table('tryout_users')->where('id_user', $orangtua->id_orang_tua)->whereNull('deleted_at')
            ->groupBy('referensi')->get()->count();
        $total_kursus = DB::table('kursus_siswa')->where('id_siswa', $orangtua->id_orang_tua)->whereNull('deleted_at')
            ->groupBy('referensi')->get()->count();
        $total_transaksi = DB::table('transaksis')->where('id_user', $orangtua->id_orang_tua)->whereNull('deleted_at')
            ->groupBy('kode')->get()->count();

        $data = [
            'total_tryout' => $total_tryout,
            'total_kursus' => $total_kursus,
            'total_transaksi' => $total_transaksi,
        ];

        return responseData($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = OrangTua::with(['siswa'])->findOrFail($id);

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $parent = OrangTua::where('id_user', $id)->first();
        $user  =  User::findOrFail($parent->id_user);
        $validate = Validator::make($request->all(), [
            'username'                  => 'required|unique:users,username,' . $user->id,
            'nama_lengkap'              => 'required',
            'email'                     => 'required|email|unique:users,email,' . $user->id,
            'nomor_telephone'           => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $parent->nama_lengkap    = $request->nama_lengkap;
            $parent->email           = $request->email;
            $parent->nomor_telephone = $request->nomor_telephone;

            $parent->save();

            $user->username    = $request->username;
            $user->email       = $request->email;
            $user->save();

            DB::commit();

            return response()->json([
                'success' => true,
                'messages' => 'update orang tua success',
                'data' => $parent,
            ], 200);
        } catch (\Exception $e) {

            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $id_user = OrangTua::where('id_user', $id)->first();
        $delete = OrangTua::destroy($id);
        $user   = User::destroy($id_user->id_user);

        return response()->json([
            'success' => true,
            'messages' => 'data berhasil di hapus',
            'data' => $id,
        ]);
    }

    public function undo(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }


        DB::beginTransaction();

        try {
            $id_user = OrangTua::withTrashed()->findOrFail($request->id);
            OrangTua::withTrashed()->find($request->id)->restore();
            User::withTrashed()->find($id_user->id_user)->restore();

            DB::commit();

            return response()->json([
                'success' => true,
                'messages' => 'data berhasil di pulihkan',
            ]);
        } catch (\Exception $e) {

            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }
}
