<?php

namespace App\Http\Controllers;

use App\Mail\TestMail;
use App\Models\Bundling;
use App\Models\BundlingProduct;
use App\Models\Kursus;
use App\Models\KursusSiswa;
use App\Models\Notification;
use App\Models\OrangTua;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Validation\Rule;
use App\Models\Transaksi;
use Carbon\Carbon;
use App\Models\Produk;
use App\Models\TabelXendit;
use App\Models\TbMbti;
use App\Models\Teacher;
use App\Utils\Util;
use Illuminate\Support\Facades\Mail;

class TransaksiController extends Controller
{
    public function __construct()
    {
        $this->utils = new Util;
    }

    public function index(Request $request)
    {
        $limit = $request->paginate ?  $request->paginate : 10;
        $q = $request->q ?? "";

        $query = Transaksi::with([
            'user:id,email,role_user',
            'user.siswa:id_siswa,nama_lengkap',
            'user.guru:id_teacher,nama_lengkap',
            'produk:id,nama_produk',
            'bank:id,nama_bank',
            'bundling:id,name'
            // 'detail_xendit:id,id_transaksi,response'
        ])
            // ->where('status', 'like', '%' . $q . '%')
            // ->orWhere('harga_produk', 'like', '%' . $q . '%')
            // ->orWhere('total_harga', 'like', '%' . $q . '%')
            // ->orWhereHas('user.siswa', function ($query) use ($q) {
            //     $query->where('nama_lengkap', 'like', '%' . $q . '%');
            // })
            // ->orWhereHas('user.guru', function ($query) use ($q) {
            //     $query->where('nama_lengkap', 'like', '%' . $q . '%');
            // })
            // ->orWhereHas('produk', function ($query) use ($q) {
            //     $query->where('nama_produk', 'like', '%' . $q . '%');
            // })
            ->where(
                function ($query) {
                    $query->where('jenis_transaksi', '!=', 'Bonus MBTI')
                        ->where('tipe', '!=', 'Free Claim');
                }
            )
            ->where(
                function ($query) use ($q) {
                    $query->where('status', 'like', '%' . $q . '%')
                        ->orWhere('harga_produk', 'like', '%' . $q . '%')
                        ->orWhere('total_harga', 'like', '%' . $q . '%')
                        ->orWhereHas('user.siswa', function ($query) use ($q) {
                            $query->where('nama_lengkap', 'like', '%' . $q . '%');
                        })
                        ->orWhereHas('user.guru', function ($query) use ($q) {
                            $query->where('nama_lengkap', 'like', '%' . $q . '%');
                        })
                        ->orWhereHas('produk', function ($query) use ($q) {
                            $query->where('nama_produk', 'like', '%' . $q . '%');
                        });
                }
            )
            ->where(
                function ($query) {
                    $query->where('total_harga', '>', 1);
                }
            )
            ->select('id', 'id_produk', 'id_bank', 'id_user', 'status', 'tanggal_transaksi', 'tipe', 'kode', 'total_harga');

        $sort_array = ['id', 'tanggal_transaksi', 'kode', 'status', 'harga_produk', 'total_harga'];
        if ($request->sort_by && !empty($request->sort_by) && in_array($request->sort_by, $sort_array)) {
            $query->orderBy($request->sort_by);
        } else {
            $query->orderBy('tanggal_transaksi', 'desc');
        }

        $data = $query->paginate($limit);
        return response()->json(['success' => true, 'message' => 'User data', 'data' => $data]);
    }

    public function mine(Request $request)
    {
        $limit = $request->paginate ?  $request->paginate : 10;
        $q = $request->q ?? "";

        if (isset($request->id_parent) && !empty($request->id_parent)) {
            $orangtua = OrangTua::where('id_user', $request->id_parent)->first();
            if ($orangtua) {
                $id_user =  $orangtua->id_orang_tua;
            }
        } else {
            $id_user = auth()->user()->id;
        }

        $query = Transaksi::with([
            'user:id,email,role_user',
            'user.siswa:id_siswa,nama_lengkap',
            'user.guru:id_teacher,nama_lengkap',
            'produk:id,nama_produk',
            'bank:id,nama_bank',
            'bundling:id,name'
            // 'detail_xendit:id,id_transaksi,response'
        ])
            ->where(
                function ($query) use ($q, $id_user) {
                    $query->where('id_user', $id_user)
                        ->where('jenis_transaksi', '!=', 'Bonus MBTI')
                        ->where('tipe', '!=', 'Free Claim');
                }
            )
            ->where(
                function ($query) use ($q) {
                    $query->where('status', 'like', '%' . $q . '%')
                        ->orWhere('harga_produk', 'like', '%' . $q . '%')
                        ->orWhere('total_harga', 'like', '%' . $q . '%')
                        ->orWhereHas('user.siswa', function ($query) use ($q) {
                            $query->where('nama_lengkap', 'like', '%' . $q . '%');
                        })
                        ->orWhereHas('user.guru', function ($query) use ($q) {
                            $query->where('nama_lengkap', 'like', '%' . $q . '%');
                        })
                        ->orWhereHas('produk', function ($query) use ($q) {
                            $query->where('nama_produk', 'like', '%' . $q . '%');
                        });
                }
            )
            ->where(
                function ($query) {
                    $query->where('total_harga', '>', 1);
                }
            )
            ->select('id', 'id_produk', 'id_bank', 'id_user', 'status', 'tanggal_transaksi', 'tipe', 'kode', 'total_harga', 'jenis_transaksi');

        $sort_array = ['id', 'tanggal_transaksi', 'kode', 'status', 'harga_produk', 'total_harga'];
        if ($request->sort_by && !empty($request->sort_by) && in_array($request->sort_by, $sort_array)) {
            $query->orderBy($request->sort_by);
        } else {
            $query->orderBy('tanggal_transaksi', 'desc');
        }

        $data = $query->paginate($limit);
        return response()->json(['success' => true, 'message' => 'Transaction user\'s data', 'data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validate = Validator::make($request->all(), [
            // 'id_user'   		=> 'required',
            'id_produk'           => 'required',
            // 'tanggal_transaksi' => 'required',
            'tipe'                => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        $user = auth()->user();
        $userDetail = $this->utils->get_user($user->role_user, $user->id);

        $produk = Produk::find($request->id_produk);

        $prefix = "TR";
        $nama_produk = "";
        $harga_produk = 10000;
        $produk_tryout_ids = null;
        if (!empty($request->jenis_transaksi)) {
            if ($request->jenis_transaksi == 'Tryout') {
                $prefix = "TR";
                $produk = Produk::find($request->id_produk);
                $nama_produk = $produk->nama_produk;
                $harga_produk = $produk->harga_produk;
            } elseif ($request->jenis_transaksi == 'MBTI') {
                $prefix = "MB";
                $produk = TbMbti::find($request->id_produk);
                $nama_produk = $produk->judul;
                $harga_produk = $produk->harga;
            } elseif ($request->jenis_transaksi == 'Les Privat') {
                $prefix = "LP";
                $produk = Kursus::find($request->id_produk);
                $nama_produk = $produk->nama_kursus;
                $harga_produk = $produk->harga_kursus;
            } elseif ($request->jenis_transaksi == 'UKTT') {
                $prefix = "UK";
                $produk = Produk::find($request->id_produk);
                $nama_produk = $produk->nama_produk;
                $harga_produk = $produk->harga_produk;
            } elseif ($request->jenis_transaksi == 'Bundling') {
                $prefix = "B";
                $produk = Bundling::find($request->id_produk);
                $nama_produk = $produk->name;
                $harga_produk = $produk->price;
                $produk_tryout_ids = BundlingProduct::where('bundling_id', $produk->id)->pluck('product_id');
            }
        } else {
            $prefix = "TR";
            $produk = Produk::find($request->id_produk);
            $nama_produk = $produk->nama_produk;
        }

        // print_r($produk_tryout_ids);
        // die;

        // $kode_transaksi = $prefix . "-UJJ" . date("Ymd") . "-" . date("His");
        $kode_transaksi = $this->generateCodePad(1, $prefix . date("Ym"));
        // $tanggal_transaksi = $request->tanggal_transaksi ? Carbon::parse($request->tanggal_transaksi) : date("Y-m-d H:i:s");
        $tanggal_transaksi = date("Y-m-d H:i:s");

        if ($request->tipe == 'Pihak Ketiga') {

            DB::beginTransaction();

            try {

                $params = [
                    'external_id' => 'xend_' . time(),
                    // 'payer_email' => 'sample_email@xendit.co',
                    'payer_email' => $user->email,
                    'description' => 'Beli ' . $nama_produk,
                    'amount' => $request->total_harga  ?? $harga_produk
                ];

                $invoice = $this->utils->invoice($params);

                $insert = Transaksi::create([
                    'id_user'               => $request->id_user ?? $user->id,
                    'id_produk'             => $request->id_produk,
                    'tanggal_transaksi' =>  $tanggal_transaksi,
                    'tipe'                  => $request->tipe,
                    'harga_produk' => $request->harga_produk ?? $harga_produk,
                    'batas_pembayaran'        => date("Y-m-d H:i:s", strtotime($tanggal_transaksi . " + 1 days")),
                    'biaya_adm' => $request->biaya_adm ?? 0,
                    'total_harga' => $request->total_harga ?? $harga_produk,
                    'kode' => $kode_transaksi,
                    'jenis_transaksi' => $request->jenis_transaksi ?? 'Tryout',
                ]);

                $kode_transaksi = $this->generateCodePad($insert->id, $prefix . date("Ym"));
                Transaksi::find($insert->id)->update(['kode' =>  $kode_transaksi]);

                $insertData = Transaksi::find($insert->id);
                if ($produk->bonus !== NULL) {
                    $this->_create_transaction_bonus_mbti($insertData);
                }

                if (!empty($produk_tryout_ids) && $request->jenis_transaksi == 'Bundling') {
                    $insertProductBundleData = [];
                    foreach ($produk_tryout_ids as $key => $produk_tryout_id) {
                        if (Transaksi::where('id_user', $insertData->id_user)->where('id_produk', $produk_tryout_id)->count() > 0) {
                            continue;
                        }
                        $insertProductBundleData[] = [
                            'id_user'                => $insertData->id_user,
                            'id_bank'               => $insertData->id_bank,
                            'id_produk'                => $produk_tryout_id,
                            'tanggal_transaksi'        =>  $insertData->tanggal_transaksi,
                            'batas_pembayaran'        => $insertData->batas_pembayaran,
                            'tipe'                    => $insertData->tipe,
                            'harga_produk' => 1,
                            'jenis_transaksi' => 'Bundling - Product',
                            'kode_unik' => 1,
                            'total_harga' => 1,
                            'kode' => $insertData->kode,
                            'status' => $insertData->status,
                            'created_at' => date('Y-m-d H:i:s'),
                        ];
                    }
                    Transaksi::insert($insertProductBundleData);
                }

                $insertXendit = TabelXendit::create([
                    'id_transaksi' => $insert->id,
                    'reference_id' => $invoice['id'],
                    'response' => json_encode($invoice),
                ]);

                $data = [
                    'id' => $insert->id,
                    'transaksi' => $insert,
                    'xendit'    => $invoice,
                ];

                DB::commit();

                // Xendit

                $notificationModel = new Notification();
                $notificationModel->insertData('insertData', [
                    'recipientType' => 4, // superadmin + admin
                    'type' => 1, // transaction
                    'title' => 'Ada Transaksi Baru!',
                    'body' => "Transaksi dengan kode <b>#$kode_transaksi</b> telah masuk. Yuk cek detailnya dan lakukan verifikasi!",
                    'data' => $insert
                ]);

                $notificationModel->insertData('insertData', [
                    'recipientId' => $user->id,
                    'recipientType' => $user->role_user == 'teacher' ? 2 : 3, // teacher or student
                    'type' => 1, // transaction
                    'title' => 'Transaksi berhasil dibuat!',
                    'body' => "Transaksi dengan kode <b>#$kode_transaksi</b> berhasil dibuat. Yuk, segera lakukan pembayaran!",
                    'data' => $insert
                ]);

                $testMail = new TestMail([
                    'subject' => "[$kode_transaksi] - Pembayaran berhasil dibuat!",
                    'to' => $user->email,
                    'toName' => !empty($userDetail) ? $userDetail->nama_lengkap : $user->username,
                    'content' => [
                        'title' => "Transaksi kamu untuk membeli $insert->jenis_transaksi telah dibuat!",
                        'body' => 'Berikut ini adalah detail dari transaksi yang perlu segera kamu selesaikan:',
                        'body2' => 'Jika kamu sudah melakukan pembayaran harap abaikan email ini.',
                        'frontUrl' => $request->front_url ?? env('WEB_URL') . '/payment/' . $insert->id . '/detail',
                        'paymentMethod' => 'Xendit'
                    ],
                    'data' => $this->__get_detail($insert->id),
                    'view' => 'email.content.customer__payment-created'
                ]);

                Mail::send($testMail);

                return response()->json(['success' => true, 'message' => 'data berhasil di inputkan', 'data' => $data], 201);
            } catch (\Exception $e) {
                DB::rollback();

                return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
            }
        } elseif ($request->tipe == 'Bank Transfer' || $request->tipe == 'Free Claim') {

            DB::beginTransaction();

            try {

                $insert = Transaksi::create([
                    'id_user'                => $request->id_user ?? $user->id,
                    'id_bank'               => $request->id_bank,
                    'id_produk'                => $request->id_produk,
                    'tanggal_transaksi'        =>  $tanggal_transaksi,
                    'batas_pembayaran'        => date("Y-m-d H:i:s", strtotime($tanggal_transaksi . " + 1 days")),
                    'tipe'                    => $request->tipe,
                    'harga_produk' => $request->harga_produk ?? $harga_produk,
                    'jenis_transaksi' => $request->jenis_transaksi ?? 'Tryout',
                    'kode_unik' => $request->kode_unik ?? 0,
                    'total_harga' => $request->total_harga ?? $harga_produk,
                    'kode' => $kode_transaksi,
                    'status' => $request->tipe == 'Free Claim' ? 'Sudah Diverifikasi' : 'Menunggu Pembayaran'
                ]);

                $kode_transaksi = $this->generateCodePad($insert->id, $prefix . date("Ym"));
                Transaksi::find($insert->id)->update(['kode' =>  $kode_transaksi]);

                $insertData = Transaksi::find($insert->id);
                if ($produk->bonus !== NULL) {
                    $this->_create_transaction_bonus_mbti($insertData);
                }

                if (!empty($produk_tryout_ids) && $request->jenis_transaksi == 'Bundling') {
                    $insertProductBundleData = [];
                    foreach ($produk_tryout_ids as $key => $produk_tryout_id) {
                        $insertProductBundleData[$key] = [
                            'id_user'                => $insertData->id_user,
                            'id_bank'               => $insertData->id_bank,
                            'id_produk'                => $produk_tryout_id,
                            'tanggal_transaksi'        =>  $insertData->tanggal_transaksi,
                            'batas_pembayaran'        => $insertData->batas_pembayaran,
                            'tipe'                    => $insertData->tipe,
                            'harga_produk' => 1,
                            'jenis_transaksi' => 'Bundling - Product',
                            'kode_unik' => 1,
                            'total_harga' => 1,
                            'kode' => $insertData->kode,
                            'status' => $insertData->status,
                            'created_at' => date('Y-m-d H:i:s'),
                        ];
                    }
                    Transaksi::insert($insertProductBundleData);
                }

                DB::commit();

                // Bank Transfer 

                $notificationModel = new Notification();
                $notificationModel->insertData('insertData', [
                    'recipientType' => 4, // superadmin + admin
                    'type' => 1, // transaction
                    'title' => 'Ada Transaksi Baru!',
                    'body' => "Transaksi dengan kode <b>#$kode_transaksi</b> telah masuk. Yuk cek detailnya dan lakukan verifikasi!",
                    'data' => $insert
                ]);

                if ($request->tipe !== 'Free Claim') {
                    $notificationModel->insertData('insertData', [
                        'recipientId' => $user->id,
                        'recipientType' => $user->role_user == 'teacher' ? 2 : 3, // teacher or student
                        'type' => 1, // transaction
                        'title' => 'Transaksi berhasil dibuat!',
                        'body' => "Transaksi dengan kode <b>#$kode_transaksi</b> berhasil dibuat. Yuk, segera lakukan pembayaran!",
                        'data' => $insert
                    ]);

                    $testMail = new TestMail([
                        'subject' => "[$kode_transaksi] - Pembayaran berhasil dibuat!",
                        'to' => $user->email,
                        'toName' => !empty($userDetail) ? $userDetail->nama_lengkap : $user->username,
                        'content' => [
                            'title' => "Transaksi kamu untuk membeli $insert->jenis_transaksi telah dibuat!",
                            'body' => 'Berikut ini adalah detail dari transaksi yang perlu segera kamu selesaikan:',
                            'body2' => 'Jika kamu sudah melakukan pembayaran harap unggah bukti transfer dengan menekan tombol di bawah.',
                            'frontUrl' => $request->front_url ?? env('WEB_URL') . '/app/payment/' . $insert->id . '/confirm',
                            'paymentMethod' => 'Bank Transfer'
                        ],
                        'data' => $this->__get_detail($insert->id),
                        'view' => 'email.content.customer__payment-created'
                    ]);

                    Mail::send($testMail);
                } else {
                    $notificationModel->insertData('insertData', [
                        'recipientId' => $user->id,
                        'recipientType' => $user->role_user == 'teacher' ? 2 : 3, // teacher or student
                        'type' => 1, // transaction
                        'title' => 'Klaim Tryout Gratis berhasil diproses!',
                        'body' => "Transaksi dengan kode <b>#$kode_transaksi</b> berhasil dibuat melalui klaim gratis.",
                        'data' => $insert
                    ]);
                }

                return response()->json(['success' => true, 'message' => 'data berhasil di inputkan', 'data' => $insert], 201);
            } catch (\Exception $e) {
                DB::rollback();

                return response()->json(['success' => false, 'messages' => $e->getTrace()], 400);
            }
        } else {
            return response()->json(['success' => false, 'messages' => 'Tipe harus Bank Transfer / Pihak Ketiga'], 400);
        }
    }

    function _create_transaction_bonus_mbti($data_insert)
    {
        $insert = Transaksi::create([
            'id_user'               => $data_insert->id_user,
            'id_produk'             => $data_insert->id_produk,
            'id_bank'             => $data_insert->id_bank,
            'tanggal_transaksi' =>  $data_insert->tanggal_transaksi,
            'tipe'                  => $data_insert->tipe,
            'harga_produk' => 0,
            'batas_pembayaran'        => date("Y-m-d H:i:s", strtotime($data_insert->tanggal_transaksi . " + 1 days")),
            'biaya_adm' => 0,
            'total_harga' => 0,
            'kode' => $data_insert->kode,
            'jenis_transaksi' => 'Bonus MBTI',
            'status' => 'Menunggu Pembayaran',
            'bukti_pembayaran' => 'lunas.jpg'
        ]);

        return $insert;
    }

    public function __get_detail($id)
    {
        $transaksi = Transaksi::with(
            [
                'user:id,email,role_user',
                'user.siswa:id_siswa,nama_lengkap',
                'user.guru:id_teacher,nama_lengkap',
                'produk',
                'mbti',
                'kursus',
                'bank',
                'bundling',
                'material'
            ]
        )->findOrFail($id);

        $data = $transaksi->toArray();
        if ($data['jenis_transaksi'] == 'MBTI') {
            $data['kursus'] = null;
            $data['produk'] = null;
            $data['bundling'] = null;
            $data['material'] = null;
        } else if ($data['jenis_transaksi'] == 'Tryout' || $data['jenis_transaksi'] == 'UKTT') {
            $data['kursus'] = null;
            $data['mbti'] = null;
            $data['bundling'] = null;
            $data['material'] = null;
        } else if ($data['jenis_transaksi'] == 'Les Privat') {
            $data['produk'] = null;
            $data['mbti'] = null;
            $data['bundling'] = null;
            $data['material'] = null;
        } else if ($data['jenis_transaksi'] == 'Bundling') {
            $data['produk'] = null;
            $data['mbti'] = null;
            $data['kursus'] = null;
            $data['material'] = null;
        } else if ($data['jenis_transaksi'] == 'Materi') {
            $data['produk'] = null;
            $data['mbti'] = null;
            $data['kursus'] = null;
            $data['bundling'] = null;
        }

        $array = [];
        switch ($transaksi->tipe) {
            case 'Bank Transfer':
            case 'Free Claim':
                // $array = collect($user)->merge(['user' => $data]);
                $array = $data;
                $array['xendit'] = null;
                break;

            case 'Pihak Ketiga':
                $payment = $transaksi->detail_xendit()->first();
                $dataPayment = $this->utils->getInvoice($payment->reference_id);
                $status = 'Menunggu Pembayaran';
                if ($dataPayment['status'] != 'PENDING') {

                    switch ($dataPayment['status']) {
                        case 'PAID':
                            $status = 'Sudah Diverifikasi';
                            break;
                        case 'SETTLED':
                            $status = 'Sudah Diverifikasi';
                            break;
                        case 'EXPIRED':
                            $status = 'Kadaluarsa';
                            break;
                        default:
                            $status = 'Menunggu Pembayaran';
                            break;
                    }

                    Transaksi::where('kode', $transaksi->kode)->update(['status' => $status]);
                    TabelXendit::where('reference_id', $payment->reference_id)->update(['response' => json_encode($dataPayment)]);
                }
                // $array = collect($user)->merge(['user' => $data, 'xendit' => $dataPayment]);
                $array = $data;
                $array['xendit'] = $dataPayment;
                break;
        }

        return $array;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->__get_detail($id);

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Transaksi::findOrFail($id);

        if ($update->jenis_transaksi == 'Bundle') {
            $update = Transaksi::where('kode', $update->kode)->first();
        }

        $validate = Validator::make($request->all(), [
            // 'id_user'   		=> 'required',
            'id_produk'           => 'required',
            // 'tanggal_transaksi' => 'required',
            'tipe'                => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        $user = auth()->user();

        try {

            $update->id_user                = $request->id_user ?? $user->id;
            $update->id_produk                = $request->id_produk;
            $update->id_bank                = $request->id_bank;
            $update->status                    = $request->status ?? 'Menunggu Pembayaran';
            $update->tanggal_transaksi        = Carbon::parse($request->tanggal_transaksi);
            $update->tanggal_konfirmasi        = $request->tanggal_konfirmasi ? Carbon::parse($request->tanggal_konfirmasi) : null;
            $update->alasan_penolakan        = $request->alasan_penolakan ?? null;
            $update->alasan_pembatalan        = $request->alasan_pembatalan ?? null;
            $update->tipe                    = $request->tipe;
            $update->bukti_pembayaran        = $request->bukti_pembayaran ?? null;
            $update->save();

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $update], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {

        $delete  = Transaksi::destroy($id);
        return response()->json([
            'success' => true,
            'messages' => 'data berhasil di hapus',
            'data' => $id,
        ]);
    }



    public function update_status(Request $request, $id)
    {
        $find = Transaksi::findOrFail($id);
        // echo $find->kode; 
        $update = Transaksi::where('kode', $find->kode);
        // $findFirst = $update->first();

        $validate = Validator::make($request->all(), [
            'status'   => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $dataUpdate['status'] = $request->status;
            // $update->status = $request->status;
            if ($request->status == 'Sudah Diverifikasi') {
                // $update->tanggal_konfirmasi = Carbon::now()->toDateTimeString();
                // $update->alasan_penolakan = null;
                // $update->alasan_pembatalan = null;
                $dataUpdate['tanggal_konfirmasi'] = date('Y-m-d H:i:s');
                $dataUpdate['alasan_penolakan'] = null;
                $dataUpdate['alasan_pembatalan'] = null;
            } else if ($request->status == 'Ditolak') {
                // $update->alasan_penolakan = $request->alasan_penolakan;
                $dataUpdate['alasan_penolakan'] = $request->alasan_penolakan;
            } else if ($request->status == 'Dibatalkan') {
                // $update->alasan_pembatalan = $request->alasan_pembatalan;
                $dataUpdate['alasan_pembatalan'] = $request->alasan_pembatalan;
            }

            // $update->save();
            $update->update($dataUpdate);

            if ($find->jenis_transaksi == 'Les Privat' && $request->status == 'Sudah Diverifikasi') {
                $id_siswa = $find->id_user;
                $id_kursus = $find->id_produk;

                // cek apakah siswa sudah ada di kelas tersebut
                $cekSiswa = KursusSiswa::where('id_siswa', $id_siswa)->where('id_kursus', $id_kursus)
                    ->whereNotIn('status_dikelas', ['Ditolak', 'Sesi Selesai'])->count();

                // jika belum ada, tambah data baru
                if ($cekSiswa < 1) {
                    $kursusSiswa = KursusSiswa::create([
                        'referensi' => $find->kode,
                        'id_kursus' => $id_kursus,
                        'id_siswa' => $id_siswa,
                        'status_dikelas' => 'Pending'
                    ]);

                    $kursusData = Kursus::find($id_kursus);
                    $tentor = Teacher::find($kursusData->id_tentor);

                    // buat notifikasi
                    $notificationModel = new Notification();

                    $notificationModel->insertData('insertData', [
                        'recipientId' => $kursusData->id_tentor,
                        'recipientType' => 2, // teacher
                        'type' => 3, // courses
                        'title' => 'Ada siswa baru mendaftar!',
                        'body' => "Ada siswa baru yang mendaftar di kelas kursus Anda! Yuk cek detailnya. Anda dapat menerima/menolak siswa tersebut.",
                        'data' => $kursusSiswa
                    ]);

                    $notificationModel->insertData('insertData', [
                        'recipientId' => $id_siswa,
                        'recipientType' => 3, // student
                        'type' => 3, // courses
                        'title' => 'Permintaan bergabung kelas kursus dikirimkan!',
                        'body' => "Kamu berhasil mengirimkan permintaan bergabung pada kelas <b>$kursusData->nama_kursus</b>. Mohon tunggu approval dari Tentor ya!",
                        'data' => $kursusSiswa
                    ]);

                    $testMail = new TestMail([
                        'subject' => "Ada siswa baru mendaftar di kelas kursus Anda! Yuk cek detailnya.",
                        'to' => $tentor->email,
                        'toName' => $tentor->nama_lengkap,
                        'content' => [
                            'title' => "Ada siswa yang mendaftar di kelas kursus kamu lho!",
                            'body' => 'Yuk cek detailnya di aplikasi <b>UjiAja</b>:',
                            'frontUrl' => $request->front_url ?? env('WEB_URL') . '/app/partner/courses/' . $id_kursus . '/students?tab=1',
                        ],
                        'data' => $kursusSiswa,
                        'view' => 'email.content.customer__new-student-enroll'
                    ]);

                    Mail::send($testMail);
                }
            }

            DB::commit();

            $notificationModel = new Notification();
            $notificationModel->insertData('insertData', [
                'recipientId' => isset($id_siswa) ? $id_siswa : $find->id_user,
                'recipientType' => isset($id_siswa) ? 3 : 2,
                'type' => 1, // transaction
                'title' => "Status pembayaran berubah!",
                'body' => "Status pembayaran untuk transaksi <b>#$find->kode</b> " . strtolower($request->status) . ". Cek detailnya disini.",
                'data' => $find
            ]);

            return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $find], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }


    public function update_upload(Request $request, $id)
    {
        $find = Transaksi::findOrFail($id);
        $update = Transaksi::where('kode', $find->kode);

        $validate = Validator::make($request->all(), [
            'bukti_pembayaran'   => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            // $update->bukti_pembayaran       = $request->bukti_pembayaran;
            // $update->status                = 'Menunggu Verifikasi';
            // $update->save();
            $update->update([
                'bukti_pembayaran' => $request->bukti_pembayaran,
                'status' => 'Menunggu Verifikasi',
            ]);

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $update], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }
}
