<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use Illuminate\Http\Request;
use Validator;

class NotificationController extends Controller
{
    function __construct()
    {
        $this->notificationModel =  new Notification();
    }

    public function index(Request $request)
    {

        $data = $this->notificationModel->getData('paginationData', $request->all(), 'paginationData');

        return responseData($data);
    }

    public function create(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'recipientType' => 'required|integer',
            'type' => 'required|integer',
            'title' => 'required',
            'body' => 'required',
        ], $this->validationMessage());

        if ($validator->fails()) {
            return responseFail($validator->messages());
        }

        $result = $this->notificationModel->insertData('insertData', $request->all());

        if (isset($result['error'])) {
            return responseFail($result);
        }
        return responseInsert($result, 200, 'Notification created successfully!');;
    }

    public function triggerOpen($id)
    {

        $reqData['id'] = $id;
        $validator = Validator::make($reqData, [
            'id' => 'required|exists:notifications,notification_id',
        ], $this->validationMessage());

        if ($validator->fails()) {
            return responseFail($validator->messages());
        }

        $result = $this->notificationModel->updateData('triggerOpen', $reqData);

        if (isset($result['error'])) {
            return responseFail($result);
        }
        return responseUpdate($result, 200, 'Notification opened!');;
    }
}
