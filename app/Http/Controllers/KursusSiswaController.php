<?php

namespace App\Http\Controllers;

use App\Mail\TestMail;
use App\Models\Kursus;
use App\Models\KursusSiswa;
use App\Models\Notification;
use App\Models\Pendapatan;
use App\Models\Pengaturan;
use App\Models\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;
use Validator;

class KursusSiswaController extends Controller
{
  function __construct()
  {
    $this->kursusSiswaModel = new KursusSiswa();
    $this->kursusModel = new Kursus();
  }

  public function index(Request $request)
  {
    $data = $this->kursusSiswaModel->getData('data', $request->all());
    return responseData($data);
  }

  public function detail($id)
  {
    $req['id'] = $id;
    $data = $this->kursusSiswaModel->getData('detailData', $req);
    if ($data) {
      return responseData($data);
    }
    return responseFail('Data tidak ditemukan!', 404);
  }

  public function studentCheck($id)
  {
    $req['id'] = $id;
    $data = $this->kursusSiswaModel->getData('detailDataByStudentId', $req);
    return responseData($data);
  }


  public function store(Request $request)
  {
    $validate = Validator::make($request->all(), [
      'referensi' => 'required',
      'id_kursus'   => 'required|exists:kursus,id,deleted_at,NULL',
      'id_siswa'   => 'required|exists:students,id_siswa,deleted_at,NULL',
      'status_dikelas'   => ['required', Rule::in('Pending', 'Bergabung', 'Keluar')],
    ], $this->kursusSiswaModel->getValidationMessage());

    if ($validate->fails()) {
      return responseFail($validate->messages(), 422);
    }

    $result = $this->kursusSiswaModel->insertData('insertData', $request->all());

    if (isset($result['error'])) {
      return responseFail($result, 500);
    } else {
      return responseInsert($result);
    }
  }

  public function joinAfterRejected(Request $request)
  {
    $validate = Validator::make($request->all(), [
      'rejectId'   => 'required',
      'from'   => 'required|exists:kursus,id,deleted_at,NULL',
      'to'   => 'required',
    ], $this->kursusSiswaModel->getValidationMessage());

    if ($validate->fails()) {
      return responseFail($validate->messages(), 422);
    }

    $dataDetail = $this->kursusSiswaModel->getData('detailData', ['id' => $request->rejectId]);
    $dataDetailKelas = $this->kursusModel->getData('detailData', ['id' => $request->from]);

    $this->kursusSiswaModel->deleteData('deleteData', ['id' => $request->rejectId]);

    $dataInsert = [
      'referensi' => $dataDetail->referensi,
      'id_kursus' => $request->to,
      'id_siswa' => $dataDetail->id_siswa,
      'status_dikelas' => 'Pending'
    ];

    $result = $this->kursusSiswaModel->insertData('insertData', $dataInsert);

    if (isset($result['error'])) {
      return responseFail($result, 500);
    } else {

      // buat notifikasi
      $notificationModel = new Notification();

      $notificationModel->insertData('insertData', [
          'recipientId' => $dataDetailKelas->id_tentor,
          'recipientType' => 2, // teacher
          'type' => 3, // courses
          'title' => 'Ada siswa baru mendaftar!',
          'body' => "Ada siswa baru yang mendaftar di kelas kursus Anda! Yuk cek detailnya. Anda dapat menerima/menolak siswa tersebut.",
          'data' => $dataInsert
      ]);

      $notificationModel->insertData('insertData', [
        'recipientId' => $dataDetail->id_siswa,
        'recipientType' => 3, // student
        'type' => 3, // courses
        'title' => 'Permintaan bergabung kelas kursus dikirimkan!',
        'body' => "Kamu berhasil mengirimkan permintaan bergabung pada kelas <b>$dataDetailKelas->nama_kursus</b>. Mohon tunggu approval dari Tentor ya!",
        'data' => $dataInsert
    ]);

      $tentor = Teacher::find($dataDetailKelas->id_tentor);

      $testMail = new TestMail([
          'subject' => "Ada siswa baru mendaftar di kelas kursus Anda! Yuk cek detailnya.",
          'to' => $tentor->email,
          'toName' => $tentor->nama_lengkap,
          'content' => [
              'title' => "Ada siswa yang mendaftar di kelas kursus kamu lho!",
              'body' => 'Yuk cek detailnya di aplikasi <b>UjiAja</b>:',
              'frontUrl' => $request->front_url ?? env('WEB_URL') . '/app/partner/courses/'.$dataDetailKelas->id.'/students?tab=1',
          ],
          'data' => $dataInsert,
          'view' => 'email.content.customer__new-student-enroll'
      ]);

      Mail::send($testMail);

      return responseInsert($result);
    }
  }

  public function update(Request $request, $id)
  {
    $req = $request->all();
    $req['id'] = $id;

    $validate = Validator::make($req, [
      'id' => 'required|exists:kursus_siswa,id',
      'referensi' => 'required',
      // 'id_kursus'   => 'required|exists:kursus,id,deleted_at,NULL',
      // 'id_siswa'   => 'required|exists:students,id_siswa,deleted_at,NULL',
      // 'status_dikelas'   => ['required', Rule::in('Pending', 'Bergabung', 'Keluar')],
    ], $this->kursusSiswaModel->getValidationMessage());

    if ($validate->fails()) {
      return responseFail($validate->messages(), 422);
    }

    $status = $request->status_dikelas;
    if (!empty($status)) {
      switch ($status) {
        case 'Pending':
          $req['tanggal_gabung'] = null;
          $req['tanggal_keluar'] = null;
          break;
        case 'Bergabung':
          $req['tanggal_gabung'] = $req['tanggal_gabung'] ?? date("Y-m-d H:i:s");
          $req['tanggal_keluar'] = null;
          break;
        case 'Keluar':
        case 'Dikeluarkan':
          $req['tanggal_keluar'] = $req['tanggal_keluar'] ?? date("Y-m-d H:i:s");
          break;
        case 'Ditolak':
          // $req['deleted_at'] = date("Y-m-d H:i:s");
          break;
      }
    }

    $result = $this->kursusSiswaModel->updateData('updateData', $req);

    if ($status == 'Ditolak') {
      // jika ditolak masukan ke kelas lain
      // $this->kursusSiswaModel->insertData('insertData', [
      //   'id_kursus' => 9999, // otw
      //   'id_siswa' => $req['id_siswa'],
      //   'status_dikelas' => 'Pending',
      //   'referensi' => $req['referensi'],
      // ]);
    }

    if (isset($result['error'])) {
      return responseFail($result, 500);
    } else {
      return responseUpdate($result);
    }
  }

  public function delete(Request $request, $id)
  {
    $req = $request->all();
    $req['id'] = $id;

    $validate = Validator::make($req, [
      'id' => 'required|exists:kursus_jadwal,id',
    ], $this->kursusSiswaModel->getValidationMessage());

    if ($validate->fails()) {
      return responseFail($validate->messages(), 422);
    }

    $result = $this->kursusSiswaModel->deleteData('deleteData', ['id' => $id]);

    if (isset($result['error'])) {
      return responseFail($result);
    }
    return responseDeleted($result);
  }

  public function updateStatus(Request $request, $id)
  {
    $req = $request->all();
    $req['id'] = $id;
    $validate = Validator::make($req, [
      'id' => 'required|exists:kursus_siswa,id',
      'status_dikelas'   => ['required', Rule::in('Pending', 'Bergabung', 'Ditolak', 'Ditolak & Sudah Bergabung Lagi', 'Menunggu Konfirmasi Selesai', 'Sesi Selesai')],
    ], $this->kursusSiswaModel->getValidationMessage());

    if ($validate->fails()) {
      return responseFail($validate->messages(), 422);
    }

    $result = $this->kursusSiswaModel->updateData('updateStatusData', $req);

    if (isset($result['error'])) {
      return responseFail($result);
    }

    $status = $request->status_dikelas;
    if ($status == 'Ditolak') {
      $dataDetail = $this->kursusSiswaModel->getData('detailData', $req);

      // cari kelas lain jika ada
      $kelasLainData = $this->kursusModel->getData('kelasLainData', ['id' => $dataDetail->id, 'harga_kursus' => $dataDetail->harga_kursus]);
      // if(!empty($kelasLainData)) {
      //   // jika ditolak masukan ke kelas lain jika ada
      //   $this->kursusSiswaModel->insertData('insertData', [
      //     'id_kursus' => $dataDetail->id_kursus, // otw
      //     'id_siswa' => $dataDetail->id_siswa,
      //     'status_dikelas' => 'Pending',
      //     'referensi' => $dataDetail->referensi,
      //   ]);
      // }
      $nama_kursus = $dataDetail->kursus ? $dataDetail->kursus->nama_kursus : 'Kelas Kursus ini';
      $notificationModel = new Notification();
      $notificationModel->insertData('insertData', [
        'recipientId' => $dataDetail->id_siswa,
        'recipientType' => 3, // student
        'type' => 3, // Course
        'title' => 'Permintaan bergabung dikelas ditolak!',
        'body' => "Permintaan anda untuk bergabung di <b>$nama_kursus</b> ditolak oleh Tentor. Silakan lihat detail dan pilih kelas lainnya.",
        'data' => [
          'detail' => $result,
          'kelaslain' => $kelasLainData
        ]
      ]);
    } else if ($status == 'Sesi Selesai') {

      $kursusSiswaDetail = $this->kursusSiswaModel->getData('detailData', ['id' => $id]);

      $kursusModel = new Kursus();
      $kursusDetail = $kursusModel->getData('detailData', ['id' => $kursusSiswaDetail->id_kursus]);

      $pengaturanModel = new Pengaturan();
      $pengaturanFeeAdmin = $pengaturanModel->getData('detailDataByKey', ['key' => 'fee_admin']);

      $pendapatanModel = new Pendapatan();

      $feePendapatanNominal = $pengaturanFeeAdmin->isi / 100 * $kursusDetail->harga_kursus;
      $nettoPendapatan = $kursusDetail->harga_kursus - $feePendapatanNominal;

      $cekRecord = $pendapatanModel->getData('detailDataByReferensi', ['kode_referensi' => $kursusSiswaDetail->referensi]);

      if (empty($cekRecord)) {
        $insertPendapatan = $pendapatanModel->insertData('insertData', [
          'id_tentor'   => $kursusDetail->id_tentor,
          'kode_referensi'   => $kursusSiswaDetail->referensi,
          'total_pendapatan'   => $kursusDetail->harga_kursus,
          'fee_pendapatan_persen'   => $pengaturanFeeAdmin->isi,
          'fee_pendapatan_nominal'   => $feePendapatanNominal,
          'netto_pendapatan'   => $nettoPendapatan,
        ]);

        if (isset($insertPendapatan['error'])) {
          return responseFail($insertPendapatan);
        }
        return responseUpdate($result, 200, 'Sesi kelas selesai. Pendapatan berhasil dicatat!');
      }
    }

    if (isset($result['error'])) {
      return responseFail($result);
    }
    return responseUpdate($result);
  }
}
