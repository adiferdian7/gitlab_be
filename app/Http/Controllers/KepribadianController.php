<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\Kepribadian;

class KepribadianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(Request $request)
    {
        $limit = $request->paginate ?  $request->paginate : 10;
        $q = $request->q;
        $data = Kepribadian::where('nama_singkat', 'like', '%' .$q. '%')
                    ->orWhere('nama_panjang', 'like', '%' .$q. '%')
                    ->orWhere('jenis', 'like', '%' .$q. '%')
                    ->paginate($limit);

        return response()->json(['success' => true, 'data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'nama_singkat'   => 'required|unique:kepribadians,nama_singkat,NULL,id,deleted_at,NULL',
            'nama_panjang'   => 'required',
            'jenis'          => 'required|unique:kepribadians,jenis,NULL,id,deleted_at,NULL',
            'deskripsi'      => 'required',
            'saran'          => 'required',
        ]);

        if($validate->fails()) {
            return response()->json([
                'success' => false, 
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $insert = Kepribadian::create([
                'nama_singkat' => $request->nama_singkat,
                'nama_panjang' => $request->nama_panjang,
                'jenis'        => $request->jenis,
                'deskripsi'    => $request->deskripsi,
                'saran'        => $request->saran,  
                'profesi'      => $request->profesi,
                'partner'      => $request->partner,
            ]);

            DB::commit();

             return response()->json(['success' => true, 'message' => 'data berhasil di inputkan', 'data' => $insert], 201);

        } catch (\Exception $e) {
             DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Kepribadian::findOrFail($id);

        return response()->json([
            'success' => true, 
            'data' => $data
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Kepribadian::findOrFail($id);

        $validate = Validator::make($request->all(), [
            'nama_singkat'   => 'required|unique:kepribadians,nama_singkat,'.$id.',id,deleted_at,NULL',
            'nama_panjang'   => 'required',
            'jenis'          => 'required|unique:kepribadians,jenis,'.$id.',id,deleted_at,NULL',
            'deskripsi'      => 'required',
            'saran'          => 'required',
        ]);

        if($validate->fails()) {
            return response()->json([
                'success' => false, 
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

                $update->nama_singkat = $request->nama_singkat;
                $update->nama_panjang = $request->nama_panjang;
                $update->jenis        = $request->jenis;
                $update->deskripsi    = $request->deskripsi;
                $update->saran        = $request->saran;  
                $update->profesi      = $request->profesi;
                $update->partner      = $request->partner;

                $update->save();
                
            DB::commit();

             return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $update], 201);

        } catch (\Exception $e) {
             DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
      
        $delete  = Kepribadian::destroy($id);
        return response()->json([
            'success' => true,
            'messages' => 'data berhasil di hapus',
            'data' => $id,
        ]);
    }
}
