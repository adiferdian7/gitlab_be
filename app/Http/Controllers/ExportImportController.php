<?php

namespace App\Http\Controllers;

use App\Exports\QuestionAndAndswerExport;
use App\Imports\QuestionAndAndswerImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExportImportController extends Controller
{
    public function export_soal_pertanyaan()
    {
        return Excel::download(new QuestionAndAndswerExport, 'question_and_answer_' . date('YmdHis') . '.xlsx');
    }

    public function import_soal_pertanyaan(Request $request)
    {
        $request->validate([
            'file' => 'required'
        ]);

        $import = Excel::import(new QuestionAndAndswerImport, $request->file);

        return response()->json(['success' =>true]);

    }
}
