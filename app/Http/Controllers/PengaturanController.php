<?php

namespace App\Http\Controllers;

use App\Models\Pengaturan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;
use Validator;

class PengaturanController extends Controller
{
  function __construct()
  {
    $this->pengaturanModel = new Pengaturan();
  }

  public function index(Request $request)
  {
    $data = $this->pengaturanModel->getData('data', []);

    return responseData($data);
  }

  public function detail($key)
  {
  }

  public function updateMultiple(Request $request)
  {
    $reqData =  $request->all();

    foreach ($reqData as $key => $isi) {
      if (!empty($isi)) {
        $this->pengaturanModel->updateData('updateDataByKey', ['key' => $key, 'isi' => $isi]);
      }
    }
    return responseUpdate([], 200, 'Pengaturan berhasil diperbarui!');
  }
}
