<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\ProgramStudi;
use App\Models\ProgramStudiBindMapel;
use App\Models\ProgramStudiPerguruanTinggi as ProgramStudiBindPerguruan;

use Illuminate\Support\Str;

class ProgramStudiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->paginate ?  $request->paginate : 10;
        $q = $request->q;

        $data = ProgramStudi::with([
            'penjurusan:id,id_kelas,nama_penjurusan',
            'penjurusan.kelas:id,nama_kelas',
            'list_mapel:id,id_program_studi,id_mapel',
            'list_mapel.mapel:id,nama_mapel',
            'listperguruan:id_program_studi,id_perguruan_tinggi,akreditasi_program_studi,passing_grade_prodi'
        ])
            ->select('id', 'nama_studi', 'deskripsi', 'id_penjurusan', 'id_mapel', 'icon_prodi', 'alasan', 'prospek', 'slug');

        if (!empty($request->mapel)) {

            // $data->where('id_mapel', $request->mapel);
            $m = $request->mapel;
            $data->whereHas('list_mapel', function ($query) use ($m) {
                $query->where('id_mapel', 'like', '%' . $m . '%');
            });
        }

        if (!empty($request->penjurusan)) {
            $f = $request->penjurusan;
            $data->whereHas('penjurusan', function ($query) use ($f) {
                $query->where('nama_penjurusan', 'like', '%' . $f . '%');
            });
        }

        if (!empty($request->id_perguruan_tinggi)) {
            $ipt = $request->id_perguruan_tinggi;
            $data->whereHas('listperguruan', function ($query) use ($ipt) {
                $query->where('id_perguruan_tinggi', 'like', '%' . $ipt . '%');
            });
        }

        if (!empty($request->akreditasi_program_studi)) {
            $ak = $request->akreditasi_program_studi;
            $data->whereHas('listperguruan', function ($query) use ($ak) {
                $query->where('akreditasi_program_studi', 'like', '%' . $ak . '%');
            });
        }

        $data->where(function ($query) use ($q) {
            $query->orWhere('nama_studi', 'like', '%' . $q . '%');
            $query->orWhere('deskripsi', 'like', '%' . $q . '%');
            $query->orWhereHas('mapel', function ($f) use ($q) {
                $f->where('nama_mapel', 'like', '%' . $q . '%');
            });
            $query->orWhereHas('penjurusan', function ($f) use ($q) {
                $f->where('nama_penjurusan', 'like', '%' . $q . '%');
            });
            $query->orWhereHas('penjurusan.kelas', function ($f) use ($q) {
                $f->where('nama_kelas', 'like', '%' . $q . '%');
            });
        });


        $array = $data->latest()->paginate($limit);


        return response()->json(['success' => true, 'data' => $array]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'nama_studi'   => 'required|unique:program_studi,nama_studi,NULL,id,deleted_at,NULL',
            'deskripsi'    => 'required',
            'id_penjurusan' => 'required',
            // 'id_mapel'     => 'required',
            // 'icon_prodi'   => 'required',
            'alasan'       => 'required',
            'prospek'      => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $insert = ProgramStudi::create([
                'nama_studi'   => $request->nama_studi,
                'deskripsi'    => $request->deskripsi,
                'id_penjurusan' => $request->id_penjurusan,
                // 'id_mapel'     => $request->id_mapel,
                'icon_prodi'   => $request->icon_prodi,
                'alasan'       => $request->alasan,
                'prospek'      => $request->prospek,
                'kelompok'      => $request->kelompok,
                'id_rumpun'      => $request->id_rumpun,
                'kelompok'      => $request->kelompok,
                'slug'         => Str::slug($request->nama_studi, '-'),
            ]);

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di inputkan', 'data' => $insert], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ProgramStudi::with([
            'mapel:id,nama_mapel',
            'penjurusan:id,id_kelas,nama_penjurusan',
            'penjurusan.kelas:id,nama_kelas',
            'listperguruan:id,id_program_studi,id_perguruan_tinggi,akreditasi_program_studi,passing_grade_prodi',
            'listperguruan.perguruan:id,nama_perguruan,akreditasi',
            'list_mapel.mapel:id,nama_mapel'
        ])->findOrFail($id);

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    public function detail_slug($slug)
    {
        $data = ProgramStudi::with([
            'mapel:id,nama_mapel',
            'penjurusan:id,id_kelas,nama_penjurusan',
            'penjurusan.kelas:id,nama_kelas',
            'listperguruan:id,id_program_studi,id_perguruan_tinggi,akreditasi_program_studi,passing_grade_prodi',
            'listperguruan.perguruan:id,nama_perguruan,akreditasi',
            'list_mapel.mapel:id,nama_mapel'
        ])->where('slug', $slug)->first();

        if ($data == NULL) {
            return response()->json([
                'success' => false,
                'messages' => 'Data tidak di temukan'
            ], 404);
        }

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $program_studi = ProgramStudi::findOrFail($id);

        $validate = Validator::make($request->all(), [
            'nama_studi'   => 'required|unique:program_studi,nama_studi,' . $id . ',id,deleted_at,NULL',
            'deskripsi'    => 'required',
            'id_penjurusan' => 'required',
            'id_mapel'     => 'required',
            // 'icon_prodi'   => 'required',
            'alasan'       => 'required',
            'prospek'      => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $program_studi->nama_studi   = $request->nama_studi;
            $program_studi->deskripsi    = $request->deskripsi;
            $program_studi->id_penjurusan = $request->id_penjurusan;
            $program_studi->id_mapel     = $request->id_mapel;
            $program_studi->icon_prodi   = $request->icon_prodi;
            $program_studi->alasan       = $request->alasan;
            $program_studi->prospek      = $request->prospek;
            $program_studi->kelompok      = $request->kelompok;
            $program_studi->id_rumpun      = $request->id_rumpun;
            $program_studi->kelompok      = $request->kelompok;
            $program_studi->slug         = Str::slug($request->nama_studi, '-');
            $program_studi->save();

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $program_studi], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $program_studi = ProgramStudi::findOrFail($id);
        $list = $program_studi->listperguruan()->delete();
        $delete  = ProgramStudi::destroy($id);
        return response()->json([
            'success' => true,
            'messages' => 'data berhasil di hapus',
            'data' => $id,
        ]);
    }


    public function multiple(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'nama_studi'   => 'required|unique:program_studi,nama_studi,NULL,id,deleted_at,NULL',
            'deskripsi'    => 'required',
            'id_penjurusan' => 'required',
            // 'id_mapel'     => 'required',
            // 'icon_prodi'   => 'required',
            'alasan'       => 'required',
            'prospek'      => 'required',
            'program_studi_dan_perguruan_tinggi' => 'required',
            'mapels' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $insert = ProgramStudi::create([
                'nama_studi'   => $request->nama_studi,
                'deskripsi'    => $request->deskripsi,
                'id_penjurusan' => $request->id_penjurusan,
                // 'id_mapel'     => $request->id_mapel,
                'icon_prodi'   => $request->icon_prodi,
                'alasan'       => $request->alasan,
                'prospek'      => $request->prospek,
                'id_rumpun'      => $request->id_rumpun,
                'rumpun'      => $request->rumpun,
                'kelompok'      => $request->kelompok,
                'slug'         => Str::slug($request->nama_studi, '-'),
            ]);

            foreach ($request->program_studi_dan_perguruan_tinggi as $key => $value) {
                ProgramStudiBindPerguruan::create([
                    'id_program_studi'   => $insert->id,
                    'id_perguruan_tinggi' => $value['id_perguruan_tinggi'],
                    'akreditasi_program_studi' => $value['akreditasi_program_studi'],
                    'passing_grade_prodi' => $value['passing_grade_prodi'],

                ]);
            }

            foreach ($request->mapels as $key => $value) {
                ProgramStudiBindMapel::create([
                    'id_program_studi'   => $insert->id,
                    'id_mapel' => $value['id_mapel'],
                ]);
            }

            DB::commit();

            $list = ProgramStudiBindPerguruan::where('id_program_studi', $insert->id)->get();

            return response()->json(['success' => true, 'message' => 'data berhasil di inputkan', 'data' => [
                'program_studi' => $insert,
                'list' => $list
            ]], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    public function updateMultiple(Request $request, $id)
    {
        $program_studi = ProgramStudi::findOrFail($id);

        $validate = Validator::make($request->all(), [
            'nama_studi'   => 'required|unique:program_studi,nama_studi,' . $id . ',id,deleted_at,NULL',
            'deskripsi'    => 'required',
            'id_penjurusan' => 'required',
            // 'id_mapel'     => 'required',
            // 'icon_prodi'   => 'required',
            'alasan'       => 'required',
            'prospek'      => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $program_studi->nama_studi   = $request->nama_studi;
            $program_studi->deskripsi    = $request->deskripsi;
            $program_studi->id_penjurusan = $request->id_penjurusan;
            // $program_studi->id_mapel     = $request->id_mapel;
            $program_studi->icon_prodi   = $request->icon_prodi;
            $program_studi->alasan       = $request->alasan;
            $program_studi->prospek      = $request->prospek;
            $program_studi->id_rumpun      = $request->id_rumpun;
            $program_studi->rumpun      = $request->rumpun;
            $program_studi->kelompok      = $request->kelompok;
            $program_studi->slug         = Str::slug($request->nama_studi, '-');
            $program_studi->save();

            if (!empty($request->program_studi_dan_perguruan_tinggi)) {
                // ProgramStudiBindPerguruan::where('id_program_studi', $id)->delete();

                foreach ($request->program_studi_dan_perguruan_tinggi as $key => $value) {
                    $count = ProgramStudiBindPerguruan::where('id_program_studi', $id)->where('id_perguruan_tinggi', $value['id_perguruan_tinggi'])->count();
                    if ($count < 1) {
                        ProgramStudiBindPerguruan::create([
                            'id_program_studi'   => $id,
                            'id_perguruan_tinggi' => $value['id_perguruan_tinggi'],
                            'akreditasi_program_studi' => $value['akreditasi_program_studi'],
                            'passing_grade_prodi' => $value['passing_grade_prodi'],

                        ]);
                    }
                }
            }

            if (!empty($request->mapels)) {

                ProgramStudiBindMapel::where('id_program_studi', $id)->delete();

                foreach ($request->mapels as $key => $value) {
                    ProgramStudiBindMapel::create([
                        'id_program_studi'   => $id,
                        'id_mapel' => $value['id_mapel'],
                    ]);
                }
            }

            DB::commit();

            $list = $program_studi->listperguruan()->get();

            return response()->json([
                'success' => true, 'message' => 'data berhasil di update', 'data' =>
                [
                    'program_studi' => $program_studi,
                    'list' => $list
                ]
            ], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }
}
