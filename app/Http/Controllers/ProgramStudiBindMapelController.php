<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Validator;
use App\Models\ProgramStudiBindMapel as ProdiMapel;

class ProgramStudiBindMapelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'id_program_studi'   => 'required',
            'id_mapel' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $insert = ProdiMapel::create([
                'id_program_studi'   => $request->id_program_studi,
                'id_mapel' => $request->id_mapel,
            ]);

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di inputkan', 'data' => $insert], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ProdiMapel::with([
            'program_studi:id,nama_studi,akreditasi,deskripsi,id_penjurusan',
            'mapel:id,nama_mapel'
        ])->findOrFail($id);

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = ProdiMapel::findOrFail($id);

        $validate = Validator::make($request->all(), [
            'id_program_studi'   => 'required',
            'id_mapel' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json([
                'success' => false,
                'messages' => $validate->messages()
            ], 422);
        }

        DB::beginTransaction();

        try {

            $update->id_program_studi    = $request->id_program_studi;
            $update->id_mapel = $request->id_mapel;
            $update->save();

            DB::commit();

            return response()->json(['success' => true, 'message' => 'data berhasil di update', 'data' => $update], 201);
        } catch (\Exception $e) {
            DB::rollback();

            return response()->json(['success' => false, 'messages' => $e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete  = ProdiMapel::destroy($id);
        return response()->json([
            'success' => true,
            'messages' => 'data berhasil di hapus',
            'data' => $id,
        ]);
    }

}
