<?php

namespace App\Http\Controllers;

use App\Models\Kursus;
use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Validator;

class KursusController extends Controller
{
    function __construct()
    {
        $this->kursusModel = new Kursus();
    }

    public function index(Request $request)
    {
        $data = $this->kursusModel->getData('paginationData', $request->all(), 'paginationData');
        return responseData($data);
    }

    public function ownByStudent(Request $request)
    {
        $data = $this->kursusModel->getData('ownByStudentPaginationData', $request->all(), 'studentPaginationData');
        return responseData($data);
    }

    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'nama_kursus'   => 'required|unique:kursus,nama_kursus,NULL,id,deleted_at,NULL',
            // 'video_kursus'   => 'required',
            // 'sampul_kursus'   => 'required',
            'id_tentor'   => 'required|exists:teachers,id_teacher',
            'id_mapel'   => 'required|exists:mapels,id',
            'id_jenjang'   => 'required|exists:jenjangs,id',
            'harga_kursus'   => 'required',
            'model_belajar'   => 'required',
            'alamat'   => Rule::requiredIf($request->model_belajar == 'Offline' || $request->model_belajar == 'Hybrid'),
            'id_provinsi'   => Rule::requiredIf($request->model_belajar == 'Offline' || $request->model_belajar == 'Hybrid'),
            'id_kota'   => Rule::requiredIf($request->model_belajar == 'Offline' || $request->model_belajar == 'Hybrid'),
            'id_kecamatan'   => Rule::requiredIf($request->model_belajar == 'Offline' || $request->model_belajar == 'Hybrid'),
        ], $this->kursusModel->getValidationMessage());

        if ($validate->fails()) {
            return responseFail($validate->messages(), 422);
        }

        if ($request->jadwals) {
            $result = $this->kursusModel->insertData('insertDataWithJadwal', $request->all());
        } else {
            $result = $this->kursusModel->insertData('insertData', $request->all());
        }

        if (isset($result['error'])) {
            return responseFail($result, 500);
        } else {
            $notificationModel = new Notification();
            $notificationModel->insertData('insertData', [
                'recipientId' => '',
                'recipientType' => 4, // superadmin + admin
                'type' => 3, // courses
                'title' => 'Ada Kelas Kursus Baru!',
                'body' => 'Ada Kelas Kursus baru telah terdaftar. Yuk cek detailnya dan verifikasi!',
                'data' => $result
            ]);

            if ($this->user()->role_user == 'teacher') {
                $notificationModel->insertData('insertData', [
                    'recipientId' => $request->id_tentor,
                    'recipientType' => 2, // teacher
                    'type' => 3, // courses
                    'title' => 'Kelas Kursus berhasil dibuat!',
                    'body' => "Kelas kursus berhasil dibuat dan sedang dalam proses peninjauan. Mohon tunggu verifikasi dari kami.",
                    'data' => $result
                ]);
            }

            return responseInsert($result);
        }
    }

    public function detail($id = '')
    {
        $data = $this->kursusModel->getData('detailData', ['id' => $id], 'detailData');
        if (!empty($data)) {
            return responseData($data);
        } else {
            return responseFail('Data tidak ditemukan!', 404);
        }
    }


    public function update(Request $request, $id)
    {
        $kelas = Kursus::find($id);

        if (!$kelas) {
            return responseNotFound();
        }

        $validate = Validator::make($request->all(), [
            'nama_kursus'   => 'required|unique:kursus,nama_kursus,' . $id . ',id,deleted_at,NULL',
            // 'video_kursus'   => 'required',
            // 'sampul_kursus'   => 'required',
            'id_tentor'   => 'required|exists:teachers,id_teacher',
            'id_mapel'   => 'required|exists:mapels,id',
            'id_jenjang'   => 'required|exists:jenjangs,id',
            'harga_kursus'   => 'required',
            'model_belajar'   => 'required',
            'alamat'   => Rule::requiredIf($request->model_belajar == 'Online'),
            'id_provinsi'   => Rule::requiredIf($request->model_belajar == 'Online'),
            'id_kota'   => Rule::requiredIf($request->model_belajar == 'Online'),
            'id_kecamatan'   => Rule::requiredIf($request->model_belajar == 'Online'),
        ], $this->kursusModel->getValidationMessage());

        if ($validate->fails()) {
            return responseFail($validate->messages(), 422);
        }

        $data = $request->all();
        $data['id'] = $id;
        $result = $this->kursusModel->updateData('updateData', $data);

        if (isset($result['error'])) {
            return responseFail($result);
        } else {
            return responseUpdate($result);
        }
    }

    public function delete(Request $request, $id)
    {
        $kelas = Kursus::find($id);
        if (!$kelas) {
            return responseNotFound();
        }
        $result = $this->kursusModel->deleteData('deleteData', ['id' => $id]);

        if (isset($result['error'])) {
            return responseFail($result);
        }
        return responseDeleted($result);
    }

    // Toggle Data Kursus
    public function toggleActive(Request $request, $id)
    {
        if (!in_array($request->user()->role_user, ['superAdmin', 'admin', 'teacher'])) {
            return responseFail([
                'role_user' => 'Unauthorized.'
            ], 403);
        }

        
        $req = $request->all();
        $req['id'] = $id;
        $validate = Validator::make($req, [
            'id'   => 'required|exists:kursus,id,deleted_at,NULL',
            'status'   => ['required', Rule::in(0, 1)],
        ], $this->kursusModel->getValidationMessage());
        
        // check if it's own by the user
        $kursus = Kursus::find($id);
        if($kursus->id_tentor != $request->user()->id) {
            return responseFail([
                'error' => 'Unauthorized. Not your own!'
            ], 403);
        }
        
        if ($validate->fails()) {
            return responseFail($validate->messages(), 422);
        }

        $result = $this->kursusModel->updateData('toggleActive', ['id' => $id, 'status' => $request->status]);

        if (isset($result['error'])) {
            return responseFail($result);
        }
        return responseUpdate($result);
    }

    // Toggle Verifikasi Kursus
    public function toggleVerification(Request $request, $id)
    {
        if ($request->user()->role_user != 'superAdmin' && $request->user()->role_user != 'admin') {
            return responseFail([
                'role_user' => 'Unauthorized'
            ], 403);
        }

        $req = $request->all();
        $req['id'] = $id;
        $validate = Validator::make($req, [
            'id'   => 'required|exists:kursus,id,deleted_at,NULL',
            'status'   => ['required', Rule::in(0, 1)],
        ], $this->kursusModel->getValidationMessage());

        if ($validate->fails()) {
            return responseFail($validate->messages(), 422);
        }
        $result = $this->kursusModel->updateData('toggleVerification', ['id' => $id, 'status' => $request->status]);

        if ($request->status == 0) {
            $result = $this->kursusModel->updateData('toggleActive', ['id' => $id, 'status' => 0]);
        }

        if (isset($result['error'])) {
            return responseFail($result);
        }

        if ($request->status == 1) {
            $notificationModel = new Notification();
            $notificationModel->insertData('insertData', [
                'recipientId' => $result->id_tentor,
                'recipientType' => 2, // teacher
                'type' => 3, // Course
                'title' => 'Kelas Kursus diverifikasi!',
                'body' => "Kelas kursus yang Anda buat telah kami verifikasi. Sekarang Anda sudah bisa menerima siswa baru.",
                'data' => $result
            ]);
        }

        return responseUpdate($result);
    }

    // Harga Kursus - Data Option
    public function coursePriceOption()
    {
        $data = $this->kursusModel->getData('dataCoursePriceOption', []);
        return responseData($data);
    }

    public function getKelasLain(Request $request, $id)
    {
        $req['id'] = $id;
        $dataDetail = $this->kursusModel->getData('detailData', $req);

        // cari kelas lain jika ada
        $kelasLainData = $this->kursusModel->getData('kelasLainData', ['id' => $dataDetail->id, 'harga_kursus' => $dataDetail->harga_kursus]);

        return responseData($kelasLainData);
    }
}
