<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KategoriTo;

class KategoriToController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $limit = $request->paginate ?  $request->paginate : 10;
    $q = $request->q;
    $data = KategoriTo::where('kategori', 'like', '%' . $q . '%')
      ->orWhere('kelompok', 'like', '%' . $q . '%')
      ->orWhere('jenjang', 'like', '%' . $q . '%')
      ->orWhere('kelas', 'like', '%' . $q . '%')
      ->orWhere('penjurusan', 'like', '%' . $q . '%')
      ->paginate($limit);
    return response()->json(['success' => true, 'data' => $data]);
  }

  public function indexWithoutPagination(Request $request)
  {
    $data = KategoriTo::all();
    return response()->json(['success' => true, 'data' => $data]);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $data = KategoriTo::findOrFail($id);

    return response()->json([
      'success' => true,
      'data' => $data
    ]);
  }
}
