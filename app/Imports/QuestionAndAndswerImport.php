<?php

namespace App\Imports;

use App\Models\SoalPertanyaan;
use App\Models\SoalTryout;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;

class QuestionAndAndswerImport implements ToModel, WithStartRow
{
    public function model(array $row)
    {
        return SoalPertanyaan::create([
            'id_soal_tryout' => $row['0'],
            'bab_mapel' => $row['1'],
            'penjelasan_pertanyaan' => $row['2'],
            'template_pertanyaan' => $row['3'],	
            'soal' => $row['4'],
            'gambar' => $row['5'],
            'opsi_pertanyaan' => $row['6'],
            'pembahasan_pertanyaan' => $row['7'],
            'parent_soal_pertanyaan' => $row['8'],
            'jawaban_pertanyaan' => $row['9']
        ]);
    }

    public function startRow(): int
    {
        return 2;
    }
}
