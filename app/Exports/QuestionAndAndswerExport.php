<?php

namespace App\Exports;

use App\Models\SoalPertanyaan;
use App\Models\Tryout;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class QuestionAndAndswerExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $packageQuestionId = request()->soal_tryout_id;
        if ($packageQuestionId) {
            return SoalPertanyaan::where('id_soal_tryout', $packageQuestionId)->get()->makeHidden('id');
        }

        return SoalPertanyaan::all();
    }

    public function headings(): array
    {
        return [
            'id_soal_tryout',
            'bab_mapel',
            'penjelasan_pertanyaan',
            'template_pertanyaan',
            'soal',
            'gambar',
            'opsi_pertanyaan',
            'pembahasan_pertanyaan',
            'parent_soal_pertanyaan',
            'jawaban_pertanyaan'
        ];
    }
}
