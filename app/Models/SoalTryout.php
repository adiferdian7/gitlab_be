<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SoalTryout extends Model
{
    // use HasFactory, SoftDeletes;
    use HasFactory;

    protected $guarded = [];
    protected $dates = ['deleted_at'];
    
    public function tryout(){
    	return $this->belongsTo('App\Models\Tryout', 'id_tryout');
    }

     public function mapel(){
    	return $this->belongsTo('App\Models\Mapel', 'id_mapel');
    }

    public function pertanyaan(){
    	return $this->hasMany('App\Models\SoalPertanyaan', 'id_soal_tryout');
    }
}
