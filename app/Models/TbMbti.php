<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TbMbti extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];
    protected $dates = ['deleted_at'];


    public function mbti_dimensi(){
    	return $this->hasMany('App\Models\MbtiDimensi', 'id_mbti');
    }

    public function transaksi()
    {
        return $this->hasOne('App\Models\Transaksi', 'id_produk')->where('jenis_transaksi', 'MBTI');
    }

    public function tes()
    {
        return $this->hasOne('App\Models\MbtiJawaban', 'id_mbti');
    }
}
