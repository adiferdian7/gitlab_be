<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class cmsContent9 extends Model
{
    use HasFactory;
    
    protected $table = 'cms_content9s';
    protected $guarded = [];
}
