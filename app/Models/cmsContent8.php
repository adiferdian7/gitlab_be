<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class cmsContent8 extends Model
{
    use HasFactory;
    
    protected $table = 'cms_content8s';
    protected $guarded = [];
}
