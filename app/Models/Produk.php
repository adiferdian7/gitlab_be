<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Produk extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];
    protected $dates = ['deleted_at'];

    protected $casts = [
        'perhitungan' => 'json',
        'bonus' => 'json',
    ];

    public function list()
    {
        return $this->hasMany('App\Models\ProdukPaket', 'id_produk');
    }

    public function transaksi_list()
    {
        return $this->hasMany('App\Models\Transaksi', 'id_produk');
    }

    public function transaksi_user()
    {
        return $this->hasOne('App\Models\Transaksi', 'id_produk')
        ->where('id_user', auth()->user()->id)
        ->orderBy('id', 'ASC')
        ->groupBy('kode');
    }

    public function transaksi_user_product_bundling()
    {
        return $this->hasOne('App\Models\Transaksi', 'id_produk')
        ->where('id_user', auth()->user()->id)
        ->orderBy('id', 'ASC');
    }
    
    public function tryout_user()
    {
        return $this->hasMany('App\Models\TryoutUser', 'id_produk');
    }


    public function tryout_user_user()
    {
        return $this->hasMany('App\Models\TryoutUser', 'id_produk')->where('id_user', auth()->user()->id);
    }

    public function level()
    {
        return $this->belongsTo('App\Models\Level', 'uktt_id_level');
    }

    public function mapel()
    {
        return $this->belongsTo('App\Models\Mapel', 'id_mapel');
    }
}
