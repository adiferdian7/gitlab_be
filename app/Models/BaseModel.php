<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BaseModel extends Model {

  protected $validationMessage = [
    'required' => ':Attribute diperlukan.',
    'unique' => ':Attribute sudah terdaftar.',
    'exists' => ':Attribute tidak terdaftar.',
  ];

  public function getUser() {
    return Auth::user();
  }

  public function getUserId() {
    return Auth::user()->id;
  }

  public function getValidationMessage()
  {
    return $this->validationMessage;
  }

  public function formatRupiah($num, $currency = false)
  {
    $num_format = number_format($num, 0, ',', '.');
    if(!$currency) {
      return $num_format;
    } 
    return 'Rp ' . $num_format;
  }

}