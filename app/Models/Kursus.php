<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Kursus extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $table = 'kursus';
    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function tentor()
    {
        return $this->belongsTo('App\Models\Teacher', 'id_tentor');
    }

    public function mapel()
    {
        return $this->belongsTo('App\Models\Mapel', 'id_mapel');
    }

    public function kelas()
    {
        return $this->belongsTo('App\Models\Kelas', 'id_kelas');
    }

    public function jenjang()
    {
        return $this->belongsTo('App\Models\Jenjang', 'id_jenjang');
    }

    public function penjurusan()
    {
        return $this->belongsTo('App\Models\Penjurusan', 'id_penjurusan');
    }

    public function jadwals()
    {
        return $this->hasMany('App\Models\KursusJadwal', 'id_kursus');
    }

    public function siswas()
    {
        return $this->hasMany('App\Models\KursusSiswa', 'id_kursus');
    }

    public function ulasans()
    {
        return $this->hasMany('App\Models\KursusUlasan', 'id_kursus');
    }

    public function transaksi()
    {
        return $this->hasOne('App\Models\Transaksi', 'id_produk')->where('id_user', auth()->user()->id);
    }

    public function getTotalSiswaAttribute()
    {
        return $this->hasMany('App\Models\KursusSiswa', 'id_kursus')->whereIn('status_dikelas', ['Bergabung', 'Sesi Selesai'])->count();
    }

    public function rerata_ulasan()
    {
        return $this->ulasans()
            ->selectRaw('avg(nilai) as aggregate, id_kursus')
            ->groupBy('id_kursus');
    }

    public function getRerataUlasanAttribute()
    {
        if (!array_key_exists('rerata_ulasan', $this->relations)) {
            $this->load('rerata_ulasan');
        }

        $relation = $this->getRelation('rerata_ulasan')->first();

        return ($relation) ? round($relation->aggregate, 2) : 0;
    }

    public function getData($flag = '', $data, $convertLabel = '')
    {
        $limit = isset($data['paginate']) ?  $data['paginate'] : 10;
        $keyword = isset($data['q']) ? $data['q'] : '';
        switch ($flag) {
            case 'paginationData':
                $where = [];
                // $where['menerima_peserta'] = 1;
                // $where['status_verifikasi'] = 1;
                if (isset($data['status']) && !empty($data['status'])) {
                    $where['menerima_peserta'] = $data['status'];
                }
                if (isset($data['verifikasi']) && !empty($data['verifikasi'])) {
                    $where['status_verifikasi'] = $data['verifikasi'];
                }
                if (isset($data['id_tentor']) && !empty($data['id_tentor'])) {
                    $where['id_tentor'] = $data['id_tentor'];
                }
                if (isset($data['model_belajar']) && !empty($data['model_belajar'])) {
                    $where['model_belajar'] = $data['model_belajar'];
                }
                if (isset($data['id_provinsi']) && !empty($data['id_provinsi'])) {
                    $where['id_provinsi'] = $data['id_provinsi'];
                }
                if (isset($data['id_kota']) && !empty($data['id_kota'])) {
                    $where['id_kota'] = $data['id_kota'];
                }
                if (isset($data['id_kecamatan']) && !empty($data['id_kecamatan'])) {
                    $where['id_kecamatan'] = $data['id_kecamatan'];
                }
                $result = $this
                    ->with([
                        'tentor:id_teacher,id_level,nama_lengkap,foto,nama_kota,nama_kecamatan',
                        'tentor.level:id,nama_level',
                        'mapel:id,nama_mapel',
                        'jenjang:id,nama_jenjang',
                        // 'kelas:id,id_jenjang,nama_kelas',
                        // 'kelas.jenjang:id,nama_jenjang',
                        'penjurusan:id,nama_penjurusan,id_kelas',
                        'penjurusan.kelas:id,id_jenjang,nama_kelas',
                        'penjurusan.kelas.jenjang:id,nama_jenjang',
                        'transaksi' => function ($query) {
                            return $query->selectRaw('transaksis.id, id_produk, kode, status, status_dikelas')
                                ->join('kursus_siswa', 'kursus_siswa.referensi', '=', 'transaksis.kode', 'left')
                                ->where('jenis_transaksi', 'Les Privat')
                                ->where('id_user', $this->getUserId())
                                // ->whereIn('status_dikelas', ['Pending', 'Bergabung', 'Ditolak', 'Menunggu Konfirmasi Selesai'])
                                ->orderBy('transaksis.created_at', 'DESC');
                        }
                    ])
                    ->where(function ($query) use ($keyword, $data) {
                        return $query
                            ->where('model_belajar', 'like', '%' . @$data['model_belajar'] . '%')
                            ->where('nama_kursus', 'like', '%' . $keyword . '%')
                            ->orWhere('harga_kursus', 'like', '%' . $keyword . '%')
                            ->orWhere('batas_peserta', 'like', '%' . $keyword . '%')
                            ->orWhereHas('tentor', function ($query) use ($keyword) {
                                $query->where('nama_lengkap', 'like', '%' . $keyword . '%');
                            });
                    })
                    ->whereHas('mapel', function ($query) use ($keyword, $data) {
                        $query->where('id', 'like', '%' . @$data['id_mapel'] . '%');
                    })
                    ->whereHas('jenjang', function ($query) use ($keyword, $data) {
                        $query->where('id', 'like', '%' . @$data['id_jenjang'] . '%');
                    });
                $result = $result->where($where);
                $result = $result->orderByDesc('created_at');
                $result = $result->paginate($limit);
                break;
            case 'ownByStudentPaginationData':
                $where = [];
                if (isset($data['status']) && !empty($data['status'])) {
                    $where['menerima_peserta'] = $data['status'];
                }
                if (isset($data['verifikasi']) && !empty($data['verifikasi'])) {
                    $where['status_verifikasi'] = $data['verifikasi'];
                }
                $result = $this
                    ->with([
                        'tentor:id_teacher,nama_lengkap,foto,nama_kota,nama_kecamatan',
                        'tentor.level:id,nama_level',
                        'mapel:id,nama_mapel',
                        'jenjang:id,nama_jenjang',
                        'penjurusan:id,nama_penjurusan,id_kelas',
                        'penjurusan.kelas:id,id_jenjang,nama_kelas',
                        'penjurusan.kelas.jenjang:id,nama_jenjang',
                        'transaksi' => function ($query) {
                            // return $query->selectRaw('transaksis.id, id_produk, kode, status')
                            return $query->selectRaw('transaksis.id, id_produk, kode, status, status_dikelas')
                                ->join('kursus_siswa', 'kursus_siswa.referensi', '=', 'transaksis.kode', 'left')
                                // ->whereIn('status', ['Sudah Diverifikasi'])
                                // ->whereNotIn('status_dikelas', ['Sesi Selesai'])
                                ->where('jenis_transaksi', 'Les Privat')->where('id_user', $this->getUserId())
                                ->orderBy('transaksis.created_at', 'DESC');
                        }
                    ])
                    ->where(function ($query) use ($keyword) {
                        return $query
                            ->where('nama_kursus', 'like', '%' . $keyword . '%')
                            ->orWhere('harga_kursus', 'like', '%' . $keyword . '%')
                            ->orWhere('batas_peserta', 'like', '%' . $keyword . '%')
                            ->orWhereHas('tentor', function ($query) use ($keyword) {
                                $query->where('nama_lengkap', 'like', '%' . $keyword . '%');
                            });
                    });
                $result = $result->where($where)
                    ->has('transaksi', '>=', 1, 'and', function ($query) {
                        return $query->join('kursus_siswa', 'kursus_siswa.referensi', '=', 'transaksis.kode', 'left')
                            ->where('id_user', $this->getUserId())
                            ->where('jenis_transaksi', 'Les Privat')
                            ->whereIn('status', ['Sudah Diverifikasi'])
                            ->whereNotIn('status_dikelas', ['Sesi Selesai'])
                            ->orderBy('transaksis.created_at', 'DESC');
                    });
                $result = $result->orderByDesc('created_at');
                $result = $result->paginate($limit);
                break;
            case 'detailData':
                $result = $this->with([
                    'tentor:id_teacher,nama_lengkap,id_level,rate_mengajar,nomor_telephone',
                    'tentor.level:id,nama_level',
                    'mapel:id,nama_mapel',
                    'jenjang:id,nama_jenjang',
                    'penjurusan:id,nama_penjurusan,id_kelas',
                    'penjurusan.kelas:id,id_jenjang,nama_kelas',
                    'penjurusan.kelas.jenjang:id,nama_jenjang',
                    'jadwals:id,id_kursus,hari_jadwal,jam_mulai_jadwal,jam_akhir_jadwal,is_tutup',
                    'transaksi' => function ($query) {
                        // return $query->where('jenis_transaksi', 'Les Privat');
                        return $query->selectRaw('transaksis.id, id_produk, kode, status, status_dikelas')
                            ->join('kursus_siswa', 'kursus_siswa.referensi', '=', 'transaksis.kode', 'left')
                            // ->whereIn('status_dikelas', ['', 'Pending', 'Bergabung', 'Ditolak', 'Menunggu Konfirmasi Selesai'])
                            ->where('jenis_transaksi', 'Les Privat')
                            ->where('id_user', $this->getUserId())
                            ->orderBy('transaksis.created_at', 'DESC');
                    }
                ])
                    ->find($data['id']);
                break;
            case 'dataCoursePriceOption':
                $harga = [
                    [
                        'text' => 'Rp 50.000 / 90 mnt',
                        'value' => 50000,
                    ]
                ];
                $teacher = Teacher::where('id_teacher', auth()->user()->id)->first();
                if ($teacher) {
                    switch ($teacher->nama_level) {
                        case 'SUPER':
                            array_push($harga,  [
                                'text' => 'Rp 75.000 / 90 mnt',
                                'value' => 75000,
                            ]);
                            break;
                        case 'ELITE':
                            array_push($harga,  [
                                'text' => 'Rp 75.000 / 90 mnt',
                                'value' => 75000,
                            ]);
                            array_push($harga,  [
                                'text' => 'Rp 125.000 / 90 mnt',
                                'value' => 125000,
                            ]);
                            break;
                        case 'MASTER':
                            array_push($harga,  [
                                'text' => 'Rp 75.000 / 90 mnt',
                                'value' => 75000,
                            ]);
                            array_push($harga,  [
                                'text' => 'Rp 125.000 / 90 mnt',
                                'value' => 125000,
                            ]);
                            array_push($harga,  [
                                'text' => 'Rp 200.000 / 90 mnt',
                                'value' => 200000,
                            ]);
                            break;
                        case 'LEGEND':
                            array_push($harga,  [
                                'text' => 'Rp 75.000 / 90 mnt',
                                'value' => 75000,
                            ]);
                            array_push($harga,  [
                                'text' => 'Rp 125.000 / 90 mnt',
                                'value' => 125000,
                            ]);
                            array_push($harga,  [
                                'text' => 'Rp 200.000 / 90 mnt',
                                'value' => 200000,
                            ]);
                            array_push($harga,  [
                                'text' => 'Rp 350.000 / 90 mnt',
                                'value' => 350000,
                            ]);
                            break;
                        default:
                            $harga = [
                                [
                                    'text' => 'Rp 50.000 / 90 mnt',
                                    'value' => 50000,
                                ]
                            ];
                            break;
                    }
                }
                // end if check teacher level
                $result = $harga;
                break;
            case 'kelasLainData':
                $result = $this->with(['tentor'])->where('harga_kursus', $data['harga_kursus'])
                    ->where('status_verifikasi', 1)
                    ->where('menerima_peserta', 1)
                    ->whereNotIn('id', [$data['id']])->get();
                // $result = $this->with(['tentor'])->get();
                break;
            default:
                $result = $data;
                break;
        }
        if (!empty($convertLabel)) {
            $result = $this->convertData($result, $convertLabel);
        }
        return $result;
    }

    public function convertData($data, $label = '')
    {
        $result = [];
        switch ($label) {
            case 'paginationData':
                $dataArray = $data->toArray();
                $result['data'] = [];
                foreach ($data as $key => $row) {
                    $dataRow = [];
                    $dataRow = array_merge($row->toArray(), ['rerata_ulasan' => $row->rerata_ulasan]);
                    array_push($result['data'], $dataRow);
                }
                $result['current_page'] = $dataArray['current_page'];
                $result['first_page_url'] = $dataArray['first_page_url'];
                $result['from'] = $dataArray['from'];
                $result['last_page'] = $dataArray['last_page'];
                $result['last_page_url'] = $dataArray['last_page_url'];
                $result['links'] = $dataArray['links'];
                $result['next_page_url'] = $dataArray['next_page_url'];
                $result['path'] = $dataArray['path'];
                $result['per_page'] = $dataArray['per_page'];
                $result['prev_page_url'] = $dataArray['prev_page_url'];
                $result['to'] = $dataArray['to'];
                $result['total'] = $dataArray['total'];
                break;
            case 'studentPaginationData':
                $dataArray = $data->toArray();
                $result['data'] = [];

                $kursusSiswaModel = new KursusSiswa();
                foreach ($data as $key => $row) {
                    $dataRow = [];
                    $dataRow = array_merge($row->toArray(), ['rerata_ulasan' => $row->rerata_ulasan]);

                    // print_r($row->transaksi->kode);
                    if ($row->transaksi != NULL) {
                        $kursusSiswaData = $kursusSiswaModel->getData('detailDataByReferensi', ['referensi' => $row->transaksi->kode]);
                        if (!empty($kursusSiswaData)) {
                            $dataRow['status_dikelas'] = $kursusSiswaData->status_dikelas;
                        } else {
                            $dataRow['status_dikelas'] = NULL;
                        }
                    } else {
                        $dataRow['status_dikelas'] = NULL;
                    }

                    array_push($result['data'], $dataRow);
                }
                $result['current_page'] = $dataArray['current_page'];
                $result['first_page_url'] = $dataArray['first_page_url'];
                $result['from'] = $dataArray['from'];
                $result['last_page'] = $dataArray['last_page'];
                $result['last_page_url'] = $dataArray['last_page_url'];
                $result['links'] = $dataArray['links'];
                $result['next_page_url'] = $dataArray['next_page_url'];
                $result['path'] = $dataArray['path'];
                $result['per_page'] = $dataArray['per_page'];
                $result['prev_page_url'] = $dataArray['prev_page_url'];
                $result['to'] = $dataArray['to'];
                $result['total'] = $dataArray['total'];
                break;
            case 'detailData':
                if ($data !== NULL) {
                    $result = $data->toArray();
                    $result['total_siswa'] = $data->total_siswa;
                    $result['rerata_ulasan'] = $data->rerata_ulasan;
                    $result['harga_kursus_label'] = $this->formatRupiah($data['harga_kursus']);
                } else {
                    $result = NULL;
                }
                break;
            default:
                $result = $data;
                break;
        }
        return $result;
    }

    public function insertData($flag = '', $data)
    {
        $result = false;
        switch ($flag) {
            case 'insertData':
                DB::beginTransaction();
                try {
                    $kursus = $this->create([
                        'nama_kursus'   => $data['nama_kursus'],
                        'deskripsi_kursus' => $data['deskripsi_kursus'] ?? '',
                        'video_kursus'  => $data['video_kursus'] ?? '-',
                        'sampul_kursus' => $data['sampul_kursus'] ?? '',
                        'file_kursus' => $data['file_kursus'] ?? '',
                        'id_tentor'     => $data['id_tentor'],
                        'id_mapel'      => $data['id_mapel'],
                        'id_jenjang'    => $data['id_jenjang'],
                        'id_penjurusan' => $data['id_penjurusan'],
                        'harga_kursus'  => $data['harga_kursus'],
                        'model_belajar'  => $data['model_belajar'],
                        'alamat'  => $data['alamat'],
                        'id_provinsi'  => $data['id_provinsi'],
                        'nama_provinsi'  => $data['nama_provinsi'],
                        'id_kota'  => $data['id_kota'],
                        'nama_kota'  => $data['nama_kota'],
                        'id_kecamatan'  => $data['id_kecamatan'],
                        'nama_kecamatan'  => $data['nama_kecamatan'],
                        'menerima_peserta' => 1,
                        'status_verifikasi' => 0,
                    ]);
                    DB::commit();
                    return $kursus;
                } catch (\Exception $e) {
                    DB::rollback();
                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
            case 'insertDataWithJadwal':
                DB::beginTransaction();
                try {
                    $kursus = $this->create([
                        'nama_kursus'   => $data['nama_kursus'],
                        'deskripsi_kursus' => $data['deskripsi_kursus'] ?? '',
                        'video_kursus'  => $data['video_kursus'] ?? '-',
                        'sampul_kursus' => $data['sampul_kursus'] ?? '',
                        'file_kursus' => $data['file_kursus'] ?? '',
                        'id_tentor'     => $data['id_tentor'],
                        'id_mapel'      => $data['id_mapel'],
                        'id_jenjang'    => $data['id_jenjang'],
                        'id_penjurusan' => $data['id_penjurusan'],
                        'harga_kursus'  => $data['harga_kursus'],
                        'model_belajar'  => $data['model_belajar'],
                        'alamat'  => $data['alamat'],
                        'id_provinsi'  => $data['id_provinsi'],
                        'nama_provinsi'  => $data['nama_provinsi'],
                        'id_kota'  => $data['id_kota'],
                        'nama_kota'  => $data['nama_kota'],
                        'id_kecamatan'  => $data['id_kecamatan'],
                        'nama_kecamatan'  => $data['nama_kecamatan'],
                        'menerima_peserta' => 1,
                        'status_verifikasi' => 0,
                    ]);

                    if (isset($data['jadwals'])) {
                        $kursusJadwal = new KursusJadwal();
                        // $kursus = $kursus->toArray();
                        // $kursus['jadwal'] = [];
                        foreach ($data['jadwals'] as $key => $jadwal) {
                            $jadwal = $kursusJadwal->create([
                                'id_kursus' => $kursus['id'],
                                'hari_jadwal' => $jadwal['hari_jadwal'],
                                'jam_mulai_jadwal' => $jadwal['jam_mulai_jadwal'] ?? '00:00',
                                'jam_akhir_jadwal' => $jadwal['jam_akhir_jadwal'] ?? '00:00',
                                'is_tutup' => $jadwal['is_tutup'] ?? 0,
                            ]);
                            // array_push($kursus['jadwal'], $jadwal->toArray());
                        }
                    }

                    DB::commit();
                    return $kursus;
                } catch (\Exception $e) {
                    DB::rollback();
                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
        }
        return $result;
    }

    public function updateData($flag = '', $data)
    {
        $result = false;
        switch ($flag) {
            case 'updateData':
                DB::beginTransaction();
                try {

                    $kursus = $this->find($data['id']);

                    $update = $kursus->update([
                        'nama_kursus'   => $data['nama_kursus'],
                        'deskripsi_kursus' => $data['deskripsi_kursus'],
                        'video_kursus'  => $data['video_kursus'],
                        'sampul_kursus' => $data['sampul_kursus'],
                        'file_kursus' => $data['file_kursus'],
                        'id_tentor'     => $data['id_tentor'],
                        'id_mapel'      => $data['id_mapel'],
                        'id_jenjang'      => $data['id_jenjang'],
                        'id_penjurusan'      => $data['id_penjurusan'],
                        'harga_kursus'  => $data['harga_kursus'],
                        'alamat'  => $data['alamat'],
                        'id_provinsi'  => $data['id_provinsi'],
                        'nama_provinsi'  => $data['nama_provinsi'],
                        'id_kota'  => $data['id_kota'],
                        'nama_kota'  => $data['nama_kota'],
                        'id_kecamatan'  => $data['id_kecamatan'],
                        'nama_kecamatan'  => $data['nama_kecamatan'],
                    ]);

                    DB::commit();

                    return $this->find($data['id']);
                } catch (\Exception $e) {
                    DB::rollback();

                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
            case 'toggleActive':
                $update = $this->find($data['id'])->update(['menerima_peserta' => $data['status']]);
                if ($update) {
                    return $this->find($data['id']);
                } else {
                    return [
                        'error' => true,
                        'message' => 'Update failed!'
                    ];
                }
                break;
            case 'toggleVerification':
                $update = $this->find($data['id'])->update(['status_verifikasi' => $data['status']]);
                if ($update) {
                    return $this->find($data['id']);
                } else {
                    return [
                        'error' => true,
                        'message' => 'Update failed!'
                    ];
                }
                break;
        }
        return $result;
    }

    public function deleteData($flag = '', $data)
    {
        $result = false;
        switch ($flag) {
            case 'deleteData':
                DB::beginTransaction();
                try {

                    $kursus = $this->find($data['id']);
                    $kursus->update(['menerima_peserta' => 0]);

                    $delete = $kursus->delete();
                    DB::commit();
                    return $kursus;
                } catch (\Exception $e) {
                    DB::rollback();

                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
        }
        return $result;
    }
}
