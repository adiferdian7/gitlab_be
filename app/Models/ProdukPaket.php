<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ProdukPaket extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function tryout(){
    	return $this->belongsTo('App\Models\Tryout', 'id_tryout');
    }

    public function produk(){
    	return $this->belongsTo('App\Models\Produk', 'id_produk');

    }

   
}
