<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use HasFactory, SoftDeletes
    ;
    protected $primaryKey = 'id_siswa';
    protected $guarded = [];
     protected $dates = ['deleted_at'];

    public function parent(){
    	return $this->hasOne('App\Models\OrangTua', 'id_orang_tua');
    }

    public function penjurusan(){
    	return $this->belongsTo('App\Models\Penjurusan', 'id_penjurusan');
    }

    public function prodi_satu() {
        return $this->hasOne('App\Models\ProgramStudiPerguruanTinggi', 'id', 'id_prodi_bind_perguruan');
    }

    public function prodi_dua() {
        return $this->hasOne('App\Models\ProgramStudiPerguruanTinggi', 'id', 'id_prodi_bind_perguruan_2');
    }
}
