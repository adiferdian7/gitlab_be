<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SuperAdmin extends Model
{
    use HasFactory, SoftDeletes;
    
    protected $primaryKey = 'id_super_admin';
    protected $table = 'super_admins';
    protected $guarded = [];

    protected $dates = ['deleted_at'];
}
