<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Penarikan extends BaseModel
{
    use HasFactory;

    // protected $guarded = [];
    protected $fillable = ['kode', 'id_tentor', 'nominal_penarikan', 'status_penarikan', 'bukti_transfer', 'nama_rekening', 'nomor_rekening', 'bank_rekening'];

    public function tentor()
    {
        return $this->belongsTo('App\Models\Teacher', 'id_tentor', 'id_teacher');
    }

    public function getData($flag = '', $data, $convertLabel = '')
    {
        $limit = isset($data['paginate']) ?  $data['paginate'] : 10;
        $keyword = isset($data['q']) ? $data['q'] : '';
        $sortBy = isset($data['sortBy']) ? $data['sortBy'] : 'created_at';
        $sortDir = isset($data['sortDir']) ? $data['sortDir'] : 'desc';
        switch ($flag) {
            case 'data':
                $result = $this->all();
            case 'dataByTentor':
                $result = $this->where('id_tentor', $this->getUserId());
                if (isset($data['status_penarikan']) && !empty($data['status_penarikan'])) {
                    $result = $this->where('status_penarikan', $data['status_penarikan']);
                }
                $result = $result->get();
                break;
            case 'paginationData':
                $where = [];
                $query = $this;
                if ($this->getUser()->role_user == 'teacher') {
                    // $result = $this->where('id_tentor', $this->getUserId());
                    $where['id_tentor'] = $this->getUserId();
                } else if (isset($data['id_tentor']) && !empty($data['id_tentor'])) {
                    // $result = $this->where('id_tentor', $data['id_tentor']);
                    $where['id_tentor'] = $data['id_tentor'];
                } else {
                   $query = $query->with(['tentor:id_teacher,nama_lengkap,foto,email,verifikasi', 'tentor.user:id,username']);
                }
                if (isset($data['status_penarikan']) && !empty($data['status_penarikan'])) {
                    $where['status_penarikan'] = $data['status_penarikan'];
                }
                $query = $query->where($where)->orderBy($sortBy, $sortDir);
                $result = $query->paginate($limit);
                break;
            case 'dataByTentorPerBulan':
                $result = $this->where('id_tentor', $data['id_tentor']);
                $result = $result->groupBy('bulan')->get(
                    [DB::raw('nominal_penarikan as penarikan'), DB::raw('MONTH(created_at) as bulan')]
                );
                break;
            case 'detailDataByReferensi':
                $result = $this->where('kode_referensi', $data['kode_referensi'])->first();
                break;
            default:
                $result = null;
                break;
        }
        if (!empty($convertLabel)) {
            $result = $this->convertData($result, $convertLabel);
        }
        return $result;
    }

    public function convertData($data, $convertLabel = '')
    {
        $result = [];

        switch ($convertLabel) {
            case 'paginationData':
                $dataArray = $data->toArray();
                $result['data'] = [];
                foreach ($data as $dataRow) {
                    array_push($result['data'], $dataRow);
                }
                $result['current_page'] = $dataArray['current_page'];
                $result['first_page_url'] = $dataArray['first_page_url'];
                $result['from'] = $dataArray['from'];
                $result['last_page'] = $dataArray['last_page'];
                $result['last_page_url'] = $dataArray['last_page_url'];
                $result['links'] = $dataArray['links'];
                $result['next_page_url'] = $dataArray['next_page_url'];
                $result['path'] = $dataArray['path'];
                $result['per_page'] = $dataArray['per_page'];
                $result['prev_page_url'] = $dataArray['prev_page_url'];
                $result['to'] = $dataArray['to'];
                $result['total'] = $dataArray['total'];
                break;
            default:
                $result = $data;
                break;
        }

        return $result;
    }

    public function insertData($flag = '', $data)
    {
        $result = false;
        switch ($flag) {
            case 'insertData':
                DB::beginTransaction();
                try {
                    $pendapatan = $this->create([
                        'id_tentor'   => $data['id_tentor'],
                        'kode'   => $data['kode'],
                        'nominal_penarikan'   => $data['nominal_penarikan'],
                        'status_penarikan'   => $data['status_penarikan'] ?? 'Pending',
                        'bukti_transfer'   => $data['bukti_transfer'] ?? null,
                        'nama_rekening'   => $data['nama_rekening'],
                        'bank_rekening'   => $data['bank_rekening'],
                        'nomor_rekening'   => $data['nomor_rekening'],
                    ]);
                    DB::commit();
                    return $pendapatan;
                } catch (\Exception $e) {
                    DB::rollback();
                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
        }
        return $result;
    }

    public function updateData($flag = '', $data)
    {
        $result = false;
        switch ($flag) {
            case 'updateData':
                DB::beginTransaction();
                try {

                    $dataUpdate = [];

                    foreach ($this->fillable as $field) {
                        if (isset($data[$field]) && !empty($data[$field])) {
                            $dataUpdate[$field] = $data[$field];
                        }
                    }

                    $penarikan = $this->find($data['id']);
                    $update = $penarikan->update($dataUpdate);

                    DB::commit();

                    return $this->find($data['id']);
                } catch (\Exception $e) {
                    DB::rollback();

                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
            case 'updateStatus':
                DB::beginTransaction();
                try {

                    $penarikan = $this->find($data['id']);
                    $update = $penarikan->update([
                        'status_penarikan'   => $data['status_penarikan'],
                    ]);

                    DB::commit();

                    return $this->find($data['id']);
                } catch (\Exception $e) {
                    DB::rollback();

                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
        }
        return $result;
    }
}
