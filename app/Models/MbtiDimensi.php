<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MbtiDimensi extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function dimensi(){
    	return $this->belongsTo('App\Models\Dimensi', 'id_dimensi');
    }

    public function pertanyaan(){
    	return $this->hasMany('App\Models\MbtiPertanyaan', 'id_mbti_dimensi');
    }
}
