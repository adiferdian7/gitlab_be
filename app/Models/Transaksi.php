<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaksi extends Model
{
    use HasFactory, SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $guarded = [];

    public function user(){
    	return $this->belongsTo('App\Models\User', 'id_user');
    }

    public function produk(){
    	return $this->belongsTo('App\Models\Produk', 'id_produk');
    }

    public function mbti()
    {
        return $this->belongsTo('App\Models\TbMbti', 'id_produk');
    }

    public function bundling(){
    	return $this->belongsTo('App\Models\Bundling', 'id_produk');
    }

    public function kursus()
    {
        return $this->belongsTo('App\Models\Kursus', 'id_produk');
    }

    public function material()
    {
        return $this->belongsTo('App\Models\Material', 'id_produk');
    }

    public function bank(){
    	return $this->belongsTo('App\Models\Bank', 'id_bank');
    }

    public function detail_xendit(){
        return $this->hasOne('App\Models\TabelXendit', 'id_transaksi');
    }
}
