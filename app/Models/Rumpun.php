<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rumpun extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];
    protected $table = 'rumpun';
    protected $dates = ['deleted_at'];

    // public function kategori()
    // {
    //     return $this->belongsTo('App\Models\KategoriTo', 'id_to_kategori');
    // }

    public function klaster()
    {
        return $this->hasMany('App\Models\Klaster', 'id_rumpun');
    }

    public function mapel()
    {
        return $this->hasManyThrough('App\Models\Mapel', 'App\Models\Klaster', 'id_rumpun', 'id');
    }
}
