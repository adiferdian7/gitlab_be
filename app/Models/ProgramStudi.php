<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ProgramStudi extends Model
{
    use HasFactory, SoftDeletes;
    
    protected $table = 'program_studi';
    protected $guarded = [];

    protected $dates = ['deleted_at'];

    public function mapel(){
    	return $this->belongsTo('App\Models\Mapel', 'id_mapel');
    }

    public function penjurusan(){
    	return $this->belongsTo('App\Models\Penjurusan', 'id_penjurusan');
    }

    public function listperguruan(){
        return $this->hasMany('App\Models\ProgramStudiPerguruanTinggi', 'id_program_studi')->orderBy('id', 'asc');
    }

    public function list_mapel(){
        return $this->hasMany('App\Models\ProgramStudiBindMapel', 'id_program_studi');
    }
}
