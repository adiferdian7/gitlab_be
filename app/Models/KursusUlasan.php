<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;

class KursusUlasan extends BaseModel
{
  use HasFactory;

  protected $table = 'kursus_ulasan';
  protected $guarded = [];

  private $tableAlias = [
    'id_kursus' => 'id_kursus',
    'id_siswa' => 'id_siswa',
    'nilai' => 'nilai',
    'ulasan' => 'ulasan',
    'privasi' => 'privasi',
  ];

  public function kursus()
  {
    return $this->belongsTo('App\Models\Kursus', 'id_kursus');
  }

  public function siswa()
  {
    return $this->belongsTo('App\Models\Student', 'id_siswa');
  }

  public function getData($flag = '', $data, $convertLabel = '')
  {
    $limit = isset($data['paginate']) ?  $data['paginate'] : 10;
    $keyword = isset($data['q']) ? $data['q'] : '';
    $sortBy = isset($data['sortBy']) ? $data['sortBy'] : 'created_at';
    $sortDir = isset($data['sortDir']) ? $data['sortDir'] : 'desc';
    switch ($flag) {
      case 'paginationData':

        $where = [];
        if (isset($data['id_kursus']) && !empty($data['id_kursus'])) {
          $where['id_kursus'] = $data['id_kursus'];
        }
        if (isset($data['nilai']) && !empty($data['nilai'])) {
          $where['nilai'] = $data['nilai'];
        }

        $result = $this
          ->with([
            'kursus:id,nama_kursus,id_tentor',
            'kursus.tentor:id_teacher,foto',
            'siswa:id_siswa,nama_lengkap,foto'
          ])
          ->where(function ($query) use ($keyword) {
            return $query
              ->where('ulasan', 'like', '%' . $keyword . '%')
              ->orWhere('nilai', 'like', '%' . $keyword . '%')
              ->orWhereHas('kursus', function ($query) use ($keyword) {
                $query->where('nama_kursus', 'like', '%' . $keyword . '%');
              })
              ->orWhereHas('siswa', function ($query) use ($keyword) {
                $query->where('nama_lengkap', 'like', '%' . $keyword . '%');
              });
          });

        if (isset($data['id_tentor']) && !empty($data['id_tentor'])) {
          $result = $result->whereHas('kursus.tentor', function ($query) use ($data) {
            return $query->where('id_teacher', $data['id_tentor']);
          });
        }

        $result = $result->where($where);
        $result = $result->orderBy($sortBy, $sortDir);
        $result = $result->paginate($limit);
        break;
      case 'detailData':
        $result = $this->with(['kursus:id,nama_kursus,id_tentor', 'kursus.tentor:id_teacher,nama_lengkap'])
          ->find($data['id']);
        break;
      default:
        $result = null;
        break;
    }
    if (!empty($convertLabel)) {
      $result = $this->convertData($result, $convertLabel);
    }
    return $result;
  }

  public function convertData($data, $label = '')
  {
    $result = [];
    switch ($label) {
      case 'paginationData':
        $result = $data->toArray();
        $data = $result['data'];
        $result['data'] = [];
        foreach ($data as $key => $row) {
          array_push($result['data'], [
            'id' => $row['id'],
            'id_kursus' => $row['id_kursus'],
            'id_siswa' => $row['id_siswa'],
            'nilai' => $row['nilai'] * 1,
            'ulasan' => $row['ulasan'],
            'privasi' => $row['privasi'],
            'kursus' => @$row['kursus']['nama_kursus'],
            'pengulas' => @$row['siswa']['nama_lengkap'],
            'foto_pengulas' => @$row['siswa']['foto'],
            'tanggal' => $row['created_at'],
            'balasan' => $row['balasan'],
            'tanggal_balasan' => $row['tanggal_balasan'],
            'foto_balasan' => @$row['kursus']['tentor']['foto'],
          ]);
        }
        break;
      case 'memberPaginationData':
        $result = $data->toArray();
        $data = $result['data'];
        $result['data'] = [];
        foreach ($data as $key => $row) {
          $nama_siswa = @$row['siswa']['nama_lengkap'];
          $pengulas = '';
          if ($row['privasi'] == 'Anonim') {
            $lastIndex = strlen($nama_siswa);
            $pengulas = $nama_siswa[0];
            for ($i = 0; $i < $lastIndex; $i++) {
              $pengulas .= '*';
            }
            $pengulas .= $nama_siswa[$lastIndex - 1];
          } else {
            $pengulas = $nama_siswa;
          }

          array_push($result['data'], [
            'id' => $row['id'],
            'id_kursus' => $row['id_kursus'],
            'id_siswa' => $row['id_siswa'],
            'nilai' => $row['nilai'] * 1,
            'ulasan' => $row['ulasan'],
            'privasi' => $row['privasi'],
            'kursus' => @$row['kursus']['nama_kursus'],
            'pengulas' => $pengulas,
            'foto_pengulas' => @$row['siswa']['foto'],
            'tanggal' => $row['created_at'],
            'balasan' => $row['balasan'],
            'tanggal_balasan' => $row['tanggal_balasan'],
            'foto_balasan' => @$row['kursus']['tentor']['foto'],
          ]);
        }
        break;
      case 'detailData':
        $result = $data;
        $nama_siswa = @$data['siswa']['nama_lengkap'];
        $pengulas = '';
        if ($data['privasi'] == 'Anonim') {
          $lastIndex = strlen($nama_siswa);
          $pengulas = $nama_siswa[0];
          for ($i = 0; $i < $lastIndex; $i++) {
            $pengulas .= '*';
          }
          $pengulas .= $nama_siswa[$lastIndex - 1];
        } else {
          $pengulas = $nama_siswa;
        }
        $result['pengulas'] = $pengulas;
        unset($result['kursus']);
        unset($result['siswa']);
        break;
      default:
        $result = $data;
        break;
    }
    return $result;
  }

  public function insertData($flag = '', $data)
  {
    $result = false;
    switch ($flag) {
      case 'insertData':
        DB::beginTransaction();
        try {
          $kursus = $this->create([
            'id_kursus'   => $data['id_kursus'],
            'id_siswa'  => $data['id_siswa'],
            'nilai'  => $data['nilai'],
            'ulasan'  => $data['ulasan'] ?? '',
            'privasi'  => $data['privasi'] ?? 'Publik',
          ]);
          DB::commit();
          return $kursus;
        } catch (\Exception $e) {
          DB::rollback();
          return [
            'error' => true,
            'message' => $e->getMessage()
          ];
        }
        break;
    }
    return $result;
  }

  public function updateData($flag = '', $data)
  {
    $result = false;
    switch ($flag) {
      case 'updateData':
        DB::beginTransaction();
        try {

          $dataUpdate = [];

          unset($this->tableAlias['id_kursus']);
          unset($this->tableAlias['id_siswa']);

          foreach ($this->tableAlias as $alias => $field) {
            if (isset($data[$alias])) {
              $dataUpdate[$field] = $data[$alias];
            }
          }

          $ulasan = $this->find($data['id']);
          $update = $ulasan->update($dataUpdate);

          DB::commit();

          return $this->getData('detailData', $data, 'detailData');
        } catch (\Exception $e) {
          DB::rollback();

          return [
            'error' => true,
            'message' => $e->getMessage()
          ];
        }
        break;
      case 'tentorReplyUlasan':
        DB::beginTransaction();
        try {

          $ulasan = $this->find($data['id']);


          $update = $ulasan->update([
            'balasan' => $data['balasan'],
            'tanggal_balasan' => ($ulasan->tanggal_balasan) ?? date("Y-m-d H:i:s")
          ]);

          DB::commit();

          return $this->find($data['id']);
        } catch (\Exception $e) {
          DB::rollback();

          return [
            'error' => true,
            'message' => $e->getMessage()
          ];
        }
        break;
    }
    return $result;
  }

  public function deleteData($flag = '', $data)
  {
    $result = false;
    switch ($flag) {
      case 'deleteData':
        DB::beginTransaction();
        try {
          $materi = $this->find($data['id']);
          $delete = $materi->delete();
          DB::commit();
          return $materi;
        } catch (\Exception $e) {
          DB::rollback();

          return [
            'error' => true,
            'message' => $e->getMessage()
          ];
        }
        break;
    }
    return $result;
  }
}
