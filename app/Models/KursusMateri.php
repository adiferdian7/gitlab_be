<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;

class KursusMateri extends BaseModel
{
    use HasFactory;

    protected $table = 'kursus_materi';
    protected $guarded = [];

    public function kursus()
    {
        return $this->belongsTo('App\Models\Kursus', 'id_kursus');
    }

    public function getData($flag = '', $data, $convertLabel = '')
    {
        $limit = isset($data['paginate']) ?  $data['paginate'] : 10;
        $keyword = isset($data['q']) ? $data['q'] : '';
        switch ($flag) {
            case 'paginationData':
                $result = $this
                    ->with(['kursus:id,nama_kursus'])
                    ->where('judul_materi', 'like', '%' . $keyword . '%')
                    ->orWhere('deskripsi_materi', 'like', '%' . $keyword . '%')
                    ->orWhere('link_file_materi', 'like', '%' . $keyword . '%')
                    ->orWhereHas('kursus', function ($query) use ($keyword) {
                        $query->where('nama_kursus', 'like', '%' . $keyword . '%');
                    });

                $where = [];
                if (isset($data['id_kursus']) && !empty($data['id_kursus'])) {
                    $where['id_kursus'] = $data['id_kursus'];
                }

                $result = $result->orderByDesc('created_at');
                $result = $result->where($where);
                $result = $result->paginate($limit);
                break;
            case 'detailData':
                $result = $this->with(['kursus:id,nama_kursus'])
                    ->find($data['id']);
                break;
            case 'data':
                $where = [];
                if (isset($data['id_kursus']) && !empty($data['id_kursus'])) {
                    $where['id_kursus'] = $data['id_kursus'];
                }
                $result = $this
                    ->with(['kursus:id,nama_kursus'])
                    ->where($where)->get();
                break;
            default:
                $result = null;
                break;
        }
        if (!empty($convertLabel)) {
            $result = $this->convertData($result, $convertLabel);
        }
        return $result;
    }

    public function convertData($data, $label = '')
    {
        $result = [];
        switch ($label) {
            case 'detailData':
                $result = $data;
                break;
            default:
                $result = $data;
                break;
        }
        return $result;
    }

    public function insertData($flag = '', $data)
    {
        $result = false;
        switch ($flag) {
            case 'insertData':
                DB::beginTransaction();
                try {
                    $kursus = $this->create([
                        'id_kursus'   => $data['id_kursus'],
                        'judul_materi'  => $data['judul_materi'],
                        'deskripsi_materi'  => $data['deskripsi_materi'] ?? '',
                        'link_file_materi'  => $data['link_file_materi'],
                        'link_video_materi'  => $data['link_video_materi'] ?? '',
                        'is_gratis'  => $data['is_gratis'] ?? 0,
                    ]);
                    DB::commit();
                    return $kursus;
                } catch (\Exception $e) {
                    DB::rollback();
                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
        }
        return $result;
    }

    public function updateData($flag = '', $data)
    {
        $result = false;
        switch ($flag) {
            case 'updateData':
                DB::beginTransaction();
                try {

                    $materi = $this->find($data['id']);
                    $update = $materi->update([
                        'id_kursus'   => $data['id_kursus'],
                        'judul_materi'  => $data['judul_materi'],
                        'deskripsi_materi'  => $data['deskripsi_materi'] ?? '',
                        'link_file_materi'  => $data['link_file_materi'],
                        'link_video_materi'  => $data['link_video_materi'] ?? '',
                        'is_gratis'  => $data['is_gratis'] ?? 0,
                    ]);

                    DB::commit();

                    return $this->find($data['id']);
                } catch (\Exception $e) {
                    DB::rollback();

                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
        }
        return $result;
    }

    public function deleteData($flag = '', $data)
    {
        $result = false;
        switch ($flag) {
            case 'deleteData':
                DB::beginTransaction();
                try {
                    $materi = $this->find($data['id']);
                    $delete = $materi->delete();
                    DB::commit();
                    return $materi;
                } catch (\Exception $e) {
                    DB::rollback();

                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
        }
        return $result;
    }
}
