<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;

class KursusJadwal extends BaseModel
{
    use HasFactory;

    protected $table = 'kursus_jadwal';
    protected $guarded = [];

    public function insertData($flag = '', $data)
    {
        $result = false;
        switch ($flag) {
            case 'insertData':
                DB::beginTransaction();
                try {
                    $jadwal = $this->create([
                        'id_kursus'   => $data['id_kursus'],
                        'hari_jadwal'  => $data['hari'],
                        'jam_mulai_jadwal'  => $data['jam_mulai'],
                        'jam_akhir_jadwal'  => $data['jam_akhir'],
                        'is_tutup'  => $data['is_tutup'] ?? 0,
                    ]);
                    DB::commit();
                    return $jadwal;
                } catch (\Exception $e) {
                    DB::rollback();
                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
        }
        return $result;
    }

    public function updateData($flag = '', $data)
    {
        $result = false;
        switch ($flag) {
            case 'updateData':
                DB::beginTransaction();
                try {

                    $jadwal = $this->find($data['id']);
                    $update = $jadwal->update([
                        'hari_jadwal'   => $data['hari'],
                        'jam_mulai_jadwal'   => $data['jam_mulai'],
                        'jam_akhir_jadwal'   => $data['jam_akhir'],
                        'is_tutup'   => $data['is_tutup'],
                    ]);

                    DB::commit();

                    return $this->find($data['id']);
                } catch (\Exception $e) {
                    DB::rollback();

                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
        }
        return $result;
    }

    public function deleteData($flag = '', $data)
    {
        $result = false;
        switch ($flag) {
            case 'deleteData':
                DB::beginTransaction();
                try {
                    $jadwal = $this->find($data['id']);
                    $delete = $jadwal->delete();
                    DB::commit();
                    return $jadwal;
                } catch (\Exception $e) {
                    DB::rollback();

                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
        }
        return $result;
    }

}
