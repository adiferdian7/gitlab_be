<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class KursusSiswa extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $table = 'kursus_siswa';
    protected $guarded = [];

    public function kursus()
    {
      return $this->belongsTo('App\Models\Kursus', 'id_kursus');
    }

    public function siswa()
    {
      return $this->belongsTo('App\Models\Student', 'id_siswa');
    }

    public function getData($flag = '', $data, $convertLabel = '')
    {
        $limit = isset($data['paginate']) ?  $data['paginate'] : 10;
        $keyword = isset($data['q']) ? $data['q'] : '';
        switch ($flag) {
            case 'data':
                $where = [];
                $whereIn = [];
                if(isset($data['id_kursus']) && !empty($data['id_kursus'])) {
                  $where['id_kursus'] = $data['id_kursus'];
                }
                $result = $this
                    ->with([
                        'kursus:id,nama_kursus',
                        'siswa:id_siswa,nama_lengkap,email,nomor_telephone,jenis_kelamin,alamat',
                    ]);
                $result = $result->where($where);
                
                if(isset($data['status_dikelas']) && !empty($data['status_dikelas'])) {
                    if(is_array($data['status_dikelas'])) {
                        $result = $result->whereIn('status_dikelas', $data['status_dikelas']);
                    } else {
                        $where['status_dikelas'] = $data['status_dikelas'];
                        $result = $result->where($where);
                    }
                }
            
                $result = $result->get();
                break;
            case 'detailData':
                $result = $this->with(['kursus'])->find($data['id']);
                return $result;
            case 'detailDataByStudentId':
                $id_siswa = auth()->user()->id;
                $id_kursus = $data['id'];
                $result = $this->where('id_siswa', $id_siswa)
                ->where('id_kursus', $id_kursus)
                // ->where('')
                ->orderByDesc('created_at')
                ->first();
                return $result;
            case 'detailDataByReferensi':
                if(isset($data['status_dikelas']) && !empty($daa['status_dikelas'])) {
                    $this->whereIn('status_dikelas', $data['status_dikelas']);
                }
                $result = $this->where('referensi', $data['referensi'])->first();
                return $result;
            default:
                $result = null;
                break;
        }
        if(!empty($convertLabel)) {
            $result = $this->convertData($result, $convertLabel);
        }
        return $result;
    }

    public function insertData($flag = '', $data)
    {
        $result = false;
        switch ($flag) {
            case 'insertData':
                DB::beginTransaction();
                try {
                    $kursus = $this->create([
                        'referensi' => $data['referensi'] ?? null,
                        'id_kursus'   => $data['id_kursus'],
                        'id_siswa'  => $data['id_siswa'],
                        'status_dikelas'  => $data['status_dikelas'] ?? 'Pending',
                        'tanggal_gabung'  => (isset($data['tanggal_gabung']) ? $data['tanggal_gabung'] : $data['status_dikelas']) == 'Bergabung' ? date('Y-m-d H:i:s') : null,
                        'tanggal_keluar'  => $data['tanggal_keluar'] ?? null,
                    ]);
                    DB::commit();
                    return $kursus;
                } catch (\Exception $e) {
                    DB::rollback();
                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
        }
        return $result;
    }

    public function updateData($flag = '', $data)
    {
        $result = false;
        switch ($flag) {
            case 'updateData':
                DB::beginTransaction();
                try {

                    $kelas_siswa = $this->find($data['id']);
                    $update = $kelas_siswa->update([
                        // 'id_kursus'   => $data['id_kursus'],
                        // 'id_siswa'  => $data['id_siswa'],
                        'status_dikelas'  => $data['status_dikelas'] ?? $kelas_siswa->status_dikelas,
                        'tanggal_gabung'  => $data['tanggal_gabung'] ?? null,
                        'tanggal_keluar'  => $data['tanggal_keluar'] ?? null,
                    ]);

                    DB::commit();

                    return $this->find($data['id']);
                } catch (\Exception $e) {
                    DB::rollback();

                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
            case 'updateStatusData':
                DB::beginTransaction();
                try {
                    
                    $kelas_siswa = $this->find($data['id']);

                    switch ($data['status_dikelas']) {
                    case 'Pending':
                        $data['tanggal_gabung'] = null;
                        $data['tanggal_keluar'] = null;
                        break;
                    case 'Bergabung':
                        $data['tanggal_gabung'] = $data['tanggal_gabung'] ?? date("Y-m-d H:i:s");
                        $data['tanggal_keluar'] = null;
                        break;
                    case 'Keluar':
                    case 'Dikeluarkan':
                        $data['tanggal_keluar'] = $data['tanggal_keluar'] ?? date("Y-m-d H:i:s");
                        break;
                    case 'Ditolak':
                        // $data['id_kursus'] = null;
                        // $data['deleted_at'] = date("Y-m-d H:i:s");
                        break;
                    }

                    $kelas_siswa->update($data);
                    DB::commit();

                    return $kelas_siswa;
                } catch (\Exception $e) {
                    DB::rollback();

                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
        }
        return $result;
    }

    public function deleteData($flag = '', $data)
    {
        $result = false;
        switch ($flag) {
            case 'deleteData':
                DB::beginTransaction();
                try {
                    $kelas_siswa = $this->find($data['id']);
                    $delete = $kelas_siswa->delete();
                    DB::commit();
                    return $kelas_siswa;
                } catch (\Exception $e) {
                    DB::rollback();

                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
        }
        return $result;
    }

}
