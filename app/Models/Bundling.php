<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Bundling extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];

    // relation
    public function product_actives()
    {
        return $this->belongsToMany(Produk::class, 'bundling_products', 'bundling_id', 'product_id')->wherePivot('status_produk', 'Aktif');
    }

    public function bundling_products()
    {
        return $this->hasMany(BundlingProduct::class);
    }

    public function products()
    {
        return $this->belongsToMany(Produk::class, 'bundling_products', 'bundling_id', 'product_id');
    }

    public function transaksi_user()
    {
        return $this->hasOne('App\Models\Transaksi', 'id_produk')->where('id_user', auth()->user()->id)->where('jenis_transaksi', 'Bundling')->orderBy('id', 'ASC')->groupBy('kode');
    }

    // custom

    public function getData($flag = '', $data = null, $convertLabel = '')
    {
        $limit = isset($data['paginate']) ?  $data['paginate'] : 10;
        $keyword = isset($data['q']) ? $data['q'] : '';
        switch ($flag) {
            case 'paginationData':
                $where = [];
                if (isset($data['show']) && !empty($data['show'])) {
                    $where['show'] = $data['show'];
                }
                $result = $this
                    ->with([
                        'products',
                        'transaksi_user'
                        // 'products:id,nama_produk,kategori_produk,jenis_produk,harga_produk',
                    ])
                    ->where(function ($query) use ($keyword, $data) {
                        return $query
                            ->where('name', 'like', '%' . $keyword . '%')
                            ->orWhere('desc', 'like', '%' . $keyword . '%')
                            ->orWhere('price', 'like', '%' . $keyword . '%')
                            ->orWhereHas('products', function ($query) use ($keyword) {
                                $query->where('nama_produk', 'like', '%' . $keyword . '%');
                            });
                    });
                $result = $result->where($where);
                $result = $result->orderByDesc('created_at');
                $result = $result->paginate($limit);
                break;
            case 'detailData':
                $result = $this->with(['bundling_products.product'])
                    ->find($data['id']);
                break;
            case 'detailWithTrans':
                $result = $this->with(
                    [
                        'bundling_products.product.transaksi_user', 'products.transaksi_user_product_bundling', 'transaksi_user',
                        'products.list:id,id_produk,id_tryout',
                        'products.list.tryout:id,kategori,jenis_soal,kelompok_soal,id_penjurusan,alokasi_waktu,jeda_waktu,id_jenjang',
                        'products.list.tryout.penjurusan:id,id_kelas,nama_penjurusan',
                        'products.list.tryout.penjurusan.kelas:id,id_jenjang,nama_kelas',
                        'products.list.tryout.penjurusan.kelas.jenjang:id,nama_jenjang',
                        'products.list.tryout.soal:id,id_tryout,jenis_soal,kelompok_soal,id_mapel',
                        'products.list.tryout.soal.mapel:id,nama_mapel',
                        'products.list.tryout.soal.pertanyaan:id,id_soal_tryout,bab_mapel,penjelasan_pertanyaan,template_pertanyaan,soal,opsi_pertanyaan,pembahasan_pertanyaan,jawaban_pertanyaan',
                        'products.list.tryout.jenjang:id,nama_jenjang'
                    ]
                )
                    ->find($data['id']);
                break;
            default:
                $result = $data;
                break;
        }
        if (!empty($convertLabel)) {
            $result = $this->convertData($result, $convertLabel);
        }
        return $result;
    }

    public function convertData($data, $label = '')
    {
        $result = [];
        switch ($label) {
            case 'detailWithTrans':
                $result = $data;
                foreach ($result->products as $key => $productRow) {
                    $kategori = [];
                    $jenis_tryout = [];
                    $jumlah_soal = 0;
                    $waktu  = 0;
                    $penjurusan = [];
                    $kelompok_soal = [];
                    $jenjang = [];
                    $mapel = [];
                    foreach ($productRow->list as $row) {

                        $kategori[]     = $row->tryout->kategori;
                        $kelompok_soal[] = $row->tryout->kelompok_soal ?? '';
                        $jenis_tryout[] = $row->tryout->jenis_soal ?? '';
                        $penjurusan[]   = $row->tryout->penjurusan->kelas->jenjang->nama_jenjang ?? '';
                        // $jumlah_soal   += count($row->tryout->soal);

                        $jenjang[] = $row->tryout->jenjang ? $row->tryout->jenjang->nama_jenjang : '';

                        foreach ($row->tryout->soal as $soal) {
                            $jumlah_soal   += count($soal->pertanyaan);
                            $mapel[] = $soal->mapel ? $soal->mapel->nama_mapel : '';
                        }

                        $waktu += $row->tryout->alokasi_waktu;
                    }

                    unset($productRow->list);

                    $productRow->harga_label = 'Rp ' . number_format($productRow->harga_produk, 0, ',', '.');
                    $productRow->tanggal_mulai_label = Carbon::parse($productRow->tanggal_mulai)->locale('id')->format('d/M/Y  H:i');
                    $productRow->tanggal_berakhir_label = Carbon::parse($productRow->tanggal_berakhir)->locale('id')->format('d/M/Y  H:i');
                    $productRow->jumlah_soal = $jumlah_soal;
                    $productRow->waktu = $waktu;
                    $productRow->jenis_tryout = collect($jenis_tryout)->unique();
                    $productRow->kelompok_soal = collect($kelompok_soal)->unique();
                    $productRow->penjurusan = collect($penjurusan)->unique();
                }
                break;
            default:
                $result = $data;
                break;
        }
        return $result;
    }

    public function insertData($flag = '', $data = null)
    {
        $result = false;
        switch ($flag) {
            case 'insertData':
                DB::beginTransaction();
                try {
                    $bundling = $this->create([
                        'name'   => $data['name'],
                        'desc' => $data['desc'] ?? '-',
                        'price'  => $data['price'] ?? '-',
                        'show'  => $data['show'] ?? 1,
                    ]);
                    DB::commit();
                    return $bundling;
                } catch (\Exception $e) {
                    DB::rollback();
                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
            case 'insertDataWithProducts':
                DB::beginTransaction();
                try {
                    $bundling = $this->create([
                        'name'   => $data['name'],
                        'desc' => $data['desc'] ?? '-',
                        'price'  => $data['price'] ?? '-',
                        'show'  => $data['show'] ?? 1,
                    ]);

                    // if (isset($data['products'])) {
                    $bundlingProduct = new BundlingProduct();
                    foreach ($data['products'] as $key => $product) {
                        $cek = $bundlingProduct->where('bundling_id', $bundling['id'])
                            ->where('product_id', $product['id'])->count();
                        if ($cek < 1) {
                            $product = $bundlingProduct->create([
                                'bundling_id' => $bundling['id'],
                                'product_id' => $product['id'],
                                'custom_price' => $product['custom_price']
                            ]);
                        }
                    }
                    // }

                    DB::commit();
                    return $bundling;
                } catch (\Exception $e) {
                    DB::rollback();
                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
        }
        return $result;
    }

    public function updateData($flag = '', $data = null)
    {
        $result = false;
        switch ($flag) {
            case 'updateData':
                DB::beginTransaction();
                try {

                    $bundling = $this->find($data['id']);
                    $update = $bundling->update([
                        'name'   => $data['name'],
                        'desc' => $data['desc'],
                        'price'  => $data['price'],
                        'show'  => $data['show'],
                    ]);

                    DB::commit();

                    return $bundling->refresh();
                } catch (\Exception $e) {
                    DB::rollback();

                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
        }
        return $result;
    }

    public function deleteData($flag = '', $data = null)
    {
        $result = false;
        switch ($flag) {
            case 'deleteData':
                DB::beginTransaction();
                try {

                    $findData = $this->find($data['id']);
                    $delete = $findData->delete();
                    DB::commit();
                    return $findData;
                } catch (\Exception $e) {
                    DB::rollback();

                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
        }
        return $result;
    }
}
