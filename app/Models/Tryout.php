<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tryout extends Model
{
    // use HasFactory, SoftDeletes;
    use HasFactory;

    protected $guarded = [];
    protected $dates = ['deleted_at'];
    
    public function soal(){
    	return $this->hasMany('App\Models\SoalTryout', 'id_tryout');
    }

    public function penjurusan(){
    	return $this->belongsTo('App\Models\Penjurusan', 'id_penjurusan');
    }

    public function jenjang(){
    	return $this->belongsTo('App\Models\Jenjang', 'id_jenjang');
    }
}
