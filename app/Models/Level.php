<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Level extends Model
{
    use HasFactory;

    // protected $guarded = [];

    protected $fillable = [
        'nama_level',
        'deskripsi_level',
        'honor_level',
        'sesi_honor_level',
        'id_uktt',
        'minimal_rate_mengajar',
        'minimal_total_mengajar',
        'maksimal_waktu_mengajar'
    ];

    public function getData($flag = '', $data = null, $convertLabel = '')
    {
        $limit = isset($data['paginate']) ?  $data['paginate'] : 5;
        $keyword = isset($data['q']) ? $data['q'] : '';
        switch ($flag) {
            case 'data':
                $this->orderBy('id', 'asc');
                $result = $this->get();
                return $result;
            default:
                $result = null;
                break;
        }
        if(!empty($convertLabel)) {
            $result = $this->convertData($result, $convertLabel);
        }
        return $result;
    }

    public function updateData($flag = '', $data = null)
    {
        $result = false;
        switch ($flag) {
            case 'updateData':
                DB::beginTransaction();
                try {

                    $dataUpdate = [];

                    foreach ($this->fillable as $field) {
                        if (isset($data[$field]) && ($data[$field] != "" || $data[$field] != NULL)) {
                            $dataUpdate[$field] = $data[$field];
                        }
                    }

                    $level = $this->find($data['id']);
                    $update = $level->update($dataUpdate);

                    DB::commit();

                    return $this->find($data['id']);
                } catch (\Exception $e) {
                    DB::rollback();

                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
        }
        return $result;
    }
}
