<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BundlingProduct extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function product()
    {
        return $this->belongsTo(Produk::class, 'product_id', 'id');
    }

    public function insertData($flag = '', $data =null)
    {
        $result = false;
        switch ($flag) {
            case 'insertBatchData':
                DB::beginTransaction();
                try {
                    $bundle_id = $data['bundle_id'];
                    $dataProducts = $data['products'];
                    $bundleProductInsert = [];
                    foreach ($dataProducts as $key => $product) {
                        $cek = $this->where('bundling_id', $bundle_id)->where('product_id', $product['id']);
                        $bundleProductInsert['bundling_id'] = $bundle_id;
                        $bundleProductInsert['product_id'] = $product['id'];
                        $bundleProductInsert['custom_price'] = $product['custom_price'];
                        if ($cek->count() < 1) {
                            $this->create($bundleProductInsert);
                        } else {
                            $cek->update($bundleProductInsert);
                        }
                    }
                    DB::commit();
                    return true;
                } catch (\Exception $e) {
                    DB::rollback();
                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
            case 'insertData':
                DB::beginTransaction();
                try {
                    $bundlingProduct = $this->create([
                        'bundling_id' => $data['bundling_id'],
                        'product_id' => $data['product_id'],
                        'custom_price' => $data['custom_price'],
                    ]);
                    DB::commit();
                    return $bundlingProduct;
                } catch (\Exception $e) {
                    DB::rollback();
                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
        }
        return $result;
    }

    public function deleteData($flag = '', $data = null)
    {
        $result = false;
        switch ($flag) {
            case 'deleteData':
                DB::beginTransaction();
                try {

                    $findData = $this->find($data['id']);
                    $delete = $findData->delete();
                    DB::commit();
                    return $findData;
                } catch (\Exception $e) {
                    DB::rollback();

                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
        }
        return $result;
    }
}
