<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Material extends BaseModel
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'title',
        'desc',
        'price',
        'cover_image_link',
        'file_link',
        'published'
    ];

    // relation
    public function transaksi_user()
    {
        return $this->hasOne('App\Models\Transaksi', 'id_produk')->where('id_user', auth()->user()->id)->where('jenis_transaksi', 'Materi')->orderBy('id', 'ASC');
    }

    // custom

    public function getData($flag = '', $data, $convertLabel = '')
    {
        $limit = isset($data['paginate']) ?  $data['paginate'] : 10;
        $keyword = isset($data['q']) ? $data['q'] : '';
        switch ($flag) {
            case 'paginationData':
                $where = [];
                if (isset($data['published']) && !empty($data['published'])) {
                    $where['published'] = $data['published'];
                }
                $result = $this->where(function ($query) use ($keyword, $data) {
                    return $query
                        ->where('title', 'like', '%' . $keyword . '%')
                        ->orWhere('desc', 'like', '%' . $keyword . '%')
                        ->orWhere('price', 'like', '%' . $keyword . '%');
                });
                $result = $result->where($where);
                $result = $result->orderByDesc('created_at');
                $result = $result->paginate($limit);
                break;
            case 'detailData':
                $result = $this->find($data['id']);
                break;
            case 'detailWithTrans':
                $result = $this->with(['transaksi_user'])
                    ->find($data['id']);
                break;
            default:
                $result = $data;
                break;
        }
        if (!empty($convertLabel)) {
            $result = $this->convertData($result, $convertLabel);
        }
        return $result;
    }

    public function convertData($data, $label = '')
    {
        $result = [];
        switch ($label) {
            case 'paginationData':
                break;
            default:
                $result = $data;
                break;
        }
        return $result;
    }

    public function insertData($flag = '', $data)
    {
        $result = false;
        switch ($flag) {
            case 'insertData':
                DB::beginTransaction();
                try {
                    $material = $this->create([
                        'title'   => $data['title'],
                        'desc' => $data['desc'],
                        'price'  => $data['price'],
                        'published'  => $data['published'] ?? true,
                        'cover_image_link'  => $data['cover_image_link'],
                        'file_link'  => $data['file_link'],
                    ]);
                    DB::commit();
                    return $material;
                } catch (\Exception $e) {
                    DB::rollback();
                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
        }
        return $result;
    }

    public function updateData($flag = '', $data)
    {
        $result = false;
        switch ($flag) {
            case 'updateData':
                DB::beginTransaction();
                try {

                    $material = $this->find($data['id']);

                    $dataUpdate = [];
                    foreach ($this->getFillable() as $key => $field) {
                        if (isset($data[$field]) && ($data[$field] != null || $data[$field] != '')) {
                            $dataUpdate[$field] = $data[$field];
                        }
                    }
                    $update = $material->update($dataUpdate);

                    DB::commit();

                    return $material->refresh();
                } catch (\Exception $e) {
                    DB::rollback();

                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
        }
        return $result;
    }

    public function deleteData($flag = '', $data)
    {
        $result = false;
        switch ($flag) {
            case 'deleteData':
                DB::beginTransaction();
                try {

                    $findData = $this->find($data['id']);
                    $delete = $findData->delete();
                    DB::commit();
                    return $findData;
                } catch (\Exception $e) {
                    DB::rollback();

                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
        }
        return $result;
    }
}
