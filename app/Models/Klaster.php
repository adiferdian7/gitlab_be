<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Klaster extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];
    protected $table = 'klaster';
    protected $dates = ['deleted_at'];

    public function rumpun(){
    	return $this->belongsTo('App\Models\Rumpun', 'id_rumpun');
    }

    public function mapel(){
    	return $this->belongsTo('App\Models\Mapel', 'id_mapel');
    }
}
