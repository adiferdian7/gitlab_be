<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class ProgramStudiPerguruanTinggi extends Model
{
    use HasFactory;

    protected $table = 'program_studi_and_perguruan_tinggi';
    protected $guarded = [];

    public function program_studi(){
    	return $this->belongsTo('App\Models\ProgramStudi', 'id_program_studi');
    }

    public function perguruan(){
    	return $this->belongsTo('App\Models\PerguruanTinggi', 'id_perguruan_tinggi');
    }
}
