<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TryoutUser extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = [];
    protected $dates = ['deleted_at'];
    protected $cast = [
        'temp_jawaban_user' => 'json'
    ];
    
    public function produk()
    {
        return $this->belongsTo('App\Models\Produk', 'id_produk');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'id_user');
    }

    public function tryout()
    {
        return $this->belongsTo('App\Models\Tryout', 'id_produk');
    }

    public function tentor()
    {
        return $this->belongsTo('App\Models\Teacher', 'id_user');
    }

    public function student()
    {
        return $this->belongsTo('App\Models\Student', 'id_user');
    }

    public function jawabans() {
        return $this->hasMany('App\Models\TryoutUserJawaban', 'id_tryout_user', 'id');
    }

}
