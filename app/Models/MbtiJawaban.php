<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MbtiJawaban extends Model
{
    use HasFactory, SoftDeletes;
    
    protected $guarded = [];

    protected $casts = [
        'hitungan' => 'json',
    ];

    protected $dates = ['deleted_at'];

    public function dimensi(){
    	return $this->belongsTo('App\Models\Dimensi', 'id_dimensi');
    }

    public function pertanyaan(){
    	return $this->hasMany('App\Models\MbtiPertanyaan', 'id_mbti_dimensi');
    }

    public function jawaban_detail()
    {
        return $this->hasMany('App\Models\MbtiJawabanDetail', 'id_mbti_jawaban');
    }

    public function kepribadian()
    {
        return $this->belongsTo('App\Models\Kepribadian', 'id_kepribadian');
    }

}
