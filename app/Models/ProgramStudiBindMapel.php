<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class ProgramStudiBindMapel extends Model
{
    use HasFactory;

    protected $table = 'program_studi_bind_mapel';
    protected $guarded = [];

    public function program_studi(){
    	return $this->belongsTo('App\Models\ProgramStudi', 'id_program_studi');
    }

    public function mapel(){
    	return $this->belongsTo('App\Models\Mapel', 'id_mapel');
    }
}
