<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class cmsContent7 extends Model
{
    use HasFactory;
    
    protected $table = 'cms_content7s';
    protected $guarded = [];
}
