<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TryoutHasil extends Model {

  protected $casts = [
    'riwayat_pengerjaan' => 'json',
    'riwayat_perhitungan' => 'json'
  ];

  protected $guarded = [];

}