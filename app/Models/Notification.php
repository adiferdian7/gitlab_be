<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;

class Notification extends BaseModel
{
    use HasFactory;

    protected $guarded = [];

    protected $primaryKey = 'notification_id';

    protected $recipientTpeList = [
        0 => 'SuperAdmin',
        1 => 'Admin',
        2 => 'Teacher',
        3 => 'Student',
        4 => 'All Administrator',
        5 => 'Parent'
    ];

    protected $typeList = [
        0 => 'Registration',
        1 => 'Transaction',
        2 => 'Tryout',
        3 => 'Course',
        4 => 'Review',
        5 => 'Level',
        6 => 'System',
    ];

    public function getData($flag = '', $data = null, $convertLabel = '')
    {
        $limit = isset($data['limit']) ?  $data['limit'] : 10;
        $keyword = isset($data['search']) ? $data['search'] : '';
        $sortBy = isset($data['sortBy']) ? $data['sortBy'] : 'created_at';
        $sortDir = isset($data['sortDir']) ? $data['sortDir'] : 'desc';
        switch ($flag) {
            case 'paginationData':

                $where = [];

                $query = $this;

                if (isset($data['recipientId']) && !empty($data['recipientId'])) {
                    $where['notification_recipient_id'] = $data['recipientId'];
                } else if ($this->getUser() && in_array($this->getUser()->role_user, ['siswa', 'teacher'])) {
                    $where['notification_recipient_id'] = $this->getUserId();
                }

                if (isset($data['recipientType']) && !empty($data['recipientType'])) {
                    $where['notification_recipient_type'] = $data['recipientType'];
                } else if ($this->getUser()) {
                    $user = $this->getUser();
                    if ($user->role_user == 'teacher') {
                        $where['notification_recipient_type'] = 2;
                    } else if ($user->role_user == 'siswa') {
                        $where['notification_recipient_type'] = 3;
                    } else if ($user->role_user == 'parent') {
                        $where['notification_recipient_type'] = 5;
                    }
                    else if ($user->role_user == 'superAdmin') {
                        $query = $query->where(function($query) {
                            return $query->whereIn('notification_recipient_type', [0, 4]);
                        });
                    } else if ($user->role_user == 'admin') {
                        $query = $query->where(function($query) {
                            return $query->whereIn('notification_recipient_type', [1, 4]);
                        });
                    }
                }

                if (isset($data['type']) && !empty($data['type'])) {
                    $where['notification_type'] = $data['type'];
                }

                if (isset($data['open']) && !empty($data['open'])) {
                    $where['notification_open'] = $data['open'];
                }

                $result = $query->where(function ($query) use ($keyword) {
                    return $query
                        ->where('notification_title', 'like', '%' . $keyword . '%');
                    // ->orWhere('', 'like', '%' . $keyword . '%')
                    // ->orWhereHas('', function ($query) use ($keyword) {
                    //     $query->where('', 'like', '%' . $keyword . '%');
                    // });
                });

                $result = $result->where($where)->orderBy($sortBy, $sortDir);
                $result = $result->paginate($limit);
                break;
            case 'detailData':
                $result = $this->find($data['id']);
                break;
            default:
                $result = $data;
                break;
        }
        if (!empty($convertLabel)) {
            $result = $this->convertData($result, $convertLabel);
        }
        return $result;
    }

    public function convertData($data = null, $label = '')
    {
        $result = [];
        switch ($label) {
            case 'paginationData':
                $dataArray = $data->toArray();
                $result['data'] = [];
                foreach ($data as $dataRow) {
                    $dataRow['notification_type_label'] = @$this->typeList[$dataRow->notification_type];
                    $dataRow['notification_recipient_type_label'] = @$this->recipientTpeList[$dataRow->notification_recipient_type];
                    array_push($result['data'], $dataRow);
                }
                $result['current_page'] = $dataArray['current_page'];
                $result['first_page_url'] = $dataArray['first_page_url'];
                $result['from'] = $dataArray['from'];
                $result['last_page'] = $dataArray['last_page'];
                $result['last_page_url'] = $dataArray['last_page_url'];
                $result['links'] = $dataArray['links'];
                $result['next_page_url'] = $dataArray['next_page_url'];
                $result['path'] = $dataArray['path'];
                $result['per_page'] = $dataArray['per_page'];
                $result['prev_page_url'] = $dataArray['prev_page_url'];
                $result['to'] = $dataArray['to'];
                $result['total'] = $dataArray['total'];
                break;
            case 'detailData':
                if ($data !== NULL) {
                    $result = $data->toArray();
                } else {
                    $result = NULL;
                }
                break;
            default:
                $result = $data;
                break;
        }
        return $result;
    }

    public function insertData($flag = '', $data = null)
    {
        $result = false;
        switch ($flag) {
            case 'insertData':
                DB::beginTransaction();
                try {
                    $notification = $this->create([
                        'notification_recipient_id'   => isset($data['recipientId']) && !empty($data['recipientId']) ? $data['recipientId'] : '',
                        'notification_recipient_type'   => $data['recipientType'],
                        'notification_type'   => $data['type'],
                        'notification_title'   => $data['title'],
                        'notification_body'   => $data['body'],
                        'notification_data'   => isset($data['data']) ? json_encode($data['data']) : null,
                        'notification_open'   => $data['open'] ?? 0,
                    ]);
                    DB::commit();
                    return $notification;
                } catch (\Exception $e) {
                    DB::rollback();
                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
            case 'insertBatchData':
                DB::beginTransaction();
                try {
                    $dataInsert = [];
                    foreach ($data as $key => $dataRow) {
                        $dataInsert[$key] = [
                            'notification_recipient_id'   => isset($dataRow['recipientId']) && !empty($dataRow['recipientId']) ? $dataRow['recipientId'] : '',
                            'notification_recipient_type'   => $dataRow['recipientType'],
                            'notification_type'   => $dataRow['type'],
                            'notification_title'   => $dataRow['title'],
                            'notification_body'   => $dataRow['body'],
                            'notification_data'   => isset($dataRow['data']) ? json_encode($dataRow['data']) : null,
                            'notification_open'   => $dataRow['open'] ?? 0,
                            'created_at' => date('Y-m-d H:i:s'),
                        ];
                    }
                    $notification = $this->insert($dataInsert);
                    DB::commit();
                    return $notification;
                } catch (\Exception $e) {
                    DB::rollback();
                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
            default:
                $result = [
                    'error' => true,
                    'message' => ''
                ];
                break;
        }
        return $result;
    }

    public function updateData($flag = '', $data = [])
    {
        $result = false;
        switch ($flag) {
            case 'triggerOpen':
                DB::beginTransaction();
                try {
                    $notification = $this->find($data['id'])->update(['notification_open' => 1]);
                    DB::commit();
                    return $this->find($data['id']);
                } catch (\Exception $e) {
                    DB::rollback();
                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
            default:
                $result = [
                    'error' => true,
                    'message' => ''
                ];
                break;
        }
        return $result;
    }
}
