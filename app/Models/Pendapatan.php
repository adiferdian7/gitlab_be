<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Pendapatan extends BaseModel
{
    use HasFactory;

    protected $guarded = [];

    public function kursus_siswa()
    {
        return $this->hasOne('App\Models\KursusSiswa', 'referensi', 'kode_referensi');
    }

    public function getData($flag = '', $data, $convertLabel = '')
    {
        $limit = isset($data['paginate']) ?  $data['paginate'] : 10;
        $keyword = isset($data['q']) ? $data['q'] : '';
        switch ($flag) {
            case 'data':
                $result = $this->all();
            case 'dataByTentor':
                $result = $this->with(['kursus_siswa', 'kursus_siswa.kursus'])
                    ->whereHas('kursus_siswa.kursus', function ($query) use ($data) {
                        return $query->where('id_tentor', $data['id_tentor']);
                    });
                $result = $result->get();
                break;
            case 'dataByTentorPerkursus':
                $result = DB::table('pendapatans')
                    ->selectRaw('kursus.id as id_kursus, kursus.nama_kursus, SUM(netto_pendapatan) as netto_pendapatan')
                    // ->join('kursus_siswa', 'kursus_siswa.referensi', '=', 'pendapatans.kode_referensi')
                    ->join('transaksis', 'transaksis.kode', '=', 'pendapatans.kode_referensi')
                    ->join('kursus', 'kursus.id', '=', 'transaksis.id_produk')
                    ->where('kursus.id_tentor', $data['id_tentor'])
                    ->where('transaksis.jenis_transaksi', 'Les Privat')
                    ->groupBy('transaksis.id_produk')
                    ->get();
                break;
            case 'detailDataByTentorPerkursus':
                $result = DB::table('pendapatans')
                    ->selectRaw('kursus.id as id_kursus, kursus.nama_kursus, SUM(netto_pendapatan) as netto_pendapatan')
                    ->join('transaksis', 'transaksis.kode', '=', 'pendapatans.kode_referensi')
                    ->join('kursus', 'kursus.id', '=', 'transaksis.id_produk')
                    ->where('kursus.id_tentor', $data['id_tentor'])
                    ->where('transaksis.jenis_transaksi', 'Les Privat')
                    ->where('transaksis.id_produk', $data['id_kursus'])
                    ->groupBy('transaksis.id_produk')
                    ->first();
                break;
            case 'dataByTentorPerkursusDetailList':
                $result = DB::table('pendapatans')
                    ->selectRaw('pendapatans.created_at as tanggal, students.nama_lengkap as nama_siswa, netto_pendapatan')
                    ->join('transaksis', 'transaksis.kode', '=', 'pendapatans.kode_referensi')
                    ->join('students', 'students.id_siswa', '=', 'transaksis.id_user')
                    ->join('kursus', 'kursus.id', '=', 'transaksis.id_produk')
                    ->where('kursus.id_tentor', $data['id_tentor'])
                    ->where('transaksis.jenis_transaksi', 'Les Privat')
                    ->where('transaksis.id_produk', $data['id_kursus'])
                    ->orderBy('pendapatans.created_at', 'desc')
                    ->paginate($limit);
                break;
            case 'detailDataByReferensi':
                $result = $this->where('kode_referensi', $data['kode_referensi'])->first();
                break;
            case 'dataByTentorPerBulan':
                $result = $this->with(['kursus_siswa', 'kursus_siswa.kursus'])
                    ->whereHas('kursus_siswa.kursus', function ($query) use ($data) {
                        return $query->where('id_tentor', $data['id_tentor']);
                    });
                $result = $result->groupBy('bulan')
                ->get(
                    [DB::raw('netto_pendapatan as pendapatan'), DB::raw('MONTH(created_at) as bulan')]
                );
                break;
            default:
                $result = null;
                break;
        }
        if (!empty($convertLabel)) {
            $result = $this->convertData($result, $convertLabel);
        }
        return $result;
    }

    public function convertData($data, $convertLabel = '')
    {
        $result = [];

        switch ($convertLabel) {
            case 'dataByTentor':
                $result = $data->toArray();
                foreach ($result as $key => $value) {
                    unset($result[$key]['kursus_siswa']);
                }
                break;
            case 'dataByTentorPerkursus':
                foreach ($data as $key => $dataRow) {
                    $result[$key]['id_kursus'] = $dataRow->id_kursus;
                    $result[$key]['nama_kursus'] = $dataRow->nama_kursus;
                    $result[$key]['netto_pendapatan'] = $this->formatRupiah($dataRow->netto_pendapatan);
                }
                break;
            case 'detailDataByTentorPerkursus':
                if (!empty($data)) {
                    $result['id_kursus'] = $data->id_kursus;
                    $result['nama_kursus'] = $data->nama_kursus;
                    $result['netto_pendapatan'] = $this->formatRupiah($data->netto_pendapatan);
                }
                break;
            case 'dataByTentorPerkursusDetailList':
                $dataArray = $data->toArray();
                $result['data'] = [];
                foreach ($data as $key => $dataRow) {
                    $result['data'][$key]['tanggal'] = $dataRow->tanggal;
                    $result['data'][$key]['nama_siswa'] = $dataRow->nama_siswa;
                    $result['data'][$key]['netto_pendapatan'] = $this->formatRupiah($dataRow->netto_pendapatan);
                }
                $result['current_page'] = $dataArray['current_page'];
                $result['first_page_url'] = $dataArray['first_page_url'];
                $result['from'] = $dataArray['from'];
                $result['last_page'] = $dataArray['last_page'];
                $result['last_page_url'] = $dataArray['last_page_url'];
                $result['links'] = $dataArray['links'];
                $result['next_page_url'] = $dataArray['next_page_url'];
                $result['path'] = $dataArray['path'];
                $result['per_page'] = $dataArray['per_page'];
                $result['prev_page_url'] = $dataArray['prev_page_url'];
                $result['to'] = $dataArray['to'];
                $result['total'] = $dataArray['total'];
                break;
            default:
                $result = $data;
                break;
        }

        return $result;
    }

    public function insertData($flag = '', $data)
    {
        $result = false;
        switch ($flag) {
            case 'insertData':
                DB::beginTransaction();
                try {
                    $pendapatan = $this->create([
                        'id_tentor'   => $data['id_tentor'],
                        'kode_referensi'   => $data['kode_referensi'],
                        'total_pendapatan'   => $data['total_pendapatan'],
                        'fee_pendapatan_persen'   => $data['fee_pendapatan_persen'],
                        'fee_pendapatan_nominal'   => $data['fee_pendapatan_nominal'],
                        'netto_pendapatan'   => $data['netto_pendapatan'],
                    ]);
                    DB::commit();
                    return $pendapatan;
                } catch (\Exception $e) {
                    DB::rollback();
                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
        }
        return $result;
    }
}
