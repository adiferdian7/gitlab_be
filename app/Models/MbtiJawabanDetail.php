<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MbtiJawabanDetail extends Model
{
    use HasFactory;
    
    protected $guarded = [];

    public function mbti(){
    	return $this->belongsTo('App\Models\TbMbti', 'id_mbti');
    }

    public function user(){
    	return $this->hasMany('App\Models\User', 'id_user');
    }

    public function pertanyaan()
    {
        return $this->belongsTo('App\Models\MbtiPertanyaan', 'id_mbti_pertanyaan');
    }
}
