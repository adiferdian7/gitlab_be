<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrangTua extends Model
{
    use HasFactory, SoftDeletes;

    protected $primaryKey = 'id_orang_tua';
    protected $table = 'orang_tua';
    protected $guarded = [];
    protected $dates = ['deleted_at'];


    public function siswa(){
    	return $this->belongsTo('App\Models\Student', 'id_orang_tua');
    }
}
