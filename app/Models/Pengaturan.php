<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Pengaturan extends BaseModel
{
    use HasFactory;

    protected $guarded = [];

    public function getData($flag = '', $data = null, $convertLabel = '')
    {
        $limit = isset($data['paginate']) ?  $data['paginate'] : 10;
        $keyword = isset($data['q']) ? $data['q'] : '';
        switch ($flag) {
            case 'data':
                $result = $this->all();
                return $result;
            case 'detailDataByKey':
                $result = $this->where('key', $data['key'])->first();
                if ($result == NULL) {
                    $this->create([
                        'label' => @$data['label'] ? $data['label'] : $data['key'],
                        'key' => $data['key'],
                        'isi' => @$data['isi'] ? $data['isi'] : ''
                    ]);
                    $result = $this->where('key', $data['key'])->first();
                }
                return $result;
            default:
                $result = null;
                break;
        }
        if (!empty($convertLabel)) {
            $result = $this->convertData($result, $convertLabel);
        }
        return $result;
    }

    public function updateData($flag = '', $data = null)
    {
        $result = false;
        switch ($flag) {
            case 'updateDataByKey':
                DB::beginTransaction();
                try {

                    $this->where('key', $data['key'])->update(['isi' => $data['isi']]);

                    DB::commit();

                    return $this->where('key', $data['key'])->first();
                } catch (\Exception $e) {
                    DB::rollback();

                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
        }
        return $result;
    }
}
