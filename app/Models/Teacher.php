<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Teacher extends Model
{
    use HasFactory, SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $primaryKey = 'id_teacher';
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'id_teacher');
    }

    public function user_doc()
    {
        return $this->hasMany('App\Models\UserDoc', 'user_id', 'id_teacher');
    }

    public function level()
    {
        return $this->belongsTo('App\Models\Level', 'id_level');
    }

}
