<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SoalPertanyaan extends Model
{
    // use HasFactory, SoftDeletes;
    use HasFactory;

    protected $fillable = [
	    'id_soal_tryout',
	    'bab_mapel',
	    'penjelasan_pertanyaan',
	    'template_pertanyaan',	
	    'soal',
	    'gambar',
	    'opsi_pertanyaan',
        'pembahasan_pertanyaan',
	    'parent_soal_pertanyaan',
        'jawaban_pertanyaan'
    ];

    protected $guarded = [];
    protected $dates = ['deleted_at'];

   protected $casts = [
        'bab_mapel' => 'json',
        'opsi_pertanyaan' => 'json',
        'jawaban_pertanyaan' => 'json',
    ];

    public function pertanyaan_parent(){
    	return $this->belongsTo('App\Models\SoalPertanyaan', 'parent_soal_pertanyaan');
    }
     public function pertanyaan_child(){
        return $this->hasMany('App\Models\SoalPertanyaan', 'parent_soal_pertanyaan');
    }
}
