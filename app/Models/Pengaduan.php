<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Pengaduan extends BaseModel
{
    use HasFactory, SoftDeletes;

    // protected $guarded = [];

    protected $fillable = [
        'id_user',
        'id_admin',
        'kode',
        'subjek',
        'permasalahan',
        'prioritas',
        'status',
        'tgl_buka',
        'tgl_ditutup',
    ];

    public function admin()
    {
        return $this->belongsTo('App\Models\Admin', 'id_admin');
    }

    public function tentor()
    {
        return $this->belongsTo('App\Models\Teacher', 'id_user', 'id_teacher');
    }

    public function siswa()
    {
        return $this->belongsTo('App\Models\Student', 'id_user', 'id_siswa');
    }

    public function getData($flag = '', $data, $convertLabel = '')
    {
        $limit = isset($data['paginate']) ?  $data['paginate'] : 10;
        $keyword = isset($data['q']) ? $data['q'] : '';
        $sortBy = isset($data['sortBy']) ? $data['sortBy'] : 'created_at';
        $sortDir = isset($data['sortDir']) ? $data['sortDir'] : 'desc';
        switch ($flag) {
            case 'paginationData':

                $where = [];

                if (isset($data['status']) && !empty($data['status'])) {
                    $where['status'] = $data['status'];
                }
                if (isset($data['prioritas']) && !empty($data['prioritas'])) {
                    $where['prioritas'] = $data['prioritas'];
                }

                if ($this->getUser() && in_array($this->getUser()->role_user, ['teacher', 'siswa'])) {
                    $where['id_user'] = $this->getUserId();
                } else if ($this->getUser() && $this->getUser()->role_user == 'parent') {
                    $parent = OrangTua::where('id_user', $this->getUserId())->first();
                    $where['id_user'] = $parent->id_orang_tua;
                }

                $result = $this
                    ->with([
                        'admin:id_admin,username,nama_lengkap,email',
                        'tentor:id_teacher,nama_lengkap,email,nomor_telephone,foto,id_level',
                        'tentor.level:id,nama_level',
                        'siswa:id_siswa,nama_lengkap,email,nomor_telephone,foto'
                    ])
                    ->where(function ($query) use ($keyword) {
                        return $query
                            ->where('kode', 'like', '%' . $keyword . '%')
                            ->orWhere('subjek', 'like', '%' . $keyword . '%')
                            ->orWhere('permasalahan', 'like', '%' . $keyword . '%')
                            ->orWhereHas('tentor', function ($query) use ($keyword) {
                                $query->where('nama_lengkap', 'like', '%' . $keyword . '%');
                                $query->orWhere('email', 'like', '%' . $keyword . '%');
                                $query->orWhere('nomor_telephone', 'like', '%' . $keyword . '%');
                            })
                            ->orWhereHas('siswa', function ($query) use ($keyword) {
                                $query->where('nama_lengkap', 'like', '%' . $keyword . '%');
                                $query->orWhere('email', 'like', '%' . $keyword . '%');
                                $query->orWhere('nomor_telephone', 'like', '%' . $keyword . '%');
                            });
                    });

                $result = $result->where($where)->orderBy($sortBy, $sortDir);
                $result = $result->paginate($limit);
                break;
            case 'detailData':
                $result = $this->with([
                    'admin:id_admin,username,nama_lengkap,email',
                    'tentor:id_teacher,nama_lengkap,email,nomor_telephone,foto,id_level',
                    'tentor.level:id,nama_level',
                    'siswa:id_siswa,nama_lengkap,email,nomor_telephone,foto'
                ])
                    ->find($data['id']);
                break;
            default:
                $result = $data;
                break;
        }
        if (!empty($convertLabel)) {
            $result = $this->convertData($result, $convertLabel);
        }
        return $result;
    }

    public function convertData($data, $label = '')
    {
        $result = [];
        switch ($label) {
            case 'paginationData':
                $dataArray = $data->toArray();
                $result['data'] = [];
                foreach ($data as $dataRow) {
                    array_push($result['data'], $dataRow);
                }
                $result['current_page'] = $dataArray['current_page'];
                $result['first_page_url'] = $dataArray['first_page_url'];
                $result['from'] = $dataArray['from'];
                $result['last_page'] = $dataArray['last_page'];
                $result['last_page_url'] = $dataArray['last_page_url'];
                $result['links'] = $dataArray['links'];
                $result['next_page_url'] = $dataArray['next_page_url'];
                $result['path'] = $dataArray['path'];
                $result['per_page'] = $dataArray['per_page'];
                $result['prev_page_url'] = $dataArray['prev_page_url'];
                $result['to'] = $dataArray['to'];
                $result['total'] = $dataArray['total'];
                break;
            case 'detailData':
                if ($data !== NULL) {
                    $result = $data->toArray();
                } else {
                    $result = NULL;
                }
                break;
            default:
                $result = $data;
                break;
        }
        return $result;
    }

    public function insertData($flag = '', $data)
    {
        $result = false;
        switch ($flag) {
            case 'createData':
                DB::beginTransaction();
                try {
                    $pengaduan = $this->create([
                        'id_user'   => $data['id_user'],
                        'id_admin'   => $data['id_admin'],
                        'kode'   => $data['kode'],
                        'subjek'   => $data['subjek'],
                        'permasalahan' => $data['permasalahan'],
                        'prioritas'  => $data['prioritas'],
                        'status'  => $data['status'],
                        'tgl_buka'  => $data['tgl_buka'],
                        'tgl_ditutup'  => $data['tgl_ditutup'],
                    ]);
                    DB::commit();
                    return $pengaduan;
                } catch (\Exception $e) {
                    DB::rollback();
                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
            default:
                $result = [
                    'error' => true,
                    'message' => ''
                ];
                break;
        }
        return $result;
    }

    public function updateData($flag = '', $data)
    {
        $result = false;
        switch ($flag) {
            case 'updateData':
                DB::beginTransaction();
                try {

                    $dataUpdate = [];

                    foreach ($this->fillable as $field) {
                        if (isset($data[$field])) {
                            $dataUpdate[$field] = $data[$field];
                        }
                    }

                    $pengaduan = $this->find($data['id']);
                    $update = $pengaduan->update($dataUpdate);

                    DB::commit();

                    return $this->find($data['id']);
                } catch (\Exception $e) {
                    DB::rollback();

                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
            case 'updateStatus':
                DB::beginTransaction();
                try {

                    $pengaduan = $this->find($data['id']);
                    $update = $pengaduan->update([
                        'status'   => $data['status'],
                    ]);

                    DB::commit();

                    return $this->find($data['id']);
                } catch (\Exception $e) {
                    DB::rollback();

                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
            case 'updatePrioritas':
                DB::beginTransaction();
                try {

                    $pengaduan = $this->find($data['id']);
                    $update = $pengaduan->update([
                        'prioritas'   => $data['prioritas'],
                    ]);

                    DB::commit();

                    return $this->find($data['id']);
                } catch (\Exception $e) {
                    DB::rollback();

                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
            case 'assignAdmin':
                DB::beginTransaction();
                try {

                    $pengaduan = $this->find($data['id']);
                    $update = $pengaduan->update([
                        'id_admin'   => $data['id_admin'],
                    ]);

                    DB::commit();

                    return $this->find($data['id']);
                } catch (\Exception $e) {
                    DB::rollback();

                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
        }
        return $result;
    }

    public function deleteData($flag = '', $data)
    {
        $result = false;
        switch ($flag) {
            case 'deleteData':
                DB::beginTransaction();
                try {

                    $pengaduan = $this->find($data['id']);
                    $deleted = $pengaduan->delete();

                    DB::commit();

                    return $pengaduan;
                } catch (\Exception $e) {
                    DB::rollback();

                    return [
                        'error' => true,
                        'message' => $e->getMessage()
                    ];
                }
                break;
        }
        return $result;
    }
}
