<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class TestMail extends Mailable
{
    use Queueable, SerializesModels;

    public $mailView = 'email.simple';
    public $mailTo = null;
    public $mailToName = '';
    public $mailFrom = '';
    public $mailFromName = 'UjiAja.com';
    public $mailSubject = 'Test Mail UjiAja.com';
    public $mailContent = 'Test';
    public $mailAttachmentFile = null;
    public $mailData = [];

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data = [])
    {
        $this->mailData = $data;

        $this->mailFrom = env('MAIL_FROM_ADDRESS', 'ujiaja@wesclic.studio');
        
        if (isset($data['to'])) {
            $this->mailTo = $data['to'];
        }
        if (isset($data['toName'])) {
            $this->mailToName = $data['toName'];
        }
        if (isset($data['subject'])) {
            $this->mailSubject = $data['subject'];
        }
        if (isset($data['content'])) {
            $this->mailContent = $data['content'];
        }
        if (isset($data['view'])) {
            $this->mailView = $data['view'];
        }
        
        if (isset($data['attachmentFile'])) {
            $this->mailAttachmentFile = $data['attachmentFile'];
        }

        if (!isset($data['data'])) {
            $data['data'] = [];
        } 
        
        $logo = $this->_get_setting('logo');
        $data['data']['logo'] = env('APP_URL') . '/' . $logo;
        $data['data']['whatsapp'] = $this->_get_setting('whatsapp');
        $data['data']['facebook'] = $this->_get_setting('facebook');
        $data['data']['instagram'] = $this->_get_setting('instagram');
        $data['data']['email'] = $this->_get_setting('email');
        $this->mailData = $data;
        // print_r($this->mailData);

    }

    private function _get_setting($key) {
        $setting = DB::table('pengaturans')->where('key', $key)->first();
        if($setting != NULL) {
            return $setting->isi;
        }
        return '-';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // dd($this->mailFrom); die;
        // echo env('MAIL_FROM_ADDRESS'); die;
        if($this->mailAttachmentFile != null) {
            return $this->subject($this->mailSubject)
           ->from($this->mailFrom, $this->mailFromName)
           ->to($this->mailTo, $this->mailToName)
           ->attach($this->mailAttachmentFile)
           ->view($this->mailView, $this->mailData);
        } else {
            return $this->subject($this->mailSubject)
           ->from($this->mailFrom, $this->mailFromName)
           ->to($this->mailTo, $this->mailToName)
           ->view($this->mailView, $this->mailData);
        }
    }
}
