<?php

namespace App\Utils;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\Response;

/**
 * 
 */
class Wilayah
{

	// function __construct(argument)
	// {
	// 	# code...
	// }

	public static function GetProvinsi()
	{

		$response = Http::get('https://dev.farizdotid.com/api/daerahindonesia/provinsi');
		$data = $response->json();
		foreach ($data['provinsi'] as $key => $provinsi) {
			if (strpos($provinsi['nama'], 'Di Yogyakarta') !== false) {
				$data['provinsi'][$key]['nama'] = 'D.I. Yogyakarta';
			} elseif (strpos($provinsi['nama'], 'Dki Jakarta') !== false) {
				$data['provinsi'][$key]['nama'] = 'DKI Jakarta';
			}
		}
		return $data;
	}

	public static function GetKota($params)
	{
		$response = Http::get('https://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi=' . $params);

		return $response->json();
	}

	public static function GetKecamatan($params)
	{
		$response = Http::get('https://dev.farizdotid.com/api/daerahindonesia/kecamatan?id_kota=' . $params);

		return $response->json();
	}
}
