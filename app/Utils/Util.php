<?php

namespace App\Utils;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\Response;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\Admin;
use App\Models\SuperAdmin;
use App\Models\OrangTua;

use Xendit\Xendit;

/**
 * 
 */
class Util
{
	// private  static $apiKey = 'xnd_development_qlTscm11ICw0014FrbTcb123EbKTUPf95xdaiV21jb8hvpoT4I5wL26Uxx4h';
	
	private $apiKey = null;
	private $devApiKey = 'xnd_development_qlTscm11ICw0014FrbTcb123EbKTUPf95xdaiV21jb8hvpoT4I5wL26Uxx4h';
	private $prodApiKey = 'xnd_production_DISINI';

	function __construct()
	{
		if(env('APP_ENV') == 'production') {
			$this->apiKey = $this->prodApiKey;
		} else if (env('APP_ENV') == 'local') {
			$this->apiKey = $this->devApiKey;
		}
	}

	public static function get_user($role, $id)
	{

		switch ($role) {
			case 'superAdmin':
				$data = SuperAdmin::find($id);
				break;

			case 'admin':
				$data = Admin::find($id);
				break;

			case 'teacher':
				$data = Teacher::find($id);
				break;

			case 'siswa':
				$data = Student::find($id);
				break;
				
			case 'parent':
				$data = OrangTua::find($id);
				break;

			default:
				$data = null;
				break;
		}

		return $data;
	}

	public static function tglIndo($tanggal)
	{
		$date = strtotime($tanggal);
	}

	public static function getBulan($bln)
	{
		switch ($bln) {
			case '01':
				return "Januari";
				break;
			case '02':
				return "Februari";
				break;
			case '03':
				return "Maret";
				break;
			case '04':
				return "April";
				break;
			case '05':
				return "Mei";
				break;
			case '06':
				return "Juni";
				break;
			case '07':
				return "Juli";
				break;
			case '08':
				return "Agustus";
				break;
			case '09':
				return "September";
				break;
			case '10':
				return "Oktober";
				break;
			case '11':
				return "November";
				break;
			case '12':
				return "Desember";
				break;
		}
	}

	public static function getHari($hari)
	{
		switch ($hari) {
			case 'Sun':
				$hari = "Minggu";
				break;
			case 'Mon':
				$hari = "Senin";
				break;
			case 'Tue':
				$hari = "Selasa";
				break;
			case 'Wed':
				$hari = "Rabu";
				break;
			case 'Thu':
				$hari = "Kamis";
				break;
			case 'Fri':
				$hari = "Jum'at";
				break;
			case 'Sat':
				$hari = "Sabtu";
				break;
			default:
				$hari = "Hari Eror";
		}

		return $hari;
	}


	public function invoice($params)
	{
		Xendit::setApiKey($this->apiKey);
		$data = \Xendit\Invoice::create($params);

		return $data;
	}

	public function getInvoice($id)
	{
		Xendit::setApiKey($this->apiKey);
		$data = \Xendit\Invoice::retrieve($id);
		return $data;
	}

	public function getBalance($params)
	{
		Xendit::setApiKey($this->apiKey);
		$getBalance = \Xendit\Balance::getBalance('CASH', $params);
		return $getBalance;
	}
}
